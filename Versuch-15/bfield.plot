set terminal epslatex color colortext
set output 'Bfield.tex'
lin(x) = m*x+b
poly5(x) = a+c*x+d*x**2+e*x**3+f*x**4+g*x**5
poly4(x) = c+2*d*x+3*e*x**2+4*f*x**3+5*g*x**4
fit [20:80] lin(x) 'Messwerte.txt' index 0 using 1:($2+0.0075):3 via m,b
fit [0:80] poly5(x) 'Messwerte.txt' index 0 using 1:($2+0.0075):3 via a,c,d,e,f,g
set xrange [-3:83]
set xlabel 'Höhe $h$ [$\unit{mm}$]'
set ylabel 'Magnetische Flussdichte $B$ [$\unit{T}$]'
plot 'Messwerte.txt' index 0 using 1:($2+0.0075):3 with errorbars title 'Messwerte', lin(x) title 'Lin. Regression ($h \geq \unit[20]{mm}$)' lt 1 lc 2, poly5(x) title 'Polynomfit' lt 1 lc 3
set output 'Bfield2.tex'
set ylabel '$B \Dfrac{B}{h}\,[\unitfrac{T^2}{m}]$'
plot m*lin(x)*1000 title 'Lin. Regression', poly4(x)*poly5(x)*1000 title 'Polynomfit' lt 1 lc 2
