reset
lin1(x) = m1*x+b1
lin2(x) = m2*x+b2
lin3(x) = m3*x+b3
lin4(x) = m4*x+b4
fit lin1(x) 'Messwerte.txt' index 1 using 1:2 via m1,b1
fit lin2(x) 'Messwerte.txt' index 1 using 1:3 via m2,b2
fit lin3(x) 'Messwerte.txt' index 1 using 1:4 via m3,b3
fit lin4(x) 'Messwerte.txt' index 1 using 1:5 via m4,b4
set terminal epslatex color colortext
set output 'current-induction.tex'
set xlabel 'Höhe $h$ [$\unit{mm}$]'
set ylabel 'Magnetische Flussdichte $B$ [$\unit{mT}$]'
plot 'Messwerte.txt' index 1 using 1:2 title '$\unit[0.8]{A}$', 'Messwerte.txt' index 1 using 1:3 title '$\unit[1.0]{A}$', 'Messwerte.txt' index 1 using 1:4 title '$\unit[1.2]{A}$', 'Messwerte.txt' index 1 using 1:5 title '$\unit[1.4]{A}$', lin1(x) notitle lt 1 lc 1, lin2(x) notitle lt 1 lc 2, lin3(x) notitle lt 1 lc 3, lin4(x) notitle lt 1 lc 4
set output
