reset
set xlabel 'Stromstärke $I\,[\unit{A}]$'
set ylabel 'Kraft $F\,[\unit{N}]$'
set xrange [0:1.5]
set yrange [-150:275]
set grid
set key top left
f(x) = a*x+b
fit f(x) 'Messwerte.txt' index 3 u 1:(1000*$2) via a,b
set terminal epslatex color colortext
set output 'current-force.tex'
plot 'Messwerte.txt' index 3 u 1:(1000*$2) title 'Messwerte', f(x) title 'Lineare Regression'
set output
!epstopdf 'current-force.eps'
