reset
set terminal epslatex color colortext
set size 1.2
set output 'h2o_drop_dipole_hist.tex'
set xlabel 'Frequenz [$\unit{THz}$]'
set ylabel 'Amplitude [arb. units]'
hist(x,width)=width*floor(x/width)+width/2.0
width=20*0.02998
set xrange [0:120]
set yrange [0:4]
set boxwidth width*0.9
#set style fill solid 0.5 #fillstyle
plot 'Dipole_Spectrum_Convoluted.dat' using (hist($1*0.02998,width)):($2) smooth freq w boxes title 'Amplituden der Eigenfrequenzen des Wassertropfens', '< echo "98.3344 3.0"' w impulse title 'Symmetrische Streckschwingung' lw 4, '< echo "49.2871 3.5"' w impulse title 'Scherschwingung' lw 4, '< echo "104.6302 3.0"' w impulse title 'Asymmetrische Streckschwingung' lw 4
set output
