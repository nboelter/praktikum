reset
set terminal epslatex color colortext
set size 1.2
set output 'time_autocorrelation.tex'
set xlabel 'Zeitdifferenz $\Delta T$ [$\unit{ps}$]'
set ylabel 'Autokorrelation $C(T)$ [-]'
set xrange [0:0.8]
plot 'vac.xvg' index 0 with lines lw 3 title 'gemessene Autokorrelationsfunktion', '' index 1 with lines title 'Exponentielle Regression' lw 3, 1/exp(1) lw 3 title '$1/e$ Niveau'
set output
