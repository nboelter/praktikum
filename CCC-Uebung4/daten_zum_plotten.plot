#!/usr/local/bin/gnuplot
reset
linear(x) = m*x+b
set key bottom right
set xrange[-5:205]
set grid
set xlabel '$R_\alpha [\Omega]$'
set ylabel '$\frac{1}{\varphi} \left[\frac{1}{rad}\right]$'
fit linear(x) 'daten_zum_plotten.dat' using 1:2:3 via m,b
set terminal epslatex color
set output "daten_zum_plotten.tex"
plot 'daten_zum_plotten.dat' using 1:2:3 with errorbars title "Messwerte", linear(x) title 'lineare Regression'
set output
!epstopdf daten_zum_plotten.eps
