reset
f(x) = a+b*x+c*x**2+d*x**3+e*x**4+g*x**5+h*x**6+i*x**7
fit [1.2:2.1] f(x) 'nvt0.8/correlation.2500.000000.txt' using 1:2 via a,b,c,d,e,g,h,i
set xrange [0:12]
set yrange [0:2.5]
set samples 10000
set terminal epslatex color colortext
set output 'correlation.tex'
set xlabel 'Teilchenabstand [$\sigma$]'
set ylabel 'Radiale Paarkorrelationsfunktion [-]'
plot 'nvt0.8/correlation.2500.000000.txt' using 1:2 with dots title '$\rho = 0.8$', f(x) title 'Regression'
set output

