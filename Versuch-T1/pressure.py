import numpy
N = 2000
for t in xrange(1,9):
  dn = "nvt0." + str(t) # Name of Directory
  rho  = float(t)/10.0 # Density
  volume = 1.0/rho*N
  fn = dn + "/conf.log"
  f = open(fn, "r")
  v = []
  newline = False
  i = 0 
  for line in f:
    i = i + 1
    if(i < 100): # Wait until system is in equilibrium
      continue
    if(line[0] == '#'): # This is a comment
      continue
    if(line == "\n"): # The velocity distribution part of conf.log starts with \n\n
      if(newline):
        break
      newline = True
      continue

    token = line.strip().split()
    v.append(float( token[9] ))
  print rho, volume, numpy.mean(v), numpy.std(v) 
