reset
set terminal epslatex color colortext
set output 'v_dist.tex'
set xlabel 'Geschwindigkeit $v$ [$\frac{\sigma}{\tau}$]'
set ylabel 'Verteilung'
set xrange [0:10]
set yrange [0:0.5]
f(x)=sqrt(2/pi)*x*x/a/a/a*exp(-x*x/(2*a*a))
fit f(x) 'conf_v.log' index 1 via a
plot 'conf_v.log' index 1 w lines title 'Geschwindigkeitshistogramm', f(x) title 'Maxwell-Boltzmann-Verteilung'
set output
