/* -------------------------------------------------------------------------*/
/*      compile								    */
/*  	cc -lm -O2 -o MD2 -Wall md2.c 					    */
/*      usage 								    */
/*      ./MD2 time dt dt_ana						    */
/* -------------------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/* FLAGS--------------------------------------------------------------------*/
#define USE_BOX
#define NVT
#define VELOCITY_BINS 1000
#define MAX_VELOCITY_FACTOR 0.1 // Max Velocity = 10

/* MODEL AND RUN PARAMETERS-------------------------------------------------*/
#define RANGE      (1.12246204830937298)               /* 2^(1/6) "Cut-Off" */
#define SHIFT      (0.25)                              /* so V(2^(1/6)) = 0 */
#define RANGE2     (RANGE*RANGE)
#define VCUT       (1e-2)  /* distance at which V is truncated for small r2 */
#ifdef NVT
#define LAMBDA     (1.0)                        /* frequency for thermostat */
#endif

/* -------------------------------------------------------------------------*/
/* interactions 							    */
static double V(double r2)
{
double 	r6inv,value;

	if (r2<=VCUT) {
		fprintf(stderr,"\nWARNING: overflow interactions r2= %g -> %g\n",r2,VCUT);
		r2 = VCUT;
	}
	r6inv = 1./(r2*r2*r2);
	value = 4.*(r6inv*(r6inv-1.0)+SHIFT);
	return(value);
}
static double F_divr(double r2)
{
double 	r6inv,value;

	if (r2<=VCUT) r2=VCUT;
	r6inv = 1./(r2*r2*r2);
	value = 24.*r6inv/r2*(2.0*r6inv-1.0);
	return(value);
}

/* -------------------------------------------------------------------------*/
struct particle	/* contains the properties of a particle */
{
	int		index;			/* particle's index */
	double 		*x; 			/* particle's position */
	double		*p;			/* particle's momentum */
	double 		m;			/* particle's mass */
#ifdef USE_BOX
	struct box	*box;			/* pointer to box */
	struct particle	*before;		/* pointers to other particles in box list */
	struct particle *after;
#endif
};

typedef struct particle PARTICLE;

#ifdef USE_BOX
typedef struct box
{
	int		index;
	int		Nmembers;
	struct particle *first;
	int		Nneighbors;
	struct box	**neighbor;

} BOX;
#endif

typedef struct conf	/* contains all information about the configuration */
{
	int		Ndim;		/* dimension of space */
	int 		Nparticles;	/* number of particles */
	double 		time;		/* time */
	double		kT;		/* temperature */
	double		*L;		/* system size in each dimension */
	PARTICLE 	*particle;	/* array of particles */
#ifdef USE_BOX
	int		Nboxes;		/* total numer of boxes */
	int		*Nbox;		/* number of boxes in each dimension */
	double		*Lbox;		/* size of boxes in each dimension */
	BOX		*box;		/* array of boxes */
#endif
	double		**force1;	/* forces (pointers to forces on particles) */
	double		**force2;
	double		**force_new;	/* just for bookkeeping */
	double		**force_old;
	double		energy_pot;	/* potential energy */
	double		energy_kin;	/* kinetic energy */
	double 		*momentum;
	double		h;		/* time step for integration */
} CONF;

/* -------------------------------------------------------------------------*/
static int 	read_conf(CONF *conf, char *fname);
static int 	write_conf(CONF *conf, char *fname, char *mode);
static int 	create_conf(CONF *conf);
#ifdef USE_BOX
static int 	create_box(CONF *conf);
static int	populate(CONF *conf);
static int	insert(BOX *box, PARTICLE *particle);
static int	delete(BOX *box, PARTICLE *particle);
static BOX	*calc_box(CONF *conf, double *x);
static void    loop_mm1(CONF *conf, BOX *box,int level, int *j,int *i, int *ip, int *im);
static int	calc_energy_with_box(CONF *conf);
static int	calc_force_with_box(CONF *conf);
#endif
static int 	calc_force(CONF *conf);
static int 	vv_step(CONF *conf);
static int 	calc_momentum(CONF *conf);
static int 	calc_energy(CONF *conf);
static int calc_velocity_distribution(CONF *conf);
static int write_velocity_distribution(CONF *conf);

static int 	ana(CONF *conf);
static double 	*period(CONF *conf, double *x);	/* attention: return value has to be copied */
static double 	mic(CONF *conf, double *x, double *y, double *d);
#ifdef NVT
static int	thermostat(CONF *conf);
static int	init_T(CONF *conf);
#endif
/* Global Variables */

int velocity_distribution[VELOCITY_BINS] = {0};
int velocity_distribution_count = 0;

/* -------------------------------------------------------------------------*/
int main(int argc,char **argv)
{
int 	i;
double	time_end,time_ana,dt_ana,hsave;
CONF	configuration,*conf=&configuration;

/* read command line parameters */
	i=1;					/* read in arguments from command line */
	time_end = atof(argv[i++]);		/* time to stop integration */
	conf->h  = atof(argv[i++]);		/* step size (initial guess) */
	dt_ana   = atof(argv[i++]);		/* time interval between analysis */

/* initialize system */
	read_conf(conf,"conf.ascii");		/* read configuration from disk */
#ifdef NVT
	init_T(conf);			        /* set desired temperature */
#endif
#ifdef USE_BOX
	create_box(conf);
	populate(conf);
	calc_force_with_box(conf);		/* calculate forces between particles */
#else
	calc_force(conf);			/* calculate forces between particles */
#endif
	ana(conf);				/* analyze starting configuration */

/* integrate equations of motion */
	time_ana = conf->time + dt_ana;		/* calculate next time to analyze configuration */
	while (conf->time+conf->h<time_end) {	/* integrate equations of motion */
		if (conf->time+conf->h>=time_ana) {	/* analyze configuration */
			hsave = conf->h;	/* adjust step size to integrate up to time_ana (exactly) */
			conf->h = time_ana-conf->time;
			vv_step(conf);		/* velocity verlet step */
			conf->h = hsave;	/* restore step size */
			ana(conf);		/* analyze configuration */
			time_ana = conf->time + dt_ana;
#ifdef NVT
	                thermostat(conf);	/* heat bath */
#endif
		}
		else {				/* just integrate */
			vv_step(conf);		/* velocity verlet step */
#ifdef NVT
	                thermostat(conf);	/* heat bath */
#endif
		}
	}
	conf->h = time_end-conf->time;		/* adjust step size to integrate up to time_end (exactly) */
	vv_step(conf);				/* velocity verlet step */

/* finish */
	//write_conf(conf,"conf.ascii","w");	/* save ending configuration */
	ana(conf);
    write_velocity_distribution(conf);
	return(0);
}/* end main */

/* -------------------------------------------------------------------------*/
static int read_conf(CONF *conf, char *fname)	/* read configuration from disk */
{
FILE 	*dp;
int	i,idim;
PARTICLE 	*particle;
/* a typical configuration file looks like (in 3 dimesnions)
	# D= 3 ; N= 2000 ; time= 0 ; kT= 1.68 ; L= 10.0 10.0 10.0
	x_1 y_1 z_1 ; p_x_1 p_y_1 p_z_1 ; m_1
	x_2 y_2 z_2 ; p_x_2 p_y_2 p_z_2 ; m_2
	...
*/

	if ((dp=fopen(fname,"r"))==NULL) {	/* open configuration file for reading */
		fprintf(stderr,"ERROR: Cannot read configuration file %s\n",fname);
		exit(1);
	}
	fscanf(dp,"# D= %d ; N= %d ; time= %lg ; kT= %lg ; L= ",&conf->Ndim,&conf->Nparticles,&conf->time,&conf->kT);	/* read header */
	if ((conf->L=(double*)calloc(conf->Ndim,sizeof(double)))==NULL) {		/* allocate memory for system size */
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	for (idim=0;idim<conf->Ndim-1;idim++)
		fscanf(dp,"%lg ",&(conf->L[idim]));
	fscanf(dp,"%lg\n",&(conf->L[idim]));
	fprintf(stderr,"INFO: %dD starting configuration @ %g contains %d particles ( ",conf->Ndim,conf->time,conf->Nparticles);
	for (idim=0;idim<conf->Ndim-1;idim++)
		fprintf(stderr,"%lg ",conf->L[idim]);
	fprintf(stderr,"%lg )\n",conf->L[idim]);
	create_conf(conf);			/* allocate memory and set up pointers for configuration */

	for (i=0,particle=conf->particle;i<conf->Nparticles;i++,particle++) {
		particle->index = i;
		for (idim=0;idim<conf->Ndim-1;idim++)				/* read particle's position */
			fscanf(dp,"%lg ",&(particle->x[idim]));
		fscanf(dp,"%lg ; ",&(particle->x[idim]));
		for (idim=0;idim<conf->Ndim-1;idim++)				/* read particle's momentum */
			fscanf(dp,"%lg ",&(particle->p[idim]));
		fscanf(dp,"%lg ; %lg\n",&(particle->p[idim]),&(particle->m));	/* read last component of momentum and mass */
	}
	fclose(dp);

	return(0);

}/* end read_conf */

/* -------------------------------------------------------------------------*/
static int write_conf(CONF *conf, char *fname, char *mode)	/* write configuration to disk, fname=file_name, mode="w" (write) or "a" (append) */
{
FILE 	*dp;
int	i,idim;
double  *xp;
PARTICLE *particle;

	if ((dp=fopen(fname,mode))==NULL) {
		fprintf(stderr,"ERROR: Cannot read configuration file %s\n",fname);
		exit(1);
	}
	fprintf(dp,"# D= %d ; N= %d; time= %lg ; kT= %lg ; L= ",conf->Ndim,conf->Nparticles,conf->time,conf->kT);	/* write header */
	for (idim=0;idim<conf->Ndim-1;idim++)
		fprintf(dp,"%lg ",conf->L[idim]);
	fprintf(dp,"%lg\n",conf->L[idim]);
	for (i=0,particle=conf->particle;i<conf->Nparticles;i++,particle++) {
		xp = particle->x;
/*		xp = period(conf,particle->x);*/
		for (idim=0;idim<conf->Ndim-1;idim++) 				/* write particle's position */
			fprintf(dp,"%+1.12lf ",xp[idim]);
		fprintf(dp,"%+1.12lf ; ",xp[idim]);
		for (idim=0;idim<conf->Ndim-1;idim++)				/* write particle's momentum */
			fprintf(dp,"%+1.12lf ",particle->p[idim]);
		fprintf(dp,"%+1.12lf ; %lg\n", particle->p[idim],particle->m);	/* write last component of momentum and mass */
	}
	fclose(dp);
	return(0);
}/* end write_conf */

/* -------------------------------------------------------------------------*/
static int create_conf(CONF *conf)
{
int	i;
double 	*force1,*force2,*phase_space;
PARTICLE 	*particle;

	if ((conf->particle=(PARTICLE*)calloc(conf->Nparticles,sizeof(PARTICLE)))==NULL) {	/* allocate memory for particles (structure) */
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	if ((phase_space=(double*)calloc(2*conf->Ndim*conf->Nparticles,sizeof(double)))==NULL) {/* allocate memory for positions and momenta */
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	for (i=0,particle=conf->particle;i<conf->Nparticles;i++,particle++) {	/* set pointers to ... */
		particle->x = phase_space+2*conf->Ndim*i;			/* position (vector) of particle i */
		particle->p = phase_space+2*conf->Ndim*i+conf->Ndim;		/* mementum (vector) of particle i */
	}
/* organization of phase_space:
(conf->particle  )->x	points to  	x[0][0]		(1st  coordinate of position of first particle)
				   	...
				 	x[0][Ndim-1]	(last coordinate of position of first particle)
(conf->particle  )->p	points to  	p[0][0]		(1st  coordinate of momentum of first particle)
					...
					p[0][Ndim-1]	(last coordinate of momentum of first particle)
(conf->particle+1)->x	points to  	x[1][0]		(1st  coordinate of position of 2nd   particle)
					...
				 	x[1][Ndim-1]	(last coordinate of position of 2nd   particle)
(conf->particle+1)->p   points to       p[1][0]         (1st  coordinate of momentum of 2nd   particle)
					...
					p[1][Ndim-1]	(last coordinate of momentum of 2nd   particle)
					...
and so on ... until			p[Nparticles-1][Ndim-1]	(last coordinate of momentum of last particle)
*/
	if (  ((force1=(double*)calloc(conf->Ndim*conf->Nparticles,sizeof(double)))==NULL) 	/* allocate memory for forces (twice) */
	    ||((force2=(double*)calloc(conf->Ndim*conf->Nparticles,sizeof(double)))==NULL) ){
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	if (  ((conf->force1=(double**)calloc(conf->Nparticles,sizeof(double*)))==NULL)	/* allocate memory for pointers to forces of ith particle */
	    ||((conf->force2=(double**)calloc(conf->Nparticles,sizeof(double*)))==NULL) ) {
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	for (i=0;i<conf->Nparticles;i++) {			/* conf->force[i][idim] is the idimth coordinate of force on particle i*/
		conf->force1[i] = force1 + conf->Ndim*i;	/* set pointers to force1 (vector) on particle i */
		conf->force2[i] = force2 + conf->Ndim*i;	/* set pointers to force2 (vector) on particle i */
	}
	conf->force_new = conf->force1;						/* set force_new to point at force1 */
	conf->force_old = conf->force2;						/* set force_old to point at force2 */
	if ((conf->momentum=(double*)calloc(conf->Ndim,sizeof(double)))==NULL) {/* allocate memory for components of center of mass momentum */
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	return(0);
}/* end create_conf */

#ifdef USE_BOX
/* -------------------------------------------------------------------------*/
static int create_box(CONF *conf)
{
int 	idim,ibox;
int	j[3],i[3],ip[3],im[3],itmp;
BOX	*box;

	if ((conf->Nbox=(int*)calloc(conf->Ndim,sizeof(int)))==NULL) {	/* allocate memory for number of boxes */
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	if ((conf->Lbox=(double*)calloc(conf->Ndim,sizeof(double)))==NULL) {
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	for (idim=0,conf->Nboxes=1;idim<conf->Ndim;idim++) {
		conf->Nbox[idim] = (int)floor(conf->L[idim]/(double)RANGE);
		conf->Lbox[idim] = conf->L[idim]/(double)conf->Nbox[idim];
		conf->Nboxes *=  conf->Nbox[idim];
		if (conf->Nbox[idim]<=2) {
			fprintf(stderr,"ERROR: number of boxes %d in direction %d is smaller than 3\nboxing cannot be used to calculate forces or energy\n",conf->Nbox[idim],idim);
			exit(1);
		}
	}
	if ((conf->box=(BOX*)calloc(conf->Nboxes,sizeof(BOX)))==NULL) {	/* allocate memory for boxes */
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	for (ibox=0,box=conf->box;ibox<conf->Nboxes;ibox++,box++) {	/* initialize boxes */
		box->index = ibox;
		box->Nmembers = 0;
		box->first = NULL;
		box->Nneighbors = (int)floor(pow(3.0,(double)conf->Ndim)+0.5);
		if ((box->neighbor=(BOX**)calloc(box->Nneighbors,sizeof(BOX*)))==NULL) {/* allocate memory for pointers to neighbors */
			fprintf(stderr,"ERROR: Cannot allocate memory\n");
			exit(1);
		}
		switch (conf->Ndim) {
			case 1:
				i[0] = ibox;
				break;
			case 2:
				i[1] = ibox/conf->Nbox[0];
				i[0] = ibox-i[1]*conf->Nbox[0];
				break;
			case 3:
				i[2] = ibox/(conf->Nbox[0]*conf->Nbox[1]);
				itmp = ibox-i[2]*(conf->Nbox[0]*conf->Nbox[1]);
				i[1] = itmp/conf->Nbox[0];
				i[0] = itmp-i[1]*conf->Nbox[0];
				break;
			default:
				fprintf(stderr,"ERROR: Ndim= %d is not implemented\n",conf->Ndim);
				exit(1);
				break;
		}
		for (idim=0;idim<conf->Ndim;idim++) {
			if ( (i[idim]<0) || (i[idim]>=conf->Nbox[idim]) ) {
				fprintf(stderr,"ERROR: ibox= %d idim= %d i[%d]= %d\n",ibox,idim,idim,i[idim]);
				exit(-1);
			}
			ip[idim] = i[idim]+1;
			if (ip[idim]>=conf->Nbox[idim]) 	ip[idim] = 0;
			im[idim] = i[idim]-1;
			if (im[idim]<0) 			im[idim] = conf->Nbox[idim]-1;
		}
		loop_mm1(conf,box,conf->Ndim,j,i,ip,im);
	}/* ibox */
	return(0);

}/* end create_box */

static void loop_mm1(CONF *conf, BOX *box, int level, int *j, int *i, int *ip, int *im)
{
int	idim,start,index,ii;
static int 	count = 0;

	if (level==0) {
		for (idim=0,start=1;idim<conf->Ndim;idim++) {
			if (j[idim]!= -1) 	start = 0;
		}
		if (start==1) 	count = 0;
		for (idim=conf->Ndim-1,index=0;idim>=0;idim--) {
			if (j[idim]==-1) 	ii = im[idim];
			else if (j[idim]==0) 	ii = i[idim];
			else 			ii = ip[idim];
			index = index*conf->Nbox[idim] + ii;
		}
		box->neighbor[count] = conf->box+index;
/*		fprintf(stderr,"INFO: box %d has %d nd neighbor %d\n",(int)(box-conf->box),count,index);*/
		count++;
	}
	else {
		level--;
		for (j[level]=-1;j[level]<=1;j[level]++)
			loop_mm1(conf,box,level,j,i,ip,im);
	}
}

/* -------------------------------------------------------------------------*/
static int populate(CONF *conf)
{
int 	ibox,i,sum;
BOX	*box;
PARTICLE 	*particle;

	for (ibox=0,box=conf->box;ibox<conf->Nboxes;ibox++,box++) {	/* initialize boxes */
		box->Nmembers = 0;
		box->first = NULL;
	}
	for (i=0,particle=conf->particle;i<conf->Nparticles;i++,particle++) {
		insert(calc_box(conf,particle->x),particle);
	}
	for (i=0,sum=0;i<conf->Nboxes;i++)
		sum += (conf->box[i]).Nmembers;
	fprintf(stderr,"INFO: %d particles in boxes\n",sum);
/*	for (i=0,box=conf->box;i<conf->Nboxes;i++,box++) {
		fprintf(stderr,"INFO: Box %d (%d)- ",i,box->Nmembers);
		for (sum=0,particle=box->first;sum<box->Nmembers;sum++,particle=particle->after)
			 fprintf(stderr," %d",particle->index);
		fprintf(stderr,"\n");
	}*/
	return(0);

}/* end populate */

/* -------------------------------------------------------------------------*/
static int insert(BOX *box,PARTICLE *particle)
{
PARTICLE 	*before;

	if (box->Nmembers != 0){
		before = (box->first)->before;
		before->after = particle;
		(box->first)->before = particle;
		particle->before = before;
	}
	else {
		box->first = particle;
		particle->before = particle;
	}
	box->Nmembers++;
	particle->after = box->first;
	particle->box = box;
	return(0);

}/* end insert */

/* -------------------------------------------------------------------------*/
static int delete(BOX *box,PARTICLE *particle)
{
PARTICLE 	*before,*after;

	box->Nmembers--;
	before = particle->before;
	after = particle->after;
	before->after = after;
	after->before = before;
	box->first = before;
	return(0);

}/* end delete */

/* -------------------------------------------------------------------------*/
static struct box *calc_box(CONF *conf, double *x)
{
int 	idim,index;
double 	*xperiod;

	xperiod = period(conf,x);
	for (idim=0,index=0;idim<conf->Ndim;idim++)
		index = index*conf->Nbox[idim] + (int)(xperiod[idim]/conf->Lbox[idim]);
	return(conf->box+index);

}/* end calc_box */
#endif

/* -------------------------------------------------------------------------*/
static int calc_force(CONF *conf)	/* calculate force on each particle and store in force_new */
{
int 	i,j,idim;
double 	*dist,r2,force,force_divr;
PARTICLE 	*p_i,*p_j;

	if ((dist=(double*)calloc(conf->Ndim,sizeof(double)))==NULL) {	/* allocate memory for distance vector */
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	for (i=0;i<conf->Ndim*conf->Nparticles;i++)
		conf->force_new[0][i] = 0.0;
	for (i=0,p_i=conf->particle;i<conf->Nparticles;i++,p_i++) {	/* loop over N(N-1)/2 pairs, p_i pointers to particle i */
		for (j=i+1,p_j=p_i+1;j<conf->Nparticles;j++,p_j++) {	/* p_j is pointer to particle j */
			r2 = mic(conf,p_i->x,p_j->x,dist);
			if (r2<RANGE2) {
				force_divr = F_divr(r2);
				for (idim=0;idim<conf->Ndim;idim++) {
					force = dist[idim]*force_divr;		/* e.g., force=-vec(r)/r^3 */
					conf->force_new[i][idim] += force; 	/* force on particle i */
					conf->force_new[j][idim] -= force;	/* actio = reactio */
				}
			}
		}
	}
	free(dist);							/* free memory for distance vector */
	return(0);

}/* end calc_force */

#ifdef USE_BOX
/* -------------------------------------------------------------------------*/
static int calc_force_with_box(CONF *conf)
{
int 	i,idim,ibox1,ibox2,i1,i2,index1,index2;
double 	r2,force,force_divr;
BOX	*box1,*box2;
static double 	*dist = NULL;
PARTICLE 	*particle1,*particle2;

	if (dist==NULL) {
		if ((dist=(double*)calloc(conf->Ndim,sizeof(double)))==NULL) {
			fprintf(stderr,"ERROR: Cannot allocate memory\n");
			exit(1);
		}
	}
	for (i=0;i<conf->Ndim*conf->Nparticles;i++)
		conf->force_new[0][i] = 0.0;
	for (ibox1=0,box1=conf->box;ibox1<conf->Nboxes;ibox1++,box1++) {/* loop over box */
		if (box1->Nmembers == 0) continue;
		for (ibox2=0;ibox2<box1->Nneighbors;ibox2++) {
			box2=box1->neighbor[ibox2];
			if (box2->Nmembers == 0) continue;
			if (box2>box1) {
				for (i1=0,particle1=box1->first;i1<box1->Nmembers;i1++,particle1=particle1->after) {
					for (i2=0,particle2=box2->first;i2<box2->Nmembers;i2++,particle2=particle2->after) {
						r2 = mic(conf,particle1->x,particle2->x,dist);
						if (r2<RANGE2) {
							index1 = particle1->index;
							index2 = particle2->index;
							force_divr = F_divr(r2);
							for (idim=0;idim<conf->Ndim;idim++) {
								force = dist[idim]*force_divr;
								conf->force_new[index1][idim] += force;
								conf->force_new[index2][idim] -= force;
							}
						}
					}/* i2 */
				}/* i1 */
			} else if (box1==box2) {
				for (i1=0,particle1=box1->first;i1<box1->Nmembers;i1++,particle1=particle1->after) {
					for (i2=0,particle2=box2->first;i2<box2->Nmembers;i2++,particle2=particle2->after) {
						if (particle1>particle2) {
							r2 = mic(conf,particle1->x,particle2->x,dist);
							if (r2<RANGE2) {
								index1 = particle1->index;
								index2 = particle2->index;
								force_divr = F_divr(r2);
								for (idim=0;idim<conf->Ndim;idim++) {
									force = dist[idim]*force_divr;
									conf->force_new[index1][idim] += force;
									conf->force_new[index2][idim] -= force;
								}
							}
						}
					}/* i2 */
				}/* i1 */
			}
		}/* box2 */
	}/* box1 */
	return(0);

}/* end calc_force_with_box */
#endif

/* -------------------------------------------------------------------------*/
static int vv_step(CONF *conf)		/* integrate the equations of motion from time to time+h using the velocity verlet algorithm */
{
int 	i,idim;
double 	h,hhalf,h2half,**force_tmp;
PARTICLE 	*particle;
#ifdef USE_BOX
BOX	*box_new,*box_old;
#endif

	h = conf->h;
	hhalf  = 0.5*h;
	h2half = 0.5*h*h;
	for (i=0,particle=conf->particle;i<conf->Nparticles;i++,particle++) {	/* propagate positions */
		for (idim=0;idim<conf->Ndim;idim++) {
			particle->x[idim] += h*particle->p[idim] + h2half*conf->force_new[i][idim]/particle->m;
		}
#ifdef USE_BOX
		box_old = particle->box;
		box_new = calc_box(conf,particle->x);
		if (box_old!=box_new) {
			delete(box_old,particle);
			insert(box_new,particle);
		}
#endif
	}
	force_tmp = conf->force_new;	/* swap old and new forces */
	conf->force_new = conf->force_old;
	conf->force_old = force_tmp;
#ifdef USE_BOX
	calc_force_with_box(conf);	/* calculate force at new position -> force_new */
#else
	calc_force(conf);
#endif
	for (i=0,particle=conf->particle;i<conf->Nparticles;i++,particle++) {   /* propagate momenta */
		for (idim=0;idim<conf->Ndim;idim++) {
			particle->p[idim] += hhalf*(conf->force_new[i][idim]+conf->force_old[i][idim]);
		}
	}
	conf->time += h;	/* increase time by h */
	return(0);

}/* end vv_step */

/* -------------------------------------------------------------------------*/
static int calc_momentum(CONF *conf)	/* calculate the center of mass momentum of the configuration */
{
int 	i,idim;
PARTICLE 	*particle;

	for (idim=0;idim<conf->Ndim;idim++)
		conf->momentum[idim] = 0.0;
	for (i=0,particle=conf->particle;i<conf->Nparticles;i++,particle++) {
		for (idim=0;idim<conf->Ndim;idim++) {
			conf->momentum[idim] += particle->p[idim];
		}
	}
	return(0);

}/* end calc_momentum */

/* -------------------------------------------------------------------------*/
static int calc_velocity_distribution(CONF *conf)	/* calculate the velocity distribution of the configuration */
{
    // define VELOCITY_BINS 10000
    // define MAX_VELOCITY 10.0
    // int velocity_distribution[VELOCITY_BINS] = {0};

int 	i,idim;
double 	p;
PARTICLE 	*p_i;

    velocity_distribution_count++;
	for (i=0,p_i=conf->particle;i<conf->Nparticles;i++,p_i++) {/* loop over all particles */
        p = 0.0;
		for (idim=0;idim<conf->Ndim;idim++) {
			p += p_i->p[idim]*p_i->p[idim];
        }
        p = sqrt(p)/p_i->m*MAX_VELOCITY_FACTOR*VELOCITY_BINS;
        if(p < 0.0 || p > VELOCITY_BINS)
        {
            printf("ERROR: p not in range");
        }
        velocity_distribution[(int) p]++;
    	}
        printf("%i %i\n", conf->Nparticles, velocity_distribution_count);
	return(0);

}/* end calc_velocity_distribution */

/* -------------------------------------------------------------------------*/

static int write_velocity_distribution(CONF *conf)
{
    int i;
    FILE 	*dp;
    
    
	if ((dp=fopen("conf.log","a"))==NULL) {	/* open log file and append results */
		fprintf(stderr,"ERROR: Cannot open log file\n");
		exit(1);
	}
    fprintf(dp, "\n\n\n#Velocity Distribution of Particles\n#Velocity Count\n");
    for(i = 0; i < VELOCITY_BINS; i++)
    {
        fprintf(dp, "%lf %.10lf\n", i/(VELOCITY_BINS*MAX_VELOCITY_FACTOR), (double) (velocity_distribution[i]*VELOCITY_BINS*MAX_VELOCITY_FACTOR)/(velocity_distribution_count*conf->Nparticles) );
    }
    
    return(0);
}

/* -------------------------------------------------------------------------*/
static int calc_pressure(CONF *conf)	/* calculate the pressure of the configuration */
{
int 	i,j,idim;
double 	*dist,r2,p;
PARTICLE 	*p_i,*p_j;

	if ((dist=(double*)calloc(conf->Ndim,sizeof(double)))==NULL) {
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	for (i=0,conf->energy_pot=0.0,p_i=conf->particle;i<conf->Nparticles;i++,p_i++) {/* loop over N(N-1)/2 pairs */
		for (j=i+1,p_j=p_i+1;j<conf->Nparticles;j++,p_j++) {
			r2 = mic(conf,p_i->x,p_j->x,dist);
			if (r2<RANGE2) {
				conf->energy_pot += V(r2);
			}
		}
	}
	for (i=0,conf->energy_kin=0.0,p_i=conf->particle;i<conf->Nparticles;i++,p_i++) {/* loop over all articles */
		for (idim=0;idim<conf->Ndim;idim++) {
			p = p_i->p[idim];
			conf->energy_kin += 0.5*p*p/p_i->m;	/* calculate kinetic energy */
		}
	}
	free(dist);
	return(0);

}/* end calc_pressure */

/* -------------------------------------------------------------------------*/
static int calc_energy(CONF *conf)	/* calculate the kinetic and potential energy of the configuration */
{
int 	i,j,idim;
double 	*dist,r2,p;
PARTICLE 	*p_i,*p_j;

	if ((dist=(double*)calloc(conf->Ndim,sizeof(double)))==NULL) {
		fprintf(stderr,"ERROR: Cannot allocate memory\n");
		exit(1);
	}
	for (i=0,conf->energy_pot=0.0,p_i=conf->particle;i<conf->Nparticles;i++,p_i++) {/* loop over N(N-1)/2 pairs */
		for (j=i+1,p_j=p_i+1;j<conf->Nparticles;j++,p_j++) {
			r2 = mic(conf,p_i->x,p_j->x,dist);
			if (r2<RANGE2) {
				conf->energy_pot += V(r2);
			}
		}
	}
	for (i=0,conf->energy_kin=0.0,p_i=conf->particle;i<conf->Nparticles;i++,p_i++) {/* loop over all articles */
		for (idim=0;idim<conf->Ndim;idim++) {
			p = p_i->p[idim];
			conf->energy_kin += 0.5*p*p/p_i->m;	/* calculate kinetic energy */
		}
	}
	free(dist);
	return(0);

}/* end calc_energy */

#ifdef USE_BOX
/* -------------------------------------------------------------------------*/
static int calc_energy_with_box(CONF *conf)
{
int 	idim,ibox1,ibox2,i1,i2;
double 	r2,p;
BOX	*box1,*box2;
static double 	*dist = NULL;
PARTICLE 	*particle1,*particle2;

	if (dist==NULL) {
		if ((dist=(double*)calloc(conf->Ndim,sizeof(double)))==NULL) {
			fprintf(stderr,"ERROR: Cannot allocate memory\n");
			exit(1);
		}
	}
	for (ibox1=0,box1=conf->box,conf->energy_pot=0.0;ibox1<conf->Nboxes;ibox1++,box1++) {/* loop over box */
		if (box1->Nmembers==0) continue;
		for (ibox2=0;ibox2<box1->Nneighbors;ibox2++) {
			box2=box1->neighbor[ibox2];
			if (box2->Nmembers==0) continue;
			if (box2>box1) {
				for (i1=0,particle1=box1->first;i1<box1->Nmembers;i1++,particle1=particle1->after) {
					for (i2=0,particle2=box2->first;i2<box2->Nmembers;i2++,particle2=particle2->after) {
						r2 = mic(conf,particle1->x,particle2->x,dist);
						if (r2<RANGE2) {
							conf->energy_pot += V(r2);
						}
					}/* i2 */
				}/* i1 */
			} else if (box1==box2) {
				for (i1=0,particle1=box1->first;i1<box1->Nmembers;i1++,particle1=particle1->after) {
					for (i2=0,particle2=box2->first;i2<box2->Nmembers;i2++,particle2=particle2->after) {
						if (particle1>particle2) {
							r2 = mic(conf,particle1->x,particle2->x,dist);
							if (r2<RANGE2) {
								conf->energy_pot += V(r2);
							}
						}
					}/* i2 */
				}/* i1 */
			}
		}/* box2 */
	}/* box1 */
	for (i1=0,conf->energy_kin=0.0,particle1=conf->particle;i1<conf->Nparticles;i1++,particle1++) {	/* loop over all articles */
		for (idim=0;idim<conf->Ndim;idim++) {
			p = particle1->p[idim];
			conf->energy_kin += 0.5*p*p/particle1->m;	/* calculate kinetic energy */
		}
	}
	return(0);

}/* end calc_energy_with_box */
#endif

/* -------------------------------------------------------------------------*/
static int ana(CONF *conf)		/* calculate energy and momentum, and write some properties of configuration to log file */
{
int 	idim;
double	dummy1;
FILE 	*dp;

	calc_momentum(conf);
	calc_energy(conf);
    calc_velocity_distribution(conf);
	dummy1 = conf->energy_pot;
/*	calc_energy_with_box(conf);*/
	calc_energy(conf);	/* always use the slow N^2-algorithm to check that boxing works correctly */
	if ( fabs(conf->energy_pot-dummy1)>1e-9 ) {
		fprintf(stderr,"ERROR in energy calculation: %g - %g\n",conf->energy_pot,dummy1);
		exit(1);
	}
	/* write_conf(conf,"conf.tmp","a");*/	/* write trajectory */

	if ((dp=fopen("conf.log","w"))==NULL) {	/* open log file and append results */
		fprintf(stderr,"ERROR: Cannot open log file\n");
		exit(1);
	}
	fprintf(dp,"%lg ; %8f ( %8f + %8f ) ",
			conf->time,conf->energy_pot+conf->energy_kin,
			conf->energy_pot,conf->energy_kin);
	for (idim=0;idim<conf->Ndim-1;idim++)
		fprintf(dp,"%8f ",conf->momentum[idim]);
	fprintf(dp,"%lg ;\n",conf->momentum[idim]);
	fclose(dp);	/* close log file */
	return(0);
}/* end ana */

/* -------------------------------------------------------------------------*/
static double *period(CONF *conf,double *x)	/* calculate the position within the periodic simulation cell */
{
int 	i;
double 	L,value;
static double *xstore = NULL;

	if (xstore==NULL) {
		if ((xstore=(double*)calloc(conf->Ndim,sizeof(double)))==NULL) {
			fprintf(stderr,"ERROR: Cannot allocate memory in period\n");
			exit(1);
		}
	}
	for (i=0;i<conf->Ndim;i++,x++) {
		L = conf->L[i];
		value = *x;
		value -= L*((int)(value/L));
		if (value<0) value += L;
		xstore[i] = value;
	}
	return(xstore);

}/* end period */

/* -------------------------------------------------------------------------*/
static double mic(CONF *conf,double *x,double *y,double *d) 	/* calculate distance between x and y using the minimum image convention */
{
int 	i;
double 	L,value,r2;

	for (i=0,r2=0;i<conf->Ndim;i++,x++,y++,d++) {
		L = conf->L[i];
		value = *x - *y;
		value -= L*rint(value/L);
		*d = value;
		r2 += value*value;
/*		if ((value>L/2.0)||(value<-L/2.0)) {  remove this after checking
			fprintf(stderr,"ERROR (1) in mic: %g %g = %g ouside of range [%g:%g]\n",*x,*y,*d,-L/2.0,L/2.0);
			exit(1);
		}
		value = *x - *y;
		while (value>=L/2.0) value -= L;
		while (value<=-L/2.0) value += L;
		if (fabs(value-*d)>1e-9) {
			fprintf(stderr,"WARNING (2) in mic: %g %g = %g vs %g \n",*x,*y,value,*d);
		}*/
	}
	return(r2);

}/* end mic */

#ifdef NVT
/* -------------------------------------------------------------------------*/
static int init_T(CONF *conf)	/* set velocities according to Maxwell distribution */
{
int 	i,idim;
double 	c;
PARTICLE 	*particle;

	for(i=0,particle=conf->particle;i<conf->Nparticles;i++,particle++) {
		for(idim=0;idim<conf->Ndim;idim++) {
			/* generate Gaussian random number via Box-Muller method */
			do {
				c = drand48();
			} while(c > 0.99999);
			particle->p[idim] = sqrt(-2.*conf->kT*log(1.-c))*cos(2*M_PI*drand48());
		}
	}

	return(0);
}/* end init_T */

/* -------------------------------------------------------------------------*/
static int thermostat(CONF *conf)	/* collisions with heat bath */
{
int 	i,idim,k,n;
double 	c,L,p;

        L=exp(-LAMBDA);k=-1;p=1.0; /* generate Poisson distributed random number */
        do {
                k+=1;
                p*=drand48();
        } while (p>L);
	for(i=0;i<k;i++) {
                /* pick a random particle to collide with the heat bath */
		do {
			c = drand48();
		} while(c > 0.99999);
                n=floor(c*conf->Nparticles);
		for(idim=0;idim<conf->Ndim;idim++) {
			/* generate Gaussian random number via Box-Muller method */
			do {
				c = drand48();
			} while(c > 0.99999);
			conf->particle[n].p[idim] = sqrt(-2.*conf->kT*log(1.-c))*cos(2*M_PI*drand48());
		}
	}

	return(0);
}/* end thermostat */
#endif

/* -------------------------------------------------------------------------*/
