reset
set terminal epslatex color colortext
set output 'energydiff0001.tex'
set xlabel 'Zeit $t$ [$\tau$]'
set ylabel 'Relative Energiedifferenz $\Delta H(t)/H_0$'
set xrange [0:10]
f(x)=a*x+b
fit f(x) 'energieabweichung0001.dat' via a,b
plot 'energieabweichung0001.dat' w lines title 'Energieabweichung', f(x) w lines title 'Lineare Regression'
set output
