reset
set terminal epslatex color colortext
set output 'energydiff001.tex'
set xlabel 'Zeit $t$ [$\tau$]'
set ylabel 'Relative Energiedifferenz $\Delta H(t)/H_0$'
set xrange [0:100]
f(x)=a*x+b
fit f(x) 'energieabweichung001.dat' via a,b
plot 'energieabweichung001.dat' w lines title 'Energieabweichung', f(x) w lines title 'Lineare Regression'
set output
