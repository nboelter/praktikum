reset
f1(x) = 1.8*x + c*x*x
f2(x) = d + e*exp(-x*g)
d = -0.5
e = 0.5
g = -4
f1(x) = 1.8*x + c*x*x
f2(x) = d + e*exp(-x*g)
fit f1(x) 'rho_volume_pressure.txt' using 1:3:4 via c
fit f2(x) 'rho_volume_pressure.txt' using 1:3:4 via d,e,g

set terminal epslatex color colortext
set output 'pressure.tex'
set xlabel 'Dichte [$\nicefrac{1}{\sigma^3}$]'
set ylabel 'Druck [$\nicefrac{\epsilon}{\sigma^3}$]'
plot 'rho_volume_pressure.txt' using 1:3:4 with errorbars ps 2 title 'Simulation', f1(x) title 'Virialentwicklung zweiter Ordnung' lw 2, f2(x) title 'exponentielle Regression' lw 2
set output
