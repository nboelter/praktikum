#!/usr/bin/python
# -*- coding: utf-8 -*-
data = open("conf.log", "r")

#Temperatur:
etot=0.
i=0
for line in data:
	token = line.strip().split(' ')
	etot = etot+ float(token[4])
	print etot
	i = i+1

etot=etot/float(i)
t=2./3.*etot/2000.
print "Temperatur:", t

#Energie:
i=0
h=0.
h0=0.
data.seek(0)
energy=open("energieabweichung.dat","w")
for line in data:
	token = line.strip().split()
	if i==0:
		h0=float(token[2])
	h=float(token[2])
	h=(h-h0)/h0
	energy.write(str(token[0]))
	energy.write(" ")
	energy.write(str(h))
	energy.write("\n")
	i = i+1
energy.close()

data.close()
