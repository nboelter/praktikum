reset
set key left box
set xlabel 'Fallensteifigkeit [$\unit{N/m}$]'
set ylabel 'Laserintensität [$\unit{V}$]'
set terminal epslatex color colortext
f(x)=a*x+b
fit f(x) 'B7-Messwerte.txt' index 6 u 2:1 via a,b
set output 'StiffVsPower.tex'
plot 'B7-Messwerte.txt' index 0 u 2:1:(0.1) w yerrorbars title 'Messung 1', \
     'B7-Messwerte.txt' index 1 u 2:1:(0.1) w yerrorbars title 'Messung 2', \
     'B7-Messwerte.txt' index 2 u 2:1:(0.1) w yerrorbars title 'Messung 3', \
     f(x*1e6) title 'Lineare Regression'
set output
