reset
set xrange [-0.01:0.3]
set yrange [0:5]
set xlabel 'Fluchtintensität des Lasers $[\unit{V}]$'
set ylabel 'Häufigkeit'
set grid
set boxwidth 0.03 absolute
set style fill transparent solid 0.5 noborder
binwidth=0.04999
bin(x,width)=width*floor(x/width)
d = 23.34
E = 7.46e-6
m = 0.6174
g(x) = d*E*m*x
set terminal epslatex color colortext
set output 'BacteriaForces.tex'
plot 'B7-Messwerte.txt' index 4 u (bin($1,binwidth)):(1.0) smooth freq w boxes lc rgb"green" notitle
set output
