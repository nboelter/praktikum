from math import *
def Rechne(phi, gamma):
  alpha = (180.0 - phi)/2.0
  delta = (-gamma+122.2)
  return (alpha, delta)

data = []
f = open('Messwerte.txt', 'r')
for line in f:
  if len(line) < 2:
    break
  if line[0] == '#':
    continue
  data.append(map(float, line.strip().split(' ', 2)))
f.close()

f = open('Winkeldrehung.txt', 'w')
f.write('# Einfallswinkel [deg], Drehung der Polarisationsrichtung [deg]\n')
for (phi, gamma) in data:
  v = Rechne(phi, gamma)
  f.write(str(v[0]) + ' ' + str(v[1]) + '\n')
f.close()
