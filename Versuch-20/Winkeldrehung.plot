reset
set xrange [45:90]
set yrange [0:75]
set xlabel 'Einfallswinkel $\alpha\,[\degree]$'
set ylabel 'Drehung der Schwingungsebene $\gamma\,[\degree]$'
n2 = 1.51
gF(x) = 180.0/pi * ( atan( -(cos(x-asin(sin(x)/n2)))/(cos(x+asin(sin(x)/n2))))-pi/4.0 )
fF(x)= (gF(x*pi/180.0) < 0 && x < 60) ? gF(x*pi/180.0) + 180.0 : gF(x*pi/180.0)
g(x) = 180.0/pi * ( atan( -(cos(x-asin(sin(x)/1.51)))/(cos(x+asin(sin(x)/1.51))))-pi/4.0 )
f(x)= (g(x*pi/180.0) < 0 && x < 60) ? g(x*pi/180.0) + 180.0 : g(x*pi/180.0)
h(x) = m*x+b
fit h(x) 'Winkeldrehung.txt' u 1:2 via m,b
fit fF(x) 'Winkeldrehung.txt' u 1:2 via n2
set terminal epslatex color colortext
set output 'Winkeldrehung.tex'
plot 'Winkeldrehung.txt' u 1:2 title 'Messwerte', (f(x)) title 'theoretischer Verlauf' lc 2 lt 1 lw 2, fF(x) title 'Regression' lc 3 lt 2 lw 2, h(x) title 'lineare Regression' lc 4 lt 3 lw 2
set output
