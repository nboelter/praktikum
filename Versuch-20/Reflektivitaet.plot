reset
set xrange [0:pi/2]
set yrange [0:1]
set key top left
set xlabel 'Einfallswinkel $\alpha_1$ im Bogenmaß'
set ylabel 'Reflektivität $\rho$'
set xtics ('$\frac{\pi}{8}$' pi/8)
set xtics add ('$\frac{\pi}{4}$' pi/4)
set xtics add ('$\frac{3\pi}{8}$' 3*pi/8)
set xtics add ('$\frac{\pi}{2}$' pi/2)
rhosenk(x)=abs(sin(x-asin((1/1.51)*sin(x)))/sin(x+asin((1/1.51)*sin(x))))
rhopara(x)=abs(tan(x-asin((1/1.51)*sin(x)))/tan(x+asin((1/1.51)*sin(x))))
set terminal epslatex color colortext
set output 'Reflektivitaet.tex'
plot rhopara(x) title 'parallel' lw 2, rhosenk(x) title 'senkrecht' lw 2
set output
