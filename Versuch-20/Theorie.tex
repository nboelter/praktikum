\subsection{Polarisation elektromagnetischer Wellen}
Da es sich bei elektromagnetischen Wellen um Transversalwellen handelt, kann man ihre Polarisationsrichtung angeben.
Diese gibt Aufschluss darüber, in welcher Richtung die Schwingung des elektrischen Feldvektors stattfindet.

Zur Messung linearer Polarisationen bietet sich zum Beispiel die Verwendung eines linearen Polarisationsfilters an.
Solche Filter sind nur für in eine bestimmte Richtung polarisiertes Licht durchlässig,
alle anderen Anteile der eingehenden Welle werden \q{gelöscht}. Auf die genaue Funktionsweise wird unten eingegagngen.
Durch die Messung der Intensität hinter dem Aufbau bei verschiedenen Winkelstellungen des Filters lässt sich eine Aussage
über den Polarisationszustand des Lichtes vor der Analyse machen.
\subsection{Verhalten an Grenzschichten und in Medien}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Grenzschicht.pdf_tex}
\end{center}
\caption{Reflektion und Transmission eines Lichtstrahls (Verändert nach: \citet{LP-Fresnel})}
\label{fig:fresnel}
\end{figure}
Trifft eine elektromagnetische Welle auf eine Grenzschicht, so wird sie im Allgemeinen sowohl reflektiert als auch transmittiert.
Für die Ein- und Ausfallswinkel gilt hierbei das \mensch{Snellius}sche Brechungsgesetz~\citep{Gerthsen}:
\begin{align}
    n_1 \sin{\alpha_1}&=n_2 \sin{\alpha_2}.\label{eq:snell}
\end{align}
Hier ist $n_1$ der Brechungsindex des Mediums, aus dem der Strahl unter dem Winkel $\alpha_1$ in das andere Medium mit Brechungsindex $n_2$ übergeht.
$\alpha_2$ ist dann der Winkel des austretenden Strahls. Beide Winkel werden zum Lot hin gemessen.

Viele Medien haben auch verschiedene Brechungsindizes für die jeweiligen Richtungen. Ein Beispiel sind Kristalle,
bei deren Gitterstruktur in verschiedenen Richtungen eine unterschiedliche Gitterkonstante vorliegt.

Diesen Umstand kann man nutzen, um einen Polarisator herzustellen.
Hierfür schneidet man ein doppelbrechendes Medium (z.B. Kalkspat) diagonal durch und verkittet die Grenzfläche (z.B. mit Kanadabalsam).
Seitlich wird das Objekt geschwärzt. Die Einfallsebene ist so gewählt, dass der Unterschied des Brechungsindexes
für zwei senkrecht zueinander polarisierte einfallende Wellen am größten ist.

Durch Totalreflexion an der Kittstelle wird der senkrecht zur Einfallsebene polarisierte Strahl abgelenkt, während der parallel polarisierte transmittiert wird. Ersterer wird dann an den Außenwänden absorbiert.
Der andere Strahl tritt leicht parallel versetzt wieder aus.

Die Totalreflexion kommt dadurch zustande, dass der Kitt einen anderen Brechunsgindex aufweist als das Prismenmedium.
Man kann dann den Winkel des Schnitts so wählen, dass der per Formel~\ref{eq:snell} berechnete Austrittswinkel $\alpha_2=90\degree$ wird.
So kann die Welle im zweiten Medium nicht mehr propagieren und es kommt zur Totalreflexion.
\subsection{Fresnelsche Formeln}
Die Intensität einer ebenen Lichtwelle ist gegeben durch~\citep{Gerthsen}:
\begin{align}
    I &= \left<|\vec{S}|\right>_t = \frac{1}{\mu_0\mu_r} \left<\left|\vec{E}\times\vec{B}\right|\right>_t = \sqrt{\frac{\epsilon_0\epsilon_r}{\mu_0\mu_r}}|E_0^2|.\label{eq:intensitaet}
\end{align}
Da die senkrecht einfallende Komponente der Intensität stetig an der Grenzfläche ist, gilt dann mit $\epsilon_j = \frac{\epsilon_0\epsilon_{r,j}}{\mu_0\mu_{r,j}}$
und den Indizes $i$ für den einfallenden, $r$ für den reflektierten und $t$ für den ttransmittierten Strahl~\citep{Gerthsen}:
\begin{align}
    I_i - I_r &= I_t\nonumber\\
    \sqrt{\epsilon_1}(E_i^2-E_r^2)\cos{\alpha_1} &= \sqrt{\epsilon_2}E_t^2\cos{\alpha_2}.\label{eq:scheisse1}
\end{align}
Weiterhin gilt aufgrund der Stetigkeit der senkrecht zur Einfallsebene polarisierten Komponente des elektrischen Feldes:
\begin{align}
    E_i+E_r &= E_t.\label{eq:scheisse2}
\end{align}
Weiterhin gilt die Maxwell-Relation $n_j=\sqrt{\epsilon_j}$ und mit dem Snelliusschen Brechungsgesetz ergibt sich~\citep{Gerthsen}:
\begin{align}
    \sqrt{\frac{\epsilon_2}{\epsilon_1}} &= \frac{n_2}{n_1} = \frac{\sin{\alpha_1}}{\sin{\alpha_2}}.
\end{align}
Teilt man nun Gleichung~\ref{eq:scheisse1} durch Gleichung~\ref{eq:scheisse2} und verwendet die binomischen Formeln, so erhält man:
\begin{align}
    E_r &= -E_i \frac{\sin{(\alpha_1-\alpha_2)}}{\sin{(\alpha_1+\alpha_2)}}\label{eq:fresnelperp}\\
    E_t &= E_i \frac{2\sin{\alpha_2}\cos{\alpha_1}}{\sin{(\alpha_1+\alpha_2)}}.\nonumber
\end{align}
Betrachtet man stattdessen die parallel zur Einfallsebene polarisierte Komponente, so ergibt sich mit
$(E_i+E_r)\cos{\alpha_1} = E_t\cos{\alpha_2}$ analog~\citep{Gerthsen}:
\begin{align}
    E_r &= -E_i \frac{\tan{(\alpha_1-\alpha_2)}}{\tan{(\alpha_1+\alpha_2)}}\label{eq:fresnelpara}\\
    E_t &= E_i \frac{2\sin{\alpha_2}\cos{\alpha_1}}{\sin{(\alpha_1+\alpha_2)}}.\nonumber
\end{align}
Per Definition gilt für das Reflexionsverhältnis $\rho = E_r/E_i$ und für das Transmissionsverhältnis $\sigma = E_t/E_i$
und man erhält die \mensch{Fresnel}schen Formeln einmal für die Komponente senkrecht zur Einfallsebene
\begin{align*}
    \rho &= -\frac{\sin{(\alpha_1-\alpha_2)}}{\sin{(\alpha_1+\alpha_2)}}\\
    \sigma &= \frac{2\sin{\alpha_2}\cos{\alpha_1}}{\sin{(\alpha_1+\alpha_2)}}
\end{align*}
und für die Komponente parallel zur Einfallsebene
\begin{align*}
    \rho &= -\frac{\tan{(\alpha_1-\alpha_2)}}{\tan{(\alpha_1+\alpha_2)}}\\
    \sigma &= \frac{2\sin{\alpha_2}\cos{\alpha_1}}{\sin{(\alpha_1+\alpha_2)}}.
\end{align*}
In Abbildung~\ref{fig:reflektivitaet} wurde der Betrag der Reflektivität gegen den Einfallswinkel aufgetragen.
Bei parallelem Licht hat dieser Verlauf offenbar ein Minimum, bei dem die Reflektivität verschwindet, und nur noch senkrecht zur Einfallsebene polarisiertes Licht reflektiert wird. 

Dieser \emph{Brewsterwinkel} $\alpha$ kann auch mit Hilfe von Gleichung~(\ref{eq:snell}) hergeleitet werden, indem man $\unit[90]{\degree}$ für den Transmissionswinkel einsetzt. \citep[S.240]{Demtroeder-Exp2}
\begin{align}
  \tan(\alpha) &= \frac{n_2}{n_1} \label{eq:brewster}
\end{align} 
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Reflektivitaet.tex}
\end{center}
\caption{Reflektivität eines Mediums ($n=1.51$) in Abhängigkeit vom Einfallswinkel bei parallel und senkrecht zur Einfallsebene polarisiertem Licht.}
\label{fig:reflektivitaet}
\end{figure}
Zur Bestimmung der Drehung der Polarisation des Lichtes nach Reflektion kann man nun den Winkel $\delta$ zwischen Polarisationsrichtung des einfallenden und des ausfallenden Lichtes berechnen.
Wie man sich leicht überlegt, gilt:
\begin{align*}
    \tan\delta &= -\frac{E_{r,\perp}}{E_{r,\parallel}},
\end{align*}
wobei die obigen $E_{r,\perp}$ und $E_{r,\parallel}$ für senkrecht und parallel zur vorherigen Polaristaionsrichtung polarisiertes Licht nach der Reflexion stehen.
Man erhält dann durch einsetzen der Gleichungen~\ref{eq:fresnelperp} und~\ref{eq:fresnelpara}:
\begin{align*}
    \tan\delta &= -\frac{\sin(\alpha_1-\alpha_2)\tan(\alpha_1+\alpha_2)}{\sin(\alpha_1+\alpha_2)\tan(\alpha_1-\alpha_2)}
    = -\frac{\cos(\alpha_1-\alpha_2)}{\cos(\alpha_1+\alpha_2)}.
\end{align*}
Da der Polarisationswinkel $\gamma$ bei $\alpha=0\degree$ genau $45\degree$ betrug, muss dieser Wert von der Winkeldifferenz $\delta$ noch abgezogen werden und es ergibt sich:
\begin{align}
    \gamma &= \arctan\left(-\frac{\cos(\alpha_1-\arcsin(\sin\frac{\alpha_1}{n}))}{\cos(\alpha_1+\arcsin(\sin\frac{\alpha_1}{n}))}\right)-45\degree.\label{eq:winkeldrehung}
\end{align}
Es gilt hier $\alpha_2=\arcsin(\sin\frac{\alpha_1}{n})$ mit dem Brechungsindex $n$.
