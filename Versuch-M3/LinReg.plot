reset
set xlabel '$\sqrt{n}$'
set ylabel 'Radius $\unit[R]{[px]}$'
set key top left
f(x)=a*x
g(x)=b*x
h(x)=c*x
fit f(x) 'Messwerte' index 1 u (sqrt($1)):($2/2.0):(4) via a
fit g(x) 'Messwerte' index 2 u (sqrt($1)):($2):(4) via b
fit h(x) 'Messwerte' index 3 u (sqrt($1)):($2):(4) via c
set terminal epslatex color colortext
set output 'LinReg.tex'
plot 'Messwerte' index 1 u (sqrt($1)):($2/2.0):(4) with errorbars title 'Messwerte', f(x) title 'Lineare Regression'
set output 'LinRegAu1.tex'
plot 'Messwerte' index 2 u (sqrt($1)):($2):(4) with errorbars title 'Messwerte', g(x) title 'Lineare Regression'
set output 'LinRegAu2.tex'
plot 'Messwerte' index 3 u (sqrt($1)):($2):(4) with errorbars title 'Messwerte', h(x) title 'Lineare Regression'
set output
