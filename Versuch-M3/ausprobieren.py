from math import *

print 'Input: 2x3 Miller Indices' #input of test indices
h1string=raw_input('h1: ')
k1string=raw_input('k1: ')
l1string=raw_input('l1: ')
h2string=raw_input('h2: ')
k2string=raw_input('k2: ')
l2string=raw_input('l2: ')

h1=int(h1string) #convert input strings to integers
k1=int(k1string)
l1=int(l1string)
h2=int(h2string)
k2=int(k2string)
l2=int(l2string)

n1=h1**2+k1**2+l1**2 #abbreviations
n2=h2**2+k2**2+l2**2

#output of the angle and the ratio of radii
print 'n_i=h_i^2+k_i^2+l_i^2'
print 'sqrt(n_1/n_2) = ', sqrt(n1/n2)
print 'cos(alpha) = ', (h1*h2+k1*k2+l1*l2)/sqrt(n1*n2)
print 'alpha = ', acos((h1*h2+k1*k2+l1*l2)/sqrt(n1*n2))*180/pi
