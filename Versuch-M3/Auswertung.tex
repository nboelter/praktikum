\emph{Anmerkung:} Alle ermittelten Fehler wurden per Gaußscher Fehlerfortpflanzung bestimmt, wenn nicht anders angegeben.
Diese berechnet sich bei einer Größe $y$, die von $n$ fehlerbehafteten Werten $x_i$ mit $i\in\{1,2,\ldots,n\}$ abhängt, wie folgt~\citep{Anleitung}:
\begin{align*}
  \sigma_y &= \sqrt{\sum_{i=1}^n\left(\partialfrac{y}{x_i}\sigma_{x_i}\right)^2}
\end{align*}
Dabei bezeichnet $\sigma_X$ den Fehler des Wertes $X$.
\subsection{Eichung der Kamera}\label{sec:alpha}
Da die Kameralänge des TEM bis zum realen Schirm angegeben ist, sich die Kamera zur Digitalisierung der Bilder aber an einer anderen Position befindet, muss zunächst der Umrechnungsfaktor $\alpha$ bestimmt werden, der den digitalen Abstand in Pixeln in einen realen Abstand in Millimetern umwandelt.
Dieser ergibt sich aus zwei Bildern, die bei einer relativen Verschiebung von $\unit[40]{mm}$ gegenüber einander aufgenommen wurden.
Bei drei markanten Strukturen des abgebildeten Bereichs, die auf beiden Bildern gut auszumachen sind, wird die jeweilige Position bestimmt.\footnote{Mit Hilfe des Programms \emph{ImageJ} lassen sich die Positionen in Pixeln messen.}
Durch Differenzbildung erhält man so einen Abstand in Pixeln.
Dieser ist für die verwendeten Punkte in Tabelle~\ref{tab:abstand} zusammengefasst.
Der Mittelwert beträgt $\unit[(607\pm2)]{px}$.
Dies entspricht dem Umrechnungsfaktor $\alpha=\unit[(6.6\pm0.4)\cdot10^{-2}]{mm/px}$.
\subsection{Wellenlänge der Elektronen, Bedeutung der relativistischen Korrektur}\label{sec:wellenlaenge}
Wie bereits erwähnt, kann einem Elektron, dass den Impuls $p$ trägt, eine Wellenlänge zugeordnet werden.
Diese berechnet sich nach \mensch{Louis de Broglie} mit Hilfe des Planck'schen Wirkungsquantums $h$ zu:
\begin{align}
  \lambda&=\frac{h}{p}\label{eq:debroglie}
\end{align}
Zur Berchnung des Impulses der Elektronen wird die mittlere kinetische Energie der Elektronen über die Beschleunigungsspannung $U_\mathrm{B}$ und die Elementarladung $e$ ausgedrückt:
\begin{align}
  E_\mathrm{kin}&=e U_\mathrm{B}\label{eq:beschl}
\end{align}
Verwendet man nun die klassische Formel für die kinetische Energie der Elektronen $E_\mathrm{kin}=p^2/2m_e$, so erhält man mit den Gleichungen~\ref{eq:debroglie} und~\ref{eq:beschl} eine Wellenlänge:
\begin{align*}
  \lambda_\mathrm{klassisch} = h\sqrt{2m_e e U_\mathrm{B}}^{\,-1}
\end{align*}
Während des Versuches wurden jedoch die Elektronen so stark beschleunigt, dass eine relativistische Korrektur notwendig ist.
Es gilt nach \mensch{Albert Einstein} für die Energie:
\begin{align}
  E = \sqrt{p^2c^2+m_e^2c^4}\label{eq:Einstein!}
\end{align}
Da es sich hierbei um die Gesamtenergie der Elektronen handelt, muss zum Ausdruck~\ref{eq:beschl} für die kinetische Energie der beschleunigten Elektronen nun noch die Ruheenergie $E_0=m_ec^2$ addiert werden~\citep{landaufieldtheory}.
Kombiniert man hiermit nun Gleichung~\ref{eq:Einstein!}, so ergibt sich die tatsächliche de-Broglie-Wellenlänge der Elektronen:
\begin{align}
  \lambda &= h\sqrt{2m_e e U_\mathrm{B}+\left(\frac{eU_\mathrm{B}}{c}\right)^2}^{\,-1}\\
  &= \sqrt{1+\frac{eU_\mathrm{B}}{2m_ec^2}}^{\,-1}
\end{align}
Bei der im Versuch verwendeten Beschleunigungsspannung $U_\mathrm{B}=\unit[120]{kV}$ ergibt dies die relativistische Korrektur:
\begin{align*}
  K_\mathrm{rel}(U_\mathrm{B}=120kV)&=\frac{\lambda_\mathrm{klassisch}-\lambda}{\lambda_\mathrm{klassisch}}\approx\unit[5.4]{\%}
\end{align*}
Für die vom Assistenten genannte Spannung $U=\unit[10]{kV}$ würde immernoch eine Abweichung von $K_\mathrm{rel}(U=10kV)\approx\unit[0.5]{\%}$ entstehen.

Ist die Kameralänge $L$ auf $\sigma_L=\unit[0.5]{\%}$ bekannt, so muss selbst bei der recht kleinen Spannung $U=\unit[10]{kV}$ die relativistische Betrachtung durchgeführt werden.
Die berechneten Wellenlängen sind:\footnote{Für diese Größen ist kein Fehler angegeben, da es nicht sonnvoll erscheint, einen willkürlichen Fehler in der Beschleunigungsspannung anzunehmen.}
\begin{align*}
  \lambda(\unit[120]{kV})&=\unit[3.35]{pm}\\
  \lambda(\unit[10]{kV})&=\unit[12.20]{pm}
\end{align*}
\subsection{Bestimmung der Beugungskonstanten mit Hilfe der TlCl-Eichprobe}\label{sec:beugung}
\begin{figure}[htb]
\begin{center}
\includegraphics[width=\textwidth]{Radien.png}
\end{center}
\caption{Abbildung der Beugungskreise und Nummerierung derselben. Zur Tonerschonung wurden die Farben invertiert.}
\label{fig:radien}
\end{figure}
Zunächst werden in den aufgenommenen Beugungsbildern der TlCl-Probe die Radien der Beugungskreise im Debye-Scherrer-Diagramm gemessen.\footnote{via \emph{ImageJ}: Ellipsenauswahlwerkzeug mit gehaltener Shift-Taste erzeugt Kreise; anschließend Messung des Feret-Durchmessers.}
Diese sind in Tabelle~\ref{tab:radien} zusammengefasst.
Die Nummerierung der gemessenen Kreise ist Abbildung~\ref{fig:radien} zu entnehmen.

Nun werden die gemessenen Radien den Millerschen Indizes zugeordnet.
Dies ist aus mehreren Gründen bis auf die Reihenfolge eindeutig.
Zunächst sind die Radien der Kreise proportional zu Netzebenenabständen des Gitters.
Nimmt man nun noch eine würfelförmige Gitterstruktur des TlCl an, so gilt mit der Gitterkonstanten $g$ für den Netzebenenabstand:
\begin{align*}
  d_{hkl} = \frac{g}{\sqrt{h^2+k^2+l^2}}
\end{align*}
Weiterhin lässt sich der Netzebenenabstand ausdrücken via $d_{hkl}R=\lambda L$ mit der Beugungskonstanten $\lambda L$ und den Radien $R$.
Nun sieht man leicht, dass die Quadrate der Radien proportional zu $n=h^2+k^2+l^2$ sind.
Folglich lässt sich mit dem kleinsten Radius $R_0$ schreiben:
\begin{align*}
  n &= \frac{R^2}{R_0^2}
\end{align*}
In Tabelle~\ref{tab:radien} sind die verschiedenen Werte für $n$ mit einer der möglichen Kombinationen $(hkl)$ aufgeführt.

Nun lässt sich die Beugungskonstante per linearer Regression bestimmen.
Hierfür wurden in Abbildung~\ref{fig:linreg} die Radien $R$ gegen den zugehörigen Wert $\sqrt{n}$ aufgetragen.
Die Steigung $m$ der Geraden gehorcht dabei wegen $R=\frac{\lambda L}{g}\sqrt{n}$ der Gleichung $\lambda L=mg$.
Aus der Regression\footnote{via \emph{gnuplot}, anzupassende Funktion: $f(x)=mx$, $\chi_\mathrm{red}^2\approx 0.015$} lässt sich die Steigung zu $m=\unit[(78.03\pm0.06)]{px}$ bestimmen.
Es folgt also für die Beugungskonstante:
\begin{align*}
  \lambda L &= \unit[(78.03\pm0.06)]{px}\cdot g
\end{align*}
Mit der Gitterkonstanten $g_\mathrm{TlCl}(T=\unit[300]{K})=\unit[(3.84247\pm0.00004)]{\mathring{A}}$~\citep{landolt} und dem in Abschnitt~\ref{sec:alpha} berechneten Wert für den Umrechnungsfaktor $\alpha=\unit[(6.6\pm0.4)\cdot10^{-2}]{mm/px}$ lässt sich nun die Beugungskonstante berechnen:
\begin{align*}
  \lambda L_\mathrm{K} &=\unit[(299.83\pm0.22)]{px\,\mathring{A}}\\
  \lambda L_\mathrm{S} &=\unit[(1.98\pm0.12)\cdot10^{-12}]{m^2}
\end{align*}
$L_\mathrm{K}$ steht hierbei für die Kameralänge in Bezug auf die Kamera, $L_\mathrm{S}$ bezieht sich auf den normalen Schirm des TEM.

Nun lässt sich noch die in Abschnitt~\ref{sec:wellenlaenge} berechnete Wellenläge der Elektronen $\lambda=\unit[3.35]{pm}$ zur Bestimmung der Kameralänge verwenden und es folgt:
\begin{align*}
  L_\mathrm{S}&=\unit[(0.59\pm0.04)]{m}
\end{align*}
\subsection{Bestimmung der Gitterkonstanten der Goldprobe}
\begin{figure}[htb]
\centering
\begin{subfigure}[b]{0.45\textwidth}
  \def\svgwidth{\textwidth}
  \input{Reflexe1.pdf_tex}
  \caption{Erstes Beugungsbild, $\vartheta=-43\degree$.}
  \label{fig:reflexes1}
\end{subfigure}
~
\begin{subfigure}[b]{0.45\textwidth}
  \def\svgwidth{\textwidth}
  \input{Reflexe2.pdf_tex}
  \caption{Zweites Beugungsbild, $\vartheta=-8\degree$.}
  \label{fig:reflexes2}
\end{subfigure}
\caption{Beugungsbilder mit Veranschaulichung der gewählten Reflexe. Zur Tonerschonung wurden die Farben invertiert.}
\label{fig:reflexes}
\end{figure}
Für zwei Beugungsbilder der Goldprobe werden die Radien und Winkel der Reflexe vermessen.
Hierfür werden zunächst zwei Reflexe ausgewählt.
Der Abstand der Reflexe vom Nullpunkt wird gemessen, sowie der Winkel zwischen den Verbindungslinien zwischen Nullstrahl und erstem Reflex und Nullstrahl und zweitem Reflex.
Dies ist in Abbildung~\ref{fig:reflexes} veranschaulicht.
Die Beugungsbilder mit vollständiger Indizierung nach den im folgenden beschriebenen Methoden findet sich in Abbildung~\ref{fig:ind}.

Mit Hilfe der folgenden Formeln kann nun eine möglichst passende Kombination der Millerschen Indizes $(hkl)$ gewählt werden.\footnote{\q{Möglichst passend} bedeutet in diesem Zusammenhang, dass die Formeln~\ref{eq:mil1} und~\ref{eq:mil2} bei den gewählten Millerschen Indizes möglichst exakt mit den ermittelten Werten für Winkel und Radien übereinstimmen.}
Sei $n_i=h_i^2+k_i^2+l_i^2$, dann lässt sich schreiben:
\begin{align}
  R_1/R_2 &= \sqrt{n_1}/\sqrt{n_2}\label{eq:mil1}\\
  \cos{\alpha} &= \frac{h_1h_2+k_1k_2+l_1l_2}{\sqrt{n_1n_2}}\label{eq:mil2}
\end{align}
\subsubsection*{Erstes Beugungsbild}
In Tabelle~\ref{tab:winkelundradien1} sind die Messergebnisse für das erste Beugungsbild (siehe Abb.~\ref{fig:reflexes1}) zusammengefasst.
Dieses wurde bei einer Winkeleinstellung am Probenhalter von $\vartheta=-43\degree$ aufgenommen.
Mit dem in Anhang~\ref{sec:py} befindlichen Python-Skript werden durch Ausprobieren die in Tabelle~\ref{tab:kombi1} eingetragenen Kombinationen ermittelt.
Als Konsistenztest wird der Radius $R_3$ eines dritten Reflexes gemessen, für den $R_3=R_1/\cos\beta=R_2\sin\beta$ gelten muss, wobei $\beta$ der Winkel zwischen den Verbindungslinien von Nullstrahl und erstem Reflex und von Nullstrahl und drittem Reflex ist.
Mit $\alpha\approx\pi/2$ muss daher auch $R_3^2=R_1^2+R_2^2$ erfüllt sein.

Mit den Werten aus Tabelle~\ref{tab:kombi1} erhält man $\sqrt{R_1^2+R_2^2}=\unit[(207.9\pm0.5)]{px}$ im Vergleich zu $R_3=\unit[207.11\pm0.5]{px}$.
Hier liegen beide Werte im Fehlerintervall des jeweils anderen Wertes, die Indizierung wird somit als korrekt angenommen.
Abbildung~\ref{fig:ind1} zeigt das vollständig indizierte erste Beugungsbild.
\begin{table}[htb]
\centering
\begin{subtable}[b]{0.45\textwidth}
  \begin{tabular}{|c|c|c|}
  \hline
  Reflex $i$&$\unit[<R_i>]{[px]}$&$(h_ik_il_i)$\\
  \hline
  1&$146.80\pm0.5$&$(100)$\\
  2&$147.20\pm0.5$&$(010)$\\
  3&$207.11\pm0.5$&$(110)$\\
  \hline
  \end{tabular}
  \caption{Mittelwerte von $R_\mathrm{1}$, $R_\mathrm{2}$, $R_\mathrm{3}$.}
  \label{tab:kombi1}
\end{subtable}
~
\begin{subtable}[b]{0.45\textwidth}
  \begin{tabular}{|c|c|c|}
  \hline
  Reflex $i$&$\unit[<R_i>]{[px]}$&$(h_ik_il_i)$\\
  \hline
  a&$129.13\pm0.5$&$(120)$\\
  b&$147.54\pm0.5$&$(112)$\\
  c&$243.91\pm0.5$&$(232)$\\
  \hline
  \end{tabular}
  \caption{Mittelwerte von $R_\mathrm{a}$, $R_\mathrm{b}$, $R_\mathrm{c}$.}
  \label{tab:kombi2}
\end{subtable}
\caption{Kombinationen der Millerschen Indizes für beide Beugungsbilder.}
\label{tab:kombi}
\end{table}
\subsubsection*{Zweites Beugungsbild}
Tabelle~\ref{tab:winkelundradien2} zeigt die Messergebnisse für das zweite Beugungsbild (siehe Abb.~\ref{fig:reflexes2}).
Es wurde bei einer Winkeleinstellung am Probenhalter von $\vartheta=-8\degree$ aufgenommen.
Wiederum wurde das bereits erwähnte Skript aus Anhang~\ref{sec:py} verwendet, um durch Probieren die richtigen Kombinationen der Millerschen Indizes zu erhalten.
Die ermittelten Werte sind in Tabelle~\ref{tab:kombi2} eingetragen.
Wieder wird der Radius $R_\mathrm{c}$ eines dritten Reflexes zur Konsistenzprüfung gemessen.
Hier sollte nun der gemessene Winkel zwischen den Verbingungslinien von Nullstrahl und erstem Reflex und von Nullstrahl und zweitem Reflex mit dem Radius übereinstimmen, den man aus den erwarteten Millerschen Indizes $(hkl)$ und Gleichung~\ref{eq:mil2} erhält.
Mit den Werten aus den Tabellen~\ref{tab:kombi2} und~\ref{tab:winkelundradien2} erhält man nun den Cosinus des gemessenen Winkels:
\begin{align*}
  \cos\varphi=0.8789\pm0.0071
\end{align*}
Der theoretisch erwartete Wert nach Gleichung~\ref{eq:mil2} ist $\cos\alpha_\mathrm{theo}=0.8677$.
Abbildung~\ref{fig:ind2} zeigt das vollständig indizierte zweite Beugungsbild.
\subsection{Messung der Gitterkonstanten}
Ist $n$ wie oben definiert, so lässt sich nun mit $d_{hkl}R=\lambda L$ und $d_{hkl}=g/n$ durch kurzes Umformen die Gitterkonstante berechnen.
Es gilt $g=n\lambda L/R$.
Für beide Beugungsbilder wird so für jeden Reflex die Gitterkonstante bestimmt.
In Tabelle~\ref{tab:gitter1} und~\ref{tab:gitter2} sind die Ergebnisse zusammengefasst.
Die Mittelwerte sind:
\begin{align*}
  g_1 &= \unit[(2.03\pm0.03)]{\mathring{A}}\\
  g_2 &= \unit[(5.17\pm0.08)]{\mathring{A}}
\end{align*}
Es bezeichnet hierbei $g_i$ den Mittelwert der ermittelten Gitterkonstanen beim $i$-ten Beugungsbild.
Das gewichtete Mittel der beiden Werte ist $\left<g\right>=\unit[(2.42\pm0.03)]{\mathring{A}}$.

Lineare Regressionen\footnote{via \emph{gnuplot}, $\chi_\mathrm{red,1}^2\approx1.77$, $\chi_\mathrm{red,2}^2\approx2.84$} ergeben wie in Abschnitt~\ref{sec:beugung} Konstanten $m_i$, $i\in\{1;2\}$, für die $\lambda L=m_ig$ gilt.
Die zugehörigen Graphen sind in Abbildung~\ref{fig:linregau} zu finden.
Hieraus lassen sich nun auch zwei Werte für die Gitterkonstante bestimmen:
\begin{align*}
  g_1^\mathrm{reg} &= \unit[(2.03\pm0.13)]{\mathring{A}}\\
  g_2^\mathrm{reg} &= \unit[(5.14\pm0.32)]{\mathring{A}}
\end{align*}
Als Fehler der Steigungen der Regressionsgeraden wurden die von \emph{gnuplot} angegebenen Standardfehler gewählt.
Das gewichtete Mittel ist hier $\left<g^\mathrm{reg}\right>=\unit[(2.45\pm0.12)]{\mathring{A}}$.

Der Literaturwert liegt bei $g_\mathrm{lit}=\unit[4.04]{\mathring{A}}$~\citep{Kiel}.
\subsection{Biegekontraste}
\begin{figure}[htb]
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{Tv15_Au_hellf_v5600.png}
    \caption{}
    \label{fig:biege1}
  \end{subfigure}
  ~
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[width=\textwidth]{Tv16_Au_hellf_v5600.png}
    \caption{}
    \label{fig:biege2}
  \end{subfigure}
  \caption{Hellfeldabbildungen der Goldprobe, bei verschiedenen Winkelpositionen der Probe aufgenommen.}
  \label{fig:biege}
\end{figure}
Abbildung~\ref{fig:biege} zeigt zwei Hellfeldbilder der Goldprobe, wobei die Probe auf Abbildung~\ref{fig:biege2} gegenüber Abbildung~\ref{fig:biege1} verdreht ist.
Zunächst fällt auf, dass beide Bilder wellenartige Strukturen aufweisen, die sich wie eine gekräuselte Wasseroberfläche über die gesamte Probe ziehen.
Dies ist wahrscheinlich ein Resultat der herstellungsbedingten Rauhigkeit der Goldoberfläche.
Weiterhin ist auf beiden Bildern eine Struktur zu sehen, die zwei Fingern ähnelt.
Auf Abbildung~\ref{fig:biege1} ist der linke Finger kaum zu sehen, Abbildung~\ref{fig:biege2} hingegen zeigt beide Finger mit starkem Kontrast.
Auch sind die Finger gegenüber der jeweils anderen Abbildung verschoben.
Beides weist darauf hin, dass es sich bei dieser Erscheinung um Biegekontraste handeln könnte.
Durch die Krümmung der Probe ist in einigen Bereichen die Bragg-Bedingung erfüllt und in anderen nicht.
Es wechseln sich dunkle und helle Schlieren in der Struktur ab, da die Bragg-Bedingung bei periodisch vorkommenden Winkeln erfüllt wird~\citep{TEMeinfuehrung}.
\subsection{Bestimmung der Korngröße}
Abbildung~\ref{fig:korn} zeigt die TlCl-Probe in einer Hellfeldabbildung mit $110000$-facher Vergrößerung.
Die einzelnen Kristallite werden durchnummeriert und ihr Durchmesser wird gemessen,\footnote{Hierfür wurde das Programm \emph{ImageJ} verwendet.} die Ergebnisse sind in Tabelle~\ref{tab:korn} zusammengefasst.
Der Mittelwert in Pixeln ist $\bar d_\mathrm{px}=\unit[(46.10\pm0.75)]{px}$.
Die Ungeneauigkeit einer einzelnen Messung wurde auf $\sigma_d=\unit[4]{px}$ geschätzt.
Aus dem Mittelwert wird durch Multiplikation mit dem in Abschnitt~\ref{sec:alpha} bestimmten Umrechnungsfaktor $\alpha$ und dem Inversen der Vergrößerung $V=110000$ die mittlere Korngröße $d$ in Metern bestimmt:
\begin{align*}
  \bar d&= \frac{\alpha\bar d_\mathrm{px}}{V} = \unit[(27.7\pm1.8)]{nm}
\end{align*}
Abbildung~\ref{fig:dunkel} zeigt eine Dunkelfeldabbildung der Probe.
Im Gegensatz zur Hellfeldabbildung werden die Kristallite hier heller als der Hintergrund abgebildet.
Dies resultiert aus der Verschiebung der Kontrastblende, die dazu führt, dass statt der nicht gebeugten Elektronen des Nullstrahls nur gebeugte Elektronen abgebildet werden.
So erscheinen die Bereiche, durch die Elektronen unabgelenkt hindurch gelangen hier dunkel.
Bereiche, in denen Elektronen gebeugt werden erscheinen hingegen hell.
