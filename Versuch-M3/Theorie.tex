\subsection*{a) Unterschied zwischen Hell- und Dunkelfeld}
Im Unterschied zum optischen Mikroskop wird beim TEM hauptsächlich durch Beugung von Elektronen an Gitteratomen ein Kontrast erzeugt, nicht durch Absorption.
Diese Beugung tritt in verschiedenen Ordnungen auf, sodass man in einem Beugungsbild eines kleinen Probenbereichs Ringstrukturen erkennen kann (siehe Abb.~\ref{fig:radien}).

Durch Auswählen einer der höheren Beugungsordnungen kann man das Bildrauschen durch nicht (oder nur schwach) gebeugte Elektronen, die direkt auf dem Schirm abgebildet werden verringern.
Die Auswahl geschieht mit einer Kontrastblende, die im Strahlengang so platziert wird, dass im Beugungsbild die nullte Ordnung ausgeblendet wird.
Da eine solche Abblendung das Bild sehr viel dunkler erscheinen lässt, spricht man hier von einer Dunkelfeldabbildung.
Hierbei werden stark streuende Bereich der Probe heller abgebildet.

Im Gegensatz dazu wird eine Hellfeldabbildung erreicht, wenn nur das nullte Beugungsmaximum die Blende passieren kann.
Nun werden wenig streuende Bereiche der Probe heller abgebildet.

Verschiebt man bei der Dunkelfeldabbildung die Kontrastblende, tragen nur Elektronen zur Abbildung bei, die weit von der optischen Achse entfernt den Strahlengang durchlaufen.
Hier sind die Abweichungen der magnetischen Linsen größer und es kommt zu Abbildungsfehlern.
Eine einfache Alternative wäre das Verkippen der Probe im Strahlengang, wodurch auch nur Beugungsmaxima höherer Ordnung abgebildet werden, nicht aber der Abstand zur optischen Achse vergößert wird~\citep{TEMeinfuehrung}.
\subsection*{b) Indizierung der Reflexe}
Das Beugungsbild zeigt einen ebenen Schnitt durch das reziproke Gitter~\citep{TEMeinfuehrung}.
Dieses weist die Symmetrien des Kristallgitters auf, wodurch bei Kenntnis der Kristallstruktur eine Zuordnung des Schnitts zu einer Ebene im Kristallgitter eindeutig\footnote{bis auf Gittersymmetrien} ist.
Auch die Auslöschungsregeln für gewisse Kristallstrukturen lassen eine Vorhersage für fehlende Reflexe zu, was eine Zuordnung erleichtert.
\subsection*{c) Körnige Struktur der Ringe um Debye-Scherrer-Diagramm}
Ein Reflex im Debye-Scherrer-Diagramm korrespondiert mit einer gewissen Ausrichtung eines Kristallits.
Bildet man gleiche Kristallite mit statistisch unabhängig ausgerichteten Kristallachsen ab, so erhält man als Beugungsbild eine Überlagerung der einzelnen Reflexe, wobei jeder Reflex um einen gewissen (statistisch verteilten) Winkel um den Nullstrahl gedreht ist.
Durch Abbidung einer großen Menge verschieden orientierter Kristallite lässt sich folglich das Bild eines Kreises erzeugen.
Dieses wird bei Verringerung der Anzahl abgebildeter Kristallite körniger.
\subsection*{d) Feinstrahl- und Feinbereichsbeugung}
Bei der Untersuchung des Beugungsbilds einer Probe ist zum einen eine hohe Auflösung der Reflexe und zum anderen eine genaue Lokalisierung des beobachteten Bereichs auf der Probe von Interesse.
Die folgenden Methoden sind für jeweils eines der beiden genannten Ziele besonders hilfreich.

Bei der Feinbereichsbeugung wird eine möglichst ebene Wellenfront (also möglichst parallel einfallende Elektronen) verwendet, um eine hochauflösende Abbildung der Beugungsreflexe zu gewährleisten~\citep{TEMeinfuehrung}.
Dabei wird der beobachtete Probenbereich eingeschränkt, indem eine Blende vor der Probe nur einen kleinen Teil des Strahls durchlässt.
Nachteilig ist hierbei die starke Verringerung der zur Verfügung stehenden Intensität durch Abblenden eines großen Teils des Elektronenstrahls.

Die Feinstrahlbeugung bezeichnet das Beleuchten eines sehr kleinen Probenbereichs durch einen stark fokussierten Elektronenstrahl.
Dies führt jedoch aufgrund der Verwendung eines konvergenten Strahls zur Aufweitung der abgebildeten Reflexe zu Scheibchen, deren Position nicht mehr allzu genau gemessen werden kann~\citep{TEMeinfuehrung}.
Durch Verwendung eines Strahls mit kleinem Konvergenzwinkel können näherungsweise die Mittelpunkte der Scheibchen als Position der Beugungsreflexe gemessen werden.
\subsection*{e) Näherung des ebenen Gitterschnitts}
Die Laue-Bedingung für konstruktive Interferenz sagt aus, dass genau dann ein Beugungsmaximum auftritt, wenn der Differenzvektor zwischen gebeugtem und einfallenden Wellenvektor gerade ein reziproker Gittervektor ist~\citep{ashcroft}.
Nimmt man an, dass die Elektronen bei der Beugung keine Energie verlieren, so haben die Wellenvektoren vor und nach der Beugung den gleichen Betrag $k$.
Man kann sich also die Laue-Bedingung als geometrische Vorschrift vorstellen.
Eine Kugelschale mit Mittelpunkt im Abstand $k$ zu einem reziproken Gitterpunkt sei gegeben. Liegen nun weitere reziproke Gitterpunkte auf dieser Schale, so werden diese abgebildet, wenn sie in der Nähe des vom einfallenden Wellenvektor getroffenen Gitterpunktes liegen.

Da die verwendeten Elektronen eine sehr scharfe Energieverteilung haben und nur die Beugung in einen kleinen Raumwinkel abgebildet wird~citep{TEMeinfuehrung}, kann die Oberfläche der Kugelschale als Ebene genähert werden.
Somit kann das Bild als ebener Schnitt durch das reziproke Gitter interpretiert werden.
\subsection*{f) Kippung der Probe ohne Änderung des Beugungsbildes}
Zunächst ist die Krümmung der Probe zu nennen, die dafür sorgt, dass der Kippwinkel nicht dem Einfallwinkel des Elektronenstrahls entspricht.
Letzterer ist kleiner als ersterer.

Weiterhin ist die für Beugung nötige~\citep{ashcroft} Bragg-Beziehung $n\lambda=2d\sin\vartheta$ durch gewisse Streuungen nicht strikt einzuhalten.
Die Wellenlänge $\lambda$ der Elektronen ist gaußverteilt und daher für eine Toleranz im Winkel verantwortlich.
Bei der Feinstrahlbeugung wird außerdem ein größerer Winkelbereich abgedeckt, was dafür sorgt, dass selbst bei Annahme einer $\delta$-verteilten Wellenlänge auch bei Verkippen der Probe die obige Beziehung noch von einem Teil des Strahlenbündels erfüllt wird.
