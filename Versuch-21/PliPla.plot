reset
set key left
Spalt(x) = m1*x+b1
Steg(x) = m2*x+b2
Lochblende(x) = m3*x+b3
Gitter(x) = m4*x+b4
Doppelloch(x) = m5*x+b5

b1 =0
b1_err=0
b2 =0
b2_err=0
b3 =0
b3_err=0
b4 =0
b4_err=0
b5 =0
b5_err=0

set fit errorvariables
fit Spalt(x) './processed.txt' index 0 u 1:2 via m1
fit Steg(x) './processed.txt' index 1 u 1:2 via m2
fit Lochblende(x) './processed.txt' index 2 u 1:2 via m3
fit Gitter(x) './processed.txt' index 3 u 1:2 via m4
fit Doppelloch(x) './processed.txt' index 4 u 1:2 via m5
set print 'Regression.txt'
print '# Objekt Steigung sigmaSteigung Y-Achsenabschnitt sigmaY-Achsenabschnitt'
print 'Spalt ',m1,m1_err,b1,b1_err
print 'Steg ',m2,m2_err,b2,b2_err
print 'Lochblende ',m3,m3_err,b3,b3_err
print 'Gitter ',m4,m4_err,b4,b4_err
print 'Doppelloch ',m5,m5_err,b5,b5_err
set print

set xlabel 'Ordnung $\frac{\epsilon_i}{\pi}$'
set ylabel '$\sin(\alpha)$'
set terminal epslatex color colortext
set output 'OrdnungSpalt.tex'
plot './processed.txt' index 0 u 1:2 title 'Messwerte', Spalt(x) lt 1 lc 3 title 'lineare Regression'
set output 'OrdnungSteg.tex'
plot './processed.txt' index 1 u 1:2 title 'Messwerte', Steg(x) lt 1 lc 3 title 'lineare Regression'
set output 'OrdnungLochblende.tex'
plot './processed.txt' index 2 u 1:2 title 'Messwerte', Lochblende(x) lt 1 lc 3 title 'lineare Regression'
set output 'OrdnungGitter.tex'
plot './processed.txt' index 3 u 1:2 title 'Messwerte', Gitter(x) lt 1 lc 3 title 'lineare Regression'
set output 'OrdnungDoppelloch.tex'
plot './processed.txt' index 4 u 1:2 title 'Messwerte', Doppelloch(x) lt 1 lc 3 title 'lineare Regression'
set output
