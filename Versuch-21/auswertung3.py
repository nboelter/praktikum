from math import *

def signum(x):
  if x < 0:
    return -1
  if x > 0:
    return 1
  return 0

data = []
maxima = []
pmax = open('./processedmaxima.txt', 'r')
pmin = open('./processedminima.txt', 'r')
out = open('./processed.txt','w')

context = ''
contexts =  []
stuff = {}
out.write('# Ordnung, sin(alpha)\n')
for line in pmax:
  if len(line) < 2:
    continue
  if line[0] == '#':
    context = line.strip()
    if not context in stuff:
      contexts.append(context)
      stuff[context] = '\n\n' + context + '\n'
    continue
  token = map(float, line.strip().split(' ', 4))
  stuff[context] = stuff[context] + (str(token[0]) + ' ' + str(token[3]) + '\n')
for line in pmin:
  if len(line) < 2:
    continue
  if line[0] == '#':
    context = line.strip()
    if not context in stuff:
      contexts.append(context)
      stuff[context] = '\n\n' + context + '\n'
    continue
  token = map(float, line.strip().split(' ', 4))
  stuff[context] = stuff[context] + (str(token[0]-0.5*signum(token[0]))) + ' ' + str(token[3]) + '\n'
for context in contexts:
  out.write(stuff[context])
