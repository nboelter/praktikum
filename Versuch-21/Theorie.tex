Aus Platzgründen wird zur grundlegenden Funktionsweise eines Lasers auf die Fachliteratur (zum Beispiel~\cite{Gerthsen}) verwiesen.
Die für diesen Versuch relevanten Beugungs- und Interferenzphänomene sollen im Folgenden kurz begutachtet werden.
\subsection{Beugung und Interferenz von Laserlicht}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Fraunhofer.pdf_tex}
\end{center}
\caption{Bezeichnungen zur Beschreibung der Fraunhofer-Näherung}
\label{fig:fraunhofer}
\end{figure}
Zunächst betrachten wir eine Welle der Form $\vec{E}=\vec{E}_0 \exp{[i(\vec{k}\cdot\vec{x}-\omega t)]}$.
Es bezeichnet $\vec{E}_0$ die komplexe Amplitude mit etwaigen Phasenverschiebungen, $\vec{k}$ den Wellenvektor und $\omega$ die Kreisfrequenz des einfallenden Lichtes.

Fällt diese Welle auf ein Objekt, so kann man die resultierende Welle als Überlagerung aller vom Objekt ausgehenden \mensch{Huygens}schen
Elementarwellen auffassen. Integriert man die so entstandenen Wellen über die gesamte Objektfläche auf, so erhält man das entstandene
Beugungsbild zum Beispiel mit Hilfe der \mensch{Fraunhofer}-Näherung. Diese geht davon aus, dass der Abstand zwischen Beugungsobjekt und
Schirm so groß ist, dass die auf dem Schirm auftreffenden Strahlen als parallel zur optischen Achse angenommen werden können.
Die \mensch{Fraunhofer}-Näherung liefert bei gegebener Eingangsfeldstärke das zu erwartende Beugungsbild als Fouriertransformierte des eingehenden elektrischen Feldes~\citep{Gerthsen}:
\begin{align}
    E(\vec{r}) &= \int_\mathrm{Apertur} e^{i\vec{K}\cdot\vec{r}}\,E_\mathrm{in}\,\D x' \D y',\label{eq:fraunhofer}
\end{align}
wobei $\vec{K}=\frac{k\vec{p}}{L}$ und $\vec{r}=(x',y')$ gilt. Die anderen Bezeichnungen finden sich in Abbildung~\ref{fig:fraunhofer}.
\subsection{Beugungsmuster der verschiedenen Objekte}
\subsubsection{Doppelloch}
Das Doppelloch besteht aus zwei näherungsweise punktförmigen Löchern im Abstand $D$.
Hier haben die beiden Elementarwellen, die hinter dem Objekt propagieren, eine Phasendifferenz $\delta$.
Diese ergibt sich durch eine Kleinwinkelnäherung aus dem Beobachtungswinkel $\alpha$, der Spaltbreite $D$ und der Wellenlänge $\lambda$ des Lichtes~\citep{Gerthsen}:
\begin{align*}
    \delta &= \frac{2\pi D\alpha}{\lambda}.
\end{align*}
Die Superposition der beiden Wellen ergibt nun:
\begin{align*}
    \vec{E} &= \vec{E}_0 (1+e^{i\delta}) = \vec{E}_0 (e^{-i\delta/2}+e^{i\delta/2})e^{i\delta/2} = 2\vec{E}_0e^{i\delta/2}\cos\frac{\delta}{2}.
\end{align*}
Hieraus ergibt sich auch die Intensität:
\begin{align}
    I &= 4E_0\cos^2\frac{\pi D\alpha}{\lambda}.
\end{align}
\subsubsection{Spalt}
Aus der Fouriertransformation der einfallenden Welle ergibt sich nach Kleinwinkelnäherung~\citep{Gerthsen}:
\begin{align*}
    E_0 \int_{-D/2}^{D/2} e^{ikx}\D x = D\,E_0\,\mathrm{sinc}\left(\frac{\pi D\alpha}{\lambda}\right)
\end{align*}
und damit die Intensität:
\begin{align}
    I &= I_0\,\mathrm{sinc}^2\left(\frac{\pi D\alpha}{\lambda}\right).
\end{align}
\subsubsection{Steg}
Das \mensch{Babinet}sche Theorem besagt, dass komplementäre Blendenstrukturen bis auf geometrisch-optische Abbildung ein gleiches Beugungsmuster hervorrufen. Somit gilt also für den Steg die gleiche Intensitätsformel wie für den Spalt.
Da dies nur für unendlich ausgedehnte Lichtquellen von parallelem kohärentem Licht gilt, kann im Versuch natürlich nur näherungsweise dieselbe Beugungsstruktur beobachtet werden.
\subsubsection{Kreisblende}
Für die Beugung an einer Kreisblende mit Durchmesser $D=2R$ und Mittelpunkt $M$ werden zunächst die folgenden Koordinaten eingeführt:
$P$ sei ein Punkt auf der Kreisblende mit den Polarkoordinaten $(\rho,\phi)$.
Q sei ein Punkt auf dem Schirm mit den Koordinaten $(x,y)$.
Der Schirm und die Blende seien im Abstand $r_0$ aufgestellt.
Außerdem seien $r = \overline{PQ}$ und $a = \overline{MQ}$.
Dann können wir schreiben:
\begin{align*}
    a &= \sqrt{r_0^2 + x^2}\\
    r &= \sqrt{r_0^2 + (x + \rho \cos\phi)^2+(\rho\sin\phi)^2}\\
    &= \sqrt{a^2+\rho^2+2x\rho\cos\phi}\\
    &\approx a+\frac{x\rho\cos\phi}{a}.
\end{align*}
Da $R<<r_0$ gilt, also auch $\rho<<a$ kann der letzte Schritt per Taylornäherung vollzogen werden.

Für den Gangunterschied aller Strahlen von dem Mittelpunktsstrahl gilt dann also:
\begin{align*}
    g(\rho,\phi) &= r-a = \frac{x\rho\cos\phi}{a} = \rho\sin\alpha\,\cos\phi
\end{align*}
Eingesetzt in Formel~\ref{eq:fraunhofer} ergibt sich nun:
\begin{align*}
    E &= \frac{E_0}{\pi R^2}\int_0^R\int_0^{2\pi}\exp(\frac{2\pi i}{\lambda} \rho\sin\alpha\,\cos\phi)\rho\,\D\rho\,\D\phi.
\end{align*}
Hieraus ergibt sich nun wieder die Intensität mit der Besselfunktion $J_1$~\citep{Gerthsen}:
\begin{align}
    I &= I_0 \left(\frac{J_1(\frac{2\pi}{\lambda}R\sin\alpha)}{\frac{2\pi}{\lambda}R\sin\alpha}\right)^2.
\end{align}
\subsubsection{Gitter}
Wir betrachten ein Gitter mit $N$ Spalten der Breite $b$ und mit Spaltabstand $d$.
Die Intensitätsformel ergibt sich zu~\citep{Gerthsen}:
\begin{align}
    I = I_0\,\mathrm{sinc}^2\left(\frac{kb}{2}\sin{\alpha}\right)\left(\frac{\sin(\frac{Nkd}{2}\sin\alpha)}{N\sin(\frac{kd}{2}\sin\alpha)}\right)^2.
\end{align}
Auf die recht lange Herleitung wird hier verzichtet, in der Fachliteratur (zum Beispiel~\cite{Demtroeder}(?)) kann zum besseren Verständnis eine ausführliche Diskussion gefunden werden.
