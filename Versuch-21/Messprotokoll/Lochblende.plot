reset
set xlabel '$x [\unit{mm}]$'
set ylabel '$U [\unit{V}]$'
set grid
set terminal epslatex color colortext size 15cm,7.7cm
set output 'Lochblende.tex'
plot 'Kreis_Haupt.dat' using ($1/400.0):($2/3.0) title 'Messwerte'
set output 'Lochblende2.tex'
plot x**2
set output
