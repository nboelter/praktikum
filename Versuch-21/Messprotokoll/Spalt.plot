reset
set xlabel '$x [\unit{mm}]$'
set ylabel '$U [\unit{V}]$'
set grid
set terminal epslatex color colortext size 15cm,7.7cm
set output 'Spalt.tex'
plot 'Spalt_Haupt_Detail.dat' using ($1/400.0):($2) title 'Messwerte'
set output 'Spalt2.tex'
plot 'Spalt_Neben2.dat' using ($1/400.0):($2/3.0) title 'Messwerte'
set output
