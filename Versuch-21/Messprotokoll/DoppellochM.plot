reset
set xlabel '$x [\unit{mm}]$'
set ylabel '$U [\unit{V}]$'
set grid
set terminal epslatex color colortext size 15cm,7.7cm
set output 'DoppellochM.tex'
plot x**2
set output 'DoppellochM2.tex'
plot x**2
set output
