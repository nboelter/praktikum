reset
set xlabel '$x [\unit{mm}]$'
set ylabel '$U [\unit{V}]$'
set grid
set terminal epslatex color colortext size 15cm,7.7cm
set output 'Doppelloch.tex'
plot 'Doppelloch_Haupt.dat' using ($1/400.0):($2) title 'Messwerte'
set output 'Doppelloch2.tex'
plot 'Doppelloch_Haupt_Detail.dat' using ($1/400.0):($2*10) title 'Messwerte'
set output
