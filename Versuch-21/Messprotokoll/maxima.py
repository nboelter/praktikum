#!/usr/bin/python
# vim: set fileencoding=utf-8 :
from math import log,fabs,pi,floor,ceil
import sys
def find_extrema(measurement):
  extrema    = []
  zero = 0
  phase = 0
  phasecount = 0
  datapoints = [(0,0,0),(0,0,0),(0,0,0),(0,0,0)]
  f = open(measurement)
  for line in f:
    line.strip()
    position,amplitude = map(float, line.split('\t'))
    datapoints.append((position, amplitude))
    l = len(datapoints)
    # Simple approach to find extrema, works like a charm
    if datapoints[l-5][1] < datapoints[l-3][1] and datapoints[l-4][1] < datapoints[l-3][1] and datapoints[l-2][1] < datapoints[l-3][1] and datapoints[l-1][1] < datapoints[l-3][1]:
      extrema.append(datapoints[l-3])
  return extrema
def main():
  print '#' + sys.argv[1]
  for ex in find_extrema(sys.argv[1]):
    print str(ex[0]/400.0) + ' ' + str(ex[1])
  print

if __name__ == '__main__': 
  main() 
