reset
set xlabel '$x [\unit{mm}]$'
set ylabel '$U [\unit{V}]$'
set grid
set terminal epslatex color colortext size 15cm,7.7cm
set output 'Gitter.tex'
plot 'Gitter_Haupt.dat' using ($1/400.0):($2*10) title 'Messwerte'
set output 'Gitter2.tex'
plot 'Spalt_Haupt_Detail.dat' using ($1/400.0):($2*10) title 'Messwerte'
set output 'Gitter3.tex'
plot 'Spalt_Neben.dat' using ($1/400.0):2 title 'Messwerte'
set output
