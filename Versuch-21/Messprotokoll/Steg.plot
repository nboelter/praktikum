reset
set xlabel '$x [\unit{mm}]$'
set ylabel '$U [\unit{V}]$'
set grid
set terminal epslatex color colortext size 15cm,7.7cm
set output 'Steg.tex'
plot 'Steg_Haupt_Detail.dat' using ($1/400.0):2 title 'Messwerte'
set output 'Steg2.tex'
plot 'Steg_Neben.dat' using ($1/400.0):($2/3.0) title 'Messwerte'
set output
