i = 0
j = 3
captions = ['m Spalt','m Steg',' der Kreisblende','m Gitter',' der Doppelkreisblende']
labels = ['Spalt','Steg','Kreisblende','Gitter','Doppelkreisblende']
g = open('Tabellen.tex', 'w')
f = open('processedmaxima.txt', 'r')
fa = f.readlines()
while i < 5:
  yn = 0
  g.write('\\begin{table}[ht]\n')
  g.write('\\centering\n')
  g.write('\\begin{tabular}{|c|c|c|c|}\n')
  g.write('\hline\n')
  g.write('$\\nicefrac{\\epsilon}{\\pi}$&absolute Position [mm]&relative Position [mm]&$\sin\\alpha$\\\\\n')
  g.write('\hline\n')
  
  data = []
  while yn == 0:
    if fa[j][0] == '#':
      j = j+1
      continue
    if len(fa[j]) < 2:
      yn = 1
      j = j+2
      continue
    data.append(map(float, fa[j].strip().split(' ', 4)))
    j = j+1
  
  for (Ord, absPos, relPos, sinalph, Ampl) in data:
    g.write(str(round(Ord,4))+'&'+str(absPos)+'&'+str(relPos)+'&'+str(round(sinalph,5))+'\\\\\n')
    g.write('\hline\n')
  
  g.write('\end{tabular}\n')
  g.write('\caption{Positionen der Maxima bei'+captions[i]+'}\n')
  g.write('\label{tab:'+labels[i]+'}\n')
  g.write('\end{table}\n')
  i = i+1

i = 0
j = 3
f = open('processedminima.txt', 'r')
fa = f.readlines()
while i < 5:
  yn = 0
  g.write('\\begin{table}[ht]\n')
  g.write('\\centering\n')
  g.write('\\begin{tabular}{|c|c|c|c|}\n')
  g.write('\hline\n')
  g.write('$\\nicefrac{\\epsilon}{\\pi}$&absolute Position [mm]&relative Position [mm]&$\sin\\alpha$\\\\\n')
  g.write('\hline\n')
  
  data = []
  while yn == 0:
    if fa[j][0] == '#':
      j = j+1
      continue
    if len(fa[j]) < 2:
      yn = 1
      j = j+2
      continue
    data.append(map(float, fa[j].strip().split(' ', 4)))
    j = j+1
  
  for (Ord, absPos, relPos, sinalph, Ampl) in data:
    g.write(str(Ord)+'&'+str(absPos)+'&'+str(relPos)+'&'+str(round(sinalph,5))+'\\\\\n')
    g.write('\hline\n')
  
  g.write('\end{tabular}\n')
  g.write('\caption{Positionen der Minima bei'+captions[i]+'}\n')
  g.write('\label{tab:'+labels[i]+'}\n')
  g.write('\end{table}\n')
  i = i+1
