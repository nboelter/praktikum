from math import *
print 'Werte wurden manuell geändert.!!'
exit
def Winkel(x):
  l = 1162
  return sin(atan(x/l))

data = []
maxima = []
f = open('./Messprotokoll/maxima.txt', 'r')
g = open('processedmaxima.txt', 'w')
g.write('# Ordnung Max, absolute Position [mm], relative Position [mm], sin alpha\n')
i = 0
imax = 0
for line in f:
  if len(line) < 2:
    ymax = -10000.0
    xmax = 0.0
    i = 0
#    g.write('\n# Ordnung Max, absolute Position [mm], relative Position [mm], sin alpha, Amplitude\n')
    for (x,y) in data:
      i = i + 1
      if ymax < y:
        ymax = y
        xmax = x
        imax = i
    maxima.append(xmax)
    i = 0
    for (x,y) in data:
      i = i+1
      g.write( str(i - imax) + ' '+str(x)+' '+str(x-xmax)+' '+str(Winkel(x-xmax))+' '+str(y)+'\n')
    continue
  if line[0] == '#':
    g.write('\n\n' + line)
    data = []
  else:
    data.append(map(float, line.strip().split(' ', 2)))

data = []
f = open('./Messprotokoll/minima.txt', 'r')
g = open('processedminima.txt', 'w')
n = 0
i = 0
g.write('# Ordnung Min, absolute Position [mm], relative Position [mm], sin alpha\n\n\n')
for line in f:
  if len(line) < 2:
    if len(data) == 0:
      continue
    i = 0
    for (x,y) in data:
      i = i + 1
      if x-maxima[n] < 0.0:
        icenter = i
    i = -1
    for (x,y) in data:
      i = i + 1
      if i == icenter:
        i = i + 1
      g.write(str(i - icenter) + ' '+str(x)+' '+str(x-maxima[n])+' '+str(Winkel(x-xmax))+' '+str(y)+'\n')
    n = n + 1
    data = []
    g.write('\n\n')
    continue
  if line[0] == '#':
    g.write(line)
    continue
  data.append(map(float, line.strip().split(' ', 2)))
