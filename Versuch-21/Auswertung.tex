Zunächst wird der Beugungswinkel $\alpha$ bestimmt, der sich aus geometrischen Überlegungen ergibt:
\begin{align*}
    \alpha &= \arctan\frac{x}{l}.
\end{align*}
Es bezeichnet $x$ den relativen Abstand zum Nullten Hauptmaximum und $l$ den Abstand des Objekts zur Photodiode.
Letzterer wurde von uns zu $l=\unit[(1.162\pm0.004)]{m}$ gemessen.

Mit
\begin{align}
    \epsilon &= \frac{\pi D\sin\alpha}{\lambda} \nonumber\\
\Rightarrow \sin\alpha &= \frac{\lambda}{D}\frac{\epsilon}{\pi} \label{eq:steigung}
\end{align}
kann man durch auftragen von $\sin\alpha_i$, dem Sinus des Winkels unter dem das $i$-te Beugungsextremum auftritt, gegen die Ordnung des Extremums $\frac{\epsilon_i}{\pi}$ die Steigung $\frac{\lambda}{D}$ bestimmen und so mit der bekannten Wellenlänge des Lasers von $\lambda = \unit[632.8]{nm}$ die charakteristische Größe $D$ des jeweiligen Objekts berechnen.
\subsection{Beugung am Spalt}
Beim Spalt wurden zuerst die Maxima der $\mathrm{sinc}(x)$ Funktion numerisch bestimmt, um die entsprechenden $\epsilon_i$ zu erhalten. Aus der linearen Regression der Funktion (\ref{eq:steigung}) ergibt sich dann die Spaltbreite $D$.
\begin{figure}[ht]
\begin{center}
\def\svgwidth{8cm}
\input{OrdnungSpalt.tex}
\end{center}
\caption{Lineare Regression zur Bestimmung der charakteristischen Größe des Spaltes}
\label{fig:OrdnungSpalt}
\end{figure}
\begin{empheq}[box=\fbox]{align*}
  \frac{D}{\lambda} &= 363.1 \pm 1.6 \\
  D &= \unit[(229.78 \pm 1.08)]{\mu m} \\
  r^2 &= 0.99972
\end{empheq}
\subsection{Beugung am Steg}
Beim Steg wurde genau wie beim Spalt verfahren, um die Stegbreite $D$ zu ermitteln.
\begin{figure}[ht]
\begin{center}
\def\svgwidth{8cm}
\input{OrdnungSteg.tex}
\end{center}
\caption{Lineare Regression zur Bestimmung der charakteristischen Größe des Stegs}
\label{fig:OrdnungSteg}
\end{figure}
\begin{empheq}[box=\fbox]{align*}
  \frac{D}{\lambda} &= 306.6 \pm 1.3 \\
  D &= \unit[(194.01 \pm 0.80)]{\mu m} \\
  r^2 &= 0.99968
\end{empheq}
\subsection{Beugung an der Kreisblende}
Bei der Kreisblende wurden die Extrema der Besselfunktion der Praktikumsanleitung \citep[S.\,191]{Anleitung} entnommen, um die $\epsilon_i$ zu erhalten. Nun erhält man durch die Steigung der linearen Regression den Lochdurchmesser $D$.
\begin{figure}[ht]
\begin{center}
\def\svgwidth{8cm}
\input{OrdnungLochblende.tex}
\end{center}
\caption{Lineare Regression zur Bestimmung der charakteristischen Größe der Lochblende}
\label{fig:OrdnungLochblende}
\end{figure}
\begin{empheq}[box=\fbox]{align*}
  \frac{D}{\lambda} &= 292.2 \pm 7.2 \\
  D &= \unit[(184.9 \pm 8.3)]{\mu m} \\
  r^2 &= 0.99819
\end{empheq}
\subsection{Beugung am Gitter}
Da die Hauptmaxima der Gitterfunktion durch die Nullstellen der $\sin\left(\frac{kd}{2}\sin(\alpha)\right)$-Funktion entstehen, befinden sich diese bei $\epsilon_i = \pi\mathbb{N}$.

Durch die Steigung erhält man nun den Abstand der Gitterspaltmittelpunkte $D+S$ (da die Intensitätsverteilung natürlich aus der Faltung eines \mensch{Dirac}-Kammes und eines Spalts entsteht).
\begin{figure}[ht]
\begin{center}
\def\svgwidth{8cm}
\input{OrdnungGitter.tex}
\end{center}
\caption{Lineare Regression zur Bestimmung der charakteristischen Größe des Gitters}
\label{fig:OrdnungGitter}
\end{figure}
\begin{empheq}[box=\fbox]{align*}
  \frac{D+S}{\lambda} &= 476.73 \pm 1.68 \\
  D+S &= \unit[(301.68 \pm 1.06)]{\mu m} \\
  r^2 &= 0.99991
\end{empheq}
\subsection{Beugung an der Doppelkreisblende}
Bei der Doppelkreisblende ist die Bestimmung der $\epsilon_i$ besonders einfach, da die Maxima der $\cos^2(x)$-Funktion natürlich den Extrema der $\cos(x)$-Funktion entsprechen. Durch die Steigung erhält man nun den Abstand der beiden Löcher $D$.
\begin{figure}[ht]
\begin{center}
\def\svgwidth{8cm}
\input{OrdnungDoppelloch.tex}
\end{center}
\caption{Lineare Regression zur Bestimmung der charakteristischen Größe der Doppelkreisblende}
\label{fig:OrdnungDoppelloch}
\end{figure}
\begin{empheq}[box=\fbox]{align*}
  \frac{D}{\lambda} &= 830.50 \pm 21.91 \\
  D &= \unit[(525.6 \pm 14.1)]{\mu m} \\
  r^2 &= 0.99515
\end{empheq}

\subsection{Wellenlänge des LASER-Lichts}
Nimmt man den Literaturwert \citep[S.\,196]{Anleitung} für die Gitterkonstante, so lässt sich auch umgekehrt die Wellenlänge berechnen.
\begin{empheq}[box=\fbox]{align*}
    \lambda &= \unit[1027.8 \pm 3.7]{nm}
\end{empheq}
