from math import *
def calculate(data, obj):
  if len(data) == 0:
    return
  print obj, ' ',
  sumX = 0
  sumY = 0
  n    = 0
  for x,y in data:
    sumX = sumX + x
    sumY = sumY + y
    n    = n + 1
  avgX = sumX/float(n)
  avgY = sumY/float(n)
  sumXY  = 0
  sumX2 = 0
  sumY2 = 0
  for x,y in data:
    sumXY = sumXY + (x-avgX)*(y-avgY)
    sumX2 = sumX2 + (x-avgX)**2
    sumY2 = sumY2 + (y-avgY)**2
  print sumXY/sqrt(sumX2*sumY2), ' ',sumXY**2/(sumX2*sumY2)

data = []
obj = ''
for line in open('processed.txt', 'r'):
  line = line.strip()
  if len(line) < 2:
    continue
  if line[0] == '#':
    calculate(data, obj)
    data = []
    obj = line
    continue
  token = map(float, line.split(' ',2))
  data.append(token)
calculate(data,obj)
