reset
set terminal epslatex color colortext
set output 'QuadrantEins.tex'
set key left top outside Right horizontal
set xlabel 'Ausgangsspannung $U_\mathrm{CE}\,[\unit{V}]$'
set ylabel 'Kollektorstrom $I_\mathrm{C}\,[\unit{mA}]$'
plot 'Messwerte.txt' index 0 using 1:2 title 'Messwerte für $\unit[0.1]{mA}$', \
     'Messwerte.txt' index 1 using 1:2 title 'Messwerte für $\unit[0.2]{mA}$', \
     'Messwerte.txt' index 2 using 1:2 title 'Messwerte für $\unit[0.3]{mA}$', \
     'Messwerte.txt' index 3 using 1:2 title 'Messwerte für $\unit[0.4]{mA}$', \
     'Messwerte.txt' index 4 using 1:2 title 'Messwerte für $\unit[0.5]{mA}$'
set output 'QuadrantEinsDetail.tex'
set xrange [0:0.6]
replot
