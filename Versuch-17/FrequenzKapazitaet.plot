reset
set xlabel 'Kapazität $C\,[\unit{\mu F}]$'
set ylabel 'Frequenz $\nu\,[\unit{kHz}]$'
f(x) = m*x+b
fit f(x) 'Messwerte.txt' index 6 using (log($1)):(log($4)) via m,b
set log xy
set terminal epslatex color colortext
set output 'FrequenzKapazitaet.tex'
plot 'Messwerte.txt' index 6 using 1:4 ps 2 lw 2 title 'Messwerte', exp(f(log(x))) lt 1 lc 2 title 'Lineare Regression', '' index 6 using 1:($2/(2*pi*$3*$1)) ps 2 lw 2 lt 2 lc 3 title 'Theoretischer Wert'
