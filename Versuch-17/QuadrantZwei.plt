reset
set xlabel 'Basisstrom $I_\mathrm{B}\,[\unit{mA}]$'
set ylabel 'Kollektorstrom $I_\mathrm{C}\,[\unit{mA}]$'
f(x) = a*x+b
fit f(x) 'Messwerte.txt' index 5 u 1:2 via a,b
set terminal epslatex color colortext
set output 'QuadrantZwei.tex'
plot 'Messwerte.txt' index 5 title 'Messdaten', f(x) title 'Lineare Regression' lt 1 lc 2
print a
