reset
set key bottom right
set xrange [-0.05:0.5]
set xlabel 'Basisstrom $I_\mathrm{B}\,[\unit{mA}]$'
set ylabel 'Kollektorstrom $I_\mathrm{C}\,[\unit{mA}]$'
f(x) = a*x**2+b*x+c
fit f(x) 'Messwerte.txt' index 5 u 1:2 via a,b,c
set terminal epslatex color colortext
set output 'QuadrantZwei.tex'
plot 'Messwerte.txt' index 5 title 'Messdaten' lw 2 ps 2, f(x) title 'Quadratische Regression' lt 1 lc 2
print b+2*a*0.4
