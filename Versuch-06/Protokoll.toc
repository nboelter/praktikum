\select@language {ngerman}
\contentsline {section}{\numberline {1}Einleitung}{4}{section.1}
\contentsline {section}{\numberline {2}Theorie}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Ideales Gas}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Temperatur, Druck und Volumen}{4}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Haupts\IeC {\"a}tze der Thermodynamik}{5}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Spezifische W\IeC {\"a}rme}{5}{subsection.2.4}
\contentsline {section}{\numberline {3}Durchf\IeC {\"u}hrung}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Gasthermometer}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Spezifische W\IeC {\"a}rme}{6}{subsection.3.2}
\contentsline {section}{\numberline {4}Auswertung}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}Gasthermometer}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Spezifische W\IeC {\"a}rme}{9}{subsection.4.2}
\contentsline {section}{\numberline {5}Diskussion}{10}{section.5}
\contentsline {subsection}{\numberline {5.1}Absoluter Nullpunkt}{10}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Spezifische W\IeC {\"a}rme der Luft und Anzahl der Freiheitsgrade}{10}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Verbesserungsvorschl\IeC {\"a}ge}{10}{subsection.5.3}
\contentsline {section}{\numberline {A}Tabellen und Grafiken}{11}{appendix.A}
