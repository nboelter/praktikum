reset
set key top left
set xrange [-10:100]
set yrange [100:135]
set xlabel 'Temperatur [\degree C]'
set ylabel 'Druck [kPa]'
set terminal epslatex color colortext
set output "gasdruck_auswertung_kuehl.tex"
f(x)=n*x+c
fit f(x) 'gasdruck.txt' u 1:(2*($3)+100.87):4 via n,c
plot 'gasdruck.txt' u 1:(2*($3)+100.87):4 with yerrorbars title 'Messung beim Abkühlen' lc 8, f(x) lt 1 lc 3 title 'Lineare Regression'
set output
!epstopdf gasdruck_auswertung_kuehl.eps
