reset
set key top left
set xlabel 'Elektrische Energie $\Delta Q$ [J]'
set ylabel 'Druckdifferenz $\Delta p$ [$\nicefrac{\mathrm{N}}{\mathrm{m}^2}$]'
f(x)=m*x+b
fit f(x) 'spezw2spalt.txt' u (0.5*0.00002*(($1)**2)):(997*9.81*0.002*($2)*(554/529)):3 via m,b
set terminal epslatex color colortext
set output "spezwaerme_auswertung.tex"
plot 'spezwaerme.txt' u (0.5*0.00002*(($1)**2)):(997*9.81*0.002*($2)*(554/529)):4 with yerrorbars title 'Messung 1' lc 8, 'spezwaerme.txt' u (0.5*0.00002*(($1)**2)):(997*9.81*0.002*($3)*(554/529)):4 with yerrorbars title 'Messung 2' lc 7, f(x) title 'Lineare Regression' lt 1 lc 3
set output
!epstopdf spezwaerme_auswertung.eps
