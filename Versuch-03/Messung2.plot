reset
set terminal epslatex color colortext
set output 'Messung2.tex'
set yrange [0:12]
set ytics (0,0)
set ytics add ("4" 2)
set ytics add ("16" 4)
set ytics add ("36" 6) 
set ytics add ("64" 8) 
set ytics add ("100" 10) 
set ytics add ("144" 12) 
set xlabel 'Zeit $t$\,[s]'
set ylabel 'Zurückgelegter Weg $x$\,[m]'
set key right bottom
lin1(x) = m1*x+b1
lin2(x) = m2*x+b2
lin3(x) = m3*x+b3
lin4(x) = m4*x+b4
fit lin1(x) 'Messwerte.txt' index 3 using 1:(sqrt($2/100)) via m1,b1
fit lin2(x) 'Messwerte.txt' index 4 using 1:(sqrt($2/100)) via m2,b2
fit lin3(x) 'Messwerte.txt' index 5 using 1:(sqrt($2/100)) via m3,b3
fit lin4(x) 'Messwerte.txt' index 6 using 1:(sqrt($2/100)) via m4,b4
plot 'Messwerte.txt' index 3 using 1:(sqrt($2/100)) lt 1 lc 1 title 'Messwerte \unit[0.1]{kg}', lin1(x) lt 1 lc 1 notitle, \
     'Messwerte.txt' index 4 using 1:(sqrt($2/100)) lt 1 lc 2 title 'Messwerte \unit[0.2]{kg}', lin2(x) lt 1 lc 2 notitle, \
     'Messwerte.txt' index 5 using 1:(sqrt($2/100)) lt 1 lc 3 title 'Messwerte \unit[0.5]{kg}', lin3(x) lt 1 lc 3 notitle, \
     'Messwerte.txt' index 6 using 1:(sqrt($2/100)) lt 1 lc 4 title 'Messwerte \unit[1.0]{kg}', lin4(x) lt 1 lc 4 notitle
set output
!epstopdf 'Messung2.eps'
