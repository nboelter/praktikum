reset
set terminal epslatex color colortext
set output 'Messung1.tex'
lin1(x) = m1*x
lin2(x) = m2*x
set yrange [0:160]
set key left top
set xlabel 'Drehmoment des Gewichts $T$ [N\,m]'
set ylabel 'Auslenkung $\phi$ [$^\circ$]'
fit lin1(x) 'Messwerte.txt' index 0 using ($1/1000*9.81*0.081):(270-$2) via m1
fit lin2(x) '' index 1 using ($1/1000*9.81*0.081):($2-275) via m2
plot 'Messwerte.txt' index 0 using ($1*0.001*9.81*0.081):(270-$2) title 'Messwerte Links' lt 1 lc rgb "blue", lin1(x) title 'Lineare Regression' lt 1 lc rgb "blue", '' index 1 using ($1*0.001*9.81*0.081):($2-275) lt 1 lc 2 title 'Messwerte Rechts', lin2(x) lt 1 lc 2 title 'Lineare Regression'
set output
!epstopdf 'Messung1.eps'
