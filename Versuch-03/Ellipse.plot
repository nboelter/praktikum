reset
set terminal epslatex color colortext
set output 'ellipse.tex'
set xlabel 'Zeit $t$ [s]'
set ylabel 'Zeit $t$ [s]'
set key right out
set size square
set xrange [-10:10]
set yrange [-10:10]
set grid polar
set angles degrees
plot './Messwerte.txt' index 2 using (cos($1)*$2):(sin($1)*$2) title 'Messwerte' lt 1 lc 0, 0.267949*x title '$J1$' lt 1 lc 1, -3.73205*x title '$J2$' lt 1 lc 2
set output
!epstopdf ellipse.eps
