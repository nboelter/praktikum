# vim: set fileencoding=utf-8 :
from math import log,fabs,pi,floor,ceil
def parse():
    emptylines = 0
    index = 0
    ellipse = {}
    f = open('Messwerte.txt')
    for line in f:
        line = line.strip()
        if line == '':
            emptylines = emptylines + 1
        else:
            emptylines = 0
        
        if emptylines == 2:
            index = index + 1
        if index == 2 and len(line) > 0 and line[0] != '#':
            angle, period = line.split('\t',1)
            angle = int(angle)
            period = float(period)
            ellipse[angle] = period
    for angle in xrange(0,180,15):
        print "angle =", angle, "deg", "T =", 0.5*(ellipse[angle]+ellipse[angle+180])

def main():
    index = parse()

if __name__ == '__main__': 
    main() 
