reset
set fit errorvariables
set terminal epslatex color colortext
set output 'Abfall-Schwingkreis.tex'
set xlabel 'Zeit $t\,[\unit{\mu s}]$'
set ylabel 'Spannung $U\,[\unit{V}]$'
SEK(x) = x
VOLT(x) = x*100/17
b1 = 360
b2 = 360
r1(x) = a1*exp(-x/b1)
r2(x) = a2*exp(-x/b2)
c=-0.1035
fit r2(x) 'Messwerte.txt' index 3 using (SEK($1)):(VOLT($2+c)) via a2,b2
fit r1(x) 'Messwerte.txt' index 2 using (SEK($1)):(VOLT($2-c)) via a1,b1
set samples 10000 
plot 'Messwerte.txt' index 2 using (SEK($1)):(VOLT($2-c)) title 'Amplituden' lt 1 lc 1, (r1(x)) title 'Einhüllende' lt 1 lc 2, 0 notitle lt 1 lc 0,\
     'Messwerte.txt' index 3 using (SEK($1)):(VOLT(-1*($2+c))) lt 1 lc 1 notitle, (-1*(r2(x))) notitle lt 1 lc 2,(VOLT(6.3+c)*cos(x*2*pi/SEK(54.10))*exp(-x*2/(b1+b2))) title 'Harmonische Oszillation' lt 1 lc 3

print (b1+b2)/2, ' +/- ', (sqrt(b1_err**2 + b2_err**2)/2)
