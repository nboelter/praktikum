reset
set fit errorvariables
set terminal epslatex color colortext
set output 'Abfall-Schwingkreis2.tex'
set xlabel 'Zeit $t\,[\unit{\mu s}]$'
set ylabel 'Spannung $U\,[\unit{V}]$'
b1 = 360
b2 = 360
VOLT(x) = x * 500/17 
r1(x) = a1*exp(-x/b1)
r2(x) = a2*exp(-x/b2)
c=-0.18
fit r2(x) 'Messwerte.txt' index 5 using 1:(VOLT($2+c)) via a2,b2
fit r1(x) 'Messwerte.txt' index 4 using 1:(VOLT($2-c)) via a1,b1
set samples 10000 
plot 'Messwerte.txt' index 4 using 1:(VOLT($2-c)) title 'Amplituden' lt 1 lc 1, (r1(x)) title 'Einhüllende' lt 1 lc 2, 0 notitle lt 1 lc 0, \
     'Messwerte.txt' index 5 using 1:(VOLT(-1*($2+c))) notitle lt 1 lc 1, (-1*(r2(x))) notitle lt 1 lc 2 ,(VOLT(6.2-c)*cos(x*2*pi/1.43382352941176470587)*exp(-x*2/(b1+b2))) title 'Harmonische Oszillation' lt 1 lc 3
print (b1+b2)/2, ' +/- ', (sqrt(b1_err**2 + b2_err**2)/2)
