\subsection{Bestimmung der Eichkonstante des Stromintegrators}
Da Eichspannung und Eingangswiderstand des Integrators bekannt sind, kann der durch den Integrator fliessende Strom einfach bestimmt werden:
\begin{align*}
  I &= \frac{U}{R} = (1.238 \pm 0.015)\cdot\unit[10^{-4}]{A}
\end{align*}
Aus den Pulsbreiten des Eichpulses und der Stromstärke kann nun die geflossene Ladung berechnet und mit dem angezeigten Skalenwert verglichen werden. Aus den sich daraus ergebenen Eichkonstanten (Tab.~\ref{tab:eichkonstanten}) wird ein gewichteter Mittelwert gebildet:
\begin{empheq}[box=\fbox]{align*}
  \kappa &= \unitfrac[(1.030 \pm 0.043)]{Skt.}{\mu C}
\end{empheq}
\subsection{Bestimmung von $C_\mathrm{Pl}$ mit Stromintegrator}
Aus der gemessenen Anfangsladung des Plattenkondensators (Tab.~\ref{tab:anfangsladungen}) kann ein gewichtetes Mittel berechnet werden:
\begin{align*}
  Q_0 &= (0.880 \pm 0.023)\,\unit{\mu C}.
\end{align*}
Also gilt damit für die Kapazität:
\begin{empheq}[box=\fbox]{align*}
  C_\mathrm{Pl} &= \frac{Q_0}{U_0} = \unit[(4.00 \pm 0.14)]{nF}
\end{empheq}
\subsection{Bestimmung der elektrischen Feldkonstante}
Mit den Angaben aus der Praktikumsanleitung lässt sich die Kapazität des Plattenkondensators auch direkt berechnen: \citep{kirchhoff1891vorlesungen}
\begin{align}
  C_\mathrm{Pl,theo} &= (n-1) \epsilon_0 \epsilon_\mathrm{r} \left(\frac{\pi r^2}{d}+r\left[\ln\left(\frac{16\pi r}{d}\right)-1\right]\right)\label{eq:kirchhoff}\\
  C_\mathrm{Pl,theo} &= \epsilon_0 \cdot \unit[415.18]{m}.\nonumber
\end{align}
Gleichsetzen mit dem obigen Ergebnis ergibt nun:
\begin{empheq}[box=\fbox]{align*}
  \epsilon_0 &= \frac{C_\mathrm{Pl}}{\unit[415.18]{m}} = \unitfrac[(9.64 \pm 0.33)\cdot10^{-12}]{F}{m}.
\end{empheq}
Die Abweichung vom Literaturwert \citep[S.~1-2]{Rubber} beträgt etwa $\unit[8.9]{\%}$.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Widerstand-ohne.tex}
\end{center}
\caption{Entladekurve des Plattenkondensators ohne unbekannten Widerstand.}
\label{fig:widerstand-ohne}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Widerstand-mit.tex}
\end{center}
\caption{Entladekurve des Plattenkondensators mit unbekanntem Widerstand.}
\label{fig:widerstand-mit}
\end{figure}
\subsection{Bestimmung von $R_\mathrm{iso}$ und $R_X$ mit Stromintegrator}
Die Entladekurven des Kondensators bei Selbstentladung und Entladung über den unbekannten Widerstand wurden in Abbildungen~\ref{fig:widerstand-ohne} bzw. \ref{fig:widerstand-mit} aufgetragen.

Eine exponentiale Regression\footnote{via gnuplot, $\chi^2_\mathrm{red,ohne} \approx 0.087$, $\chi^2_\mathrm{red,mit} \approx 0.0024$.} $Q(t) = Q_0 \cdot \exp\left(-\nicefrac{\Lambda t}{\unit[1]{s}} \right)$ liefert die logarithmischen Dekremente:
\begin{align*}
  \Lambda_\mathrm{ohne} &= (3.0 \pm 1.6)\cdot10^{-3} \\
  \Lambda_\mathrm{mit} &= (0.1422 \pm 0.0100).
\end{align*}
Identifizierung von $\Lambda = \frac{\unit[1]{s}}{R C}$ durch Formel~\ref{eq:entladekurve} liefert nun, da die Kapazität schon bestimmt wurde, sofort die Widerstände.
\begin{align*}
  R_\mathrm{iso} &= \unit[(8.36 \pm 4.49)\cdot10^{10}]{\Omega} \\
  R_\mathrm{ges} &= \unit[(1.76 \pm 0.14)\cdot10^{9}]{\Omega}
\end{align*}
Da bei der Entladung über den unbekannten Widerstand beide Widerstände parallel geschaltet sind, lässt sich nun ebenfalls der unbekannte Widerstand berechnen:
\begin{empheq}[box=\fbox]{align*}
  R_\mathrm{iso} &= \unit[(8.36 \pm 4.49)\cdot10^{10}]{\Omega} \\
  R_\mathrm{X} &= \left(R_\mathrm{ges}^{-1} - R_\mathrm{iso}^{-1}\right)^{-1} =  \unit[(1.79 \pm 0.15)\cdot10^{9}]{\Omega}.
\end{empheq}
\subsection{Bestimmung von $C_\mathrm{Pl}$ und des Innenwiderstands $R_0$}
\label{subsect:CPL}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Messung3b}
\end{center}
\caption{Entladekurve des Kondensators ohne Widerstand.}
\label{fig:Messung3b}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Messung3c}
\end{center}
\caption{Entladekurve des Plattenkondensators mit $\unit[2]{M\Omega}$-Widerstand}
\label{fig:Messung3c}
\end{figure}
Nach dem Ablesen der Messdaten aus den Ausdrucken werden jene graphisch aufgetragen (siehe Abb.~\ref{fig:Messung3b} und~\ref{fig:Messung3c}) und exponentielle Regressionen\footnote{via gnuplot, $\chi_\mathrm{red, b}^2 \approx 11.9$, $\chi_\mathrm{red, c}^2 \approx 49.9$} durchgeführt.
Aus den so erhaltenen logarithmischen Dekrementen kann nun abgelesen werden:
\begin{align}
  \alpha &= \frac{\unit[1]{s}}{C_\mathrm{Pl}\cdot R_0} \label{eq:Kapa}\\
  &= (36.15 \pm 15.93) \nonumber\\
  \beta &= \frac{\unit[1]{s}}{C_\mathrm{Pl}\cdot R_\mathrm{ges}} \nonumber\\
  &= (82.52 \pm 11.03) \nonumber
\end{align}
Da $R_0$ und $R_2$ praktisch parallel geschaltet sind, gilt $R_\mathrm{ges}^{-1} = R_0^{-1} + R_2^{-1}$ und damit:
\begin{align}
  R_0 &= \left(\frac{\beta}{\alpha}-1\right)\cdot R_2\label{eq:R0}
\end{align}
Nach Einsetzen der vorher erhaltenen Werte für $\alpha$ und $\beta$ folgt dann:
\begin{empheq}[box=\fbox]{align*}
  R_0 &= \unit[(2.86 \pm 2.67)]{M\Omega}
\end{empheq}
Durch Umstellen von Formel~\ref{eq:Kapa} lässt sich nun einfach die Kapazität des Plattenkondensators berechnen:
\begin{empheq}[box=\fbox]{align*}
  C_\mathrm{Pl} &= \unit[(9.66 \pm 9.06)]{nF}
\end{empheq}
\clearpage
\subsection{Bestimmung von $R_\mathrm{x}$}
\label{subsect:RX}
Zur Bestimmung von $R_\mathrm{x}$ wurde bei Messung d der $\unit[2]{M\Omega}$-Widerstand mit dem unbekannten Widerstand vertauscht,
sodass Formel~\ref{eq:R0} nach vertauschen von $R_2$ durch $R_\mathrm{x}$ und von $\beta$ durch $\gamma$ nach kurzem Umstellen folgendes ergibt:
\begin{align*}
  R_\mathrm{x} &= \frac{R_0}{\frac{\gamma}{\alpha}-1}
\end{align*}
Einsetzen des durch lineare Regression\footnote{via gnuplot, $\chi_\mathrm{red}^2 \approx 15.85$} (siehe auch Abb.~\ref{fig:Messung3d}) berechneten Wertes $\gamma = \unitfrac[(37.31\pm19.54)]{1}{s}$ ergibt dann:
\begin{empheq}[box=\fbox]{align*}
  R_\mathrm{x} &= \unit[(0.92 \pm 3.0)]{G\Omega}
\end{empheq}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Messung3d}
\end{center}
\caption{Entladekurve des Plattenkondensators mit unbekanntem Widerstand.}
\label{fig:Messung3d}
\end{figure}
\clearpage

\subsection{Induktivitäten und Widerstände der Spulen}
Die Spannungsverläufe der beiden LCR-Schwingkreise wurden in den Abbildungen~\ref{fig:abfall-schwingkreis} bzw. \ref{fig:abfall-schwingkreis2} aufgetragen. Mittels exponentieller Regression wurden die logarithmischen Dekremente bestimmt:
\begin{align*}
  \Lambda_\mathrm{Luftspule} &= (361.4 \pm 1.8) \\
  \Lambda_\mathrm{Drosselspule} &= (4.02 \pm 0.18)
\end{align*}
Dafür wurde die Nullage korrigiert und dann zwei Regressionen für den unteren bzw. oberen Teil der Einhüllenden erstellt, deren Mittelwert als logarithmisches Dekrement benutzt wird.
% TODO: Erkläre diese tolle Formel
Mittels Formel~(\ref{eq:induktivitaet}) kann man nun die Induktivitäten der beiden Spulen berechnen:
\begin{empheq}[box=\fbox]{align*}
  L_\mathrm{Luftspule} &= \unit[(1.852 \pm 0.065)\cdot10^{-2}]{H} \\
  L_\mathrm{Drosselspule} &= \unit[(1.30 \pm 0.11)\cdot10^{1}]{H}
\end{empheq}
Durch Umstellen mittels Formel~(\ref{eq:daempfung}) kann man nun ebenfalls den Verlustwiderstand der beiden Spulen berechnen:
\begin{empheq}[box=\fbox]{align*}
  R_\mathrm{Luftspule} &= \unit[(1.025 \pm 0.037)\cdot10^{2}]{\Omega} \\
  R_\mathrm{Drosselspule} &= \unit[(6.46 \pm 0.66)\cdot10^{3}]{\Omega}
\end{empheq}

Auch aus den Spulendaten lässt sich mit Formel~(\ref{eq:luftspule}) die Induktivität berechnen:
\begin{empheq}[box=\fbox]{align*}
  L_\mathrm{Luftspule} &= \unit[1.997\cdot10^{-2}]{H}
\end{empheq}

\subsection{Kapazität des kommerziellen Kondensators}
\label{subsect:CC}
Die Kapazität des kommerziellen Kondensators wird genau wie in \ref{subsect:CPL} bestimmt. Die dafür nötige exponentielle Regression\footnote{via gnuplot, $\chi^2_\mathrm{red} \approx 3.6$} ist in Abbildung~\ref{fig:kondensator} aufgetragen.
\begin{align*}
  \gamma &= \unitfrac[(42.0 \pm 1.138)]{1}{s}
\end{align*}

\begin{empheq}[box=\fbox]{align*}
  C_\mathrm{böse} &= \frac{\gamma}{R_2} = \unit[(1.066 \pm 0.038)\cdot10^{-8}]{F}
\end{empheq}

\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Abfall-Schwingkreis.tex}
\end{center}
\caption{Einhüllende der gedämpften harmonischen Oszillation mit Luftspule und Plattenkondensator.}
\label{fig:abfall-schwingkreis}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Abfall-Schwingkreis2.tex}
\end{center}
\caption{Einhüllende der gedämpften harmonischen Oszillation mit Drosselspule und Plattenkondensator.}
\label{fig:abfall-schwingkreis2}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Auswertung-Kondensator.tex}
\end{center}
\caption{Entladekurve des kommerziellen Kondensators mit $\unit[2]{M\Omega}$-Widerstand.}
\label{fig:kondensator}
\end{figure}
