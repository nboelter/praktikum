reset
set xlabel 'Zeit nach dem Spannungspuls $t\,[\unit{s}]$'
set ylabel 'Spannung am Kondensator $U_\mathrm{C}\,[\unit{V}]$'
f1(x) = a1*exp(-x*b1)+c1
f2(x) = a2*exp(-x*b2)+c2
f3(x) = a3*exp(-x*b3)+c3
a1 = 80
b1 = 26
a2 = 80
b2 = 82
a3 = 180
b3 = 50
fit f1(x) 'TolleOsziMesswerte.txt' index 0 u (2*($1+0.013)):2 via a1,b1,c1
fit f2(x) 'TolleOsziMesswerte.txt' index 1 u (2*($1-0.008)):2 via a2,b2,c2
fit f3(x) 'TolleOsziMesswerte.txt' index 2 u (2*($1+0.013)):2 via a3,b3,c3
set terminal epslatex color colortext
set output 'Messung3b.tex'
plot 'TolleOsziMesswerte.txt' index 0 u (2*($1+0.013)):($2) title 'Messwerte', f1(x) title 'Exponentielle Regression' lt 1 lc 3
set output 'Messung3c.tex'
plot 'TolleOsziMesswerte.txt' index 1 u (2*($1-0.008)):($2) title 'Messwerte', f2(x) title 'Exponentielle Regression' lt 1 lc 3
set output 'Messung3d.tex'
plot 'TolleOsziMesswerte.txt' index 2 u (2*($1+0.013)):($2) title 'Messwerte', f3(x) title 'Exponentielle Regression' lt 1 lc 3
