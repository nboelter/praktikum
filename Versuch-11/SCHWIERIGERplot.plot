reset
set terminal epslatex color colortext
set output 'Auswertung-Kondensator.tex'
set xlabel 'Zeit [$\unit{ms}$]'
set ylabel 'Spannung am Kondensator $U_\mathrm{C}$\,[$\unit{V}$]'
VOLT(x) = x/17*500
SEK(x) = x/17*0.050
a = 350
b = 1/0.023
c = -160
f(x) = a*exp(-x*b)+c
fit f(x) 'Messwerte.txt' index 6 u (SEK($1)):(VOLT($2)) via a,b,c
set xrange [5:55]
plot 'Messwerte.txt' index 6 using (SEK($1)*1000):(VOLT($2)) title 'Messwerte', (f(x/1000)) title 'Regression' lt 1 lc 2
