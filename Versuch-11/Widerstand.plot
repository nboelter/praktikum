reset
set fit errorvariables
set xlabel 'Zeit [$\unit{s}$]'
set ylabel 'Verbliebene Ladung [$\unit{C}$]'
r2(x) = 0.9153 * exp(-x/t2)
r(x) = 0.9153 * exp(-x/t)
fit r2(x) 'Messwerte.txt' index 0 using 1:2 via t2
t = 500
fit r(x) 'Messwerte.txt' index 1 using 1:2 via t
set terminal epslatex color colortext
set output 'Widerstand-ohne.tex'
plot 'Messwerte.txt' index 1 using 1:($2/1.02983240883018) title 'Messwerte' lt 1 lc 1, (r(x)/1.02983240883018) title 'Regression' lt 1 lc 2

set output 'Widerstand-mit.tex'
plot 'Messwerte.txt' index 0 using 1:($2/1.02983240883018) title 'Messwerte' lt 1 lc 1, (r2(x)/1.02983240883018) title 'Regression' lt 1 lc 2

print 'Log Dek. ohne Widerstand ', 1/t, ' +/- ', t_err/t**2
print 'Log Dek. mit Widerstand ', 1/t2, ' +/- ', t2_err/t2**2
