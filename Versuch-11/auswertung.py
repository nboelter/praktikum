 # vim: set fileencoding=utf-8 :
from math import *

def Einheitenrechner1(strecke):
  laenge_10_kaestchen = 17 #cm
  bedeutung_10_kaestchen = 200 #V
  return strecke*bedeutung_10_kaestchen/laenge_10_kaestchen

def Einheitenrechner2(strecke):
  laenge_10_kaestchen = 17 #cm
  bedeutung_10_kaestchen = 500 #V
  return strecke*bedeutung_10_kaestchen/laenge_10_kaestchen

data = []
f = open('OsziMesswerte.txt', 'r')
for line in f:
  if line[0] == '#' or len(line) < 2:
    continue
  data.append(map(float, line.strip().split(' ', 3)))

print "Legende:"
print "t [s], U [V] (richtiger Wert fuer b und d), U [V] (richtiger Wert fuer c)"
for (t, s, U) in data:
  t = 2*t
  print t, Einheitenrechner1(s), Einheitenrechner2(s)

print "----------------------------------------"
print " "
alpha = -36.1514
beta = -82.5222
gamma = -37.3186
R2 = 2232000
sigmaR2 = 410000.00
R0 = ((beta/alpha)-1)*R2
print "R_0 =", R0, "Ω"

print " "
R_Oszi = 11000000
print "R_X,eigenes R0 =", R0/((alpha/gamma)-1), "Ω"
print "R_X,11MΩ_R0 =", R_Oszi/((alpha/gamma)-1), "Ω"

print " "
print "Kapazität des Plattenkondensators:", -1/(R0*alpha), "F"
print "Kapazität mit 11MΩ_R0:", -1/(R_Oszi*alpha), "F"
print " "

komm = 42.0077
sigmakomm = 1.138

print " "
print "Kapazität des kommerziellen Kondensators:", 1/(R2*komm), "F"
print "Kapazität mit 11MΩ_R0:", 1/(R_Oszi*komm), "F"
print " "

sigmaR2 = 50000
sigmaalpha = 2.691
sigmabeta = 42.85
sigmagamma = 2.489

sigmaR0 = sqrt((sigmaR2*(beta/alpha-1))**2 + (R2*sigmabeta/alpha)**2 + (sigmaalpha*R2*beta/alpha**2)**2)
sigmaRX = sqrt((sigmaR0/(gamma/alpha-1))**2 + (R0*sigmagamma/(alpha*(gamma/alpha-1)**2))**2 + (R0*sigmaalpha*gamma/(alpha**2*(gamma/alpha-1)**2))**2)
sigmaC = sqrt((sigmaR0/(R0**2*alpha))**2 + (sigmaalpha/(R0*alpha**2))**2)
sigmaC2 = sqrt((sigmaR2/(R2**2*komm))**2 + (sigmakomm/(R2*komm**2))**2)


print "Fehler der Kapazität:", sigmaC
print "Fehler der kommerziellen Kapazität:", sigmaC2
print "Fehler von R_0:", sigmaR0
print "Fehler von R_X, eigenes R0:", sigmaRX
