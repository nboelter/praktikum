reset
set xlabel 'Winkelstellung $\theta\,[\degree]$'
set ylabel 'Zählrate $N\,[\unit{Hz}]$'
set key bottom
set logscale y
set terminal epslatex color colortext
set output 'kanten.tex'
plot 'messung3.1.txt.corrected' u 1:2 title '$U_A=\unit[23]{kV}$', '' u 1:3 title '$U_A=\unit[26]{kV}$', '' u 1:4 title '$U_A=\unit[29]{kV}$', '' u 1:5 title '$U_A=\unit[32]{kV}$', '' u 1:6 title '$U_A=\unit[35]{kV}$'
set output
