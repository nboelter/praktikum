reset
set key outside center top horizontal
set xlabel 'Glanzwinkel $\alpha\,[\degree]$'
set ylabel 'Zählrate $N\,[\unit{Hz}]$'
set terminal epslatex color colortext
set output 'Absorption.tex'
plot 'messung5.1.txt.corrected' title 'kein Absorber', 'messung5.2.txt.corrected' title 'Aluminium', 'messung5.3.txt.corrected' title 'Zink', 'messung5.4.txt.corrected' title 'Zinn', 'messung5.5.txt.corrected' title 'Nickel'
set output
