reset
set xlabel '$\theta [\unit{^\circ}]$'
set ylabel '$I [\unitfrac{Impulse}{s}]$'
set grid
set terminal epslatex color colortext
set output 'Messung2.tex'
set logscale y
plot 'messung2.txt.corrected' using 1:2 title 'korrigierte Messwerte'
set output
