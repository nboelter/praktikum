\subsection{Das Röntgenspektrometer}
Bei einem Röntgenspektrometer handelt es sich um eine Apparatur, mit der das Wellenlängenspektrum elektromagnetischer Strahlung vermessen werden kann.
Hierzu fokussiert man die zu analysierende Strahlung auf einen winkelverstellbaren Kristall, an dem die Strahlung gebrochen und im Glanzwinkel mit einem Detektor gemessen werden kann.
\begin{figure}[htb]
    \begin{center}
        \def\svgwidth{8cm}
        \input{reflexion.pdf_tex}
    \end{center}
    \caption{Schematische Darstellung der Reflexion elektromagnetischer Strahlung an der ersten und zweiten Kristallschicht~\citep{LP-Roentgenstrahlung}.}
    \label{fig:reflexion}
\end{figure}

Wie in Abbildung~\ref{fig:reflexion} leicht zu sehen ist, gilt für den Gangunterschied der beiden Strahlen $g = 2d\sin\theta$.
Mit der Bedingung für konstruktive Interferenz zweier Wellen $g = n\lambda$ ergibt sich die \mensch{Bragg}-Bedingung:
\begin{align}
    2d\sin\theta &= n\lambda.\label{eq:bragg}
\end{align}
Durch die Messung der unter dem Glanzwinkel auftretenden Intensität $I(\theta)$ kann man also das Wellenlängenspektrum $I(\lambda)$ erhalten.

Im Versuch wird die Strahlung durch eine Röntgenröhre erzeugt.
Diese ist in Abbildung~\ref{fig:roehre} schematisch dargestellt.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{roehre.pdf_tex}
\end{center}
\caption{Schematische Darstellung der Röntgenröhre~\citep{LP-Roentgenstrahlung}.}
\label{fig:roehre}
\end{figure}

Sie besteht aus einem evakuierten Kolben, in dem sich eine Kathode und eine angeschrägte Anode befinden.
Durch Anlegen einer Spannung von bis zu $\unit[35]{kV}$ werden aus der Kathode Elektronen ausgelöst und in Richtung der Anode beschleunigt.
Beim Auftreffen an der Anode wird sowohl \emph{Brems}- als auch \emph{charakteristische Strahlung} ausgesendet.
Bei ersterer handelt es sich um die durch Beschleunigung der Elektronen entgegen ihrer ursprünglichen Bewegungsrichtung entstehende Strahlung.
Die charakteristische Strahlung wird in Abschnitt \ref{sec:char} näher erläutert.
Diese wird im Versuch mit dem Röntgenspektrometer analysiert.
\subsection{Das Geiger-Müller-Zählrohr}
\begin{figure}[htb]
    \begin{center}
        \def\svgwidth{8cm}
        \input{zaehlrohr.pdf_tex}
    \end{center}
    \caption{Schematische Darstellung des Geiger-Müller-Zählrohrs~\citep{LP-Roentgenstrahlung}.}
    \label{fig:zaehlrohr}
\end{figure}
Beim im Versuch verwendeten Detektor handelt es sich um ein \mensch{Geiger-Müller}-Zählrohr.
Der schematische Aufbau eines solchen ist in Abbildung~\ref{fig:zaehlrohr} dargestellt.
Das Zählrohr besteht aus einem Zylinder, der mit einem Gas niedrigen Druckes gefüllt ist.
Mittig befindet sich im Zylinder ein Draht, der als Anode auf eine Potentialdifferenz im Bereich von $\unit[100]{V}$ gegenüber der metallischen Zylinderkathode gebracht wird.
Werden nun durch Einfall elektromagnetischer Strahlung einige Gasmoleküle ionisiert, führt die Hochspannung dazu, dass die freien Elektronen in Richtung Draht und die positiv geladenen Ionen in Richtung Außenwand beschleunigt werden.
Hierbei erhalten die Elektronen eine so hohe kinetische Energie, dass es zu weiteren Ionisationen im Gas kommt.
Diese führen nach dem gleichen Prinzip zu weiteren Ionisationen, sodass ein messbarer Spannungspuls erzeugt wird.

Im Gegensatz zu den Elektronen sind die massereichen Ionen weniger mobil und bleiben eher im Bereich des Anodendrahtes.
Durch die große Zahl an Ionisationen wird nach kurzer Zeit dem elektrischen Feld durch die Raumladungsdichte der Ionen entgegengewirkt.
Dadurch kommt es zu einem Abbruch der Ionisationsvorgänge und zu einem Ende des messbaren Spannungsstoßes.

Anschließend rekombinieren die Ionen und Elektronen wieder.
Während der Ionisationslawine können natürlich keine weiteren Ionisationsereignisse durch einfallende Strahlung registriert werden.
Man nennt das Zeitintervall, in dem nach einer Registration solcher Strahlung keine weiteren extern verursachten Ionisationen mehr auftreten können die \emph{Totzeit} des Detektors.
\subsection{Charakteristische Röntgenstrahlung}\label{sec:char}
Charakteristische Strahlung entsteht durch das Wechseln von Elektronen in verschiedene Hauptquantenzustände.
Bei einem solchen Wechsel wird je nach Ursprungs- und Zielzustand eine feste Energie $E_\gamma$ in Form eines Photons der Frequenz $\nu=E_\gamma/\hbar$ frei.
Die Intensität der charakteristischen Strahlung der Röntgenröhre hängt von der Anodenspannung ab: \citep[S.\,14]{Roentgenbeugung}
\begin{align}
  I &\propto (U_\mathrm{A} - U_\mathrm{K})^{\frac{3}{2}}\label{eq:komisch}
\end{align}
Dabei ist $U_\mathrm{K}$ die \emph{charakteristische Anregungsspannung}.

Für die langwelligste K-Linie im Röntgenspektrum eines Elements gilt für die Frequenz $\nu$ das Gesetz von \mensch{Moseley}: \cite[S\,864]{Gerthsen}
\begin{align}
  \nu &= \frac{3}{4}R_\infty c (Z - 1)^2\label{eq:rydberg}
\end{align}
Dabei ist $R_\infty \approx \unit[10973731]{m^{-1}}$ die sogenannte \mensch{Rydberg}-Konstante und Z die Ordnungszahl.

Die niedrigste mögliche Wellenlänge entsteht, falls die gesamte kinetische Energie $eU$ eines Elektrons beim Auftritt auf die Anode in ein einziges Photon umgewandelt wird.
Hierbei handelt es sich dann um reine Bremsstrahlung und in diesem Grenzfall gilt das Gesetz von \mensch{Duane} und \mensch{Hunt}: \citep[S\,863]{Gerthsen}
\begin{align}
  h \nu &= eU \nonumber \\
  \lambda &= \frac{hc}{eU} \label{eq:grenzwertig}
\end{align}

\subsection{Röntgenabsorption}
Wird Röntgenstrahlung durch Materialien abgeschirmt, so ergibt sich für die Intensität hinter einer Abschirmung der Dicke $x$ das \mensch{Lambert-Beer}-Gesetz: \citep[S.\,865]{Gerthsen}
\begin{align}
  I &\propto \exp(-\mu x)\label{eq:lambert-beer}
\end{align}
Bei den hier benutzten niedrigen Anodenspannungen von etwa $\unit[25]{kV}$ spielt nur der \emph{Photoeffekt} eine Rolle.
Er bezeichnet die Absorption von Photonen durch Elektronen, die durch ihre anschließend höhere Energie unter gewissen Voraussetzungen einen messbaren Stromfluss erzeugen können.
Beim Photoeffekt gilt folgender Zusammenhang: \citep[S.\,866]{Gerthsen}
\begin{align}
  \mu &= n \frac{2\pi}{3}\alpha r_n^2\omega_n^3\frac{\lambda^3}{(2\pi c)^3} \propto \lambda^3
\end{align}
Dabei bezeichnet $n$ die Anzahldichte, $\alpha$ die Feinstrukturkonstante, $r_n$ und $\omega_n$ der Radius und die Kreisfrequenz des absorbierenden Elektrons.
