#!/usr/bin/python
# -*- coding: utf-8 -*-
from math import *


Z = 30
lam = 2.0*201.0e-12*sin(18.8*pi/180.0)
slam = 2.0*201.0e-12*cos(18.8*pi/180.0)*0.1*pi/180.0
R1 = (4.0/3.0)*1.0/(lam*(Z-1)**2)
sR1 = slam*(4.0/3.0)*1.0/(lam**2*(Z-1)**2)
print 'Wellenlänge: λ = ', lam, '+/-', slam
print 'Rydberg-Konstante: R = ', R1, '+/- ', sR1

Z = 50
lam = 2.0*201.0e-12*sin(6.2*pi/180.0)
slam = 2.0*201.0e-12*cos(6.2*pi/180.0)*0.1*pi/180.0
R2 = (4.0/3.0)*1.0/(lam*(Z-1)**2)
sR2 = slam*(4.0/3.0)*1.0/(lam**2*(Z-1)**2)
print 'Wellenlänge: λ = ', lam, '+/-', slam
print 'Rydberg-Konstante: R = ', R2, '+/- ', sR2

print 'Gewichtetes Mittel: R = ', ((R1/sR1**2)+(R2/sR2**2))/((1/sR1**2)+(1/sR2**2)), '+/- ', sqrt(1/((1/sR1**2)+(1/sR2**2)))
