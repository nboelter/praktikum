from math import *

def weightedmean(data):
  mean = 0
  smean = 0
  for (value, error) in data:
    mean = mean + value/error**2
    smean = smean + 1/error**2
  mean = mean/smean
  smean = 1/sqrt(smean)
  return (mean, smean)
