from math import *
import glob
tau = 0.0001
for file in glob.glob('messung*.txt'):
  fin = open(file, 'r')
  fout = open(file + '.corrected', 'w')
  for line in fin:
    if line[0] == '#' or len(line) < 3:
      fout.write(line)
    else:
      token = line.strip().split('\t')
      for t in token:
        t = t.strip()
      token = map(float, token)
      fout.write(str(token[0]))
      del token[0]
      for t in token:
          fout.write(' '+str(t/(1.0-tau*t)))
      fout.write('\n')
      
