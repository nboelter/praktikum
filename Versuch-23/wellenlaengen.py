from math import *

def wurst(alpha, n):
  d = 201e-12
  hquer = 4.135667516e-15
  c = 299792458
  lam = 2*d*sin(alpha*pi/180)/n
  slam = 2*d*cos(alpha*pi/180)*0.05/n*pi/180
  E = hquer*c/lam
  sE = slam*E/lam
  return 'lambda = ' + str(lam) + ' +/- ' + str(slam) + '\n' + 'E = ' + str(E) + ' +/- ' + str(sE) + '\n'

def wurst2(alpha, n):
  d = 201e-12
  hquer = 4.135667516e-15
  c = 299792458
  lam = 2*d*sin(alpha*pi/180)/n
  slam = 2*d*cos(alpha*pi/180)*0.05/n*pi/180
  E = hquer*c/lam
  sE = slam*E/lam
  return (lam, slam, E, sE)

alpha1 = 19.0
alpha2 = 21.3
alpha3 = 42.5
alpha4 = 48.7

i = 0
for (alpha, n) in [(alpha1, 1), (alpha2, 1), (alpha3, 2), (alpha4, 2)]:
  i = i+1
  print i, 'tes Maximum:'
  print wurst(alpha, n)

lam1mean = 0
slam1mean = 0
E1mean = 0
sE1mean = 0
lam2mean = 0
slam2mean = 0
E2mean = 0
sE2mean = 0

(lam, slam, E, sE) = wurst2(alpha1, 1)
lam1mean = lam1mean + lam/slam**2
slam1mean = slam1mean + 1/slam**2
E1mean = E1mean + E/sE**2
sE1mean = sE1mean + 1/sE**2
(lam, slam, E, sE) = wurst2(alpha3, 2)
lam1mean = lam1mean + lam/slam**2
slam1mean = slam1mean + 1/slam**2
E1mean = E1mean + E/sE**2
sE1mean = sE1mean + 1/sE**2

lam1mean = lam1mean/slam1mean 
slam1mean = sqrt(1/slam1mean)
E1mean = E1mean/sE1mean 
sE1mean = sqrt(1/sE1mean)

(lam, slam, E, sE) = wurst2(alpha2, 1)
lam2mean = lam2mean + lam/slam**2
slam2mean = slam2mean + 1/slam**2
E2mean = E2mean + E/sE**2
sE2mean = sE2mean + 1/sE**2
(lam, slam, E, sE) = wurst2(alpha4, 2)
lam2mean = lam2mean + lam/slam**2
slam2mean = slam2mean + 1/slam**2
E2mean = E2mean + E/sE**2
sE2mean = sE2mean + 1/sE**2

lam2mean = lam2mean/slam2mean
slam2mean = sqrt(1/slam2mean)
E2mean = E2mean/sE2mean 
sE2mean = sqrt(1/sE2mean)

print 'Gewichtete Mittel:'
print 'lambda_1 = ', lam1mean, '+/- ', slam1mean
print 'lambda_2 = ', lam2mean, '+/- ', slam2mean
print 'E_1 = ', E1mean, '+/- ', sE1mean
print 'E_2 = ', E2mean, '+/- ', sE2mean
