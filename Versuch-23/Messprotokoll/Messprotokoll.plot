reset
set xlabel '$\theta [\unit{^\circ}]$'
set ylabel '$I [\unitfrac{Impulse}{s}]$'
set grid
set terminal epslatex color colortext size 15cm,7.7cm
set output 'Messung2.tex'
plot '../messung2.txt' using 1:2 ps 1 pt 2 title 'Messwerte'
set output 'Messung3-1.tex'
set key left
plot '../messung3.1.txt' using 1:2 ps 1 pt 2 title 'Messwerte für $\unit[23]{kV}$', '../messung3.1.txt' using 1:3 ps 1 pt 2 title 'Messwerte für $\unit[26]{kV}$','../messung3.1.txt' using 1:4 ps 1 pt 2 title 'Messwerte für $\unit[29]{kV}$','../messung3.1.txt' using 1:5 ps 1 pt 2 title 'Messwerte für $\unit[32]{kV}$','../messung3.1.txt' using 1:6 ps 1 pt 2 title 'Messwerte für $\unit[35]{kV}$'
set output 'Messung3-2.tex'
plot '../messung3.2.txt' using 1:2 ps 1 pt 2 title 'Messwerte für $\unit[23]{kV}$', '../messung3.2.txt' using 1:3 ps 1 pt 2 title 'Messwerte für $\unit[26]{kV}$','../messung3.2.txt' using 1:4 ps 1 pt 2 title 'Messwerte für $\unit[29]{kV}$','../messung3.2.txt' using 1:5 ps 1 pt 2 title 'Messwerte für $\unit[32]{kV}$','../messung3.2.txt' using 1:6 ps 1 pt 2 title 'Messwerte für $\unit[35]{kV}$'
set output 'Messung4-1.tex'
set key right bottom
plot '../messung4.1.txt' using 1:2 ps 2 pt 2 title 'Messwerte'
set output 'Messung4-2.tex'
set key right top
plot '../messung4.2.txt' using 1:2 ps 2 pt 2 title 'Messwerte'
set key right bottom
set output 'Messung5-1.tex'
plot '../messung5.1.txt' using 1:2 ps 2 pt 2 title 'Messwerte'
set key right top
set output 'Messung5-2.tex'
plot '../messung5.2.txt' using 1:2 ps 2 pt 2 title 'Messwerte'
set output 'Messung5-3.tex'
plot '../messung5.3.txt' using 1:2 ps 2 pt 2 title 'Messwerte'
set output 'Messung5-4.tex'
plot '../messung5.4.txt' using 1:2 ps 2 pt 2 title 'Messwerte'
set output 'Messung5-5.tex'
plot '../messung5.5.txt' using 1:2 ps 2 pt 2 title 'Messwerte'
set output
