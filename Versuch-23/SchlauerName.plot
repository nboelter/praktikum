reset
set xlabel 'Beschleunigungsspannung $U_A\,[\unit{kV}]$'
set ylabel 'Zählrate $N\,[\unit{Hz}]$'
set key left
set terminal epslatex color colortext
set output 'Beschleunigungsspannungundso-b.tex'
f(x) = a*(x-b)**(1.5)
g(x) = c*(x-d)**(1.5)
fit f(x) 'SchlauerName.txt' index 0 via a,b
fit g(x) 'SchlauerName.txt' index 1 via c,d
plot 'SchlauerName.txt' index 0 title 'Messwerte K$_\beta$' ps 2 lc 1, f(x) title 'Regression K$_\beta$' lt 1 lc 1
set output 'Beschleunigungsspannungundso-a.tex'
plot 'SchlauerName.txt' index 1 title 'Messwerte K$_\alpha$' ps 2 lc 2, g(x) title 'Regression K$_\alpha$' lt 1 lc 2
set output
