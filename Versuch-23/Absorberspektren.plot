reset
set xlabel 'Wellenlänge $\log(\lambda/\unit{m})$ [-]'
set ylabel 'Absorptionskoeffizient $\log(\mu/\rho\cdot\unit{kg}/\unit{m^2})$ [-]'
set key outside center top horizontal
a1=3
a2=3
a3=3
a4=3
f1(x) = a1*x+b1
f2(x) = a2*x+b2
f3(x) = a3*x+b3
f4(x) = a4*x+b4
fit f1(x) 'absorptionszeug.txt' u (log(2*201e-12*sin($1*pi/180))):(log((-1/0.00008)*log($3/$2)/2.7e6)) via a1,b1
fit f2(x) 'absorptionszeug.txt' u (log(2*201e-12*sin($1*pi/180))):(log((-1/0.000025)*log($4/$2)/7.14e6)) via a2,b2
fit f3(x) 'absorptionszeug.txt' u (log(2*201e-12*sin($1*pi/180))):(log((-1/0.000025)*log($5/$2)/5.769e6)) via a3,b3
fit f4(x) 'absorptionszeug.txt' u (log(2*201e-12*sin($1*pi/180))):(log((-1/0.000025)*log($6/$2)/8.908e6)) via a4,b4
set terminal epslatex color colortext
set output 'Absorptionsspektren.tex'
set xrange [-23.65:-22.9]
plot 'absorptionszeug.txt' u (log(2*201e-12*sin($1*pi/180))):(log((-1/0.00008)*log($3/$2)/2.7e6)) title 'Aluminium', \
     'absorptionszeug.txt' u (log(2*201e-12*sin($1*pi/180))):(log((-1/0.000025)*log($4/$2)/7.14e6)) title 'Zink', \
     'absorptionszeug.txt' u (log(2*201e-12*sin($1*pi/180))):(log((-1/0.000025)*log($5/$2)/5.769e6)) title 'Zinn', \
     'absorptionszeug.txt' u (log(2*201e-12*sin($1*pi/180))):(log((-1/0.000025)*log($6/$2)/8.908e6)) title 'Nickel', \
     f1(x) lw 2 lc 1 title 'Lin. Reg., Al', f2(x) lw 2 lc 2 title 'Lin. Reg., Zn', f3(x) lw 2 lc 3 title 'Lin. Reg., Sn', f4(x) lw 2 lc 4 title 'Lin. Reg., Ni' 
#     'absorptionszeug.txt' u (2*201e-12*sin($1*pi/180)):2 title 'ohne Absorber'
set output
