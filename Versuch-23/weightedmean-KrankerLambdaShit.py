from math import *

def weightedmean(data):
  mean = 0
  smean = 0
  for (value, error) in data:
    mean = mean + value/error**2
    smean = smean + 1/error**2
  mean = mean/smean
  smean = 1/sqrt(smean)
  return (mean, smean)

data = []
f = open('KrankerLambdaHochDreiZusammenhang-CamelCaseFTW.txt', 'r')
for line in f:
  if len(line) < 2:
    break
  data.append(map(float, line.strip().split(' ', 2)))

print weightedmean(data)
