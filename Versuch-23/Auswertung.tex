Zuerst werden die gemessenen Impulsraten um die Totzeit des Detektors korrigiert.
Das Röntgenspektrum der Röhre ohne Absorber wurde in Abbildung\,\ref{fig:spektrum} aufgetragen. 
\begin{figure}[ht]
\begin{center}
\def\svgwidth{8cm}
\input{Messung2.tex}
\end{center}
\caption{Spektrum der Röntgenröhre}
\label{fig:spektrum}
\end{figure}
Die Wellenlängen und zugehörigen Energien der Strahlung mit erster Beugungsordnung berechnen sich über die Bragg-Beziehung und die Einsteinsche Gleichung $E = h \nu$ zu:
\begin{empheq}[box=\fbox]{align*}
  \lambda_1 &= \unit[(1.31\pm0.20)\cdot 10^{-12}]{m}\\
  \lambda_2 &= \unit[(1.46\pm0.19)\cdot 10^{-12}]{m}\\
  E_1 &= \unit[(9473\pm1376)]{eV}\\
  E_2 &= \unit[(8490\pm1089)]{eV}
\end{empheq}
Für die zweite Ordnung erhält man:
\begin{empheq}[box=\fbox]{align*}
  \lambda_1 &= \unit[(1.36\pm0.08)\cdot 10^{-10}]{m}\\
  \lambda_2 &= \unit[(1.51\pm0.07)\cdot 10^{-10}]{m}\\
  E_1 &= \unit[(9130\pm499)]{eV}\\
  E_2 &= \unit[(8211\pm361)]{eV}
\end{empheq}
Insgesamt ergibt sich durch Bildung des Mittels:
\begin{empheq}[box=\fbox]{align*}
  \lambda_1 &= \unit[(1.35\pm0.07)\cdot 10^{-10}]{m}\\
  \lambda_2 &= \unit[(1.50\pm0.07)\cdot 10^{-10}]{m}\\
  E_1 &= \unit[(9170\pm468)]{eV}\\
  E_2 &= \unit[(8238\pm342)]{eV}
\end{empheq}
Dabei gehört $\lambda_1$ zur K$_{\alpha}$-Linie des Röntgenspektrums und $\lambda_2$ zur K$_\beta$-Linie.
Eine Unterscheidung zwischen K$_{\alpha_1}$ und K$_{\alpha_2}$ war durch die geringe Auflösung nicht möglich.
\subsection{Bestimmung des Planckschen Wirkungsquantums}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Beschleunigungsspannungundso-a.tex}
\end{center}
\caption{Abhängigkeit der Zählrate von der Anodenspannung bei der K$_\alpha$-Linie.}
\label{fig:beschl1}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Beschleunigungsspannungundso-b.tex}
\end{center}
\caption{Abhängigkeit der Zählrate von der Anodenspannung bei der K$_\beta$-Linie.}
\label{fig:beschl2}
\end{figure}
Wie in den augetragenen Messdaten (Abbildungen~\ref{fig:beschl1} und~\ref{fig:beschl2}) zu sehen, konnte der erwartete Zusammenhang aus Gleichung~\ref{eq:komisch} in etwa bestätigt werden. 

Mit den in Messung 3.3 aufgenommenen Werten für Winkel von $3\degree$ bis $15\degree$ wurde graphisch derjenige Wert bestimmt, bei dem der erste signifikante Anstieg der Zählrate zu sehen war.
Hieraus konnten mit der Bragg-Beziehung die jeweiligen Grenzwellenlängen $\lambda_\mathrm{gr}$ bestimmt werden.
Mit Hilfe des Duane-Hunt-Gesetzes~\ref{eq:grenzwertig} ergeben sich hieraus Werte für das Plancksche Wirkungsquantum und durch gewichtete Mittelung erhält man:
\begin{empheq}[box=\fbox]{align*}
  h &= \unit[(5.66\pm0.14)\cdot 10^{-34}]{J\,s}
\end{empheq}
\subsection{Bestimmung der Rydberg-Konstante}
Wie oben werden aus Messung 3.4 die Stellen der Absorptionskanten bestimmt:
\begin{align*}
\theta_\mathrm{Zn} &= \unit[18.8 \pm 0.1]{\degree} \\
\theta_\mathrm{Sn} &= \unit[6.2 \pm 0.1]{\degree}.
\end{align*}
Mit Hilfe der Bragg-Beziehung (\ref{eq:bragg}) können nun die Wellenlängen ausgerechnet werden. Dabei wird $n = 1$ gesetzt, da es sich um die niedrigste Ordnung handelt.
\begin{align*}
\lambda_\mathrm{Zn} &= \unit[(129.55 \pm 0.67)10^{12}]{m} \\
\lambda_\mathrm{Sn} &= \unit[(43.42 \pm 0.70)10^{12}]{m}
\end{align*}
Die Formel~(\ref{eq:rydberg}) lässt sich nun nach der Rydberg-Konstanten umstellen:
\begin{align*}
 R_\infty &= \frac{4}{3\lambda(Z-1)^2}.
\end{align*}
Mit dieser Formel und den bekannten Kernladungszahlen $Z = 30, 50$ für Zink bzw. Zinn ergeben sich zwei verschiedene Rydberg-Konstanten:
\begin{align*}
  R_{\infty,\mathrm{Zn}} &= \unit[(122.38 \pm 0.63)\cdot 10^{5}]{m^{-1}} \\
  R_{\infty,\mathrm{Sn}} &= \unit[(127.91 \pm 2.06)\cdot 10^{5}]{m^{-1}}.
\end{align*}
Als Gesamtergebnis wurde nun noch ein gewichtetes Mittel der beiden Werte berechnet.
\begin{empheq}[box=\fbox]{align*}
  R_\infty &= \unit[(122.85 \pm 0.61)\cdot 10^{5}]{m^{-1}}
\end{empheq}
Der angegebene Fehler ergibt sich dabei aus den fortgepflanzten Fehlern der Messdaten.
\subsection{Die Absorptionskoeffizienten}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Absorption.tex}
\end{center}
\caption{Absorptionskurven für verschiedene Absorber}
\label{fig:absorption}
\end{figure}
Durch Umstellen des Lambert-Beer-Gesetzes (Gleichung~\ref{eq:lambert-beer}) nach $\mu/\rho$ erhält man:
\begin{align*}
\frac{\mu}{\rho} &= -\frac{1}{x\rho}\log\frac{N}{N_0},
\end{align*}
wobei $x$ die Dicke des Absorbers, $N$ die Zählrate mit Absorber und $N_0$ ohne Absorber bedeuten.

Für alle Messwerte $N(\theta)$ aus Messung 3.5 kann so ein Wert für $\mu/\rho$ bestimmt werden.
Die ermittelten Werte wurden in Abbildung~\ref{fig:absorptionsspektren} gegen die zugehörige Wellenlänge $\lambda(\theta)$ aufgetragen.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Absorptionsspektren.tex}
\end{center}
\caption{Absorptionsspektren der verschiedenen Absorber in Abhängigkeit der Wellenlänge.}
\label{fig:absorptionsspektren}
\end{figure}
Aus den Steigungen der per linearer Regression erhaltenen Geradengleichungen (Tabelle\,\ref{tab:zwischenergebnisse}) ergibt sich der Exponent $a$ des Zusammenhangs $\mu/\rho\propto\lambda^a$.
Durch Bildung des gewichteten Mittelwertes erhält man:
\begin{empheq}[box=\fbox]{align*}
  a = 1.78\pm0.06
\end{empheq}
%Das ist der allerschlechteste Wert für 3, der je gemessen wurde :(
