\subsection{Trägheitsmoment aus Geometrie}
Mit den gemessenen Größen und Formel (\ref{eq:moment-of-inertia}) sowie (\ref{eq:zyl}) lassen sich nun die Trägheitsmomente bezüglich der Figurenachse $J_\mathrm{F}$ und um die vertikale Achse $J_\perp$ berechnen\footnote{Die Formeln für $J_\mathrm{Scheibe}, J_\mathrm{Stange}, J_\mathrm{Gegengewicht}$ sind eher unübersichtlich und folgen direkt aus (\ref{eq:zyl}), und befinden sich deshalb im Anhang 1.}.
\begin{align*}
J_\mathrm{F} &= \int\limits_V \rho r^2\D{V} = \int\limits_0^{r_\mathrm{Rad}}\int\limits_0^{l_6}\int\limits_0^{2\pi}\rho r^3 \D{\varphi}\D{z}\D{r} = \rho \frac{1}{4} r_\mathrm{Rad}^2 \cdot \underbrace{2\pi r_\mathrm{Rad}^2l_6}_V = \frac{1}{2}m_\mathrm{Rad} r_\mathrm{Rad}^2 \\
\sigma_{J_\mathrm{F}} &= \sqrt{\left(\frac{1}{2}r_\mathrm{Rad}^2\sigma_{m_\mathrm{Rad}}\right)^2 + \left(m_\mathrm{Rad} r_\mathrm{Rad} \sigma{r_\mathrm{Rad}}\right)^2} \\
J_\perp &= J_\mathrm{Scheibe} + J_\mathrm{Stange} + J_\mathrm{Gegengewicht} \\
\sigma{J_\perp} &= \sqrt{\sigma_{J_\mathrm{Scheibe}}^2 + \sigma_{J_\mathrm{Stange}}^2 + \sigma_{J_\mathrm{Gegengewicht}}^2} \\
\end{align*}
Als Ergebnisse für die Trägheitsmomente ergeben sich damit:

\begin{empheq}[box=\fbox]{align*}
  J_\mathrm{F} &= \unit[(99.3 \pm 0.4)\cdot10^{-4}]{kg\,m^2} \\
  J_\perp &= \unit[(595 \pm 13)\cdot10^{-4}]{kg\,m^2}.
\end{empheq}
\subsection{Trägheitsmoment aus Pendelschwingung}
Genau wie im Versuch 3 - \glqq{Trägheitsmoment}\grqq~wird nun aus der Periodendauer des physikalischen Pendels das Trägheitsmoment berechnet. Dabei muss das zusätzliche Trägheitsmoment des Zusatzgewichts natürlich abgezogen werden.
\begin{align*}
  T &= 2\pi \sqrt{\frac{J_\mathrm{F} + J_\mathrm{Zusatzgewicht}}{gm_4r_\mathrm{Rad}}} \\
  J_\mathrm{F} &= \frac{gT^2m_4r_\mathrm{Rad}}{4\pi^2} - m_4 r_\mathrm{Rad}^2 \\
  \sigma_{J_\mathrm{F}} &= \sqrt{\left(\frac{gTm_4r_\mathrm{Rad}}{2\pi^2}\sigma_T\right)^2 + J_\mathrm{F}\left(\frac{\sigma_{m_4}}{m_4}\right)^2 + \left(\frac{T^2gm_4\sigma_{r_\mathrm{Rad}}}{4\pi^2} - 2 m_4 r_\mathrm{Rad} \sigma_{r_\mathrm{Rad}}\right)^2}
\end{align*}
Mit dieser Methode ergibt sich nun:
\begin{empheq}[box=\fbox]{align*}
  J_\mathrm{F} &= \unit[(101 \pm 4)\cdot10^{-4}]{kg\,m^2}
\end{empheq}
\subsection{Trägheitsmoment aus Präzession}
Auch aus der Präzessionsfrequenz kann das Trägheitsmoment bestimmt werden, denn Umstellen von Formel (\ref{eq:praezession}) liefert:
\begin{align*}
J_\mathrm{F} &= \frac{g m l_2}{\omega_\mathrm{F}\omega_\mathrm{P}} \\
\sigma_{J_\mathrm{F}} &= \sqrt{ \left(\frac{g m \sigma_{l_2}}{\omega_\mathrm{F}\omega_\mathrm{P}}\right)^2 + \left(\frac{g \sigma_m l_2}{\omega_\mathrm{F}\omega_\mathrm{P}}\right)^2 + \left(\frac{g m l_2\sigma_{\omega_\mathrm{F}\omega_\mathrm{P}}}{(\omega_\mathrm{F}\omega_\mathrm{P})^2}\right)^2}
\end{align*}
Also gilt insbesondere für ein festes $m$:
\begin{align*}
\frac{1}{\omega_\mathrm{F}} \propto \omega_\mathrm{P}
\end{align*}
Die Präzessionsfrequenz sowie die reziproke Rotationsfrequenz des Rades sind für verschiedene Gewichte in den Abbildungen \ref{fig:praezession1}, \ref{fig:praezession2} sowie \ref{fig:praezession3} aufgetragen. Dabei wurde als Rotationsfrequenz der Mittelwert der vor bzw. nach dem Präzessionsvorgang gemessenen Frequenzen benutzt. Mit linearer Regression kann nun die Proportionalitätskonstante $\mu = \omega_\mathrm{F}\omega_\mathrm{P}$ bestimmt werden.
\begin{align*}
  \mu_\mathrm{60\,g}  &= (14.10 \pm 0.40)  \\
  \mu_\mathrm{80\,g}  &= (20.52 \pm 1.62)  \\
  \mu_\mathrm{100\,g} &= (26.19 \pm 0.78)
\end{align*}

Für die einzelnen Gewichte ergeben sich nun folgende Werte:

\begin{empheq}[box=\fbox]{align*}
  J_\mathrm{F,60\,g} &= \unit[(111.9 \pm 3.6)\cdot10^{-4}]{kg\,m^2} \\
  J_\mathrm{F,80\,g} &= \unit[(102.5 \pm 8.2)\cdot10^{-4}]{kg\,m^2} \\
  J_\mathrm{F,100\,g} &= \unit[(100.4 \pm 3.3)\cdot10^{-4}]{kg\,m^2}
\end{empheq}
\subsection{Vergleich der Trägheitsmomente}
Aus den oben bestimmten Trägheitsmomenten lässt sich nun ein gewichtetes Mittel bilden.
\begin{empheq}[box=\fbox]{align*}
  J_\mathrm{F} &= \unit[(103.0 \pm 1.7)\cdot10^{-4}]{kg\,m^2}
\end{empheq}
Die verschiedenen Zwischenergebnisse und der Mittelwert wurden in Abb.\;\ref{fig:traegheit-vergleich} aufgetragen.
\begin{figure}[ht]
\begin{center}
\def\svgwidth{8cm}
\input{Traegheitsmomente}
\end{center}
\caption{Vergleich der berechneten Trägheitsmomente}
\label{fig:traegheit-vergleich}
\end{figure}
\subsection{Nutation}
\begin{figure}[ht]
\begin{center}
\def\svgwidth{8cm}
\input{Nutation1}
\end{center}
\caption{Zusammenhang zwischen Rotationsfrequenz und Nutationsfrequenz}
\label{fig:nutation}
\end{figure}
Zunächst wird die Nutationsfrequenz $\omega_\mathrm{N}$ gegen die Rotationsfrequenz $\omega_\mathrm{R}$ aufgetragen (Abb.\,\ref{fig:nutation}). Auch hier werden die Rotationsfrequenzen wie oben gemittelt. Es ist ein linearer Zusammenhang ersichtlich, mit einer entsprechenden Regression erhält man:
\begin{align*}
  \frac{\omega_\mathrm{N}}{\omega_\mathrm{F}} &= (0.155 \pm 0.003)
\end{align*}
Mit dem geschätzten Durchmesser $d$ des Kreises, auf dem die Stangenspitze rotiert, kann nun der Öffnungswinkel des Nutationskegels bestimmt werden:
\begin{align*}
  \theta &= \arcsin\left(\frac{d}{2l_7}\right) = (8 \pm 8)^\circ.
\end{align*}
Zusammen mit der Formel (\ref{eq:nutation}) kann man jetzt auch den theoretischen Wert berechnen:
\begin{align*}
  \frac{\omega_\mathrm{N}}{\omega_\mathrm{F}} &= \frac{ J_\mathrm{F}}{\cos(\theta)J_\perp} = (0.168 \pm 0.008).
\end{align*}
Dabei wurden für die Fehlerabschätzung benutzt:
\begin{align*}
  \sigma_\theta &= \sqrt{\left(\frac{d\sigma_{l_7}}{l_7}\right)^2 + \sigma_d^2}\left(2l_7\sqrt{1-\frac{d^2}{4l_7^2}}\right)^{-1} \\
  \sigma_\frac{\omega_\mathrm{N}}{\omega_\mathrm{F}} &= \sqrt{\left(\frac{J_\mathrm{F}\sigma_{J_\perp}}{J_\perp}\right)^2 + \left(\frac{J_\mathrm{F} \sin(\theta)\sigma_\theta}{\cos(\theta)}\right)^2+\sigma_{J_\mathrm{F}}^2}\frac{1}{J_\perp \cos(\theta)}.
\end{align*}
