\subsection{Drehmoment und Drehimpuls}
Bewegt sich ein Körper mit Ortsvektor $\vec r$ auf einer Trajektorie an einem Punkt $\vec x$ vorbei, so ergibt sich aus seinem Impuls $\vec p$ und dem Ortsvektor $\vec r' = \vec r - \vec x$ des Körpers bezüglich des Punkts $\vec x$ der \emph{Drehimpuls} $\vec L$
\begin{align*}
  \vec L &= \vec r' \times \vec p.
\end{align*}
Ein konstanter Drehimpuls bedeutet also, dass sich der Körper in einer festen Ebene durch den Punkt $\vec x$ bewegt, welche zum Drehimpuls orthogonal ist. Ein Spezialfall ist die gleichförmige Kreisbewegung um $\vec x$, bei der auch die Beträge von $\vec p$ und $\vec r'$ konstant sind.\;\citep[S.\,27]{Gerthsen}

Ändert sich der Drehimpuls jedoch mit der Zeit, so wirkt ein \emph{Drehmoment} $\vec T$ auf den Körper, dieses ist wie folgt definiert:
\begin{align*}
  \vec T &=  \dot{\vec{L}}
\end{align*}
\subsection{Trägheitsmoment und Winkel}
Äquivalent zur trägen Masse bei der Translation lässt sich bei Rotationsbewegungen das \emph{Trägheitsmoment} $J$ definieren. Rotiert ein Körper mit der Winkelgeschwindigkeit $\vec \omega$ um $\vec x$, so definiert man:
\begin{align*}
  \vec L &= J \vec \omega
\end{align*}
Die Trägheitsmomente von Körpern bezüglich einer Drehachse lassen sich durch Integration bestimmen:\;\citep[S.\,74]{Gerthsen}
\begin{align}
  J = \int\limits_V r^2 \rho dV \label{eq:moment-of-inertia}
\end{align}
Dabei bezeichnet $r$ den Abstand vom betrachteten Volumenelement zur Rotationsachse und $\rho$ die Dichte des Volumenelementes.

Für einen Zylinder, der um eine Achse parallel zu seiner zweizähligen Symmetrieachse rotiert, ergibt sich nun zusammen mit dem \emph{Satz von Steiner} (Vergl. Versuch 3 \glqq Trägheitsmoment\grqq ):
\begin{align}
J_\mathrm{Zylinder} &= \int\limits_0^{r}\int\limits_0^{l}\int\limits_0^{2\pi}\rho (z^2+(r'\sin(\varphi))^2)\cdot r'\D{\varphi}\D{z}\D{r'} + \underbrace{m d^2}_\textrm{Satz von Steiner}  \nonumber \\
&= \underbrace{\rho \pi r^2}_{m} \left(\frac{1}{12}l^2+\frac{1}{4}r^2\right) + md^2 = m \left(\frac{1}{12}l^2+\frac{1}{4}r^2+d^2\right) \label{eq:zyl}
\end{align}
Dabei ist $l$ die Länge des Zylinders, $r$ sein Radius und $d$ der Abstand der beiden Achsen.
\subsection{Der Kreisel}
Ein symmetrischer \emph{Kreisel} ist ein rotationssymmetrischer Körper, welcher nur an einem Punkt durch eine seiner Symmetrieachsen befestigt ist. Diese Symmetrieachse wird \emph{Figurenachse} genannt.
Wird der Kreisel an seinem Schwerpunkt $S$ unterstützt, so wirkt keinerlei äußeres Drehmoment auf ihn, und er wird als \emph{momentenfreier Kreisel} bezeichnet\;\citep[S.\,88]{Gerthsen}. Bei einer gleichförmigen Rotation um die Figurenachse bleibt der Kreisel natürlich durch die Rotationssymmetrie momentenfrei.

\subsection{Nutation}
Wirkt ein Drehmoment auf den Kreisel, welches nicht mit der Figurenachse zusammenfällt, so stimmen danach die Drehimpulsachse und die Figurenachse nicht mehr überein, und die Figurenachse beginnt um die Impulsachse zu rotieren. Da beide natürlich weiterhin durch den festgehaltenen Punkt $S$ verlaufen müssen, bewegt sich die Figurenachse insgesamt auf einem Kegel, dessen Spitze mit $S$ zusammenfällt. Dieser Kegel wird \emph{Nutationskegel} genannt, der gesamte Vorgang \emph{Nutation}\;\citep[S.\,89]{Gerthsen}.

Die während der Nutation wirksamen Drehimpulse und Winkelgeschwindigkeiten lassen sich in eine Komponente parallel zur Figurenachse ($L_\mathrm{F}, \omega_\mathrm{F}$), und in eine Komponente senkrecht dazu ($L_\perp, \omega_\perp$) zerlegen. Für diese Achsen gelten im Allgemeinen verschiedene Trägheitsmomente:
\begin{align}
  L_\mathrm{F} = J_\mathrm{F} \omega_\mathrm{F} \label{eq:n1}\\
  L_\perp = J_\perp \omega_\perp. \label{eq:n2}
\end{align}
\begin{figure}[ht]
  \begin{center}
    \def\svgwidth{8cm}
    \input{nutation.pdf_tex}
  \end{center}
  \caption{Nutationsfrequenz und Drehimpuls}
  \label{fig:nutfreq}
\end{figure}
Die Figurenachse schliesst auf dem Nutationskegel einen Winkel von $\theta$ mit dem Drehimpuls (und damit $\vec \omega_\mathrm{N}$) ein. (Abb.\;\ref{fig:nutfreq})
Mit dem Öffnungswinkel $2\theta$ des Nutationskegels gilt nun für die Nutationsfrequenz $\vec \omega_\mathrm{N}$\;\citep[S.\,105]{Paus}:
\begin{align}
  \omega_\perp &= |\vec \omega_\mathrm{N}| \sin(\theta). \label{eq:n4}
\end{align}
Da aber $L_\mathrm{F}$ die Projektion von $\vec L$ auf die Figurenachse ist, gilt außerdem:
\begin{align} 
  L_\mathrm{F} &= |\vec L| \cos(\theta) \label{eq:n5} \\
  L_\perp &= |\vec L| \sin(\theta). \label{eq:n6}
\end{align} 
Einsetzen ergibt nun:
\begin{align}
  |\vec \omega_\mathrm{N}| &\stackrel{(\ref{eq:n4})}{=} \frac{\omega_\perp}{\sin(\theta)} \stackrel{(\ref{eq:n2})}{=} \frac{L_\perp}{J_\perp \sin(\theta)} \stackrel{(\ref{eq:n6})}{=} \frac{|\vec L|}{J_\perp} \stackrel{(\ref{eq:n5})}{=} \frac{L_\mathrm{F}}{\cos(\theta)J_\perp} \stackrel{(\ref{eq:n1})}{=} \frac{ J_\mathrm{F} \omega_\mathrm{F}}{\cos(\theta)J_\perp}. \label{eq:nutation}
\end{align}

\subsection{Präzession}
Versucht man, den Kreisel durch eine äußere Kraft $\vec F$ senkrecht zur Drehachse zu schwenken, so ergibt sich ein kippendes Drehmoment $\vec T = \vec r \times \vec F$, dabei ist $r$ der Kraftarm von $S$ zum Punkt, an dem die Kraft ansetzt. Dann steht aber $\vec T$ immer orthogonal auf dem Drehimpuls $\vec L$, so dass der Drehimpuls, und damit die momentane Drehachse beginnt, um $S$ zu rotieren. Dieser Vorgang wird Präzession genannt.\;\citep[S.\,89]{Gerthsen}

Wird die Präzession durch die Schwerkraft verursacht, so gilt für die Frequenz, mit der die Präzessionsbewegung ausgeführt wird:\;\citep[S.\,90]{Gerthsen}
\begin{align}
  \omega_p &= \frac{m g r}{J_\mathrm{F}\omega_\mathrm{F}} \label{eq:praezession}
\end{align}

