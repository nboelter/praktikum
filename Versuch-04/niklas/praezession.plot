reset
set key top left
set xlabel 'Reziproke Rotationsfrequenz $\frac{1}{\omega_\mathrm{F}}$ [s]'
set ylabel 'Präzessionsfrequenz $\omega_\mathrm{P}$ [$\frac{1}{\mathrm{s}}$]'
lin1(x) = m1*x+b1
lin2(x) = m2*x+b2
lin3(x) = m3*x+b3
fit lin1(x) 'Messwerte.txt' index 0 using ($1/(2*pi)):((pi)/$2):(0.2*pi/($2*$2)) via m1,b1
fit lin2(x) 'Messwerte.txt' index 1 using ($1/(2*pi)):((pi)/$2):(0.2*pi/($2*$2)) via m2,b2
fit lin3(x) 'Messwerte.txt' index 2 using ($1/(2*pi)):((pi)/$2):(0.2*pi/($2*$2)) via m3,b3
set terminal epslatex color colortext
set output 'Praezession1.tex'
plot 'Messwerte.txt' index 0 using ($1/(2*pi)):((pi)/$2):(0.2*pi/($2*$2)) lt 1 lc 1 title 'Messwerte' with errorbars, lin1(x) lt 1 lc 3 title "lineare Regression"
  
set output 'Praezession2.tex'
plot 'Messwerte.txt' index 1 using ($1/(2*pi)):((pi)/$2):(0.2*pi/($2*$2)) lt 1 lc 1 title 'Messwerte' with errorbars, lin2(x) lt 1 lc 3 title "lineare Regression"
  
set output 'Praezession3.tex'
plot 'Messwerte.txt' index 2 using ($1/(2*pi)):((pi)/$2):(0.2*pi/($2*$2)) lt 1 lc 1 title 'Messwerte' with errorbars, lin3(x) lt 1 lc 3 title "lineare Regression"
set output
!epstopdf Praezession1.eps
!epstopdf Praezession2.eps
!epstopdf Praezession3.eps
 
