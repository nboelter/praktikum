reset
set xlabel 'Rotationsfrequenz $\omega_\mathrm{F}$ [$\frac{1}{\mathrm{s}}$]'
set ylabel 'Nutationsfrequenz $\omega_\mathrm{P}$ [$\frac{1}{\mathrm{s}}$]'
lin1(x) = m1*x
set key right bottom
fit lin1(x) 'Messwerte.txt' index 3 using (2*pi/$1):((2*pi)/$2):(2*pi*0.05/($2*$2))  via m1
set terminal epslatex color colortext
set output 'Nutation1.tex'
plot 'Messwerte.txt' index 3 using (2*pi/$1):((2*pi)/$2):(2*pi*0.05/($2*$2)) lt 1 lc 1 title 'Messwerte' with errorbars, lin1(x) lt 1 lc 3 title "lineare Regression", 0.168448*x lt 1 lc 2 title 'theoretischer Wert ($\pm 1\sigma$)', 0.161697*x lt 2 lc 2 notitle, 0.1752*x lt 2 lc 2 notitle
  
set output
!epstopdf Nutation1.eps
 
