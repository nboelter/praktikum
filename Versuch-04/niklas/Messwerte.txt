# Rotationsperiode halbe Präzessionsperiode - Gewicht: 60g
0.09225  12.02
0.11325  9.99
0.13575  8.82
0.15775  7.67
0.1870  6.61


# Rotationsperiode halbe Präzessionsperiode - Gewicht: 80g
0.1020  8.66
0.11925  7.20
0.1365  6.35
0.15375  5.83
0.1755  5.35


# Rotationsperiode halbe Präzessionsperiode - Gewicht: 100g
0.07875  9.66
0.0930  8.13
0.1080  7.15
0.12175  6.14
0.13575  5.58


# Rotationsperiode Nutationsperiode
0.0960  0.665 
0.0760  0.483
0.0940  0.590
0.1085  0.687
0.13875  0.877


# Niklas: Traegheitsmomente # geo, pendel, 3x präzession

1 0.00993414  0.000324727
2 0.0101641 0.00071637
3 0.0111873 0.000351979
4 0.0102494 0.000818247
5 0.0100391 0.000323314
