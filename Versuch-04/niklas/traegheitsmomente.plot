reset
set xtics ("Geometrie" 1)
set xtics add ("Pendel" 2)
set xtics add ('$\unit[60]{g}$' 3)
set xtics add ('$\unit[80]{g}$' 4)
set xtics add ('$\unit[100]{g}$' 5)
set label 'Präzession' at first 4, screen 0.01 center
set xrange [0:6]
set ylabel 'Trägheitsmoment $J_\mathrm{F}$ [$10^-2$ kg m$^2$]'
set label 
mw  = 1.02949
smw = 0.01655 
set key right bottom
set terminal epslatex color colortext
set output 'Traegheitsmomente.tex'
plot 'Messwerte.txt' index 4 using 1:(100*$2):(100*$3) with errorbars lt 1 lc 3 title "Zwischenergebnisse", mw title 'Gewichteter MW ($\pm 1\sigma$)' lt 1 lc 0, mw-smw notitle lt 2 lc 0, mw+smw notitle lt 2 lc 0
set output
!epstopdf Traegheitsmomente.eps
