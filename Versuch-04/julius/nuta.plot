reset
set key top left
set xlabel 'Rotationsfrequenz $\omega_R\,[\unit{Hz}]$'
set ylabel 'Nutationsfrequenz $\omega_N\,[\unit{Hz}]$'
f(x) = a*x + b
fit f(x) 'Messwerte.txt' index 3 using (2*pi/($1)):(2*pi/($2)):(0.05*2*pi/(($2)*($2))) via a,b
set terminal epslatex color colortext
set output 'Nuta.tex'
plot 'Messwerte.txt' index 3 using (2*pi/($1)):(2*pi/($2)):(0.05*2*pi/(($2)*($2))) with errorbars title 'Messdaten', \
     f(x) title 'lineare Regression' lt 1 lc 9
set output
!epstopdf Nuta.eps
