from math import *

def Ipara(m, r):
  return (m*r**2)/2

def sIpara(m, sm, r, sr):
  return Ipara(m, r) * sqrt((sm/m)**2 + (2*sr/r)**2)

def Iperp(m, l, r, a):
  return m*((r**2)/4 + (l**2)/12 + a**2)

def sIperp(m, sm, l, sl, r, sr, a, sa):
  return sqrt((sm*Iperp(m, l, r, a)/m)**2 + (sr*m*r/2)**2 + (sl*m*l/6)**2 + (sa*a*m*2)**2)

print "Moment of Inertia - theoretical values"
print "--------------------------------------"
print "Key:"
print " MoI  - Moment of Inertia"
print " para - parallel"
print " perp - perpendicular"
print ""


mr = 1.324
smr = 0.0005
rr = 0.1225
srr = 0.00025
lr = 0.028
slr = 0.0005
ar = 0.116 + 0.014
sar = sqrt(0.002**2 + 0.0005**2)

Iparar = Ipara(mr, rr)
sIparar = sIpara(mr, smr, rr, smr)
Iperpr = Iperp(mr, lr, rr, ar)
sIperpr = sIperp(mr, smr, lr, slr, rr, srr, ar, sar)
print "MoI, para, wheel = ", Iparar, " +/- ", sIparar
print "MoI, perp, wheel = ", Iperpr, " +/- ", sIperpr
print ""


ms = 2*0.1532
sms = 0.002
rs = 0.004725
srs = 0.00025
ls = 0.097 + 0.043 + 0.142 + 0.116 + 0.028 + 0.0735
sls = sqrt(4*0.0005**2 + 2*0.002**2)
aS = 0.03225
saS = 0.006

Iparas = Ipara(ms, rs)
sIparas = sIpara(ms, sms, rs, srs)
Iperps = Iperp(ms, ls, rs, aS)
sIperps = sIperp(ms, sms, ls, sls, rs, srs, aS, saS)
print "MoI, para, staff = ", Iparas, " +/- ", sIparas
print "MoI, perp, staff = ", Iperps, " +/- ", sIperps
print ""


mg = 0.936
smg = 0.0005
rg = 0.03
srg = 0.00025
lg = 0.043
slg = 0.0005
ag = 0.142 + 0.0215
sag = 0.002 + 0.00025

Iparag = Ipara(mg, rg)
sIparag = sIpara(mg, smg, rg, srg)
Iperpg = Iperp(mg, lg, rg, ag)
sIperpg = sIperp(mg, smg, lg, slg, rg, srg, ag, sag)
print "MoI, para, weight = ", Iparag, " +/- ", sIparag
print "MoI, perp, weight = ", Iperpg, " +/- ", sIperpg
print ""


Iparages = Iparar + Iparas + Iparag
sIparages = sqrt(sIparar**2 + sIparas**2 + sIparag**2)
Iperpges = Iperpr + Iperps + Iperpg
sIperpges = sqrt(sIperpr**2 + sIperps**2 + sIperpg**2)
print "MoI, para, all = ", Iparages, " +/- ", sIparages
print "MoI, perp, all = ", Iperpges, " +/- ", sIperpges
print ""



def MoI(T):
  g = 9.81
  z = 0.1225 + 0.03
  m = 0.1
  return (T**2*g*z*m)/(4*pi**2) - m*z**2

def sMoI(T, sT):
  g = 9.81
  z = 0.1225 + 0.03
  sz = 0.0005
  m = 0.1
  sm = 0.0005
  return sqrt((sT*T*g*z*m/(2*pi**2))**2 + (sz*(T**2*g*m/(4*pi**2) - 2*m*z))**2 + (sm*MoI(T)/m)**2)

print "Moment of Inertia - measured values"
print "-----------------------------------"

T = (18.74 + 19.00 + 18.76 + 18.81 + 18.51 + 18.74)/60
sT = sqrt(6*0.5**2)/60
Imeasured = MoI(T)
sImeasured = sMoI(T, sT)
print "MoI, para, wheel, measured = ", Imeasured, " +/- ", sImeasured
print ""



def Iprecession(wpwr, m):
  r = 0.0783 + 0.043 + 0.142
  g = 9.81
  F = m*g
  return r*F/(wpwr)

def sIprecession(wpwr, swpwr, m):
  r = 0.0783 + 0.043 + 0.142
  sr = 2*0.0005 + 0.002
  g = 9.81
  sm = 0.0005
  F = m*g
  sF = g*sm
  return sqrt((sr*F/wpwr)**2 + (r*sF/wpwr)**2 + (swpwr*r*F/(wpwr**2))**2)

print "Moment of Inertia - precession"
print "------------------------------"

wpwr = 14.1003
swpwr = 0.3976
m = 0.06
I1 = Iprecession(wpwr, m)
sI1 = sIprecession(wpwr, swpwr, m)
print "MoI, para, all, precession = ", I1, " +/- ", sI1
wpwr = 20.5209
swpwr = 1.617
m = 0.08
I2 = Iprecession(wpwr, m)
sI2 = sIprecession(wpwr, swpwr, m)
print "MoI, para, all, precession = ", I2, " +/- ", sI2
wpwr = 26.1883
swpwr = 0.7799
m = 0.1
I3 = Iprecession(wpwr, m)
sI3 = sIprecession(wpwr, swpwr, m)
print "MoI, para, all, precession = ", I3, " +/- ", sI3
print ""
print "MoI, para, all, precession, mean = ", (I1/sI1**2 + I2/sI2**2 + I3/sI3**2)/(1/sI1**2 + 1/sI2**2 + 1/sI3**2), " +/- ", 1/sqrt(1/sI1**2 + 1/sI2**2 + 1/sI3**2)
print ""



print "Frequency of nutation"
print "---------------------"
print ""
phi = asin(3.0/(11.6+2.8+7.35))
print phi*360/(2*pi)
print "MoI, par / MoI, perp = ", Iparar/Iperpges, " +/- ", sqrt((sIparar/Iperpges)**2 + (sIperpges*Iparar/(Iperpges**2))**2)
print "MoI, par / (MoI, perp * cos(phi)) = ", Iparar/(Iperpges*cos(phi))
