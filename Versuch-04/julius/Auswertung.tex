\subsection{Trägheitsmomente aus der Geometrie}
Zunächst werden aus Gleichung~\ref{eq:mominertia_para} und~\ref{eq:mominertia_perp}
die Trägheitmomente des Kreisels aus seiner Geometrie bestimmt:
\begin{empheq}[box=\fbox]{align*}
	I_\| &= \unit[(9.93 \pm 0.04)\cdot10^{-3}]{kg\,m^2}\\
	I_\perp &= \unit[(59.50 \pm 1.01)\cdot10^{-3}]{kg\,m^2}
\end{empheq}
Der Fehler ergibt sich aus den angenommenen Fehlern der Gewichte $\sigma_\mathrm{Gew.} = \unit[0.5]{g}$
und der gemessenen Längen:
\begin{align*}
  \sigma_{I_\|} &= \sqrt{\left(\frac{1}{2}r_\mathrm{r}^2\sigma_{m_\mathrm{r}}\right)^2
+ \left(m_\mathrm{r} r_\mathrm{r} \sigma_{r_\mathrm{r}}\right)^2}\\
  \sigma_{I_{\perp, \mathrm{i}}} &= \sqrt{\left( \frac{\sigma_\mathrm{m,i}}{m_\mathrm{i}}I_{\perp, \mathrm{i}} \right)^2
+ \left( \frac12 \sigma_\mathrm{r,i}m_\mathrm{i}r_\mathrm{i} \right)^2
+ \left( \frac16 \sigma_\mathrm{l,i}m_\mathrm{i}l_\mathrm{i} \right)^2
+ \left( \frac12 \sigma_\mathrm{a,i}m_\mathrm{i}a_\mathrm{i} \right)^2}
\end{align*}
Insgesamt gilt dann für den Fehler des Trägheitsmoments bei zur Achse orthogonalem Drehimpuls:
\begin{align*}
  \sigma_{I_\perp} &= \sqrt{\sigma_{I_{\perp, \mathrm{r}}}^2 + \sigma_{I_{\perp, \mathrm{s}}}^2 + \sigma_{I_{\perp, \mathrm{g}}}^2}
\end{align*}
\subsection{Trägheitsmoment aus der Pendelbewegung}
Anschließend wird zur wiederholten Berechnung des Trägheitsmoments $I_\|$ mittels
$I_\mathrm{ges} = \frac1{4\pi^2} T^2 g z m - m z^2$ (siehe Versuch 3: \q{Das Trägheitsmoment})
die Periodendauer per arithmetischer Mittelung aus den Messwerten bestimmt.
In der Formel bezeichnet $T$ die Periodendauer, $g$ die Gravitationskonstante,
$z$ den Abstand von der Drehachse und $m$ die Masse des Zusatzgewichtes.
Es ergibt sich dann:
\begin{empheq}[box=\fbox]{align*}
	I_\| = \unit[(9.21 \pm 0.24)\cdot10^{-3}]{kg\,m^2}
\end{empheq}
\begin{align*}
  \sigma_{I_\|} &= \sqrt{\left(\sigma_T\frac{gTm_\mathrm{g}r_\mathrm{r}}{2\pi^2}\right)^2
+ \left(\frac{\sigma_{m_\mathrm{g}}gT^2r_\mathrm{r}}{4\pi^2} - \sigma_{m_\mathrm{g}} r_\mathrm{r}^2\right)^2
+ \left(\frac{\sigma_{r_\mathrm{r}}T^2gm_\mathrm{g}}{4\pi^2} - \sigma_{r_\mathrm{r}} 2 m_\mathrm{g} r_\mathrm{r}\right)^2}
\end{align*}
\subsection{Trägheitsmomente aus der Präzessionsbewegung}
Nun wird das Trägheitsmoment $I_\|$ erneut aus den Messungen zur Präzession bestimmt.
Hierfür werden die gemessenen Werte für die Präzessionsfrequenz gegen die inverse Rotationsfrequenz aufgetragen
und durch lineare Regression\footnote{via gnuplot, $\chi_\mathrm{red,1}^2 = 0.30$, $\chi_\mathrm{red,2}^2 = 1.03$, $\chi_\mathrm{red,3}^2 = 0.21$, siehe Abb.~\ref{fig:praez60g},~\ref{fig:praez80g} und~\ref{fig:praez100g}}
der Faktor $|\vec{\omega}_\mathrm{P}|\omega_\mathrm{R}$ bestimmt.
Für die jeweilige Rotationsfrequenz werden hier die Werte vor und nach der Messung gemittelt.
Mit Hilfe von Formel~\ref{eq:praez} können nun drei Werte für das Trägheitsmoment um die Figurenachse bestimmt werden:
\begin{empheq}[box=\fbox]{align*}
  I_{\|,1} &= \unit[(10.99 \pm 0.35)\cdot10^{-3}]{kg\,m^2} \\
  I_{\|,2} &= \unit[(10.07 \pm 0.81)\cdot10^{-3}]{kg\,m^2} \\
  I_{\|,3} &= \unit[(9.86 \pm 0.32)\cdot10^{-3}]{kg\,m^2} \\
  \Rightarrow \bar I_{\|} &= \unit[(10.35 \pm 0.23)\cdot10^{-3}]{kg\,m^2}
\end{empheq}
\begin{align*}
  \sigma_{I_\|} &= \sqrt{ \left(\frac{\sigma_{\mathrm{a}_\mathrm{z}} g m}{\omega_\mathrm{R}\omega_\mathrm{P}}\right)^2
+ \left(\frac{\sigma_m a_\mathrm{z}g}{\omega_\mathrm{R}\omega_\mathrm{P}}\right)^2
+ \left(\frac{\sigma_{\omega_\mathrm{R}\omega_\mathrm{P}}g m a_\mathrm{z}}{(\omega_\mathrm{R}\omega_\mathrm{P})^2}\right)^2}
\end{align*}
\subsection{Messung der Nutation}
Nach dem graphischen Auftragen der Nutationsfrequenz gegen die Rotationsfrequenz
und einer linearen Regression\footnote{via gnuplot, $\chi_\mathrm{red}^2 = 0.38$, siehe Abb.~\ref{fig:nuta}} kann die so erhaltene Steigung
mit $\omega_N / \omega_R$ identifiziert werden.
Die Rotationsfrequenz wird wie gehabt aus den jeweiligen Messungen vor und nach der Nutationsmessung gemittelt.
Es ergibt sich:
\begin{empheq}[box=\fbox]{align*}
  \frac{\omega_\mathrm{N}}{\omega_\mathrm{R}} = 0.145 \pm 0.017
\end{empheq}
Dieser Faktor stimmt wegen Gleichung~\ref{eq:nuta} theoretisch mit dem Wert für% $I_\| / I_\perp$ bzw.
$I_\| / (I_\perp \cos{\varphi})$ überein:
\begin{align*}
%  \frac{I_\|}{I_\perp} &= 0.168 \pm 0.004 \\
  \frac{I_\|}{I_\perp\cos{\varphi}} &= \frac{\omega_\mathrm{N}}{\omega_\mathrm{R}} = 0.169 \pm 0.007
\end{align*}
Mit einem abgeschätzten Durchmesser des Nutationskegels $d = \unit[6.0]{cm}$ und
der Entfernung vom Unterstützungspunkt zur Spitze der Stange am Ende des Rades $a$ wurde der Winkel $\varphi$ bestimmt durch:
\begin{align*}
  \varphi &= \arctan{\frac{d}{2\,a}} = (8.0 \pm 2.0)\degree\\
  \sigma_\varphi &= \sqrt{\frac{\left(\sigma_{a}d/a\right)^2
+ \sigma_d^2}{4a-d^2}} \\
  \sigma_\frac{\omega_\mathrm{N}}{\omega_\mathrm{R}} &= \frac1{I_\perp \cos(\varphi)}\sqrt{\left(I_\|\sigma_{I_\perp}/I_\perp\right)^2
+ \left(I_\| \tan(\varphi)\sigma_\varphi\right)^2
+ \sigma_{I_\|}^2}
\end{align*}
