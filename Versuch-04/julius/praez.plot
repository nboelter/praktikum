reset
set key top left
set xlabel 'Reziproke Rotationsfrequenz $1/\omega_\mathrm{R}\,[\unit{s}]$'
set ylabel 'Präzessionsfrequenz $\omega_\mathrm{P}$\,[\unit{Hz}]'
f(x) = a1*x + b1
g(x) = a2*x + b2
h(x) = a3*x + b3
fit f(x) 'Messwerte.txt' index 0 using (($1)/(2*pi)):(pi/($2)):(0.2*pi/(($2)*($2))) via a1,b1
fit g(x) 'Messwerte.txt' index 1 using (($1)/(2*pi)):(pi/($2)):(0.2*pi/(($2)*($2))) via a2,b2
fit h(x) 'Messwerte.txt' index 2 using (($1)/(2*pi)):(pi/($2)):(0.2*pi/(($2)*($2))) via a3,b3
set terminal epslatex color colortext
set output 'Praez60g.tex'
plot 'Messwerte.txt' index 0 using (($1)/(2*pi)):(pi/($2)):(0.2*pi/(($2)*($2))) with errorbars title 'Messdaten', \
     f(x) title 'Lineare Regression' lt 1 lc 9
set output 'Praez80g.tex'
plot 'Messwerte.txt' index 1 using (($1)/(2*pi)):(pi/($2)):(0.2*pi/(($2)*($2))) with errorbars title 'Messdaten', \
     g(x) title 'Lineare Regression' lt 1 lc 9
set output 'Praez100g.tex'
plot 'Messwerte.txt' index 2 using (($1)/(2*pi)):(pi/($2)):(0.2*pi/(($2)*($2))) with errorbar title 'Messdaten', \
     h(x) title 'Lineare Regression' lt 1 lc 9
set output
!epstopdf Praez60g.eps
!epstopdf Praez80g.eps
!epstopdf Praez100g.eps
