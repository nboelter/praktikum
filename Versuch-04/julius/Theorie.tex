Unter einem Kreisel versteht man einen beliebigen rotierenden starren Körper.
Da die allgemeine Beschreibung solcher Körper recht aufwendig sein kann,
beschränkt sich der Theorieteil auf sogenannte \emph{symmetrische Kreisel}, zu denen auch der Versuchskreisel zählt.
Sie besitzen eine Symmetrieachse, die \emph{Figurenachse}, um die die Rotation durchgeführt wird.
Der Kreisel wird dabei an Translationsbewegungen gehindert,
indem er im Schwerpunkt oder in einem anderen Punkt auf der Figurenachse festgehalten wird.
\subsection{Kräftefreie Kreisel}
Ein Kreisel heißt \emph{kräftefrei},
wenn alle auf den Kreisel wirkenden äußeren Drehmomente
sich zu Null addieren.
Um den Kreisel anfangs in Drehung zu versetzen,
benötigt man ein kurzzeitig von außen angreifendes Drehmoment.
Stimmt die Richtung hiervon mit der Richtung der Figurenachse überein,
beginnt der Kreisel,
um diese Achse zu rotieren und behält die Drehung bei,
bis äußere Kräfte (z.B. Reibung, Schwerkraft, etc) ihn beeinträchtigen.
\subsection{Nutation}
Fallen die Richtung von Figurenachse und anfänglichem Drehmoment nicht zusammen oder
wirkt zwischenzeitlich ein weiteres Drehmoment auf den Kreisel,
beschreibt die Figurenachse während der Drehung einen Kegelmantel
(siehe Abb.~\ref{fig:kegel}).
Die Spitze des Kegels liegt im Unterstützungspunkt.
In diesem Fall nennt man die Richtung der Winkelgeschwindigkeit $\vec{\omega}(t)$ die \emph{momentane Drehachse}.
Gewichtet man die parallel zur Figurenachse ($\vec{\omega}_\|$) und senkrecht auf ihr stehende ($\vec{\omega}_\perp$) Komponente der Winkelgeschwindigkeit mit den zugehörigen Hauptträgheitsmomenten $I_\|$ und $I_\perp$,
so erhält man den Gesamtdrehimpuls~\cite[20-8]{Feynman-I} (siehe auch Abb.~\ref{fig:achsen}):
\begin{align}
	\vec{L} = \vec{L}_\| + \vec{L}_\perp = I_\| \vec{\omega}_\| + I_\perp \vec{\omega}_\perp
\end{align}
Dieser definiert dann eine im Raum feste Achse, um die die Figurenachse und die momentane Drehachse jeweils Kegelmäntel verschiedener Öffnungswinkel beschreiben.
Den Kegel der Figurenachse nennt man \emph{Nutationskegel}, den der momentanten Drehachse \emph{Rastpolkegel}.
Entsprechend wird die Rotation der Figurenachse um die Drehimpulsachse als \emph{Nutation} bezeichnet.\\
Wegen $|\vec{L}| = |\vec{L}_\||(\cos{\varphi})^{-1} = |\vec{L}_\perp|(\sin{\varphi})^{-1}$ gilt
für die Winkelgeschwindigkeit der Nutation $\vec\omega_N$ bei einem Öffungswinkel des Nutationskegels von $\varphi$~\cite{Paus}:
\begin{align}
	|\vec\omega_\mathrm{N}| = \frac{\omega_\perp}{\sin{\varphi}} = \frac{I_\|}{I_\perp}\,\frac{\omega_\|}{\cos{\varphi}} \label{eq:nuta}
\end{align}
\subsection{Präzession}
Wirkt ab dem Zeitpunkt $t_0$ ein konstantes Drehmoment $\vec{D}$ in Bezug auf den Unterstützungspunkt auf den Kreisel, so ändert sich der Drehimpuls $\vec{L}$ mit der Zeit $t$
und es gilt \cite{Gerthsen}:
\begin{align}
	\vec{D} = \Dfrac{\vec{L}}{t} = \vec\omega_\mathrm{P} \times \vec{L}_0
\end{align}
$\vec{L}_0$ bezeichnet hier den Drehimpuls bei $t=t_0$
%Die Änderung des Drehimpulses ist folglich parallel zum angreifenden Drehmoment.
%Da sich mit der Zeit $t$ auch die Richtung des Drehimpulses um den Winkel $\varphi$ ändert,
%ergibt sich außerdem \cite{Gerthsen}:
%\begin{align}
%|\D\vec{L}| &= |\vec{L}|\D\varphi.\\
%\Rightarrow |\vec{D}| &= |\vec{L}| \Dfrac{\varphi}{t} = |\vec{L}| |\vec{\omega}_p|.
%\end{align}
und $\vec{\omega}_\mathrm{P}$ die Winkelgeschwindigkeit,
mit der sich der Drehimpuls um die das Drehmoment erzeugende Kraft $\vec{F}$ dreht.
\begin{figure}[htb!]
  \begin{center}
    \def\svgwidth{8cm}
    \input{Achsen.pdf_tex}
  \end{center}
  \caption{Die verschiedenen Achsen der Kreiselbewegung, verändert nach \cite{LP-Praezession}.}
  \label{fig:achsen}
\end{figure}
\begin{figure}[htb!]
  \begin{center}
    \def\svgwidth{8cm}
    \input{Kegel.pdf_tex}
  \end{center}
  \caption{Kegelmäntel der Achsenbewegungen beim Kreisel nach \cite{LP-Praezession}.}
  \label{fig:kegel}
\end{figure}
Diese Drehung bezeichnet man als \emph{Präzession}.
Beträgsmäßig folgt also, wenn $\alpha$ den Winkel zwischen Kraft- und Impulsachse bezeichnet:
\begin{align}
	|\vec{D}| &= |\vec{L}| |\vec{\omega}_\mathrm{P}| \sin{\alpha} = I \omega\,|\vec{\omega}_\mathrm{P}| \sin{\alpha},\\
	\Rightarrow I &= \frac{|\vec{D}|}{|\vec{\omega}_\mathrm{P}|\,\omega \sin{\alpha}}
  = \frac{|\vec{r}\times\vec{F}|}{|\vec{\omega}_\mathrm{P}|\,\omega \sin{\alpha}}
  = \frac{|\vec{r}|\cdot|\vec{F}|\,\sin{\alpha}}{|\vec{\omega}_\mathrm{P}|\,\omega \sin{\alpha}}
  = \frac{|\vec{r}|\cdot|\vec{F}|}{|\vec{\omega}_\mathrm{P}|\,\omega} \label{eq:praez}
\end{align}
mit $|\vec{L}| = I \omega$,
wobei $I$ das Trägheitsmoment des Kreisels um seine Figurenachse und $\omega$ der Betrag seiner Winkelgeschwindigkeit ist.
Natürlich gilt~\cite[20-6]{Feynman-I}: $|\vec{L}|=|\vec{L}_0|.$
\subsection{Versuchsspezifische Details}
In diesem Versuch betrachten wir einen Kreisel mit einer horizontalen Dreh- und Figurenachse.
Er besteht aus einem mittig unterstützten Stab, an dessen einem Ende sich ein Rad befindet,
dessen Masse durch ein zylindrisches Gewicht auf der gegenüberliegenden Seite ausgeglichen wird (siehe Abb.~\ref{fig:aufbau}).
Die gesamte Anordnung ist folglich zylindersymmetrisch um die Achse des Stabes.
Für die Trägheitsmomente eines Vollzylinders mit Masse $m$ und Radius $r$
durch seine Figuren- und eine dazu orthogonale Achse gilt~\cite[19-7]{Feynman-I}:
\begin{align}
	I_{\|} &= \frac12 m r^2 \label{eq:mominertia_para}\\
	I_{\perp} &= \frac14 m r^2 + \frac1{12} m l^2 \label{eq:mominertia_perp}
\end{align}
wobei $l$ die Länge des Zylinders bezeichnet.\\
Das Trägheitsmoment der gesamten Anordnung bei Drehung um die Figurenachse ergibt sich aus der Addition der einzelnen Trägheitsmomente,
wobei die Indizes wie folgt zu verstehen sind: Rad \textasciitilde~r, Stab \textasciitilde~s, Gewicht \textasciitilde~g.
Ordnet man den Objekten noch die Massen $m_i$ zu, so ergibt sich:
\begin{align}
	I_{\mathrm{ges}, \|} &= I_\mathrm{r} + I_\mathrm{s} + I_\mathrm{g}\\
	&= \frac12 m_\mathrm{r} r_\mathrm{r}^2 + \frac12 m_\mathrm{s} r_\mathrm{s}^2 + \frac12 m_\mathrm{g} r_\mathrm{g}^2
\end{align}
Haben die Bestandteile die Längen $l_\mathrm{i}$ und die Abstände vom Unterstützungspunkt $a_\mathrm{i}$, 
so folgt für eine vertikale Achse durch den Unterstützungspunkt mit Hilfe des \mensch{Steiner}schen Satzes~\cite{Gerthsen}:
\begin{align}
	I_{\mathrm{ges}, \perp} &= (I_{\mathrm{r}, \perp} + m_\mathrm{r} a_\mathrm{r}^2) + (I_{\mathrm{s}, \perp} + m_\mathrm{s} a_\mathrm{s}^2) + (I_{\mathrm{g}, \perp} + m_\mathrm{g} a_\mathrm{g}^2) \\
	&= m_\mathrm{r} \left(\frac14 r_\mathrm{r}^2 + \frac1{12} l_\mathrm{r}^2 + a_\mathrm{r}^2\right)
		+ m_\mathrm{s} \left(\frac14 r_\mathrm{s}^2 + \frac1{12} l_\mathrm{s}^2 + a_\mathrm{s}^2\right)
		+ m_\mathrm{g} \left(\frac14 r_\mathrm{g}^2 + \frac1{12} l_\mathrm{g}^2 + a_\mathrm{g}^2\right)
\end{align}
Es bezeichnen die $m_\mathrm{i}$ die jeweiligen Massen, die $l_\mathrm{i}$ die Längen und die $a_\mathrm{i}$ die Abstände von der Drehachse.\\
Wird nun auf der Seite des Ausgleichgewichts ein zusätzliches Gewicht der Masse $m_\mathrm{z}$ am Stab befestigt,
so bewirkt die Schwerkraft $\vec{F}_\mathrm{G}$ ein Drehmoment $\vec{D}$:
\begin{align}
	\vec{D} &= \vec{F}_\mathrm{G} \times \vec{a}_\mathrm{z} = m_\mathrm{z}\,\vec{g} \times \vec{a}_\mathrm{z} \\
	\Rightarrow |\vec{D}| &= g\,m_\mathrm{z}\,a_\mathrm{z}\,\sin{\alpha}
\end{align}
Hier bezeichnet $\vec{a}_\mathrm{z}$ den Vektor vom Unterstützungspunkt zum Anhängepunkt des Gewichtes und
$\alpha$ den Winkel zwischen $\vec{F}$ und $\vec{a}_\mathrm{z}$.
