# Teil A
# Messungen 1 & 2: Gesamtvergrößerung des Mikroskops für beide Okulare
# Objektmikrometerskala [0.5mm], reelle Skala [cm], Objektmikrometerskala [0.5mm], Größe des reellen Zwischenbilds [mm]
# 8x Okular
1.5 6.4  2.0 10.9
2.0 10.4 2.0 10.3
2.0 9.2  2.0 11.0


# 12x Okular
1.5 12.0 2.0 10.75
1.0 5.8  2.0 10.8
1.5 8.9  2.0 10.25


# Messung 3.a & 3.b: Objektivvergrößerung
# Tubuslänge: 91.4 mm
# Abstand zum Objektiv: 76.6 mm
# Objektmikrometerskala [0.5mm], Bild bei Mattscheibe auf Tubus [mm], Objektmikrometerskala [0.5mm], Bild bei Mattscheibe auf unterem Rand [mm]
3.0 15.4  10 20.35
3.0 14.95 10 20.90
3.0 14.6  10 20.50
#
#
# Messung 4: Haarvermessung
# Maßstab Objektmikrometer/Okularmikrometer: 1/4
# Haardicke: 3 mal gemessen, immer 0.8 Skt. des Okularmikrometers
##############
# Teil B
# Vergrößerung Okular: 10x
# Brennweite Okular: 1 cm
# Tubuslänge: 18.5 cm
# Messung 1: Abstand Spalt-Objekt: 64 mm
#         2.a: Abstand Spalt-Objekt: 61.8 mm
#         2.b: Spaltbreite: 0.5 mm
#         3: Anzahl sichtbarer Linien: 11
#            Länge des Plexiglasstabes: 49.95 mm
