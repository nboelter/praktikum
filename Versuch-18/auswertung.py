from math import *

def weightedmean(data):
  mean = 0
  smean = 0
  for (value, error) in data:
    mean = mean + value/error**2
    smean = smean + 1/error**2
  mean = mean/smean
  smean = 1/sqrt(smean)
  return (mean, smean)

#Auswertungspunkt 1###########################################################
sG = 0.05e-3    #Fehler Gegenstandsgroesse

sBS = 0.5e-2   #Fehler Bildgroesse bei Skalenokular 8x und 12x
sBS12 = sBS     #
B1S = 6.4e-2    #Bildgroessen bei Skalenokular 8x
B2S = 10.4e-2   #
B3S = 9.2e-2    #
G1S = 1.5*0.5e-3    #Gegenstandsgroessen bei Skalenokular 8x
G2S = 2.0*0.5e-3    #
G3S = 2.0*0.5e-3    #
B1S12 = 12.0e-2 #Bildgroessen bei Skalenokular 12x
B2S12 = 5.8e-2  #
B3S12 = 8.9e-2  #
G1S12 = 1.5*0.5e-3  #Gegenstandsgroessen bei Skalenokular 12x
G2S12 = 1.0*0.5e-3  #
G3S12 = 1.5*0.5e-3  #

sBM = 0.1e-3 #Fehler Bildgroesse bei Mattscheibenokular
B1M = 10.9e-3 #Bildgroessen bei Mattscheibenokular
B2M = 10.3e-3 #
B3M = 11.0e-3 #
GM = 2.0*0.5e-3   #Gegenstandsgroesse bei Mattscheibenokular

#Arrays zur Berechnung des gewichteten Mittels via oben definierter Fkt.
vgS = [(B1S/G1S, B1S/G1S*sqrt((sBS/B1S)**2 + (sG/G1S)**2)), (B2S/G2S, B2S/G2S*sqrt((sBS/G2S)**2 + (sG/B2S)**2)), (B3S/G3S, B3S/G3S*sqrt((sBS/B3S)**2 + (sG/G3S)**2))]
vgS12 = [(B1S12/G1S12, B1S12/G1S12*sqrt((sBS12/B1S12)**2 + (sG/G1S12)**2)), (B2S12/G2S12, B2S12/G2S12*sqrt((sBS12/B2S12)**2 + (sG/G2S12)**2)), (B3S12/G3S12, B3S12/G3S12*sqrt((sBS12/B3S12)**2 + (sG/G3S12)**2))]
vgM = [(B1M/GM, B1M/GM*sqrt((sBM/B1M)**2 + (sG/GM)**2)), (B2M/GM, B2M/GM*sqrt((sBM/B2M)**2 + (sG/GM)**2)), (B3M/GM, B3M/GM*sqrt((sBM/B3M)**2 + (sG/GM)**2))]

print 'Vergroesserung Skalenokular 8x (Wert, Fehler): ', weightedmean(vgS)
print 'Vergroesserung Skalenokular 12x (Wert, Fehler): ', weightedmean(vgS12)
print 'Vergroesserung Mattscheibenokular (Wert, Fehler): ', weightedmean(vgM)

#Berechnung der Okularvergroesserungen fuer 8x und 12x Okular
(VM, sVM) = weightedmean(vgM)
(VS, sVS) = weightedmean(vgS)
V = VS/VM
sV = V*sqrt((sVS/VS)**2 + (sVM/VM)**2)
print 'Okularvergroesserung 8x: ', (V, sV)
(VS, sVS) = weightedmean(vgS12)
V = VS/VM
sV = V*sqrt((sVS/VS)**2 + (sVM/VM)**2)
print 'Okularvergroesserung 12x: ', (V, sV)

#Auswertungspunkt 2###########################################################
t = 91.8e-3+76.6e-3
st = 1e-3

sB = 0.5e-2
sG = 0.05*0.05e-3

Bo1 = 15.4e-3
Bo2 = 14.95e-3
Bo3 = 14.6e-3
Go = 3*0.5e-3

Bu1 = 20.35e-3
Bu2 = 20.9e-3
Bu3 = 20.5e-3
Gu = 10*0.5e-3

Vo1 = Bo1/Go
Vo2 = Bo2/Go
Vo3 = Bo3/Go
Vu1 = Bu1/Gu
Vu2 = Bu2/Gu
Vu3 = Bu3/Gu

sVo1 = Vo1*sqrt((sB/Bo1)**2 + (sG/Go)**2)
sVo2 = Vo2*sqrt((sB/Bo2)**2 + (sG/Go)**2)
sVo3 = Vo3*sqrt((sB/Bo3)**2 + (sG/Go)**2)
sVu1 = Vu1*sqrt((sB/Bu1)**2 + (sG/Gu)**2)
sVu2 = Vu2*sqrt((sB/Bu2)**2 + (sG/Gu)**2)
sVu3 = Vu3*sqrt((sB/Bu3)**2 + (sG/Gu)**2)

f1 = t/(Vo1-Vu1)
f2 = t/(Vo2-Vu2)
f3 = t/(Vo3-Vu3)

sf1 = f1*sqrt((sVo1/(Vo1-Vu1))**2 + (sVu1/(Vo1-Vu1))**2 + (st/t)**2)
sf2 = f2*sqrt((sVo2/(Vo2-Vu2))**2 + (sVu2/(Vo2-Vu2))**2 + (st/t)**2)
sf3 = f3*sqrt((sVo3/(Vo3-Vu3))**2 + (sVu3/(Vo3-Vu3))**2 + (st/t)**2)

F= [(f1, sf1), (f2, sf2), (f3, sf3)] 

print 'Brennweite des Objektivs (Wert, Fehler): ', weightedmean(F)
