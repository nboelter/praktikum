from math import *
# Teil B
# Auswertungspunkt 3#########################################
b = 50e-3     #Spaltbreite
sb = 5e-3
L = 64     #Abstand Blende-Glasmassstab
sL = 0.5
lam = 650e-6  #Wellenlaenge des verwendeten Lichts

print 'Aufloesungsvermoegen [mm^-1]: A = ', b/(L*lam), '+/- ', b/(L*lam)*sqrt((sb/b)**2 + (sL/L)**2)
#
# Auswertungspunkt 5#########################################
d = 5.5e-3
sd = 0.25e-3
L = 49.95e-3
sL = 0.05e-3
np = 1.49
sinalph = (d/2.0)/sqrt( (d**2/4.0) + L**2 )
N = np*sinalph
sN = sqrt( (sL*(4*L*d/(4*L**2 + d**2)**(3.0/2.0)))**2 + (sd*(4*L**2/(4*L**2 + d**2)**(3.0/2.0)))**2 )

print 'Numerische Apertur: N = ', N, '+/- ', sN
print 'Aufloesung A = ', N/lam, '+/- ', sN/lam
