\subsection{Das Zeiss-Mikroskop}
\subsubsection{Gesamt-, Objektiv- und Okularvergrößerung}
Es werden wie gehabt das Okular mit Skala mit dem Index 1 und das Okular mit 12.5facher Vergrößerung mit dem Index 2 bezeichnet.
Zur Bestimmung der Gesamtvergrößerungen $V_\mathrm{ges,1}$ und $V_\mathrm{ges,2}$ ergibt sich mit Gleichung~\ref{eq:gesVG}:
\begin{empheq}[box=\fbox]{align*}
  V_\mathrm{ges,1} &= 89.5\pm5.4\\
  V_\mathrm{ges,2} &= 131.2\pm7.1
\end{empheq}
Die Objektivvergrößerung erhält man durch dieselbe Formel mit dem Unterschied, dass nun die Größe des reellen Zwischenbildes $B$ verwendet wird:
\begin{empheq}[box=\fbox]{align*}
  V_\mathrm{Objektiv} &= 10.72\pm0.32
\end{empheq}
Hieraus lassen sich nun die beiden Okularvergrößerungen $V_1$ und $V_2$ berechnen:
\begin{empheq}[box=\fbox]{align*}
  V_1 &= 8.35\pm0.56\\
  V_2 &= 12.24\pm0.76
\end{empheq}
\subsection{Brennweite des Objektivs}
Zur Bestimmung der Brennweite des Objektivs nutzen wir zunächst die Linsenmachergleichung $f^{-1} = b^{-1} + g^{-1}$, die eine gute Näherung für dünne Linsen darstellt.
Nun seien $V_\mathrm{oben}$ und $V_\mathrm{unten}$ die Objektivvergrößerungen mit Tubus (oben) und ohne Tubus (unten).
Dann gilt mit der Bildweite $b$, der Gegenstandsweite $g$ und der Tubuslänge $t$:
\begin{align*}
  V_\mathrm{unten} &= \frac{b}{g}
  &V_\mathrm{oben} = \frac{b+t}{g}.
\end{align*}
Einsetzen liefert nun:
\begin{align*}
  f &= \frac{t}{V_\mathrm{oben}-V_\mathrm{unten}}.
\end{align*}
Durch Bildung des gewichteten Mittels aus den drei Messreihen erhalten wir so die Objektivbrennweite:
\begin{empheq}[box=\fbox]{align*}
  f &= \unit[(28.5\pm9.8)]{mm}
\end{empheq}
\subsection{Die Vermessung des Haars}
An dieser Stelle möchten wir ein herzliches Dankeschön an Markus T. aussprechen, der uns freundlicherweise ein Haar zur Verfügung gestellt hat.

Die Eichung des Okularmikrometers ergibt einen Skalenfaktor $k$ von:
\begin{align*}
  k &= \frac{\unit[0.5]{mm}}{\unit[4]{Skt.}} = 0.125\frac{\unit{mm}}{\unit{Skt.}}
\end{align*}
Hiermit lässt sich die Dicke des Haares nun leicht bestimmen:
\begin{align*}
  D_\mathrm{Haar} &= k\cdot \unit[0.8]{Skt.} = \unit[(100\pm4)]{\mu m}.
\end{align*}
\subsection{Die optische Schiene}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Strahlengang-Aufloesungsbestimmung.pdf_tex}
\end{center}
\caption{Strahlengang bei der Bestimmung des Auflösungsvermögens.}
\label{fig:strahlen1}
\end{figure}
Aus der bekannten Skaleneinteilung des Glasmaßstabes von $s = \unit[0.5]{mm}$ lässt sich die theoretische Auflösung $A = s^{-1}$ des Mikroskops berechnen, da dieses genau so eingestellt wurde, dass zwei Linien auf dem Maßstab gerade nicht mehr aufgelöst werden können:
\begin{align*}
  A &= \unit[2]{mm^{-1}}.
\end{align*}
Zur experimentellen Bestimmung des Auflösungsvermögens wird die gemessene Spaltbreite $b' = \unit[500]{\mu m}$ verwendet.
Berechnet man die Vergrößerung des benutzten 10x-Okulars mit ein, so erhält man die reale Spaltbreite:
\begin{align}
 b &= \unit[50]{\mu m}.
\end{align}
Nun kann man den gemessenen Abstand von Blende und Glasmaßstab verwenden, um aus der Geometrie der Anordnung den Öffnungswinkel $\alpha$ zu bestimmen:
\begin{align*}
  \alpha &= \arcsin\frac{b}{L}.
\end{align*}
Hierbei ist die Länge des Tubus $L = \unit[64]{mm}$.
Über die gefilterte Wellenlänge $\lambda = \unit[650]{nm}$ und mit $n_\mathrm{Luft}\approx~1$ erhält man nun das Auflösungsvermögen:
\begin{align*}
  A &= \frac{\sin\alpha}{\lambda} = \frac{b}{\lambda L}.
\end{align*}
Einsetzen ergibt dann:
\begin{empheq}[box=\fbox]{align*}
  A &= \unit[(1.20\pm0.13)]{mm^{-1}}
\end{empheq}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Strahlengang-Plexiglas.pdf_tex}
\end{center}
\caption{Strahlengang bei der Messung am Plexiglasmaßstab.}
\label{fig:strahlen2}
\end{figure}
Nun wird die numerische Apertur und das Auflösungsvermögen des Mikroskops mit Plexiglasstab bestimmt (zur Benennung der verwendeten Variablen siehe auch Abb.~\ref{fig:strahlen2}).

Hierzu wird zunächst über die Anzahl der gezählten sichtbaren Skaleneinteilungen $z=11$ auf dem Maßstab sowie ihren jeweiligen Abstand $s=\unit[0.5]{mm}$ die Länge $d = z\cdot s = \unit[5.5]{mm}$ bestimmt.
Mit der Länge des Plexiglasstabs $L$ erhält man jetzt aus geometrischen Gründen:
\begin{align*}
  \sin\alpha_\mathrm{P} &= \frac{d/2}{\sqrt{d^2/4+L^2}}.
\end{align*}
Mit Hilfe des Snelliusschen Brechungsgesetzes ergibt sich hieraus die numerische Apertur:
\begin{align*}
  N_A &= n_\mathrm{Luft}\sin\alpha = n_\mathrm{P}\sin\alpha_\mathrm{P}\\
    &= n_\mathrm{P}\frac{d/2}{\sqrt{d^2/4+L^2}}.
\end{align*}
Einsetzen der gemessenen Werte und Verwendung von Formel (\ref{eq:xmin}) für die Auflösung ergibt folglich:
\begin{empheq}[box=\fbox]{align*}
  N_A &= (82\pm3)\cdot 10^{-3}\\
  A &= \unit[(126\pm4)]{mm^{-1}}
\end{empheq}
