\subsection{Ablenkungswinkel}
Die Fehler der gemessenen Werte wurden als ein Skalenteil angenommen. Alle weiteren Fehler entstehen wie immer durch \mensch{Gauß}sche Fehlerfortpflanzung.

Zuerst werden die tatsächlichen Ablenkungswinkel berechnet (Tab.\,\ref{tab:ablenkungswinkel}). Dabei wird zum abgelesenen Wert der Winkelskala $\varphi_0$ noch die sich durch die Abweichung des Feintriebs $x$ von der Nulllage $x_0$ ergebende zusätzliche Ablenkung hinzuaddiert. 
Es folgt aus der Geometrie:
\begin{align*}
  \delta &= \varphi_0 - \arctan\left(\frac{x-x_0}{L}\right). \\
\end{align*}
Dabei ist $L$ der Abstand zwischen Objektiv und Okular.
\subsection{Prismenspektrometer}
Der Winkelabstand der beiden gelben Linien lässt sich leider nicht berechnen, da die beiden Linien gar nicht getrennt aufgelöst werden konnten. Stattdessen wird mit den ausgerechneten Ablenkungswinkeln der Winkelabstand der gelben und grünen Linien bestimmt:
\begin{empheq}[box=\fbox]{align*}
  \Delta\delta_\mathrm{Kronglas} &= \unit[(0.13 \pm 0.15)]{\degree} \\
  \Delta\delta_\mathrm{leichtes Flintglas} &= \unit[(0.31 \pm 0.15)]{\degree}
\end{empheq}
Mittelt man die Literaturwerte der Praktikumsanleitung \citep[S.\,173]{Anleitung} für die Wellenlängen der gelben Spektrallinien und subtrahiert die Wellenlänge der grünen Spektrallinie, so ergibt sich eine Wellenlängendifferenz von $\Delta\lambda = \unit[31.945]{nm}$.
Nun können die Winkeldispersionen mittels Formel (\ref{eq:Winkeldispersion}) berechnet werden.

Diese können nun mit den Literaturwerte aus der Praktikumsanleitung \citep[S.\,173]{Anleitung} verglichen werden:
\begin{empheq}[box=\fbox]{align*}
 \Dfrac{\delta}{\lambda}_\mathrm{Kronglas} &= \unitfrac[(4.17 \pm 4.43)\cdot10^6]{\degree}{m} \\
 \Dfrac{\delta}{\lambda}_\mathrm{Literaturwert} &= \unitfrac[3.47\cdot10^6]{\degree}{m} \\ 
 \Dfrac{\delta}{\lambda}_\mathrm{leichtes Flintglas} &= \unitfrac[(9.78 \pm 4.45)\cdot10^6]{\degree}{m} \\
 \Dfrac{\delta}{\lambda}_\mathrm{Literaturwert} &= \unitfrac[9.01\cdot10^6]{\degree}{m}
\end{empheq}
Der Abbildungsmaßstab des Linsensystems hängt von Brennweite $F$ und Gegenstandsweite $G$ ab \citep[S.\,495]{Gerthsen}, vergl. auch \emph{Versuch 18 - Das Mikroskop}.
\begin{align*}
  \beta &= \frac{F}{G} = \frac{f_\mathrm{Objektiv}-f_\mathrm{Okular}}{f_2} \stackrel{\sim}{=} \frac{L}{f_2} \\
\end{align*}
Dabei ist $f_2$ die Brennweite der Linse, die die vom Spalt ausgehenden Elementarwellen in Ebene Wellen umwandelt, denn der Spalt ist in der Brennebene dieser Linse. Da am Okular keine Brennweite angegeben war, wurde $f_\mathrm{Objektiv}-f_\mathrm{Okular}$ durch die Entfernung der Linsen $L$ abgeschätzt.

Zusammen mit Formel (\ref{eq:Basis}) und der gemessenen Breite $b$ der Abbildung des Spalts lässt sich nun die effektive Basisbreite ausrechnen:
\begin{align*}
  B &= \frac{2b}{\beta} \frac{\sin\left(\frac{\epsilon}{2}\right)}{\cos\left(\frac{\epsilon+\delta_\mathrm{Gelb}}{2}\right)} \\
\end{align*}
\begin{empheq}[box=\fbox]{align*}
  B_\mathrm{Kronglas} &= \unit[(2.36\pm 0.18)]{mm} \\
  B_\mathrm{leichtes Flintglas} &= \unit[(4.44\pm 0.18)]{mm}
\end{empheq}

Mit der effektiven Basisbreite, den Literaturwerten \citep[S.\,173]{Anleitung} für die Dispersion und Formel (\ref{eq:PrismaAufloesung}) lässt sich nun das Auflösungsvermögen berechnen. Da die Wellenlängen der gelben Linien bekannt sind, kann das Auflösevermögen auch aus der Definition berechnet werden.
\begin{empheq}[box=\fbox]{align*}
  A_\mathrm{Kronglas} &= \unit{(92.1 \pm 5.6)} \\
  A_\mathrm{leichtes Flintglas} &= \unit{(410 \pm 29)} \\
  A &= \frac{\lambda}{\Delta\lambda} = 274
\end{empheq}

Würde man die gesamte Breite des Prismas als Spektrometer benutzen, also $B = \unit[(4.95 \pm 0.05)]{cm}$, so vergrößert sich das Auflösungsvermögen natürlich entsprechend. Ausgehend von den gemessenen Ablenkungswinkeln für gelbes Licht lässt sich dieses nun berechnen, und damit auch die kleinste Wellenlängendifferenz, die man noch auflösen kann.
\begin{empheq}[box=\fbox]{align*}
  A_\mathrm{Kronglas,max} &= 6331 \pm 117 \\
  \Delta\lambda_\mathrm{Kronglas,min} &= \nicefrac{\lambda}{A} = \unit[(9.13 \pm 0.17)\cdot 10^{-2}]{nm} \\
  A_\mathrm{leichtes Flintglas,max} &= 5768 \pm 123 \\ 
  \Delta\lambda_\mathrm{Kronglas,min} &= \nicefrac{\lambda}{A} = \unit[(10.02 \pm 0.22)\cdot 10^{-2}]{nm} 
\end{empheq}
\subsection{Gitterspektrometer}
Mittels Formel (\ref{eq:GitterMaxima}) und den Literaturwerten für die Wellenlängen \citep[S.\,173]{Anleitung} lassen sich nun leicht die Gitterkonstanten $a$ berechnen:
\begin{align*}
  d &= \frac{k \lambda}{\sin(\phi)}
\end{align*}
Die Zwischenergebnisse sind in Tab.\,\ref{tab:Gitterkonstanten} zusammengefasst. Der gewichtete Mittelwert beträgt:
\begin{empheq}[box=\fbox]{align*}
  d &= \unit[(1.028 \pm 0.009)\cdot10^{-5}]{m}
\end{empheq}
Mit der gleichen Formel und der nun bekannten Gitterkonstanten lässt sich auch leicht die Wellenlängendifferenz aus dem Winkelabstand der beiden gelben Linien berechnen und mit den Literaturwerten \citep[S.\,173]{Anleitung} vergleichen.
\begin{empheq}[box=\fbox]{align*}
\Delta\lambda_\mathrm{Literatur} &= \unit[2.11]{nm} \\
\Delta\lambda_\mathrm{1. Ordnung} &= \unit[(1.0 \pm 25.5)]{nm} \\
\Delta\lambda_\mathrm{4. Ordnung} &= \unit[(2.0 \pm 6.9)]{nm} \\
\Delta\lambda_\mathrm{8. Ordnung} &= \unit[(2.3 \pm 2.9)]{nm}
\end{empheq}

Dieselbe Rechnung lässt sich auch für das gewichtete Mittel der beiden gelben Winkel und dem Winkel unter dem die grüne Linie gemessen wurde durchführen.
\begin{empheq}[box=\fbox]{align*}
\Delta\lambda_\mathrm{Literatur} &= \unit[31.945]{nm} \\
\Delta\lambda_\mathrm{1. Ordnung} &= \unit[(32.0 \pm 18.1)]{nm} \\
\Delta\lambda_\mathrm{4. Ordnung} &= \unit[(32.35 \pm 4.83)]{nm} \\
\Delta\lambda_\mathrm{8. Ordnung} &= \unit[(32.00 \pm 2.64)]{nm}
\end{empheq}

Die Spaltbreite des Spaltes, bei denen die gelben Linien gerade noch getrennt aufgelöst werden konnten, wurde gemessen.
Teilt man diese Spaltbreite durch die Gitterkonstante, so ergibt sich die Anzahl der beleuchteten Gitterspalte $N$, und mittels Formel (\ref{eq:GitterAufloesung}) lässt sich das Auflösungsvermögen $A$ berechnen. Dieses wird genau wie beim Prismenspektrometer mit dem Literaturwert \citep[S.\,173]{Anleitung} verglichen.
\begin{empheq}[box=\fbox]{align*}
A_\mathrm{Literatur} &= \frac{\lambda}{\Delta\lambda} = 274 \\
A_\mathrm{1. Ordnung} &= (292 \pm 49) \\
A_\mathrm{4. Ordnung} &= (292 \pm 195) \\
A_\mathrm{8. Ordnung} &= (389 \pm 49)
\end{empheq}

Benutzt man nun die gesamte Gitterbreite $d = \unit[15]{mm}$, um das Auflösungsvermögen eines vollständig beleuchteten Gitters zu berechnen, so vergrößert sich $A$ linear, da $N$ linear von $d$ abhängt:
\begin{empheq}[box=\fbox]{align*}
A_\mathrm{Gesamt,1. Ordnung} &= (1459 \pm 12)
\end{empheq}

Außerdem lässt sich auch noch die Wellenlänge der beobachteten violetten Spektrallinie aus der Winkelablenkung bei den verschiedenen Hauptmaxima mittels Formel (\ref{eq:GitterMaxima}) berechnen.
\begin{align*}
\lambda_\mathrm{violett,1. Ordnung} &= \unit[(404 \pm 19)]{nm} \\
\lambda_\mathrm{violett,4. Ordnung} &= \unit[(449 \pm 6)]{nm} \\
\lambda_\mathrm{violett,8. Ordnung} &= \unit[(507 \pm 5)]{nm}
\end{align*}

Der Literaturwert \citep[S.\,173]{Anleitung} und das gewichtete Mittel betragen nun schließlich:
\begin{empheq}[box=\fbox]{align*}
\lambda_\mathrm{violett} &= \unit[(430 \pm 53)]{nm} \\
\lambda_\mathrm{violett,Literaturwert} &= \unit[406.22]{nm}
\end{empheq}
