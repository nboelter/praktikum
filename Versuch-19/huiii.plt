reset
set xrange [0:5]
set xtics ('Gelb 1' 1)
set xtics add ('Gelb 2' 2)
set xtics add ('Grün' 3)
set xtics add ('Violett' 4)
set key left
set ylabel 'λ/a'
plot 'Messwerte.txt' index 0 using ($1-0.2):(sin($2/360*2*pi)):(cos($2/360*2*pi)*$3) with errorbars title '1. Ordnung', \
'Messwerte.txt' index 1 using 1:(sin($2/360*2*pi)/4):(cos($2/360*2*pi)*$3) with errorbars title '4. Ordnung', \
'Messwerte.txt' index 2 using ($1+0.2):(sin($2/360*2*pi)/8):(cos($2/360*2*pi)*$3) with errorbars title '8. Ordnung'
