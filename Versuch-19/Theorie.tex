\subsection{Spektrographie}
Bei der \emph{Spektrographie} wird das Licht einer Lichtquelle mit diskreten Spektrallinien frequenzabhängig aufgespalten, so dass dessen Zusammensetzung gemessen werden kann.

Bei diesem Versuch ist das \emph{Auflösungsvermögen} $A$ des Spektrometers von entscheidender Bedeutung, es ist folgendermaßen definiert:\citep[S.\,529]{Gerthsen}
\begin{align}
  A &= \frac{\lambda}{\Delta\lambda}
\end{align}
Dabei bezeichnen $\lambda$ und $\lambda + \Delta\lambda$ zwei Wellenlängen, die vom Spektrometer gerade noch unterschieden werden können.
\subsection{Das Prismenspektrometer}
\begin{figure}[ht]
\begin{center}
\def\svgwidth{9cm}
\input{Prisma.pdf_tex}
\end{center}
\caption{Strahlengang beim Prismenspektrometer (Quelle: \citet[LP]{LP-Spektrometer})}
\label{fig:Prisma}
\end{figure}
Beim Übergang von Licht zwischen zwei Medien verschiedenen Brechungsindexes wird jenes gebrochen. Weist eines der Medien einen frequenzabhängigen Brechungsindex auf (\emph{Dispersion}), so werden die unterschiedlichen Farbanteile des Lichts natürlich unterschiedlich stark gebrochen. Dies ist die Funktionsweise eines \emph{Prismenspektrometers}.

In Abb.\,\ref{fig:Prisma} ist der Strahlengang durch ein Prisma zu sehen. Für den Spezialfall eines symmetrischen Strahlenverlaufs ist der Winkel $\delta$ minimal und es gilt die \mensch{Fraunhofer}-Formel:\citep[S.\,489]{Gerthsen}
\begin{align}
  n &= \frac{\sin\left(\frac{\delta+\epsilon}{2}\right)}{\sin\left(\frac{\epsilon}{2}\right)}.
\end{align}
Dabei ist $n$ der Brechungsindex des Prismas.

Beim Prisma spricht man von der sogenannten \emph{Winkeldispersion}, welche die Abhängigkeit des Ablenkwinkels $\delta$ von der Wellenlänge beschreibt.
\begin{align}
  D &= \Dfrac{n}{\lambda} \label{eq:Winkeldispersion}
\end{align}

Und für das Auflösungsvermögen des Prismas ergibt sich:\citep[S.\,530]{Gerthsen}
\begin{align}
  A &= B \left|D\right| \label{eq:PrismaAufloesung}
\end{align}

Dabei ist $B$ die effektive Basisbreite des Prismas, also die Breite eines hypothetischen Prismas, das gerade groß genug wäre, um den Lichstrahl komplett aufzunehmen.

\begin{figure}[ht]
\begin{center}
\def\svgwidth{9cm}
\input{EffektiveBasisbreite.pdf_tex}
\end{center}
\caption{Skizze zur effektiven Basisbreite (Verändert nach \citet[LP]{LP-Spektrometer})}
\label{fig:EffektiveBasisbreite}
\end{figure}

Um diese aus dem Strahldurchmesser $d$ zu bestimmen (Siehe Abb.\,\ref{fig:EffektiveBasisbreite}) folgt aus dem geometrischen Zusammenhang:
\begin{align}
  B &= 2 d \frac{\sin\left(\nicefrac{\epsilon}{2}\right)}{\cos\left(\frac{\epsilon+\delta}{2}\right)}. \label{eq:Basis} \\
\end{align}
 
\subsection{Das Gitterspektrometer}
An einem Gitter wird einfallendes Licht gebeugt, so dass dahinter ein Interferenzmuster sichtbar wird. Dabei führen unterschiedliche Wellenlängen zu unterschiedlichen Interferenzmustern, so dass die Hauptmaxima höherer Ordnungen für unterschiedliche Wellenlängen nicht zusammenfallen. Bei bekannter Geometrie kann nun über die Abstände der Hauptmaxima die Wellenlänge der im Energiespektrum vorhandenen Spektrallinien berechnet werden.

Für den Winkel $\phi$, unter dem die Hauptmaxima hinter einem Gitter auftreten ergibt sich folgender Zusammenhang:\citep[S.\,171]{Gerthsen}
\begin{align}
  \sin(\phi) &= \frac{k \lambda}{d}. \label{eq:GitterMaxima}
\end{align}
Dabei bezeichnen $k$ die betrachtete Ordnung, $\lambda$ die Wellenlänge des Lichtes und $d$ die Gitterkonstante.

Laut dem \mensch{Rayleigh}-Kriterium können zwei verschiedene Maxima unterschieden werden, falls der Abstand zwischen den Maxima verschiedener Wellenlängen größer ist als der Abstand zwischen dem Maximum und dem nächsten Minimum der gleichen Wellenlänge. Für die Winkel der Minima gilt folgende Formel:\citep[S.\,529]{Gerthsen}
\begin{align*}
  \sin(\phi) &= \frac{n \lambda}{N d}\qquad n \in \mathbb{N} \setminus N \mathbb{N}
\end{align*}
Dabei ist die neue Variable $N$ die Anzahl der beleuchteten Gitterspalte.

Damit lässt sich nun  das Auflösungsvermögen des Spektrometers bestimmen:\citep[S.\,529]{Gerthsen}
\begin{align}
  A &= k N \label{eq:GitterAufloesung}
\end{align}
