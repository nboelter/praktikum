reset
k=115
b=2*pi/550
c=0.3
d=-0.0001
e=10
m=-0.001
g(x)=k+cos(b*x+c)*exp(d*x)*e+m*x
fit g(x) 'Plus45.txt' via k,b,c,d,e,m
set terminal epslatex color colortext
set output 'plus45.tex'
set xlabel '$t\,[\unit{s}]$'
set ylabel '$y\,[\unit{cm}]$'
plot g(x) title 'Regression' lt 1 lc 8, 'Plus45.txt' title 'Messwerte' lc 3
set output
!epstopdf plus45.eps
