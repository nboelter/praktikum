reset
k=136
b=2*pi/550
c=0.3
d=-0.0002
e=-20
m=0.00001
h(x)=k+cos(b*x+c)*exp(d*x)*e+m*x
fit h(x) 'Minus45.txt' via k,b,c,d,e,m
set terminal epslatex color colortext
set output 'minus45.tex'
set xlabel '$t\,[\unit{s}]$'
set ylabel '$y\,[\unit{cm}]$'
plot h(x) title 'Regression' lt 1 lc 8, 'Minus45.txt' title 'Messwerte' lc 3
set output
!epstopdf minus45.eps
