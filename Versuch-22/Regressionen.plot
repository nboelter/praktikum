reset
lf1(x) = m1*x+b1
rf1(x) = m2*x+b2
lf2(x) = m3*x+b3
rf2(x) = m4*x+b4
lf3(x) = m5*x+b5
rf3(x) = m6*x+b6
set fit errorvariables
fit [16.9:17.2] lf1(x) './Messwerte.txt' index 1 via m1,b1
fit [17.2:17.5] rf1(x) './Messwerte.txt' index 1 via m2,b2
fit [33.2:33.6] lf2(x) './Messwerte.txt' index 2 via m3,b3
fit [33.8:34.0] rf2(x) './Messwerte.txt' index 2 via m4,b4
fit [50.6:50.8] lf3(x) './Messwerte.txt' index 3 via m5,b5
fit [51.1:51.4] rf3(x) './Messwerte.txt' index 3 via m6,b6
set terminal epslatex color colortext
set output 'Max1.tex'
plot './Messwerte.txt' index 1 title 'Messwerte', lf1(x) title 'linke Flanke' lw 2 lc 2, rf1(x) title 'rechte Flanke' lw 2 lc 3
set output 'Max2.tex'
plot './Messwerte.txt' index 2 title 'Messwerte', lf2(x) title 'linke Flanke' lw 2 lc 2, rf2(x) title 'rechte Flanke' lw 2 lc 3
set output 'Max3.tex'
plot './Messwerte.txt' index 3 title 'Messwerte', lf3(x) title 'linke Flanke' lw 2 lc 2, rf3(x) title 'rechte Flanke' lw 2 lc 3
set output
set print 'Regression.txt'
print (b2-b1)/(m1-m2),sqrt( (b2_err/(m1-m2))**2 + (b1_err/(m1-m2))**2 + ((b2-b1)*m1_err/((m1-m2)**2))**2 + ((b2-b1)*m2_err/((m1-m2)**2))**2 )
print (b4-b3)/(m3-m4),sqrt( (b4_err/(m3-m4))**2 + (b3_err/(m3-m4))**2 + ((b4-b3)*m3_err/((m3-m4)**2))**2 + ((b4-b3)*m4_err/((m3-m4)**2))**2 )
print (b6-b5)/(m5-m6),sqrt( (b6_err/(m5-m6))**2 + (b5_err/(m5-m6))**2 + ((b6-b5)*m5_err/((m5-m6)**2))**2 + ((b6-b5)*m6_err/((m5-m6)**2))**2 )
set print
