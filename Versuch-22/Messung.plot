reset
set xlabel 'Beschleunigungsspannung $U_2$'
set ylabel 'Auffangstrom $I$ [arb. unit]'
set terminal epslatex color colortext
set output 'Verlauf.tex'
plot 'Messwerte.txt' index 0 using 1:($2/1000):(0.0005) with errorbars title 'Messwerte'
set output
