\subsection{Das Magnetfeld}
Die fundamentale Messgröße dieses Versuchs ist das \emph{Magnetfeld} $\vec B$, welches eine wichtige Rolle in der Elektrodynamik spielt. Aus dieser ergibt sich auch der \emph{Magnetfluss} $\Phi$, welcher sich anschaulich als die Häufigkeit der Feldlinien, die eine gegebene Fläche $\vec A$ durchstoßen, deuten lässt: \citep[S.~356]{Gerthsen}
\begin{align*}
  \Phi &= \iint_{\vec A} \vec B \cdot \D{\vec A}.
\end{align*}
Um die stoffabhängige Verstärkung bzw. Abschwächung des Magnetfeldes quantifizieren zu können, wird außerdem die \emph{magnetische Erregung} $\vec H$ eingeführt: \citep[S.~357]{Gerthsen}
\begin{align}
 \vec B &= \mu_0 \mu_\mathrm{r} \vec H. \label{eq:erregung}
\end{align}
Dabei wird $\mu_0 := 4\pi\cdot10^{-7} \unitfrac{N}{A^2}$ die \emph{magnetische Feldkonstante} genannt, und $\mu_\mathrm{r}$ die materialabhängige \emph{Permeabilitätszahl}. Da in diesem Versuch nur Luftspulen benutzt werden, wird im weiteren Verlauf $\mu_\mathrm{r} = \mu_\mathrm{r,Luft} \approx 1$ angenommen.
\subsection{Die Zylinderspule}
Bei einer im Verhältnis zum Radius sehr langen stromdurchflossenen Spule gilt für das Magnetfeld im Inneren näherungsweise: \citep[S.~359]{Gerthsen}
\begin{align}
 \vec B &= \frac{\mu_0 I N}{l}. \label{eq:unendlich}
\end{align}
Dabei ist $I$ der durch die Spule fließende Strom, $N$ die Anzahl der Windungen und $l$ die Länge der Spule.

Genauer lässt sich das Feld direkt mit dem Gesetz von \mensch{Biot-Savart} berechnen. Für ein infinitesimales Stück entlang der Symmetrieachse ergibt sich folgender Beitrag zum Magnetfeld: \citep[S.~107]{Demtroeder-Exp2}
\begin{align*}
  \D{B} &= \frac{\mu_0 \cdot I \cdot A \cdot N}{2 \pi l \left[ R^2 + ( z - \zeta)^2\right]^{\nicefrac{3}{2}}} \D{\zeta}
\end{align*}
Die zusätzlichen Größen sind die Querschnittsfläche $A$ der Spule, ihr Radius $R$ und die Entfernung vom Mittelpunkt. Diese sowohl für den Punkt, an dem das Magnetfeld berechnet wird ($z$), als auch für den Teil der Spule, dessen Beitrag berechnet wird ($\zeta$).

Eine Integration liefert folgenden Zusammenhang:
\begin{align}
  B(z) &= \int\limits_{-\frac{1}{2}l}^{+\frac{1}{2}l} \frac{\mu_0 \cdot I \cdot A \cdot N}{2 \pi l \left[ R^2 + ( z - \zeta)^2\right]^{\nicefrac{3}{2}}} \D{\zeta} \nonumber \\
       &=
\frac{\mu_0 N I}{2 l}\left(\frac{z+\nicefrac{l}{2}}{\sqrt{R^2+(z+\nicefrac{l}{2})^2}}+\frac{z-\nicefrac{l}{2}}{\sqrt{R^2+(z-\nicefrac{l}{2})^2}}\right). \label{eq:zylinderspule}
\end{align}

\subsection{Das Helmholtzspulenpaar}
Die \mensch{Helmholtz}-Spulen werden benutzt, um ein möglichst homogenes Magnetfeld zu erzeugen. Auf der Symmetrieachse der Spulen gilt dann: \citep[S.~91]{Demtroeder-Exp2}
\begin{align}
  B(z) \approx \frac{\mu_0 I}{(5/4)^{\nicefrac{3}{2}}R}\left[1-\frac{144}{125}\frac{z^4}{R^4}\right]. \label{eq:helmholtz}
\end{align}
\subsection{Die Hall-Sonde}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{HallSonde.pdf_tex}
\end{center}
\caption{Skizze der Hallsonde (Verändert nach LP \citep{LP-Magnetfeld-von-Spulen})}
\label{fig:HallSonde}
\end{figure}
Die Hall-Sonde (Abb.~\ref{fig:HallSonde}) ist eine dünne Halbleiterscheibe, die von einem konstanten Strom durchflossen wird. Auf diese bewegten Ladungsträger wirkt nun die Lorentz-Kraft: \citep[S.348]{Gerthsen}
\begin{align*}
  \vec F_\mathrm{L} = Q \vec v \times \vec B.
\end{align*}
Die dadurch abgelenkten Ladungsträger sammeln sich an den Seiten, was zu einem elektrischen Feld in der Sonde führt. Die Potentialdifferenz $U_\mathrm{H}$ kann mit einem Voltmeter bestimmt werden: \citep[S.~350]{Gerthsen}
\begin{align*}
  U_\mathrm{H} &= -\frac{1}{en}\frac{IB}{d}
\end{align*}
Dabei ist $n$ die Ladungsträgerdichte des Materials, $e$ die Ladung der Ladungsträger, $I$ der Strom, $B$ das Magnetfeld, welches senkrecht zu Stromrichtung und elektrischer Feldrichtung die Sonde durchsetzt, und $d$ die Dicke der Scheibe.

Die Formel erklärt auch die Benutzung eines Halbleiters, denn diese haben eine deutlich niedrigere Ladungsträgerdichte als Metalle.
\subsection{Die Induktionsspule}
Während des An- bzw. Abschaltens eines äußeren Magnetfelds wird gemäß des Induktionsgesetzes in der Spule ein Spannungsstoß induziert: \citep[S.~383]{Gerthsen}
\begin{align*}
\int\limits_0^t U_\mathrm{ind} \D{t} &= \int\limits_0^t N \Dfrac{\Phi}{t} \D{t} = N \Phi = N B A.
\end{align*}
Dabei bezeichnet $N$ die Windungszahl der Induktionsspule, $\Phi$ den magnetischen Fluss pro Spulenwindung und $A$ die vom Magnetfeld durchsetzte Querschnittsfläche der Spule.
Schließt man nun den Stromkreis mit einem bekannten Widerstand $R$, so kann man mit einem Ladungsmessgerät wie dem Stromintegrator die geflossene Ladung $Q$ messen und über das \mensch{Ohm}sche Gesetz den Spannungsstoß berechnen:
\begin{align*}
\int\limits_0^t U_\mathrm{ind} \D{t} &= \int\limits_0^t R\cdot I \D{t} = R Q.
\end{align*}
Einfaches Umstellen führt nun zu:
\begin{align}
B &= \frac{R \cdot Q}{N \cdot A}. \label{eq:induktion}
\end{align}
