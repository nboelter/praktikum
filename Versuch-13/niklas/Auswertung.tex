\subsection{Eichkonstante}
Mit den gemessenen Widerständen und der Eichspannung lässt sich die Stromstärke des Eichstromes durch den Stromintegrator berechnen.\footnote{Die Fehler für die folgenden Messgrößen wurden, falls nicht anders erwähnt, alle den Bedienungsanleitungen der Multimeter entnommen und die abgeleiteten Ergebnisse mittels gaußscher Fehlerfortpflanzung berechnet.} 

Für den Fehler des Stromintegrators wurden $\unit[5]{Skt.}$ angenommen, da die Messwerte bei den beiden Polungen etwa um diesen Wert auseinanderliegen.

Aufgrund der Parallel- und Reihenschaltung der drei Widerstände ergibt sich mit Knoten- und Maschenregel die folgende Formel:
\begin{align*}
  I_\mathrm{Int} &= \frac{U_\mathrm{Eich}}{\left(R_{1000} + \left(R_{3.3}^{-1} + R_{Int}^{-1}\right)^{-1}\right)\left(\frac{R_\mathrm{Int}}{R_{3.3}}+1\right)} = \unit[(7.42 \pm 0.40)\cdot10^{-6}]{A}
\end{align*}
Dabei bezeichnen $R_\mathrm{Int}$ den Eingangswiderstand des Integrators, $R_{3.3}$ den $\unit[3.3]{\Omega}$ Widerstand und $R_{1000}$ den $\unit[1000]{\Omega}$ Widerstand. Der Widerstand der Sekundärspule wurde gegenüber dem hohen Eingangswiderstand des Stromintegrators von $\unit[(901 \pm 0.17)]{\Omega}$ vernachlässigt.

Damit können nun die Eichkonstanten berechnet werden (Tabelle~\ref{tab:eichkonst}). Als gewichter Mittelwert ergibt sich:
\begin{align*}
  \xi &= \unitfrac[(4.86 \pm 0.11)\cdot10^{-9}]{C}{Skt.}
\end{align*}
Mittels Formel (\ref{eq:induktion}) kann man nun durch Einsetzen der Messwerte einen direkten Zusammenhang zwischen der Ausgabe des Stromintegrators und dem Magnetfeld herstellen:
\begin{align*}
  \kappa &= \unitfrac[(4.36 \pm 0.11)\cdot 10^{-6}]{T}{Skt.}
\end{align*}

\subsection{Magnetfelder}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-lang.tex}
\end{center}
\caption{Magnetfeld in der langen Spule}
\label{fig:feld-lang}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-lang-detail.tex}
\end{center}
\caption{Magnetfeld in der langen Spule (Detailansicht)}
\label{fig:feld-lang-detail}
\end{figure}
Die mit dem Ergebnis von oben berechneten und die mit der Hallsonde direkt gemessenen Magnetfelder wurden in Abbildung~\ref{fig:feld-lang} aufgetragen. Zusätzlich wurden die mittels Formel (\ref{eq:unendlich}) und (\ref{eq:zylinderspule}) berechneten theoretischen Verläufe für eine unendlich lange Spule und eine Zylinderspule eingetragen.

Wegen der vielen Fehlerbalken ist die Abbildung etwas unübersichtlich geworden, weshalb noch eine Detailansicht in Abbildung~\ref{fig:feld-lang-detail} hinzugefügt wurde.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-dick.tex}
\end{center}
\caption{Magnetfeld in der dicken Spule}
\label{fig:feld-dick}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-HH.tex}
\end{center}
\caption{Magnetfeld in den Helmholtzspulen}
\label{fig:feld-hh}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-Vergleich.tex}
\end{center}
\caption{Vergleich der Homogenität und Feldstärke bei den drei Spulen }
\label{fig:feld-vergleich}
\end{figure}

Die anderen Spulen wurden nur mit der Hallsonde vermessen, diese Ergebnisse wurden in den Abbildungen~\ref{fig:feld-dick} sowie \ref{fig:feld-hh} aufgetragen. Für den theoretischen Wert bei den \mensch{Helmholtz}-Spulen wurde Formel~(\ref{eq:helmholtz}) benutzt.

Um die Homogenität und die Feldstärke vergleichen zu können, wurden die Magnetfelder der drei Spulen in Abbildung~\ref{fig:feld-vergleich} aufgetragen. Dabei wurden die Spulen so skaliert, dass ihre Enden übereinstimmen.

Es fällt deutlich auf, dass die \mensch{Helmholtz}-Spulen über den gesamten Spulenbereich ein sehr homogenes Feld erzeugen, dass erst am Rand abschwächt. Die lange Spule hat zumindest in einem relativ großen Bereich im Inneren ein recht homogenes Feld, während das Feld in der dicken Spule schon um die Spulenmitte herum abfällt.

Das Feld der \mensch{Helmholtz}-Spulen ist auch deutlich stärker als das der anderen Spulen.
\subsection{Magnetische Feldkonstante}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Permeab-Vergleich.tex}
\end{center}
\caption{Berechnete magnetische Feldkonstante}
\label{fig:permeab-vergleich}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Permeab-Vergleich-Detail.tex}
\end{center}
\caption{Berechnete magnetische Feldkonstante (Detailansicht)}
\label{fig:permeab-vergleich-detail}
\end{figure}
Um die magnetische Feldkonstante $\mu_0$ zu bestimmen, wurden die genutzten Formeln mit den gemessenen Werten gleichgesetzt. Da alle Formeln die magnetische Feldkonstante als Vorfaktor enthalten, lassen sie sich sehr einfach nach $\mu_0$ umstellen. Die Ergebnisse sind in Abbildung~\ref{fig:permeab-vergleich} sowie in einer Detailansicht in Abbildung~\ref{fig:permeab-vergleich-detail} aufgetragen. Dabei sind die mit \q{Anschalten} bzw \q{Abschalten} bezeichneten Messwerte mit der Induktionsspule aufgenommen, alle anderen mit der Hall-Sonde.

Da die Werte nur in der Mitte der Spulen konstant bleiben, wurden in Tabelle~\ref{tab:feldkonstanten} die bestimmten Feldkonstanten aus der Mitte der jeweiligen Spule eingetragen.
