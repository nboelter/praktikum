Eichspannung: 2.01 V
Offset Primärspule: 10.0 cm
Offset Sekundärspule: -33.0 cm
Offset Hallsonde: -29.4 cm
Widerstand des Stromintegrators: 901 Ohm
Widerstand 3.3 Ohm: 3.3 Ohm
Widerstand 1 kOhm: 985 kOhm

Spulenwerte:
Lang:
  ø_Draht = 0.5 mm
  ø = 12.55 cm
  N = 832
  l = 45 cm
Dick:
  ø_Draht = 0.5 mm
  ø = 200 mm
  N = 501
  l = 270 mm
Helmholtz:
  ø = 12.5 cm
  N = 507
  d_mitte = 6.3 cm
  d_außen = 7.2 cm
  d_innen = 5.2 cm
Induktionsspule (Ja, das ist auch die Sekundärspule...):
  ø = 59 mm
  N = 369
  l = 6 cm
