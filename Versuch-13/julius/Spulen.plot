set ylabel 'Magnetische Flussdichte $[\unit[10^{-3}]{T}]$'
set xlabel 'Abstand zum Spulenmittelpunkt $[\unit[10^{-2}]{m}]$'
set key left bottom Left width -5
B1(a) = 0.000580846 * ( (a/sqrt(0.06275**2+a**2)) + ((0.45 - a)/sqrt(0.06275**2+(0.45 - a)**2)) )
set terminal epslatex color colortext
set output 'Feld-lang.tex'
plot 	'Messwerte.txt' index 1 using ($1-43):(1000*$3 * 0.00000435481482949486):(sqrt((1000*-1*5* 0.00000435481482949486)**2 + (1000*-1*$3 * 3.434*10**(-11))**2)) with errorbars title 'Spule (Anschalten)' lt 1 lc 1, \
	'Messwerte.txt' index 2 using ($1-39.4):(1000*$2/10000.0):(0.2/10) with errorbars title 'Hall-Sonde' lt 1 lc 2, \
	'Messwerte.txt' index 1 using ($1-43):(1000*-1*$2 * 0.00000435481482949486):(sqrt((1000*-1*5* 0.00000435481482949486)**2 + (1000*-1*$2 * 3.434*10**(-11))**2)) with errorbars title 'Spule (Abschalten)' lt 1 lc 3, \
	(1000* B1((x + 22.5)/100.0)) title 'Theoretischer Wert' lt 1 lc 4, \
	(1000*0.000580846*2) title 'Unendliche Spule' lt 1 lc 5
set output 'Feld-lang-detail.tex'
set xrange [0:20]
plot 	'Messwerte.txt' index 1 using ($1-43):(1000*$3 * 0.00000435481482949486):(sqrt((1000*-1*5* 0.00000435481482949486)**2 + (1000*-1*$3 * 3.434*10**(-11))**2)) with errorbars title 'Spule (Anschalten)' lt 1 lc 1, \
	'Messwerte.txt' index 2 using ($1-39.4):(1000*$2/10000.0):(0.2/10) with errorbars title 'Hall-Sonde' lt 1 lc 2, \
	'Messwerte.txt' index 1 using ($1-43):(1000*-1*$2 * 0.00000435481482949486):(sqrt((1000*-1*5* 0.00000435481482949486)**2 + (1000*-1*$2 * 3.434*10**(-11))**2)) with errorbars title 'Spule (Abschalten)' lt 1 lc 3, \
	(1000* B1((x + 22.5)/100.0)) title 'Theoretischer Wert' lt 1 lc 4, \
	(1000*0.000580846*2) title 'Unendliche Spule' lt 1 lc 5
set key right center
set xrange [-20:60]
B2(a) = 0.00058294 * ( (a/sqrt(0.1**2+a**2)) + ((0.27 - a)/sqrt(0.1**2+(0.27 - a)**2))  )
set output 'Feld-dick.tex'
plot 'Messwerte.txt' index 3 using ($1-39.4):(1000*$2/10000.0) title 'Hall-Sonde' lt 1 lc 1, (1000*B2((x+13.5)/100)) title 'Theoretischer Wert', (1000*0.00058294*2) title 'Unendliche Spule' lt 1 lc 2

set xrange [-30:60]
set output 'Feld-HH.tex'
set key right center Left width -5
plot 'Messwerte.txt' index 4 using ($1-39.4):(1000*$2/10000.0):(0.2/10) with errorbars title 'Hall-Sonde' lt 1 lc 1, (1000*4*pi*10**(-7)*0.5*(4.0/5.0)**(3/2)*507/0.063) title 'Theorie (Mitte)' lt 1 lc 2
set output 'Permeab-Vergleich.tex'
set ylabel 'Magnetische Permeabilität $\mu_0$ [$10^{-6}$]'
set xlabel 'Abstand zum Spulenmittelpunkt $[\unit[10^{-2}]{m}]$'
set key left top
plot 'Messwerte.txt' index 4 using (($1-39.4)):(1000000*$3) title 'Helmholtzspulen' lt 1 lc 1, \
'Messwerte.txt' index 2 using (($1-39.4)):(1000000*$3) title 'lange Spule' lt 1 lc 2, \
'Messwerte.txt' index 1 using (($1-43)):(1000000*$4) title '- Anschalten' lt 1 lc 3, \
'Messwerte.txt' index 1 using (($1-43)):(1000000*$5) title '- Abschalten' lt 1 lc 4, \
'Messwerte.txt' index 3 using (($1-39.4)):(1000000*$3) title 'dicke Spule' lt 1 lc 5, \
(1000000*4*pi*10**(-7)) title 'Literaturwert' lt 1 lc 0
set key center bottom
set xrange [-10:10] 
set output 'Permeab-Vergleich-Detail.tex'
replot
set ylabel 'Magnetische Flussdichte $[\unit[10^{-3}]{T}]$'
set xtics ('Spulenanfang' -0.5)
set xtics add ('Spulenmitte' 0.0)
set xtics add ('Spulenende' +0.5)

set key right center
set grid
set xrange [-1:1]
set xlabel 'Position der Hall-Sonde'
set output 'Feld-Vergleich.tex'
plot 'Messwerte.txt' index 4 using (($1-39.4)/6.3):(($2/10000.0)*1000):(0.2/10) with errorbars  title 'Helmholtzspulen' lt 1 lc 1, \
'Messwerte.txt' index 2 using (($1-39.4)/45.0):(($2/10000.0)*1000):(0.2/10) with errorbars title 'lange Spule' lt 1 lc 2, \
'Messwerte.txt' index 3 using (($1-39.4)/27.0):(($2/10000.0)*1000):(0.2/10) with errorbars title 'dicke Spule' lt 1 lc 3
