Fließt Strom durch einen Leiter, so erzeugt jener abhängig von verschiedenen Faktoren ein Magnetfeld $\vec{B}$.
Dieses verhält sich den \mensch{Maxwell}-Gleichungen entsprechend~\citep[S.\,326]{Griffiths}:
\begin{align}
  \nabla \cdot \vec{E} &= \rho/\varepsilon_0\label{eq:Maxwell1}\\
  \nabla \times \vec{E} &= -\vec{\dot B}\label{eq:Maxwell2}\\
  \nabla \cdot \vec{B} &= 0\label{eq:Maxwell3}\\
  \nabla \times \vec{B} &= \mu_0\vec{j} + \mu_0\epsilon_0\vec{\dot E}\label{eq:Maxwell4}
\end{align}
Zusätzlich wird der magnetische Fluss $\Phi$ durch eine Fläche $\vec A$ definiert als~\citep[S.\,295]{Griffiths}:
\begin{align*}
  \Phi = \vec{B}\cdot\vec{A}.
\end{align*}
Fließt ein zeitlich veränderlicher magnetischer Fluss durch eine Leiterschleife der Fläche $\vec{A}$, so wird in dieser Schleife eine Spannung induziert, für die gilt~\citep[S.\,296]{Griffiths}:
\begin{align}
  U_\mathrm{ind} &= -\dot\Phi\nonumber\\
  &= \vec{\dot B}\cdot\vec{A}.\label{eq:Fluss}
\end{align}
Formel~\ref{eq:Maxwell2} ist zu entnehmen, dass ein veränderliches Magnetfeld ein elektrisches Feld erzeugt (und umgekehrt).
Dieses kann in einfachen Fällen mittels Formel~\ref{eq:Fluss} berechnet werden.

Wendet man letztere auf eine Spule mit Querschnittsfläche $\vec A$ und $N$ Windungen an, so ergibt sich~\citep[S.\,383]{Gerthsen}:
\begin{align}
  U_\mathrm{ind} &= N\vec{\dot B}\cdot\vec{A}\nonumber\\
  \Rightarrow \int_{t_0}^{t_1} U_\mathrm{ind}\D{t} &= N\vec{B}\cdot\vec{A}.\label{eq:Feld}
\end{align}
Mit dem \mensch{Ohm}schen Gesetz $U=RI$ ergibt sich für einen Stromkreis mit dem Gesamtwiderstand $R$, in dem sich eine Spule befindet, ein einfacher Zusammenhang zur Bestimmung der induzierten Spannung und damit des Magnetfeldes:
\begin{align}
  \int_{t_0}^{t_1} U_\mathrm{ind}\D{t} &= R \int_{t_0}^{t_1} I_\mathrm{ind}\D{t} = RQ_\mathrm{ind},\label{eq:Eich}
\end{align}
wobei $Q_\mathrm{ind}$ die im Stromkreis geflossene Ladung darstellt.
Diese Messung kann zum Beispiel mit einem Stromintegrator vorgenommen werden.

Bei einer realen Spule kann man abhängig von Länge $l$, Abstand vom Spulenende $a$, Spulenradius $R$, Windungszahl $N$ und Spulenstrom $I$ die magnetische Flussdichte bestimmen durch~\citep[S.\,107]{Demtroeder-Exp2}:
\begin{align}
  B &= \frac{\mu_0NI}{2l}\left(\frac{a}{\sqrt{R^2+a^2}} + \frac{l-a}{\sqrt{R^2+(l-a)^2}}\right).\label{eq:feld-theorie}
\end{align}
Oft werden auch sogenannte \mensch{Helmholtz}-Spulen verwendet.
Diese weisen ein besonders homogenes Magnetfeld auf und es gilt für die magnetische Flussdichte in der Mitte zwischen den Spulen (vgl. Versuch 12 - \q{Die spezifische Elektronenladung}):
\begin{align}
  B &= \mu_0 I \sqrt{\frac{4}{5}}^3 \frac{N}{R}.\label{eq:helmholtz}
\end{align}
\subsection{Die Hall-Sonde}
Um Magnetfelder zu messen, wird oft die sogenannte \mensch{Hall}-Sonde verwendet.
Diese besteht meist aus einem Halbmetallplättchen, das wie in Abbildung~\ref{fig:hall} mit Messgeräten und Spannungsquellen verbunden ist.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{hall.pdf_tex}
\end{center}
\caption{Aufbau einer Hall-Sonde~\citep{LP-Magnetfeld-von-Spulen}}
\label{fig:hall}
\end{figure}
Durch die Lorentz-Kraft werden die Ladungsträger, die aufgrund der anliegenden Spannung in $x$-Richtung durch das Plättchen fließen, in $z$-Richtung abgelenkt und sammeln sich an einer Seite an.
Dadurch entsteht eine messbare Spannung zwischen den Seiten des Plättchens, die \emph{Hall-Spannung}.
Für die Hall-Spannung $U_H$ gilt~\citep[S.\,350]{Gerthsen}:
\begin{align*}
    U_\mathrm{H} &= -\frac{IB}{end},
\end{align*}
wobei I der Strom durch die Sonde, $B$ die Komponente des Magnetfeldes, die senkrecht zur Sonde steht, $e$ die Ladung der Ladungsträger, $n$ die Ladungsträgerdichte und $d$ die Dicke des Halbmetallscheibchens.
