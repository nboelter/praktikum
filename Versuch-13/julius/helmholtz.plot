reset
set key right top 
set xrange [-30:60]
set terminal epslatex color colortext
set output 'Feld-HH-besser.tex'
R = 0.063
plot [-30:60] [0:4.5] 'Messwerte.txt' index 4 using ($1-39.4):(1000*$2/10000.0):(0.2/10) with errorbars title 'Hall-Sonde' lt 1 lc 1, (1000*4*pi*10**(-7)*0.5*(4.0/5.0)**(3/2)*507/0.063*(1.0 - ((144*(x/100.0)**4)/(125*R**4)))) title 'Theorie' lt 1 lc 2
