\subsection{Eichung des Stromintegrators}
Zunächst muss der Stromintegrator, der die gemessene Ladung in einer unbekannten Einheit misst (im folgenden Skalenteile, Skt.), geeicht werden.
Der Fehler wird auf etwa $\unit[5]{Skt.}$ geschätzt.\footnote{Die Werte für die verschiedenen Polungen lagen etwa um diesen Wert auseinander.}

Die Kirchhoffschen Regeln ergeben aufgrund der benutzten Schaltung für den Strom $I_\mathrm{Integrator}$ durch den Integrator:
\begin{align*}
    I_\mathrm{Integrator} &= \frac{U_\mathrm{Puls}}{\left(R_{1000} + \left(R_{3.3}^{-1} + R_\mathrm{Integrator}^{-1}\right)^{-1}\right)\left(\frac{R_\mathrm{Integrator}}{R_{3.3}}+1\right)} = \unit[(7.42 \pm 0.40)\cdot10^{-6}]{A}
\end{align*}
Es bezeichnen $R_\mathrm{Integrator} = \unit[(901\pm 0.17)]{\Omega}$ den Widerstand des Stromintegrators, $R_{3.3} = \unit[3.3]{\Omega}$ einen der Ohmschen Widerstände und $R_{1000} = \unit[1000]{\Omega}$ den anderen.

Hierfür kann Gleichung~\ref{eq:Eich} verwendet werden.
Für die gemessene Ladung muss bei Impulsdauer $t$, Spannung $U$ und Widerstand $R$ gelten:
\begin{align*}
  k_\mathrm{eich} &= \frac{Q}{Q_\mathrm{mess}} = \frac{Ut}{RQ_\mathrm{mess}},
\end{align*}
wobei $Q_\mathrm{mess}$ (in Skt.) die gemessene Ladung, $k_\mathrm{eich}$ (in Coulomb/Skt.) die Eichkonstante und $Q$ die Ladung in Coulomb ist.
Für die Eichkonstante ergibt sich aus den verschiedenen Messungen nach Bildung des gewichteten Mittels mit entsprechendem Fehler:
\begin{empheq}[box=\fbox]{align*}
  k_\mathrm{eich} &= \unitfrac[(4.86 \pm 0.11)\cdot10^{-9}]{C}{Skt.}
\end{empheq}
Es lässt sich auch ein direkter Umrechnungsfaktor zwischen gemessener Ladung $Q_\mathrm{mess}$ und magnetischer Flussdichte $B$ errechnen:
\begin{align*}
  k_\mathrm{direkt} &= \frac{B}{Q_\mathrm{mess}} = \frac{Ut}{NAQ_\mathrm{mess}}
\end{align*}
Daraus ergibt sich durch Einsetzen und gewichtete Mittelung mit dem entsprechenden Fehler:
\begin{empheq}[box=\fbox]{align*}
  k_\mathrm{direkt} &= \unitfrac[(4.36 \pm 0.11)\cdot10^{-6}]{T}{Skt.}
\end{empheq}
\subsection{Ermittlung der magnetischen Flussdichten}
Mit $k_\mathrm{direkt}$ ergeben sich für die Messungen an der langen Spule mit Hilfe der Induktionsspule die in den Abbildungen~\ref{fig:feld-lang} und~\ref{fig:feld-lang-detail} dargestellten Flussdichten.

In diesen sind auch die mit der Hall-Sonde ermittelten Werte aufgetragen.
Hier mussten die gemessenen Werte nur mit $10^{-4}$ multipliziert werden, da $\unit[1]{G} = \unit[10^{-4}]{T}$ gilt.
Die Abbildungen~\ref{fig:feld-dick} und~\ref{fig:feld-hh} zeigen die mit der Hall-Sonde ermittelten Messdaten für die dicke Spule und die Helmholtz-Spulen.
Der theoretische Wert für die Helmholtz-Spulen wurde mit Formel~\ref{eq:helmholtz}bestimmt.

Zum Vergleich der drei Spulen sind in Abbildung~\ref{fig:feld-vergleich} die drei mit der Hall-Sonde gemessenen Flussdichten zusammengefasst.
Die Werte wurden dafür so skaliert, dass die jeweiligen Spulenenden sich an der gleichen Stelle befinden.

Auffällig ist die größere Flussdichte bei sehr viel homogenerem Magnetfeld bei den Helmholtz-Spulen.
Die beiden anderen Spulen weisen je einen Maximalwert der magnetischen Flussdichte auf, wohingegen bei den Helmholtz-Spulen über $\unit[4]{cm}$ Maximalwerte gemessen wurden.
Außerdem ist eine deutlich höhere Homogenität des Magnetfeldes bei der langen im Vergleich zur dicken Spule festzustellen.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-lang.tex}
\end{center}
\caption{Magnetische Flussdichte in der langen Spule}
\label{fig:feld-lang}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-lang-detail.tex}
\end{center}
\caption{Magnetische Flussdichte in der langen Spule (Detailansicht)}
\label{fig:feld-lang-detail}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-dick.tex}
\end{center}
\caption{Magnetische Flussdichte in der dicken Spule}
\label{fig:feld-dick}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-HH.tex}
\end{center}
\caption{Magnetische Flussdichte in den Helmholtzspulen}
\label{fig:feld-hh}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Feld-Vergleich.tex}
\end{center}
\caption{Vergleich der Homogenität und Feldstärke bei den drei Spulen}
\label{fig:feld-vergleich}
\end{figure}
\subsection{Bestimmung der Vakuumpermeabilität}
Vegleicht man die oben bestimmten Werte der magnetischen Flussdichte mit den theoretisch aus Formel~\ref{eq:feld-theorie} ermittelten, so kann man daraus die Vakuumpermeabilität $\mu_0$ bestimmen:
\begin{align*}
  \mu_0 &= \frac{2Bl}{N}\left(\frac{a}{\sqrt{R^2+a^2}} + \frac{l-a}{R^2+(l-a)^2}\right)^{-1}
\end{align*}
Für die bei den Helmholtz-Spulen aufgenommenen Werte wurde Formel~\ref{eq:geklaut} zur Berechnung von $\mu_0$ verwendet.

Die Ergebnisse dieser Bestimmung sind für die verschiedenen Spulen und Messverfahren in den Abbildungen~\ref{fig:permeab-vergleich} und~\ref{fig:permeab-vergleich-detail} dargestellt und mit dem Literaturwert verglichen.
Außerdem sind in Tabelle~\ref{tab:feldkonstanten} signifikante Werte für die verschiedenen Spulen zusammengefasst.
Die mit der Induktionsspule aufgenommenen Werte für die lange Spule sind dabei jeweils mit \q{Anschalten} oder \q{Abschalten} bezeichnet worden.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Permeab-Vergleich.tex}
\end{center}
\caption{Berechnete Vakuumpermeabilität}
\label{fig:permeab-vergleich}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Permeab-Vergleich-Detail.tex}
\end{center}
\caption{Berechnete Vakuumpermeabilität (Detailansicht)}
\label{fig:permeab-vergleich-detail}
\end{figure}
