reset
set grid
plot '../Messwerte.txt' index 1 using ($1-43.0):(-$2*0.0000043548) title 'Messdaten Induktionsspule (an)',\
                     '' index 1 using ($1-43.0):($3*0.0000043548) title 'Messdaten Induktionsspule (aus)',\
                     '' index 2 using ($1-39.4):($2/10000) title 'Messdaten Hall-Sonde'
