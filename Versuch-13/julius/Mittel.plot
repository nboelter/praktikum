reset
set ylabel 'Magnetische Flussdichte $[\unit[10^{-3}]{T}]$'
set xlabel 'Abstand zum Spulenmittelpunkt $[\unit[10^{-2}]{m}]$'
set key left bottom Left width -5
B1(a) = 0.000580846 * ( (a/sqrt(0.06275**2+a**2)) + ((0.45 - a)/sqrt(0.06275**2+(0.45 - a)**2)) )
set terminal epslatex color colortext
set output 'Feld-lang-mittel.tex'
plot  'Messwerte.txt' index 1 using ($1-43):(0.5*(1000*$3 * 0.00000435481482949486)+0.5*(1000*-1*$2 * 0.00000435481482949486)):(sqrt((1000*-1*5* 0.00000435481482949486)**2 + (1000*-1*$3 * 3.434*10**(-11))**2)) with errorbars title 'Spule (Mittelwert)' lt 1 lc 1, \
  'Messwerte.txt' index 2 using ($1-39.4):(1000*$2/10000.0):(0.2/10) with errorbars title 'Hall-Sonde' lt 1 lc 2, \
  (1000* B1((x + 22.5)/100.0)) title 'Theoretischer Wert' lt 1 lc 4, \
  (1000*0.000580846*2) title 'Unendliche Spule' lt 1 lc 5

