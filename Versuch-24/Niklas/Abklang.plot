reset
set fit errorvariables
set ylabel 'Impulsrate [$\nicefrac{\textrm{Impulse}}{\unit[5]{s}}$]'
set xlabel 'Zeit [$\unit{s}$]'
FIT_LIMIT=10e-20
f0(x) = N0
f1A(x) = N1A * exp(- log(2)/TA*x) + N0 
f1B(x) = N1B * exp(- log(2)/TB*x) + N0 
f1(x) = N1A * exp(- log(2)/TA*x) + N1B * exp(- log(2)/TB*x) + N0 
f2A(x) = N2A * exp(- log(2)/TA*x) + N0
f2B(x) = N2B * exp(- log(2)/TB*x) + N0
f2(x) = N2A * exp(- log(2)/TA*x) + N2B * exp(- log(2)/TB*x) + N0 
f3A(x) = N4A * exp(- log(2)/TA*x) + N0
f3B(x) = N4B * exp(- log(2)/TB*x) + N0
f3(x) = N4A * exp(- log(2)/TA*x) + N4B * exp(- log(2)/TB*x) + N0 
f4A(x) = N8A * exp(- log(2)/TA*x) + N0
f4B(x) = N8B * exp(- log(2)/TB*x) + N0
f4(x) = N8A * exp(- log(2)/TA*x) + N8B * exp(- log(2)/TB*x) + N0 

f(x,y) = (y == 0) ? f0(x) : (y == 1) ? f1(x) : (y == 2) ? f2(x) \
          : (y == 4) ? f3(x) : (y == 8) ? f4(x) : 0
fit f(x,y) '../Messwerte.txt' using 2:1:3:4 via TA, TB, N1A, N1B, \
          N2A, N2B, N4A, N4B, N8A, N8B, N0
fit f(x,y) '../Messwerte.txt' using 2:1:3:4 via TA, TB, N1A, N1B, \
          N2A, N2B, N4A, N4B, N8A, N8B, N0

set print 'Regression.txt'
print 1,TA,TA_err
print 2,TB,TB_err
print ''
print ''
print 1,N1A,N1A_err
print 2,N2A,N2A_err
print 4,N4A,N4A_err
print 8,N8A,N8A_err
print ''
print ''
print 1,N1B,N1B_err
print 2,N2B,N2B_err
print 4,N4B,N4B_err
print 8,N8B,N8B_err
set print

set terminal epslatex color colortext
set output 'Uebersicht.tex'
set style line 1 linewidth 2 linetype 1
plot [0:300]\
  f4(x) title '8 Minuten' ls 1 lc 5, \
  f3(x) title '4 Minuten' ls 1 lc 4, \
  f2(x) title '2 Minuten' ls 1 lc 3, \
  f1(x) title '1 Minute' ls 1 lc 2, \
  f0(x) title 'Nullrate' ls 1 lc 1
set output '1min.tex'
plot \
  f0(x) title 'Nullrate' ls 1 lc 1, \
  f1A(x) title 'Isotop A' ls 1 lc 3, \
  f1B(x) title 'Isotop B' ls 1 lc 4, \
  f1(x) title 'Summe' ls 1 lc 0, \
  '../minute1.txt' u 1:2:(sqrt($2)) with errorbars title 'Messwerte' lc 2
set output '2min.tex'
plot \
  f0(x) title 'Nullrate' ls 1 lc 1, \
  f2A(x) title 'Isotop A' ls 1 lc 3, \
  f2B(x) title 'Isotop B' ls 1 lc 4, \
  f2(x) title 'Summe' ls 1 lc 0, \
  '../minute2.txt' u 1:2:(sqrt($2)) with errorbars title 'Messwerte' lc 2
set output '4min.tex'
plot \
  f0(x) title 'Nullrate' ls 1 lc 1, \
  f3A(x) title 'Isotop A' ls 1 lc 3, \
  f3B(x) title 'Isotop B' ls 1 lc 4, \
  f3(x) title 'Summe' ls 1 lc 0, \
  '../minute4.txt' u 1:2:(sqrt($2)) with errorbars title 'Messwerte' lc 2
set output '8min.tex'
plot \
  f0(x) title 'Nullrate' ls 1 lc 1, \
  f4A(x) title 'Isotop A' ls 1 lc 3, \
  f4B(x) title 'Isotop B' ls 1 lc 4, \
  f4(x) title 'Summe' ls 1 lc 0, \
  '../minute8.txt' u 1:2:(sqrt($2)) with errorbars title 'Messwerte' lc 2
set logscale y
set output '1minLog.tex'
plot \
  f0(x) title 'Nullrate' ls 1 lc 1, \
  f1A(x) title 'Isotop A' ls 1 lc 3, \
  f1B(x) title 'Isotop B' ls 1 lc 4, \
  f1(x) title 'Summe' ls 1 lc 0, \
  '../minute1.txt' u 1:2:(sqrt($2)) with errorbars title 'Messwerte' lc 2
set output '2minLog.tex'
plot \
  f0(x) title 'Nullrate' ls 1 lc 1, \
  f2A(x) title 'Isotop A' ls 1 lc 3, \
  f2B(x) title 'Isotop B' ls 1 lc 4, \
  f2(x) title 'Summe' ls 1 lc 0, \
  '../minute2.txt' u 1:2:(sqrt($2)) with errorbars title 'Messwerte' lc 2
set output '4minLog.tex'
plot \
  f0(x) title 'Nullrate' ls 1 lc 1, \
  f3A(x) title 'Isotop A' ls 1 lc 3, \
  f3B(x) title 'Isotop B' ls 1 lc 4, \
  f3(x) title 'Summe' ls 1 lc 0, \
  '../minute4.txt' u 1:2:(sqrt($2)) with errorbars title 'Messwerte' lc 2
set output '8minLog.tex'
plot \
  f0(x) title 'Nullrate' ls 1 lc 1, \
  f4A(x) title 'Isotop A' ls 1 lc 3, \
  f4B(x) title 'Isotop B' ls 1 lc 4, \
  f4(x) title 'Summe' ls 1 lc 0, \
  '../minute8.txt' u 1:2:(sqrt($2)) with errorbars title 'Messwerte' lc 2
set output 'UebersichtLog.tex'
plot [0:700]\
  f4(x) title '8 Minuten' ls 1 lc 5, \
  f3(x) title '4 Minuten' ls 1 lc 4, \
  f2(x) title '2 Minuten' ls 1 lc 3, \
  f1(x) title '1 Minute' ls 1 lc 2, \
  f0(x) title 'Nullrate' ls 1 lc 1
set xlabel 'Aktivierungszeit [$\unit{Min}$]'
unset logscale
set key bottom right
set ylabel 'Anfangsaktivität [$\nicefrac{\textrm{Impulse}}{\unit[5]{s}}$]'
set output 'AktivierungA.tex'
NA = 100
aktA(x) = NA * ( 1 - exp(-x* log(2)/TA))
fit aktA(x) 'Regression.txt' index 1 using ($1*60):2:3 via NA
plot [0:9] \
  'Regression.txt' index 1 using 1:2:3 with errorbars title 'ermittelte Anfangsaktivität', \
  aktA(x*60) ls 1 lc 3 title 'Regression', \
  NA ls 1 lc 4 title 'asympt. Grenzwert'
set output 'AktivierungB.tex'
NB = 100
aktB(x) = NB * ( 1 - exp(-x* log(2)/TB))
fit aktB(x) 'Regression.txt' index 2 using ($1*60):2:3 via NB
plot [0:9]\
  'Regression.txt' index 2 using 1:2:3 with errorbars title 'ermittelte Anfangsaktivität', \
  aktB(x*60) ls 1 lc 3 title 'Regression', \
  NB ls 1 lc 4 title 'asympt. Grenzwert'
set output

set print 'Regression.txt'
print '#Halbwertszeiten'
print 1,TA,TA_err
print 2,TB,TB_err
print ''
print ''
print '#Anfangsaktivitäten bei Isotop 1'
print 1,N1A,N1A_err
print 2,N2A,N2A_err
print 4,N4A,N4A_err
print 8,N8A,N8A_err
print ''
print ''
print '#Anfangsaktivitäten bei Isotop 2'
print 1,N1B,N1B_err
print 2,N2B,N2B_err
print 4,N4B,N4B_err
print 8,N8B,N8B_err
print ''
print ''
print '#asymptotische Anfangsaktivitäten'
print 1,NA,NA_err
print 2,NB,NB_err
set print

