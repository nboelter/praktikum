reset
set xlabel 'Zeit in Sekunden'
set ylabel 'Anzahl Ereignisse pro Sekunde'
set grid
set yrange [-0.2:2]
set terminal epslatex color colortext size 15cm,7.7cm
set output 'nullrate.tex'
plot 'nullrate.txt' using 1:($2/5.0) ps 2 pt 2 title 'Messwerte'
set output
