\subsection{Der Betazerfall}
Zur Erklärung jeder bisher beobachteten Dynamik kann man die fundamentalen Wechselwirkungen \emph{Gravitation}, \emph{Coulomb-Wechselwirkung}, \emph{starke Wechselwirkung} und \emph{schwache Wechselwirkung} verwenden.
Die Theorie des radioaktiven Zerfalls subatomarer Teilchen wird hierbei durch die schwache Wechselwirkung beschrieben.

Unter die Bezeichnung $\beta$-Zerfall fallen drei verschiedene solche Reaktionen.
Diese können vereinfacht mit einer effektiven Theorie, welche mit der Kernkraft arbeitet, beschrieben werden.
Hieraus erhält man ähnlich zum Schalenmodell der Atomhülle ein Schalenmodell für den Kern.
Unter Verwendung des \mensch{Pauli}-Prinzips wird hiermit die Kernstruktur durch Energieniveaus der Nucleonen beschrieben.
Analog zum Elektronenschalenmodell resultiert ein voll besetztes Energieniveau in größerer Stabilität des Nucleus.
Auch sogenannte \q{magische} Kerne sind (ähnlich wie Edelgaskonfigurationen) besonders stabil.
Diese Beschreibung resultiert aus der Beobachtung, dass bei Hinzufügen eines weiteren Nucleons zum Kern bei bestimmten (magischen) Anzahlen bereits vorhandener Nucleonen die Bindungsenergie des zuletzt hinzugefügten Nucleons sehr viel geringer ist als die der bereits vorhandenen.

Im Fermigasmodell können Neutronen und Protonen auf den Schalen verschiedene Energieniveaus besetzen.
Abbildung~\ref{fig:topf} zeigt eine schematische Darstellung des Fermigasmodells für Nuclei.
Das Potenzial von Protonen ist leicht erhöht gegenüber dem der Neutronen und besitzt einen Coulomb-Wall.
Dies wird durch die Coulombkraft verursacht, die auf die positiv geladenen Protonen wirkt.
\begin{figure}[htb]
    \begin{center}
        \def\svgwidth{8cm}
        \input{Potentialtopf.pdf_tex}
    \end{center}
    \caption{Verlaufsschema des Potentials der Nucleonen im Atomkern.}
    \label{fig:topf}
\end{figure}

Bei radioaktiven Isotopen kann durch eine Umwandlung eines Nucleons unter Aussendung eines $\beta$-Teilchens und eines Neutrinos ein niedrigeres Energieniveau erreicht werden.
So wird zum Beispiel beim $\beta^{-}$-Zerfall aus einem Neutron ein Proton. Es werden dabei ein Elektron und ein Antineutrino ausgesendet.
Die Reaktionsgleichung lautet:
\begin{align*}
  ^\mathrm{A}_\mathrm{Z}\mathrm{X} \rightarrow ^\mathrm{A}_\mathrm{Z+1}\mathrm{Y} + e^{-} + \bar\nu_e + E
\end{align*}
\subsection{Die Am-Be-Quelle}
Zur Aufaktivierung des im Versuch verwendeten Silberpräparates aus $^{107}_{47}\mathrm{Ag}$ und $^{109}_{47}\mathrm{Ag}$ wird Quelle aus $^{241}_{95}\mathrm{Am}$ und $^9_4\mathrm{Be}$ verwendet.
Unter der $\alpha$-Strahlung des Americiums zerfällt das Beryllium unter Aussendung eines Neutrons, welches wiederum im Präparat zur Erzeugung der beiden radioaktiven Silberisotope $^{108}_{47}\mathrm{Ag}$ und $^{110}_{47}\mathrm{Ag}$ durch Kernreaktion führt.

Die beim Zerfall dieser Isotope entstehenden Isotope von Cadmium sind stabil~\citep[11-100]{Rubber}. Eine Beeinflussung der Messergebnisse hierdurch kann also ausgeschlossen werden.

Zur Registrierung der $\beta^{-}$-Strahlung dieser beiden Isotope wird ein Geiger-Müller-Zählrohr verwendet (zur Funktionsweise siehe Versuch 23 - \q{Röntgenstrahlung}).
\subsection{Gesetze des radioaktiven Zerfalls}
Hat man zum Zeitpunkt $t=0$ eine Anzahl von $N_0$ Atomen eines radioaktiven Stoffes, so gilt für die Anzahl von Atomen $N$ zum Zeitpunkt $t$~\citep[S.\,986]{Gerthsen}:
\begin{align}
  N(t) &= N_0\cdot e^{-\lambda t},\label{eq:zerfall}
\end{align}
wobei $\lambda$ die Zerfallskonstante ist. Durch Ableitung nach der Zeit erhält man den entsprechenden Zusammenhang für die Zerfallsrate:
\begin{align*}
  R(t) &= R_0 \cdot e^{-\lambda t}.
\end{align*}
Dabei ist $R(t) = \frac{\D N}{\D t} (t)$ und $R_0 = R(0)$.
$R_0$ erhält man auch über die Aktivierungszeit $\tau$ und die Zerfallsrate $R_0^\infty$ bei $\tau=\infty$~\citep[S.\,43]{Demtroeder-Exp4}:
\begin{align}
  R_0(\tau) &= R_0^\infty (1- e^{-\lambda \tau}).\label{eq:r0}
\end{align}
Es lässt sich über die Zerfallskonstante auch die Halbwertszeit $T_{1/2}$ bestimmen:
\begin{align}
  \frac{N_0}{2} &= N(T_{1/2}) = N_0\cdot e^{-\lambda T_{1/2}}\nonumber\\
  \Rightarrow T_{1/2} &= \frac{\log 2}{\lambda}.\label{eq:halbwert}
\end{align}
Interessant für eine Einordnung der Ergebnisse ist auch die Abweichung der gemessenen Ionisationsergeignisse im Zählrohr von der tatsächlichen Impulsrate.
Da die Einzelereignisse unabhängig voneinander stattfinden, lässt sich hierfür eine Poissonverteilung annehmen, die Abweichung der gemessenen Werte beträgt also~\citep[S.\,988]{Gerthsen}:
\begin{align}
  \sigma_\mathrm{N} &= \sqrt{\mathrm{N}} \label{eq:fehler}.
\end{align}
