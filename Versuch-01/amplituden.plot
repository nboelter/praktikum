reset
fit1(x) = a1* ((x**2-o1**2)**2+(2*k1*o1)**2)**(-0.5)
fit2(x) = a2* ((x**2-o2**2)**2+(2*k2*o2)**2)**(-0.5)
fit3(x) = a3* ((x**2-o3**2)**2+(2*k3*o3)**2)**(-0.5)
a1 = 17
a2 = 17
a3 = 17
k1 = 0.1
k2 = 0.1
k3 = 0.1
o1 = 1
o2 = 1
o3 = 1
set samples 1000
fit fit1(x) './auswertung/auswertung4.txt' index 0 using ($1*0.00303682):2 via a1,k1,o1
fit fit2(x) './auswertung/auswertung4.txt' index 1 using ($1*0.00303682):2 via a2,k2,o2
fit fit3(x) './auswertung/auswertung4.txt' index 2 using ($1*0.00303682):2 via a3,k3,o3
set terminal epslatex color colortext
set ylabel 'Amplitude $\varphi_0$ [$^\circ$]'
set xlabel 'Anregungsfrequenz $\nicefrac{\omega}{\omega_0}$ [-]'
set output 'amplituden.tex'
plot './auswertung/auswertung4.txt' index 1 using ($1*0.00303682):2 lt 0 lc 1 title 'Dämpfung 4mm', \
     fit1(x) notitle lt 1 lc 0, fit2(x) notitle lt 1 lc 0, fit3(x) notitle lt 1 lc 0, \
     './auswertung/auswertung4.txt' index 2 using ($1*0.00303682):2 lt 0 lc 2 title 'Dämpfung 6mm', \
     './auswertung/auswertung4.txt' index 0 using ($1*0.00303682):2 lt 0 lc 3 title 'Dämpfung 8mm'
set output
!epstopdf 'amplituden.eps'
