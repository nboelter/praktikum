Zuerst werden die Abklingkurven $\phi(t)$ aufgetragen (Abb.\,\ref{fig:messung1-0mm}, Abb.\,\ref{fig:messung1-4mm}, Abb.\,\ref{fig:messung1-6mm} sowie  Abb.\,\ref{fig:messung1-8mm}).

Nun wird die Zeit über der Zahl der Amplitudendurchläufe aufgetragen (Abb.\,\ref{fig:eigenfrequenz}), dann wird mit gnuplot via linearer Regression die Eigenfrequenz der Oszillation bestimmt, denn in einer Periode hat der Oszillator natürlich zwei Amplitudendurchläufe, also mit $t(x) = m*x+b \Rightarrow T = 2m = \nicefrac{\omega_\mathrm{e}}{2\pi}$:
\begin{empheq}[box=\fbox]{align*}
  \omega_\mathrm{e,\unit[0]{mm}} &= \unit[(2.069 \pm 0.003)]{Hz}\\
  \omega_\mathrm{e,\unit[4]{mm}} &= \unit[(2.117 \pm 0.005)]{Hz}\\
  \omega_\mathrm{e,\unit[6]{mm}} &= \unit[(2.139 \pm 0.009)]{Hz}\\
  \omega_\mathrm{e,\unit[8]{mm}} &= \unit[(2.114 \pm 0.021)]{Hz}
\end{empheq}

Jetzt werden die logarithmischen Dekremente von zwei aufeinanderfolgenden Extrema bestimmt (Tab.\,\ref{tab:log-dekrement}), als Mittelwert ergeben sich:
\begin{empheq}[box=\fbox]{align*}
  \Lambda_\mathrm{\unit[0]{mm}} &= (0.106 \pm 0.007)\\
  \Lambda_\mathrm{\unit[4]{mm}} &= (0.449 \pm 0.030)\\
  \Lambda_\mathrm{\unit[6]{mm}} &= (0.862 \pm 0.018)\\
  \Lambda_\mathrm{\unit[8]{mm}} &= (1.489 \pm 0.057)
\end{empheq}
Dabei wurde die Standardabweichung für die Fehlerabschätzung benutzt.


Mit Formel (\ref{eq:log-dekrement}) kann nun leicht die Dämpfungskonstante $k$ bestimmt werden:
\begin{align*}
  \Lambda &= k T \\
  \Rightarrow \qquad k &= \frac{\Lambda \omega_\mathrm{e}}{2\pi} \\
  \sigma_k &= \frac{1}{2\pi}\sqrt{\left(\Lambda\sigma_{\omega_\mathrm{e}}\right)^2 + \left(\omega_\mathrm{e}\sigma_\Lambda\right)^2} 
\end{align*}
\begin{empheq}[box=\fbox]{align*}
  k_\mathrm{\unit[0]{mm}} &= \unit[(0.34  \pm 0.01)10^{-1}]{Hz} \\
  k_\mathrm{\unit[4]{mm}} &= \unit[(1.51 \pm 0.03)10^{-1}]{Hz} \\
  k_\mathrm{\unit[6]{mm}} &= \unit[(2.93  \pm 0.04)10^{-1}]{Hz} \\
  k_\mathrm{\unit[8]{mm}} &= \unit[(5.01  \pm 0.15)10^{-1}]{Hz}
\end{empheq}

Mit Gleichung (\ref{eq:eigenfrequenz}) lässt sich jetzt auch die ungedämpfte Eigenfrequenz $\omega_0$ berechnen:
\begin{align*}
  \omega_\mathrm{e}^2 &= \omega_0^2 - k^2 \\
  \Rightarrow \qquad \omega_0 &= \sqrt{\omega_\mathrm{e}^2 + k^2} \\
  \sigma_{\omega_0} &= \frac{\sqrt{\left(\omega_\mathrm{e}\sigma_k\right)^2 + \left(k\sigma_{\omega_\mathrm{e}}\right)^2}}{\sqrt{\omega_\mathrm{e}^2 + k^2}}
\end{align*}
\begin{empheq}[box=\fbox]{align*}
  \omega_\mathrm{0,\unit[0]{mm}} &= \unit[(2.069  \pm 0.002)]{Hz} \\
  \omega_\mathrm{0,\unit[4]{mm}} &= \unit[(2.122  \pm 0.004)]{Hz} \\
  \omega_\mathrm{0,\unit[6]{mm}} &= \unit[(2.159  \pm 0.005)]{Hz} \\
  \omega_\mathrm{0,\unit[8]{mm}} &= \unit[(2.173  \pm 0.016)]{Hz}
\end{empheq}

Mit den Dämpfungskonstanten und der ungedämpften Eigenfrequenz $\omega_\mathrm{0,\unit[0]{mm}}$ können nun über die Formel (\ref{eq:driven-oscillator}) Vorhersagen über die Resonanzfrequenz der getriebenen Oszillation gemacht werden:
\begin{align*}
  \varphi(t) &= \frac{N}{\sqrt{(\omega_0^2-\omega^2)^2+(2k\omega)^2}}\cos(\omega t + \phi) \\
  \Rightarrow 0 & \stackrel{!}{=} \Dfrac{}{\omega} \left((\omega_0^2-\omega^2)^2+(2k\omega)^2\right) \\
  \omega_\mathrm{r} &= \sqrt{\omega_0^2 - 2k^2} \\
  \sigma_{\omega_\mathrm{r}} &= \frac{\sqrt{(\omega_0\sigma_k)^2 + (k\sigma_{\omega_0})^2}}{\sqrt{\omega_0^2 - 2k^2}}
\end{align*}
Die Ergebnisse und die experimentell bestimmten Resonanzfrequenzen sind in Tab.\,\ref{tab:resonance} zusammengefasst.
\begin{table}[ht]
  \centering
  \begin{tabular}{|l|c|c|}
    \hline
     & \multicolumn{2}{c|}{$\omega_\mathrm{r} [\unit{Hz}]$} \\
    \hline
     & Vorhersage & Experiment \\
    \hline
    $\unit[4]{mm}$ Dämpfung & $2.06 \pm 0.04$ & $2.03 \pm 0.04$ \\
    \hline
    $\unit[6]{mm}$ Dämpfung & $2.03 \pm 0.05$ & $2.00 \pm 0.04$ \\
    \hline
    $\unit[8]{mm}$ Dämpfung & $1.94 \pm 0.16$ & $1.98 \pm 0.04$ \\
    \hline
  \end{tabular}
  \caption{Resonanzfrequenzen der getriebenen Oszillation}
  \label{tab:resonance}
\end{table}

\begin{figure}[ht]
  \begin{center}
    \def\svgwidth{8cm}
    \input{amplituden.tex}
  \end{center}
  \caption{Amplituden bei verschiedenen Anregungsfrequenzen}
  \label{fig:amplituden}
\end{figure}
\begin{figure}[ht]
  \begin{center}
    \def\svgwidth{8cm}
    \input{phasendiagramm.tex}
  \end{center}
  \caption{Phasenverschiebung bei verschiedenen Anregungsfrequenzen}
  \label{fig:phasen}
\end{figure}
