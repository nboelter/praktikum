reset
set xrange[0:40000]
set yrange[-170:170]
set terminal epslatex color colortext
set pointsize 1.5
set ylabel 'Auslenkungswinkel $\varphi(t)$ [$^\circ$]'
set xlabel 'Zeit $t$ [ms]'
set output 'messung1-0mm.tex'
plot  './auswertung/messung0.txt' using 1:($2+7) pt 7 ps 0.4 lc 2 notitle, \
      './auswertung/auswertung.txt' index 0 lc 1 title 'Amplitude $\varphi_0$'
set output 'messung1-4mm.tex'
set xrange[0:20000]
!epstopdf 'messung1-0mm.eps'
plot './auswertung/messung1.txt' using 1:($2+7) pt 7 ps 0.4 lc 2 notitle, \
     './auswertung/auswertung.txt' index 1 lc 1 title 'Amplitude $\varphi_0$'
set output 'messung1-6mm.tex'
!epstopdf 'messung1-4mm.eps'
plot  './auswertung/messung2.txt' using 1:($2+7) pt 7 ps 0.4 lc 2 notitle, \
      './auswertung/auswertung.txt' index 2 lc 1 title 'Amplitude $\varphi_0$'
set output 'messung1-8mm.tex'
set xrange[0:10000]
set yrange[-200:200]
!epstopdf 'messung1-6mm.eps'
plot  './auswertung/messung3.txt' using 1:($2+7) pt 7 ps 0.4 lc 2 notitle, \
      './auswertung/auswertung.txt' index 3 lc 1 title 'Amplitude $\varphi_0$'
set output
!epstopdf 'messung1-8mm.eps'
