reset
fit1(x) = a1*atan(b1*(x-c1))+d1
fit2(x) = a2*atan(b2*(x-c2))+d2
fit3(x) = a3*atan(b3*(x-c3))+d3

a1 = 0.25
b1 = 6
c1 = 1
d1 = 0.45
a2 = 0.25
b2 = 6
c2 = 1
d2 = 0.45
a3 = 0.25
b3 = 6
c3 = 1
d3 = 0.45
fit fit1(x) './auswertung/auswertung5.txt' index 0 using ($1*0.00303682):($2/180) via a1, b1, c1, d1
fit fit2(x) './auswertung/auswertung5.txt' index 1 using ($1*0.00303682):($2/180) via a2, b2, c2, d2
fit fit3(x) './auswertung/auswertung5.txt' index 2 using ($1*0.00303682):($2/180) via a3, b3, c3, d3


set terminal epslatex color colortext
set ylabel 'Phasenverschiebung $\nicefrac{\phi}{\pi}$ [-]'
set xlabel 'Anregungsfrequenz $\nicefrac{\omega}{\omega_0}$ [-]'
set output 'phasendiagramm.tex'
set key bottom right
set xrange [0:2]
set yrange [0:1]
plot './auswertung/auswertung5.txt' index 1 using ($1*0.00303682):($2/180) title 'Dämpfung $\unit[4]{mm}$', \
     './auswertung/auswertung5.txt' index 2 using ($1*0.00303682):($2/180) title 'Dämpfung $\unit[6]{mm}$', \
     './auswertung/auswertung5.txt' index 0 using ($1*0.00303682):($2/180) title 'Dämpfung $\unit[8]{mm}$', \
     fit2(x) notitle lt 1 lc 1, fit3(x) notitle lt 1 lc 2, fit1(x) notitle lt 1 lc 3
set output
!epstopdf 'phasendiagramm.eps'
