reset
set xrange [1:15]
f1(x) = m1*x+b1
f2(x) = m2*x+b2
f3(x) = m3*x+b3
f4(x) = m4*x+b4
set xlabel 'Amplitudendurchlauf [\#]'
set ylabel 'Zeit $t$ in [$\unit{ms}$]'
set key bottom right
set terminal epslatex color colortext
set output 'Eigenfrequenz.tex'
fit f1(x) './auswertung/auswertung2.txt' index 0 using 1:2 via m1,b1
fit f2(x) './auswertung/auswertung2.txt' index 1 using 1:2 via m2,b2
fit f3(x) './auswertung/auswertung2.txt' index 2 using 1:2 via m3,b3
fit f4(x) './auswertung/auswertung2.txt' index 3 using 1:2 via m4,b4
plot  './auswertung/auswertung2.txt' index 0 using 1:2 lt 1 lc 1 lw 1 title '\unit[0]{mm} Dämpfung', \
      f1(x) title 'Lineare Regression' lt 1 lc 1, \
      './auswertung/auswertung2.txt' index 1 using 1:2 lt 1 lc 2 lw 1 title '\unit[4]{mm} Dämpfung', \
      f2(x) title 'Lineare Regression' lt 1 lc 2, \
      './auswertung/auswertung2.txt' index 2 using 1:2 lt 1 lc 3 lw 1 title '\unit[6]{mm} Dämpfung', \
      f3(x) title 'Lineare Regression' lt 1 lc 3, \
      './auswertung/auswertung2.txt' index 3 using 1:2 lt 1 lc 4 lw 1 title '\unit[8]{mm} Dämpfung', \
      f4(x) title 'Lineare Regression' lt 1 lc 4
set output
!epstopdf 'Eigenfrequenz.eps'
