degrees of freedom    (FIT_NDF)                        : 4
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 60.1649
variance of residuals (reduced chisquare) = WSSR/ndf   : 3619.82

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m4              = 1486.34          +/- 14.38        (0.9676%)
b4              = 1143.13          +/- 56.01        (4.9%)

degrees of freedom    (FIT_NDF)                        : 9
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 61.6857
variance of residuals (reduced chisquare) = WSSR/ndf   : 3805.12

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m3              = 1468.85          +/- 5.881        (0.4004%)
b3              = -455.618         +/- 39.89        (8.755%)

degrees of freedom    (FIT_NDF)                        : 10
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 37.0305
variance of residuals (reduced chisquare) = WSSR/ndf   : 1371.26

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m2              = 1483.75          +/- 3.097        (0.2087%)
b2              = 328.803          +/- 22.79        (6.931%)

degrees of freedom    (FIT_NDF)                        : 13
rms of residuals      (FIT_STDFIT) = sqrt(WSSR/ndf)    : 29.4513
variance of residuals (reduced chisquare) = WSSR/ndf   : 867.377

Final set of parameters            Asymptotic Standard Error
=======================            ==========================

m1              = 1518.31          +/- 1.76         (0.1159%)
b1              = 503.448          +/- 16           (3.179%)

>>> pi/1.51831
2.0691378266558167
>>> 2*pi/((2*1.51831)**2)*2*1.76/1000
0.002398510564321013
>>> pi/1.48375
2.117332875207948
>>> 2*pi/((2*1.48375)**2)*2*3.097/1000
0.004419464137839269
>>> pi/1.46885
2.1388110791365986
>>> 2*pi/((2*1.46885)**2)*2*5.881/1000
0.00856339854743666
>>> pi/1.48634
2.1136433478139547
>>> 2*pi/((2*1.48634)**2)*2*14.38/1000
0.02044901660559809
>>>



