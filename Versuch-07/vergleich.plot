reset
set ylabel 'Adiabatenexponent $\kappa$'
set terminal epslatex color colortext
set output "vergleich.tex"
set key out right top
set xrange [0:6]
set xtics ("1" 1)
set xlabel 'Schwingungen [\#]'
set xtics add ("10" 2)
set xtics add ("20" 3)
set xtics add ("50" 4)
set xtics add ("100" 5)
plot \
1.667 lt 1 lc 1 title "Argon", \
"argon.txt" using 1:2:3 with errorbars lt 1 lc 1 notitle, \
1.404 lt 1 lc 2 title "Luft", \
"luft.txt" using 1:2:3 with errorbars lt 1 lc 2 notitle, \
1.289 lt 1 lc 3 title 'CO$_2$', \
"co2.txt" using 1:2:3 with errorbars lt 1 lc 3 notitle
set output
!epstopdf vergleich.eps
