\subsection{Der Adiabatenexponent $\kappa$}
Wenn einem Gas die Wärmemenge $\Delta Q$ zugeführt wird, gilt nach dem 1. Hauptsatz der Thermodynamik:
$$\Delta Q = \Delta U - \Delta W$$
Dabei ist $\Delta U$ die Änderung der inneren Energie des Gases, und $\Delta W$ die Arbeit, die das System verrichtet.

Bei konstantem Volumen gilt für 1 Mol eines Gases \cite{Gerthsen}:
$$ \Delta Q = \Delta U = c_V \Delta T $$
Dabei ist $\Delta T$ die Temperaturänderung. Die Proportionalitätskonstante $c_V$ wird \emph{spezifische Wärmekapazität} bei konstantem Volumen genannt.

Falls bei der Erwärmung stattdessen der Druck konstant gehalten wird, dehnt sich das Gas aus. Nun muss zusätzlich eine Volumenarbeit geleistet werden \cite{Gerthsen}:
$$ \Delta W = -p \D V = - p V \frac{\Delta T}{T} = - RT \frac{\Delta T}{T} = - R \Delta T$$

Zusammen ergibt sich also nach dem 1. Hauptsatz:
$$ \Delta Q = c_V \Delta T + R \Delta T = (c_V + R) \Delta T = c_p \Delta T$$
Diese Proportionalitätskonstante wird ebenfalls \emph{spezifische Wärmekapazität} genannt, diesmal bei konstantem Druck.

Das Verhältnis der beiden wird \emph{Adiabatenexponent} $\kappa = \nicefrac{c_p}{c_V}$ genannt, da für adiabatische Zustandsänderungen die \mensch{Poisson}-Gleichung gilt \cite{Gerthsen}:
$$pV^\kappa = \mathrm{const}$$

\subsection{Methode nach Rüchardt}
Im Versuch schwingt eine Kugel in einem nach oben offenen Rohr mit Durchmesser $d$ und Querschnittsfläche $\nicefrac{\pi}{4}~d^2$, dabei drückt der äußere Luftdruck $b$ die Kugel nach unten und der Druck $p$ im Rohr in die entgegengesetzte Richtung. Die Kugel und die mit ihr im Rohr schwingende Luftsäule haben gemeinsam eine Masse von $m$, auf sie wirkt also die Gewichtskraft $mg$.

In der Gleichgewichtslage der Schwingung sind auch die Kräfte auf die Kugel im Gleichgewicht, es gilt also:
$$ \frac{\pi}{4}~d^2 (p-b) = mg$$
Bei einer Auslenkung um $\Delta x$ ändert sich der Gasdruck im Inneren um $\Delta p$. Dies bewirkt eine Rückstellkraft $F = \nicefrac{\pi}{4}~d^2 \Delta p$ zum Gleichgewichtspunkt hin.
Differenzieren der \mensch{Poisson}-Gleichung nach $\D V$ ergibt:
\begin{align*}
  0 &= \Dfrac{}{V}\left(pV^\kappa\right)  \\
  0 &=\Dfrac{p}{V} V^\kappa + p\kappa V^{\kappa-1} \\
  \D p &= -p\kappa \frac{\D V}{V} = -p\kappa \frac{\nicefrac{\pi}{4}~d^2 \Delta x}{V}  \\
\end{align*}
Einsetzen in die Rückstellkraft ergibt:
$$ F = m \ddot x = A \Delta p = \frac{-p\kappa \pi^2 d^4}{16 V} \Delta x$$  
Dies ist also ein \emph{harmonischer Oszillator}, denn die Rückstellkraft ist proportional zur Auslenkung. Also entsteht eine \emph{Selbststeuerung} der Schwingung.

Die Schwingungsdauer $T$ berechnet sich nun zu:
$$ T = \frac{2 \pi}{\omega_0} = 2\pi\sqrt{\frac{16m V}{p \kappa \pi^2 d^4}}.$$
Umstellen nach $\kappa$ ergibt:
\begin{align}
  \kappa &= \frac{64 m V}{T^2 p d^4}. \label{eq:ruechardt} 
\end{align}

\subsection{Methode nach Clement-Desormes}
\begin{figure}[h]
  \begin{center}
    \includegraphics{PV-Diagramm.pdf}
  \end{center}
  \caption{$pV$-Diagramm für Bestimmung  von $\kappa$ nach \mensch{Clement-Desormes} (Quelle: LP~\cite{LP-Adiabatenexponent})}
  \label{fig:pv-diagram}
\end{figure}
Bei dieser Methode wird der Adiabatenexponent über die Druckdifferenz der beiden Zustände (a) und (d) (Siehe Abb.~\ref{fig:pv-diagram}) bestimmt. 

Die Zustandsänderung von (a) nach (b) ist dabei die adiabatische Expansion des Gases bei Öffnung des Entlüftungsventil. Hier gilt also die \mensch{Poisson}-Gleichung. Bei diesem Vorgang kühlt sich das Gas ab.

Die Zustandsänderung von (c) nach (d) wiederum entspricht der Aufwärmung des Gases nach dem Schliessen des Ventils. Hierbei erhöht sich der Druck, das Volumen bleibt konstant. Es gilt also die \emph{ideale Gasgleichung}.

Umstellen der beiden Gleichungen resultiert nun in \cite{LP-Adiabatenexponent}:
\begin{align}
  \kappa = \frac{\Delta p_1}{\Delta p_1 - \Delta p_2} \label{eq:clement-desormes}
\end{align}
