reset
set ylabel 'Adiabatenexponent $\kappa$'
set terminal epslatex color colortext
set output "vergleich2.tex"
set key out right top
set xrange [0:4]
set xlabel 'Öffnungszeit [s]'
set xtics ("0.1" 1)
set xtics add ("1" 2)
set xtics add ("5" 3)
plot \
1.404 lt 1 lc 2 title "Luft", \
"luft2.txt" using 1:2:3 with errorbars lt 1 lc 2 notitle
set output
!epstopdf vergleich2.eps
