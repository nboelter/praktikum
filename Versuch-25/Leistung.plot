reset
set key left 
set xlabel 'Temperatur $T\quad[\unit{\degree C}]$'
set ylabel 'Leistung $P\quad[\unit{W}]$'
set terminal epslatex color colortext
set output 'Leistung.tex' 
set yrange [2:23]
plot 'Zwischenergebnisse.txt' index 1:5:4 using 4:8:9 with errorbars title 'Kupferdraht (Be)' lt 1 lc 1, \
     'Zwischenergebnisse.txt' index 3:7:4 using 4:8:9 with errorbars title 'Kupferdraht (Al)' lt 1 lc 3

set output
