# -*- coding: utf-8 -*-
from math import *
out = open('Zwischenergebnisse.txt', 'w')
out.write('#Zeit [t] Spannung [V] Heizspannung [V] Temperatur [°C] +/- Temp Widerstand [Ohm] +/- Widerstand Leistung [Watt] +/- Leistung')
for line in open('Messwerte.txt','r'):
  if len(line) < 2:
    out.write(line)
    continue
  if line[0] == '#':
    out.write(line)
    continue
  line = line.strip()
  token = line.split(' ', 3)
  token = map(float, token)
  token[1] = token[1] - 0.03
  if len(token) == 3:
    (t,U,UH) = token
    I  = 0.500
    sI  = 0.010
    sU  = 0.01
    sUH = 0.1
    T = 0.219 + 20.456*U-0.302*U**2+0.009*U**3
    sT = sU * (20.456-2*0.302*U+3*0.009*U**2)
    P = I*UH
    sP = sqrt( (sI*UH)**2 + (sUH*I)**2 ) 
    R = UH/I
    sR = sqrt( (sUH/I)**2 + (sI*UH/(I**2))**2 )
    out.write(line + ' ' + str(T) + ' ' + str(sT) + ' ' + str(R) + ' ' + str(sR) + ' ' + str(P) + ' ' + str(sP) + '\n')
  else:
    (t,U) = token
    T = 0.219 + 20.456*U-0.302*U**2+0.009*U**3
    sU  = 0.01
    sT = sU * (20.456-2*0.302*U+3*0.009*U**2)
    out.write(line + ' ' + str(T) + ' ' + str(sT) + '\n')

