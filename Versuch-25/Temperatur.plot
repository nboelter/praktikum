reset
set key bottom
aufw1(x) = m1*x+b1
abk1(x) = c1*exp(d1*x)+e1
c1(P,t) = (P==0)?1/0:1/4.77*P/(m1-c1*d1*exp(d1*t))
aufw2(x) = m2*x+b2
abk2(x) = c2*exp(d2*x)+e2
c2(P,t) = (P==0)?1/0:1/1.93*P/(m2-c2*d2*exp(d2*t))
aufw3(x) = m3*x+b3
aufw3p(x) = f1+f2*x+f3*x**2+f4*x**3
c3(P,t) = (P==0)?1/0:1/4.77*P/(f2+2*f3*t+3*f4*t**2 - c3*d3*exp(d3*t))
abk3(x) = c3*exp(d3*x)+e3
aufw4(x) = m4*x+b4
abk4(x) = c4*exp(d4*x)+e4
c4(P,t) = (P==0)?1/0:1/1.93*P/(m4-c4*d4*exp(d4*t))

d1 = -0.001
d2 = -0.001
d3 = -0.001
d4 = -0.001

e3 = -300.0
e4 = -300.0

fit [0:270] aufw1(x) 'Zwischenergebnisse.txt' index 1 using 1:4:5 via m1,b1
fit [300:900] abk1(x) 'Zwischenergebnisse.txt' index 1 using 1:4:5 via c1,d1,e1
fit [0:240] aufw2(x) 'Zwischenergebnisse.txt' index 3 using 1:4:5 via m2,b2
fit [300:1400] abk2(x) 'Zwischenergebnisse.txt' index 3 using 1:4:5 via c2,d2,e2
fit [0:900] aufw3p(x) 'Zwischenergebnisse.txt' index 5 using 1:4:5 via f1,f2,f3,f4
fit [400:900] aufw3(x) 'Zwischenergebnisse.txt' index 5 using 1:4:5 via m3,b3
fit [900:3000] abk3(x) 'Zwischenergebnisse.txt' index 5 using 1:4:5 via c3,d3,e3
fit [0:900] aufw4(x) 'Zwischenergebnisse.txt' index 7 using 1:4:5 via m4,b4
fit [900:3000] abk4(x) 'Zwischenergebnisse.txt' index 7 using 1:4:5 via c4,d4,e4

set xlabel 'Zeit $t\quad[\unit{s}]$'
set ylabel 'Temperatur $T\quad[\unit{^\circ C}]$'
set terminal epslatex color colortext
set output 'temp-Be-hoch.tex'
set yrange [20:90]
plot 'Zwischenergebnisse.txt' index 1 using 1:4:5 with errorbars title 'Messwerte', aufw1(x) lw 2 lc 2 title 'lineare Regression', abk1(x) title 'exponentielle Regression' lw 2 lc 3
set output 'temp-Al-hoch.tex'
set yrange [20:120]
plot 'Zwischenergebnisse.txt' index 3 using 1:4:5 with errorbars title 'Messwerte', aufw2(x) lw 2 lc 2 title 'lineare Regression', abk2(x) title 'exponentielle Regression' lw 2 lc 3
set output 'temp-Be-niedrig.tex'
set yrange [-180:-60]
plot 'Zwischenergebnisse.txt' index 5 using 1:4:5 with errorbars title 'Messwerte', abk3(x) title 'exponentielle Regression' lw 2 lc 2, aufw3p(x) lw 2 lc 4 title 'polynomiale Regression dritten Grades'
set output 'temp-Al-niedrig.tex'
set yrange [-180:-100]
plot 'Zwischenergebnisse.txt' index 7 using 1:4:5 with errorbars title 'Messwerte', aufw4(x) lw 2 lc 2 title 'lineare Regression', abk4(x) title 'exponentielle Regression' lw 2 lc 3
set output 'spezwaerme.tex'
set ylabel 'Molare Wärmekapazität $c\quad[\unitfrac{J}{mol\cdot K}]$'
set xlabel 'Temperatur $T\quad[\unit{\degree C}]$'
set yrange [*:26]
plot [-180:120] 'Zwischenergebnisse.txt' index 1 using 4:(c1($8,$1)) title 'Beryllium' lw 2 lc 1 pt 1 ps 1.5, \
     'Zwischenergebnisse.txt' index 5 using 4:(c3($8,$1)) notitle lw 2 lc 1 pt 1 ps 1.5, \
     'Zwischenergebnisse.txt' index 3 using 4:(c2($8,$1)) title 'Aluminium' lw 2 lc 3 pt 2 ps 1.5, \
     'Zwischenergebnisse.txt' index 7 using 4:(c4($8,$1)) notitle lw 2 lc 3 pt 2 ps 1.5, \
      24.9 title 'Dulong-Petit' lw 2 lc 0 
set output
