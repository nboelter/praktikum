reset
set key left 
set xlabel 'Temperatur $T\quad[\unit{\degree C}]$'
set ylabel 'Widerstand $R\quad[\unit{\Omega}]$'
set terminal epslatex color colortext
set output 'Widerstand.tex' 
set yrange [5:90]
plot 'Zwischenergebnisse.txt' index 1:5:4 using 4:6:7 with errorbars title 'Kupferdraht (Be)' lt 1 lc 1, \
     'Zwischenergebnisse.txt' index 3:7:4 using 4:6:7 with errorbars title 'Kupferdraht (Al)' lt 1 lc 3

set output
