\subsection{Wärmekapazität}
Die \emph{Wärmekapazität} eines Körpers bestimmt, wieviel Energie $\Delta E$ ihm zugefügt werden muss, um seine Temperatur um $\Delta T$ zu erhöhen.\citep[S.\,212]{Gerthsen}
\begin{align*}
  C &= \frac{\Delta E}{\Delta T}
\end{align*}
Wie bei allen vernünftigen Körpereigenschaften gibt es natürlich auch noch die \emph{spezifische Wärmekapazität}, indem man durch die Masse $M$ des Körpers teilt:
\begin{align}
  c &= \frac{\Delta E}{M \Delta T}
\end{align}

Anders als beim vorherigen \emph{Versuch 6 - Spezifische W\"arme der Luft und Gasthermometer} ändern die hier untersuchten Festkörper ihr Volumen kaum bei einer Änderung der Temperatur. Die geleistete Volumenarbeit kann also vernachlässigt werden, so dass hier die spezifische Wärmekapazität bei konstantem Volumen und bei konstantem Druck nicht unterschieden werden müssen.

\subsection{Thermoelement}
% Hier müsste eigentliche eine Skizze hin. Viel Spaß.
\label{sec:sonde}
Die Temperatur der Festkörper wird mit Hilfe des \mensch{Seebeck}-Effekts bestimmt. Dabei stellt sich an den Kontaktstellen zweier verschiedener Metalle eine Kontaktspannung ein. Gibt es nun zwei verschiedene Kontaktstellen mit unterschiedlicher Temperatur, so stellen sich verschiedene Spannungen ein \citep[S.\,345]{Gerthsen}, so dass über ein Voltmeter eine Spannung abgelesen werden kann, die von der Temperaturdifferenz der Kontaktstellen abhängt. Hält man eine der Kontaktstellen bei konstanter Temperatur, kann so die absolute Temperatur bestimmt werden.

Laut der Praktikumsanleitung \citep[S.\,224]{Anleitung} lässt sich für die benutzte Kombination von Eiswasser als Referenztemperatur und der Kombination der Metalle die Temperatur über folgende Gleichung näherungsweise bestimmen:
\begin{align}
  T\quad[\degree C] = 0.219 + 20.456\cdot U - 0.302\cdot U^2 +0.009\cdot U^3 \qquad [U] = \unit{mV} \label{eq:temperatur}
\end{align}
\subsection{Elektrische Energie}
Im vorliegenden Versuch werden die Festkörper mit Hilfe von stromdurchflossenen Spulen aufgewärmt. Der Widerstand und die elektrische Leistung lassen sich mit den bekannten Zusammenhängen aus den gemessenen Größen Spannung $U$ und Stromstärke $I$ berechnen: \citep[S.\,319 ff]{Gerthsen}
\begin{align}
  R &= \frac{U}{I} \label{eq:ohm} \\
  P &= UI = I^2 R\\
  E &= \int P \mathrm{d}t
\end{align}
\subsection{Berechnung der Wärmekapazität}
Um die Wärmekapazität der benutzten Festkörper zu bestimmen, benutzt man die Energieerhaltung für die zugeführte elektrische Arbeit $W_\mathrm{el}$, die innere Energie $U$ und die an die Umgebung abgegebene Wärme $Q$.
\begin{align*}
  \Delta W_\mathrm{el} &= \Delta U + \Delta Q \\
  P_\mathrm{el} &= \Dfrac{W_\mathrm{el}}{t} = \Dfrac{U}{t} + \Dfrac{Q}{t} = \underbrace{\Dfrac{E}{T}}_{C}\Dfrac{T}{t} + \Dfrac{Q}{t} \\ 
\end{align*}
Alle Summanden hängen nur von der Temperatur ab, da die Stromstärke konstant gehalten wird und der Widerstand nur von der Temperatur abhängt. Ebenso hängt auch die an die Umgebung abgegebene Wärme nur von der Temperatur ab.

Nun lassen sich die Gleichungen für Aufheizen und Abkühlung ($P_\mathrm{el} = 0$) aufstellen.
\begin{align*}
  U(T) I &= C \left.\Dfrac{T(T)}{t}\right|_\mathrm{Aufheizen} + \Dfrac{Q(T)}{t} \\
  0 &= C \left.\Dfrac{T(T)}{t}\right|_\mathrm{Abkühlen} + \Dfrac{Q(T)}{t}
\end{align*}
Umstellen nach $C$ ergibt nun:
\begin{align}
  C &= \frac{U(T)I}{\left.\Dfrac{T(T)}{t}\right|_\mathrm{Aufheizen} - \left.\Dfrac{T(T)}{t}\right|_\mathrm{Abkühlen}} \label{eq:waermekap} \end{align}

Für hohe Temperaturen gilt bei Festkörpern näherungsweise die Regel von \mensch{Dulong-Petit}: \citep[S.\,213]{Gerthsen}
\begin{align}
  c m &= 3 R
\end{align}
Dabei ist $R$ die universelle Gaskonstante und $m$ die molare Masse.
