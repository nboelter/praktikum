from math import *

def weightedmean(data):
  mean = 0
  smean = 0
  for (value, error) in data:
    mean = mean + value/error**2
    smean = smean + 1/error**2
  mean = mean/smean
  smean = 1/sqrt(smean)
  return (mean, smean)

# create an array of data
# (time, cepheid-brightness, refstar-brightness)
data = []
i = 0
f = open('Messwerte.txt', 'r')
for line in f:
  if len(line)<2:
    i = i+1
    continue
  if i>3:
    break
  i = 0
  if line[0] == '#':
    continue
  data.append(map(float, line.strip().split(' ', 3)))
f.close

# calculate the mean energy flux and (using the corresponding
# luminosities) the distance of the three cepheids
i = 0
meanF = 0
numberofvalues = 25
lasttime = 50739.006319
# reference star's energy flux from the manual.
refstarenergyflux = 2.33*pow(10.0,-15.0)
# Luminosities determined from the graph in the manual.
L = [1.93, 0.63, 0.9] 
# reading error
sL = 0.2*pow(10,29)
for j in [0,1,2]:
  L[j] = L[j]*pow(10.0,29.0)

distarray = []
f = open('distance-and-mean-energy-flux.txt', 'w')
f.write('#cepheid, mean energy flux, distance [m], distance [ly]\n')
for(time, cepheid, reference) in data:
  meanF = meanF + cepheid/reference
  if time == lasttime:
    meanF = meanF/numberofvalues * refstarenergyflux
    distance = sqrt(L[i]/(4.0*pi*meanF))
    sdist = distance*sL/(2*L[i])
    f.write(str(6+i) + '  ' + str(meanF) + '  ' + str(distance) + '+/-' +
            str(sdist) + '  ' + str(distance/(9.461*pow(10.0,15.0))) + 
            '+/-' + str(sdist/(9.461*pow(10.0,15.0))) + '\n')
    meanF = 0
    distarray.append((distance/(9.461*pow(10.0,15.0)),
                      sdist/(9.461*pow(10.0,15.0))))
    i = i+1

# weighted mean distance and error in kly
(dkly, sdkly) = weightedmean(distarray)
f.write('\nweighted mean: ' + str(dkly) + '+/-' + str(sdkly))
# relative aberration in comparison with literature value
litd = 162980
relab = (dkly-litd)/litd
f.write('\nrelative aberration: ' + str(relab))
f.close()
