\subsection{The $\kappa$-mechanism}\label{sec:kappa}
In general, a star is in a state of equilibrium of forces, where the gravitational force is cancelled by the nuclear fusion's radiation pressure.
When a disturbance of this equilibrium occurs, the star begins to pulsate.
The strength of this pulsation depends on the kind of repulsive force.

The aforementioned radiation pressure cannot propagate freely.
Because of the high density inside the star, the radiation is scattered frequently.
The opacity describes how impenetrable to electromagnetic radiation the star is.
Its value depends on pressure, temperature and wavelength of the radiation.
If the opacity rises with higher temperature, the $\kappa$-mechanism can occur.
It can be described as follows.

An extraneous disturbance compresses the material in a specific zone of the star's atmosphere.
It thus moves to the center of the star.
As a result of the compression, pressure and temperature of this material rise.
This also leads to a higher opacity.
The higher opacity reduces the amount of radiation that goes through the material, the radiation pressure under the material rises.
The material expands due to the radiation pressure.
Consequently, pressure, temperature and thus opacity decrease again.
The \enq{accumulated} radiation escapes and the gravitational pull on the material dominates again.
This leads to the cycle starting again.
\subsection{Cepheids and the period-luminosity relationship}
In conclusion, Cepheids are very bright stars with a strong direct relation between their luminosity and pulsation period.
They change radius, temperature and thus brightness at certain wavelengths.
These pulsations result from a variety of thermodynamic properties and relations which are results of the $\kappa$\emph{-mechanism} (see~\ref{sec:kappa}).

In good approximation, we can express the period-luminosity relation as follows~\citep{CepheidPeriod}:
\begin{align*}
  M_v &= -2.43 (\log_{10}(P) - 1) - 4.05
\end{align*}
$M_v$ is the absolute magnitude of the star, $P$ is the period in days.
The absolute magnitude of a given cepheid can then be compared with a reference star's magnitude.
If we know the brightness of the reference star, we can then calculate the cepheid's brightness via:
\begin{align*}
  M_{v,C} F_C &= M_{v,R} F_R
\end{align*}
where $X_C$ is a property of the Cepheid and $X_R$ of the reference star.

Using the information from the manual~\citep{AnleitungA2}, we can easily derive an equation for the time $\Pi$ a soundwave needs to propagate through a star:
\begin{align*}
  \Dfrac{P(r)}{r} &\approx -g(r)\rho(r),
  &\Dfrac{P(r)}{r} &\approx \frac{<P>}{R}, \\
  g(r) &\approx \frac{GM}{R^2},
  &\rho(r) &\approx <\rho> = \frac{3M}{4\pi R^3}, \\
  <c>^2 &\approx \frac{<P>}{<\rho>},
  &\Pi &\approx \frac{R}{<c>} \\\\
  \Rightarrow \Pi &\approx \sqrt{\frac{R^3}{GM}}
\end{align*}
$P(r)$ is the pressure, $g(r)$ the gravity acceleration, $\rho(r)$ the density of the star at radius $r$.
$R$ is the star's radius, $M$ its mass and $G$ the gravitational constant.

From the apparent brightness $F$ of a source with luminosity $L$ in the distance $d$ we get~\citep{AustraliaTelescope}:
\begin{align}
  d &= \sqrt{\frac{L}{4\pi F}}\label{eq:dist}
\end{align}
