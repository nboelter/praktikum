reset
set key bot left box
set xrange [50698:50740]
set xlabel 'Julian date $[\unit{d}]$'
set ylabel 'Brightness relative to reference star $[-]$'
set terminal epslatex color colortext
set output 'Cepheid6_light-curve.tex'
plot 'Messwerte.txt' index 0 u 1:($2/$3) with linespoints title 'Cepheid 6'
set output 'Cepheid7_light-curve.tex'
plot 'Messwerte.txt' index 1 u 1:($2/$3) with linespoints title 'Cepheid 7'
set output 'Cepheid8_light-curve.tex'
plot 'Messwerte.txt' index 2 u 1:($2/$3) with linespoints title 'Cepheid 8'
set output
