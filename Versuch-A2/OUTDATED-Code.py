from math import *

#input for the periods and the corresponding errors (guessed)
p1=13.7
p2=5.5
p3=7.7    #TODO!!!TODO!!!TODO!!!TODO
sp1=0.2/sqrt(4.0)
sp2=0.2/sqrt(6.0)
sp3=0.2/sqrt(4.0)

#calculation of the related absolute magnitude for a given period
def absmag(period):
  a=-2.43
  b=-1.0
  c=-4.05
  return a*(log(period,10)+b)+c

#propagation of uncertainty for the absolute magnitude
def smag(period, error):
  a=-2.43
  return abs(error*a/(period*log(10)))

#calculation of the luminosity
def lumi(Mstar):
  Msun=4.42
  Lsun=3.839*pow(10.0,26.0)
  return Lsun*pow(10.0, (Msun-Mstar)/2.5)

#propagation of uncertainty for the luminosity
def slumi(magnitude, error):
  return error*log(10)*lumi(magnitude)/2.5

#output of the calculated data
print 'cepheid| period| absolute magnitude'
print '6| ', p1, ' +/- ', sp1, '| ', absmag(p1), ' +/- ', smag(p1,sp1)
print '7| ', p2, ' +/- ', sp2, '| ', absmag(p2), ' +/- ', smag(p2,sp2)
print '8| ', p3, ' +/- ', sp3, '| ', absmag(p3), ' +/- ', smag(p3,sp3)
print '-----------------------------------'
print 'cepheid| luminosity'
print '6| ', lumi(absmag(p1)), ' +/- ', slumi(absmag(p1),smag(p1,sp1))
print '7| ', lumi(absmag(p2)), ' +/- ', slumi(absmag(p2),smag(p2,sp2))
print '8| ', lumi(absmag(p3)), ' +/- ', slumi(absmag(p3),smag(p3,sp3))

##input for the periods and the corresponding errors (guessed)
#p1=13.7
#p2=5.5
#p3=7.7    #TODO!!!TODO!!!TODO!!!TODO
#sp1=0.2/sqrt(4.0)
#sp2=0.2/sqrt(6.0)
#sp3=0.2/sqrt(4.0)
#
##calculation of the related absolute magnitude for a given period
#def absmag(period):
#  a=-2.43
#  b=-1.0
#  c=-4.05
#  return a*(log(period,10)+b)+c
#
##propagation of uncertainty for the absolute magnitude
#def smag(period, error):
#  a=-2.43
#  return abs(error*a/(period*log(10)))
#
##calculation of the luminosity
#def lumi(Mstar):
#  Msun=4.42
#  Lsun=3.839*pow(10.0,26.0)
#  return Lsun*pow(10.0, (Msun-Mstar)/2.5)
#
##propagation of uncertainty for the luminosity
#def slumi(magnitude, error):
#  return error*log(10)*lumi(magnitude)/2.5
#
##output of the calculated data
##print 'period | absolute magnitude | luminosity'
#L=[]
#for (p,sp) in [(p1,sp1), (p2,sp2), (p3,sp3)]:
#  mag = absmag(p)
#  sma = smag(p,sp)
#  lum = lumi(mag)
#  slu = slumi(mag,sma)
#  L.append((lum,slu))
#  #print p, '+/-', sp, '|', mag, '+/-', sma, '|', lum, '+/-', slu
#
#for (lum,slu) in L:
#  print lum, ' ', slu

