reset
set key right bottom
set yrange [0:75]
set xrange [0:4]
set ylabel 'Oberflächenspannung $[\unitfrac{mN}{m^2}]$'
set xtics ("Kapillare 1" 1)
set xtics add ("Kapillare 2" 2)
set xtics add ("Kapillare 3" 3)
set terminal epslatex color colortext
set output "sigma_auswertung.tex"
plot 71.99 title "Literaturwert Wasser" lt 1 lc 1, 47.99 title "Literaturwert Ethylenglykol" lt 1 lc 2, 22.07 title "Literaturwert Methanol" lt 1 lc 3, 'sigma_wasser.txt' using 1:2:3 with errorbars notitle lt 1 lc 1, 'sigma_methanol.txt' using 1:2:3 with errorbars notitle lt 1 lc 3, 'sigma_ethylenglykol.txt'  using 1:2:3 with errorbars notitle lt 1 lc 2
set output
!epstopdf sigma_auswertung.eps
