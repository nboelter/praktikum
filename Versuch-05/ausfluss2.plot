reset
f(x) = m*x+b
set xlabel 'Zeit $t$ [s]'
set ylabel '$\log(\nicefrac{h}{\unit{m}})$'
set terminal epslatex color colortext
set output "ausfluss_fit.tex"
fit f(x) "ausfluss2.txt" using 2:(log($1 - 0.01))  via m,b
set xrange [-5:500]
plot "ausfluss2.txt" using 2:(log($1 - 0.01)):3 with xerrorbars title "Messwerte für Ausflusszeit", f(x) title "Lin. Regression" lt 1 lc 2
set output
!epstopdf "ausfluss_fit.eps" 
