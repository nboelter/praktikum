reset
set key right top
set xrange [0:5]
set ylabel 'Viskosität $[\unit{mPa\cdot s}]$'
set xtics ("Kapillare 1" 1)
set xtics add ("Kapillare 2" 2)
set xtics add ("Kapillare 3" 3)
set xtics add ("Log. Ansatz" 4)
set terminal epslatex color colortext
set output "eta_auswertung.tex"
plot 1.0016 title 'Literaturwert' lt 1 lc 1, 'eta.txt' using 1:2:3 with errorbars title "Ergebnisse" lt 1 lc 2
set output
!epstopdf eta_auswertung.eps
