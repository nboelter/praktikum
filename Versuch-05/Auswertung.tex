\subsection{Oberflächenspannung}
Die arithmetischen Mittel der gemessen Durchmesser sind:
\begin{align*}
  d_1 &= \unit[(1.23 \pm 0.02) 10^{-3}]{m} \\
  d_2 &= \unit[(1.76 \pm 0.02) 10^{-3}]{m} \\
  d_3 &= \unit[(0.88 \pm 0.02) 10^{-3}]{m}
\end{align*}
Dabei wurde der Fehler als 1 Skaleneinteilung pro Messvorgang geschätzt. ($2\cdot \unit[10]{\mu m}$)

Im weiteren Verlauf des Protokolls bezeichnen die Indizes 1,2 und 3 jeweils mit diesen Kapillaren gemessene bzw. berechnete Werte.

Für die Dichte kann man wegen $\rho = \frac{T}{T_\mathrm{Wasser}} \cdot \rho_\mathrm{Wasser}, T = \sum\limits_i m_i r_i$ den absoluter Fehler folgendermaßen ausrechnen:
$$\sigma_\rho = \rho \sqrt{\left(\frac{\sigma_{T_\mathrm{Wasser}}}{T}\right)^2 + \left( \frac{\sigma_T}{T_\mathrm{Wasser}}\right)^2} $$
Dabei bezeichnen $m_i$ die Massen der Gewichtsstücke in $r_i$ Entfernung von der Drehachse.

Es ergeben sich folgende Dichten:
\begin{align*}
\rho_\mathrm{Methanol} &= (820 \pm 5.14)\,\unitfrac{kg}{m^3}\\
\rho_\mathrm{Ethylenglykol} &= (1050 \pm 2.88)\,\unitfrac{kg}{m^3} \\
\end{align*}

Umstellen der Formel für den Höhenunterschied (\ref{eq:hoehe}) nach der Oberflächenspannung ergibt:
\begin{align*}
  \sigma &= \frac{1}{2} h \rho g r
\end{align*}
Der absolute Fehler für $\sigma$ berechnet sich nun zu:
\begin{align*}
  \sigma_\sigma &= \sigma \sqrt{ \left( \frac{\sigma_h}{h} \right)^2 + \left( \frac{\sigma_\rho}{\rho} \right)^2 + \left( \frac{\sigma_g}{g} \right)^2 +  \left( \frac{\sigma_r}{r} \right)^2 }
\end{align*}


Es ergeben sich für die drei Flüssigkeiten die folgenden gewichteten Mittelwerte für die Oberflächenspannung:
\begin{empheq}[box=\fbox]{align*}
  \label{eqn:eta_log}
  \sigma_\mathrm{Wasser}        &= \unitfrac[(69.2 \pm 1.6)~10^{-3}]{N}{m}\\
  \sigma_\mathrm{Methanol}      &= \unitfrac[(24.2 \pm 0.8)~10^{-3}]{N}{m}\\
  \sigma_\mathrm{Ethylenglykol} &= \unitfrac[(50.7 \pm 1.1)~10^{-3}]{N}{m}
\end{empheq}

\subsection{Viskosität}

\subsubsection{Linearer Ansatz}
Mit dem \mensch{Hagen-Poiseuille}schen Gesetz (\ref{eq:hagen}) und $\Delta V / \Delta t \approx \dot V$ (Sekantensteigung) erhält man folgende Beziehung:
\begin{align*}
 \frac{\Delta V}{\Delta t} &= A \frac{\Delta h}{\Delta t} \stackrel{!}{=} \frac{\pi \cdot \Delta P \cdot R_{\mathrm{K}}^4}{8 \eta l},\\
\end{align*}
die sich dann mit Hilfe der Hydrostatischen Grundgleichung und durch Mittelung $(\Delta P = h-\Delta h/2)$
zu einer Formel zur Berechnung von $\eta$ umstellen lässt:
\begin{align*}
 \eta &= \frac{\rho g \left(h-\frac{\Delta h}{2}\right)}{8l} \frac{\Delta t}{\Delta h} \frac{R_\mathrm{K}^4}{R_\mathrm{G}^2}\\
\end{align*}
Der relative Fehler für $\eta$ beträgt somit:
\begin{align*}
	\frac{\sigma_\eta}{\eta} = \sqrt{ \left( \frac{4\sigma_{d_\mathrm{K}}}{d_\mathrm{K}}\right)^2 + \left( \frac{2\sigma_{d_\mathrm{G}}}{d_\mathrm{G}} \right)^2  + \left( \frac{\sigma_l}{l} \right)^2 + \left( \frac{\sigma_{\Delta h}}{\Delta h} \right)^2 + \left( \frac{\sigma_{\Delta t}}{\Delta t} \right)^2}
\end{align*}
Mit den gemessenen Werten ergeben sich für die drei Kapillaren die folgenden Mittelwerte:
\begin{empheq}[box=\fbox]{align*}
  \label{eqn:eta_lin}
  \eta_1 &= \unit[(1.30 \pm 0.10)~10^{-3}]{Pa\cdot s}\\
	\eta_2 &= \unit[(1.61 \pm 0.15)~10^{-3}]{Pa\cdot s}\\
  \eta_3 &= \unit[(1.11 \pm 0.11)~10^{-3}]{Pa\cdot s}
\end{empheq}

\subsubsection{Logarithmischer Ansatz}
Hier kann man nun $\dot V = A \dot h$ direkt in die \mensch{Hagen-Poiseuille}-Gleichung einsetzen:
\begin{align*}
	\dot V &= A \Dfrac{h}{t} \stackrel{!}{=} \frac{\pi \cdot \Delta P \cdot R_{\mathrm{K}}^4}{8 \eta l} \\
\end{align*}
Für die Druckdifferenz gilt $\Delta P = \rho h g.$

\begin{figure}
  \begin{center}
  \input{ausfluss_fit.tex}
  \end{center}
  \caption{Lineare Regression für den Zusammenhang zwischen der Höhe des Flüssigkeitsspiegels und der Ausflusszeit}
  \label{fig:fit-ausfluss}
\end{figure}

Aus der Regression der Messwerte\footnote{Mit gnuplot, $\chi_\mathrm{red}^2 = 7.20667\cdot10^{-7}$} (Siehe Abb.~\ref{fig:fit-ausfluss}) durch $\ln(h/\mathrm{m}) = \lambda t + b$ ergibt sich dann
$$\frac{\dot h}{h}=\frac{\Dfrac{}{t}\exp(\lambda t + b)}{\exp(\lambda t + b)} = \lambda = (-1.466 \pm 0.002)~10^{-3}.$$

Umstellen liefert:
\begin{align*}
	\eta &= \frac{\rho g}{8\lambda l} \frac{R_\mathrm{K}^4}{R_\mathrm{G}^2},\\
\end{align*}
wobei der relative Fehler für $\eta$ sich wie folgt ergibt:
\begin{align*}
	\frac{\sigma_\eta}{\eta} = \sqrt{ \left( \frac{4\sigma_{d_\mathrm{K}}}{d_\mathrm{K}}\right)^2 + \left( \frac{2\sigma_{d_\mathrm{G}}}{d_\mathrm{G}} \right)^2 + \left( \frac{\sigma_m}{m} \right)^2 + \left( \frac{\sigma_l}{l} \right)^2}
\end{align*}
Durch Einsetzen erhält man:
\begin{empheq}[box=\fbox]{align*}
  \label{eqn:eta_log}
  \eta &= \unit[(1.08 \pm 0.11)~10^{-3}]{Pa \cdot s}
\end{empheq}
