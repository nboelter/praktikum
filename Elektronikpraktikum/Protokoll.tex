% Festlegen des Dokumententyps
\documentclass[a4paper,twoside]{article}

% Papierformat
\usepackage{a4}

% Deutsche Sprache (Silbentrennung, usw.)
\usepackage[ngerman]{babel}

% Schrifteneinstellungen
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{textcomp}

% bessere Matheunterstützung
\usepackage{amsfonts}
\usepackage{amstext}
\usepackage{amsmath}
\usepackage{empheq}

% Kodierung
\usepackage{ucs}
\usepackage[utf8x]{inputenc}

% Grafiken einbinden
\usepackage{graphicx}
\usepackage{color}

% Floats inside Floats
\usepackage{subfig}
\usepackage[left=3cm, right=3cm]{geometry}

% Prevent Floating outside of Section
\usepackage[section]{placeins}

% Nice Header
\usepackage{fancyhdr}

% Clickable URLs and references
\usepackage{hyperref}

% Bibliography
\usepackage[comma,sort&compress]{natbib}

% Proper units
\usepackage{units}
\usepackage[section]{placeins}

% Custom packages
% Nicer paragraphs
\parskip 12pt
\parindent 0pt

\newcommand{\degree}{\ensuremath{^{\circ}}}
\newcommand{\D}[1]{\ensuremath{\mathrm{d}#1}}
\newcommand{\Dfrac}[2]{\ensuremath{\frac{\mathrm{d}#1}{\mathrm{d}#2}}}
\newcommand{\mensch}[1]{\textsc{#1}}
\newcommand{\T}{\textstyle}
\newcommand{\sym}[1]{$ #1 $}
\newcommand{\ret}{$[\hookleftarrow]$}
\newcommand{\q}[1]{\glqq{#1}\grqq}
\renewcommand{\vec}[1]{\boldsymbol{#1}}

%
\begin{document}

\begin{titlepage}
\includegraphics[height=2cm]{georg} \hfill

\normalsize

\vspace{5cm}

\begin{center}
\Huge Elektronikpraktikum \\[5mm]
{\bf Theremin}
\end{center}

\normalsize

\begin{table}[!h]
\begin{center}
\subfloat{
  \begin{tabular}{ll}
  Praktikanten: &Julius Strake\\
  &Niklas Bölter\\
  \end{tabular}
}
\\
\vspace{2.0cm}
\begin{tabular}{lr}
  E-Mail: & \ttfamily theremin-feedback@1tau.de\\
\end{tabular}
\end{center}
\end{table}
\end{titlepage}
\pagestyle{empty}
\clearpage\markboth{}{}\cleardoublepage
\pagestyle{empty}
\tableofcontents
\newpage
\pagestyle{headings}
\section{Einleitung}
Ziel dieses Projektes war der Bau eines funktionierenden Theremins mit Lautstärke- und Tonhöhenregelung via hand-waving. Die theoretischen Grundlagen beinhalten LC-Schwingkreise, Differenzverstärker mit Transistoren, Operationsverstärker und Dioden.
\section{Funktionsweise}
Die Grundidee des Theremins ist es, die Kapazität eines LC-Schwingkreises durch den Abstand der Hand des Virtuosen zu einer Antenne zu verändern, um dessen Frequenz zu beeinflussen. Da mit dieser Methode nur sehr kleine Kapazitätsänderungen in der Größenordnung von einigen Picofarad erreicht werden können, muss die Frequenz $f=(2\pi\sqrt{LC})^{-1}$ des Schwingkreises entsprechend hoch sein. Typische Werte für $f$ liegen im Bereich von einigen Hundert Kilohertz. Da diese Frequenzbereiche den menschlichen Sinnesorganen nicht zugänglich sind, wird mit Hilfe eines zweiten Schwingkreises von fester Frequenz eine Schwebung erzeugt. Die Schwebungsfrequenz befindet sich dann im hörbaren Bereich unterhalb von $\unit[20]{kHz}$.

Für die Lautstärkenregelung wird ein weiterer LC-Schwingkreis verwendet, welcher in einem vierten LC-Schwingkreis eine Schwingung erzwingt. Mit dem Abstand der zweiten Hand zur Lautstärke-Antenne wird nun die Resonanzfrequenz des letzteren Schwingkreises verändert. Die Lautstärke ist nun abhängig von der Amplitude der erzwungenen Schwingung und damit mit der Übereinstimmung der Resonanzfrequenz und der erzwungenen Frequenz.

Die Signale werden dann mit Hilfe eines spannungsgesteuerten Verstärkers weiterverarbeitet. Dabei wird die Schwingung aus dem Tonhöhenmodul in Abhängigkeit des Signals aus dem Lautstärkemodul verstärkt.
\newpage
\section{LC-Schwingkreis mit Differenzverstärker}
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.95\textwidth]{LC-diff-schematic.png}
	\caption{LC-Schwingkreis mit Differenzverstärker}
	\label{fig:lc-diff-schematic}
\end{figure}
Bei diesem Aufbau (siehe Abb.\,\ref{fig:lc-diff-schematic}) wird die Schwingung eines LC-Kreises mit Hilfe eines Differenzverstärkers erhalten. Ein Eingang des Differenzverstärkers ist kapazitiv an den Schwingkreis gekoppelt, während einer der Ausgänge dem Schwingkreis Energie zuführt. Diese Schwingung kann nun mit Hilfe eines Kondensators ausgekoppelt werden.
\newpage
\section{Tonhöhenregelung}
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.95\textwidth]{pitch-schematic.png}
	\caption{Modul für die Tonhöhenregelung}
	\label{fig:pitch-schematic}
\end{figure}
Im Tonhöhenmodul (siehe Abb.\,\ref{fig:pitch-schematic}) werden zwei LC-Schwingkreise mit Differenzverstärker bei einer Frequenz von etwa $\unit[670]{kHz}$ betrieben. Einer davon ist an die Antenne, der andere an einen weiteren Regelkreis zur Feineinstellung der Schwingungsfrequenz angeschlossen. Mit der Antenna lässt sich die Schwingungsfrequenz des ersten verändern, die des zweiten wird zu Beginn des Konzertes auf einen sinnvollen Wert gestellt, so dass sich die gewünschten Tonhöhen erreichen lassen.

Die Ausgänge der beiden Schwingkreise werden mit einem Kondensator ausgekoppelt und dann verbunden - die Einhüllende der resultierenden Schwebung ist nun in einem hörbaren Frequenzbereich und muss herausgefiltert werden. Dafür wird der Teil des Signals mit einer Spannung unter etwa $\unit[-0.2]{V}$ durch eine Schottky-Diode entfernt. Der hochfrequente Teil des resultierenden Signals wird mit einem Kondensator entfernt. Nun erhält man eine Wechselspannung mit einer hörbaren Frequenz, welche mit Hilfe eines einfachen Verstärkers direkt an einen Lautsprecher angeschlossen werden kann.
\newpage
\section{Lautstärkeregelung}
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.95\textwidth]{volume-schematic.png}
	\caption{Modul für die Lautstärkenregelung}
	\label{fig:volume-schematic}
\end{figure}
Im Lautstärkemodul (siehe Abb.\,\ref{fig:volume-schematic}) wird der regelbare LC-Schwingkreis mit Differenzverstärker aus dem Tonhöhenmodul mit etwas veränderter Beschaltung übernommen und an einen passiven Schwingkreis angeschlossen. Der passive Schwingkreis besteht aus einer Spule und der Kapazität, welche durch die Lautstärke-Antenne gebildet wird. 

Die Amplitude der erzwungenen Schwingung wird ähnlich wie im Tonhöhenmodul mit Hilfe einer Diode und eines Kondensators bestimmt, indem der positive Teil des Signals abgeschnitten wird und auf einen Kondensator gegeben wird. Kommt die Resonanzfrequenz des passiven Schwingkreises durch Handbewegung näher an die erzwungene Frequenz, steigt die Amplitude und damit die Ladung auf dem Kondensator, welche als Signal ausgelesen wird.
\newpage
\section{Spannungsgesteuerter Verstärker}
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.95\textwidth]{amp-schematic.png}
	\caption{Verstärkermodul}
	\label{fig:amplifier-schematic}
\end{figure}
Auf dem Verstärkermodul (siehe Abb.\,\ref{fig:amplifier-schematic}) werden die beiden Signale zuerst durch Impedanzwandler geleitet um die Schwingkreise nicht zu belasten und damit zu stören. Dann werden die noch vorhandenen hochfrequenten Teile des Lautstärkensignal mit einem Elektrolytkondensator entfernt und das Signal mit einem Potentiometer in den notwendingen Spannungsbereich geregelt.

Das Tonhöhensignal wird nun abhängig vom bereinigten Lautstärkensignal (spannungsgesteuert) verstärkt. Dafür wird es durch einen N-Channel JFET (Junction Field Effect Transistor) geleitet, an dessen Gate das Lautstärkensignal anliegt. Dieser arbeitet im verwendeten Spannungsbereich als spannungsgesteuerter Widerstand, da die anliegende Gate-Spannung die Kanalbreite (bzw.\ Verarmungszone) im JFET beeinflusst. Dann wird das resultierende Signal durch einen invertierenden Verstärker und danach noch einmal durch einen Impedanzwandler geleitet. Dieses Signal ist nun das gewünschte Ausgangssignal, welches in der Tonhöhe durch das Tonhöhenmodul sowie in der Lautstärke durch das Lautstärkenmodul gesteuert wird.
\appendix
\newpage
\end{document} 
