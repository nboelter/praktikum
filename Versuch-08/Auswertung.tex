Zunächst müssen die Messwerte des Widerstandsthermometers in die entsprechende Temperatur $\theta$ umgerechnet werden. Dazu sind folgende Angaben auf dem Thermometer angegeben:\label{eq:Widerstandsthermometer}
\begin{align*}
  R(\theta) &= \unit[1000]{\Omega} (1+A\theta+B\theta^2) \\
  A &= \unitfrac[3.9083\cdot10^{-3}]{1}{\degree C} \\
  B &= \unitfrac[-5.775\cdot10^{-7}]{1}{\degree C^2}.
\end{align*}
Nach kurzem Umstellen erhält man:
$$ \theta(R) = \frac{-A}{2B}\pm\sqrt{\frac{A^2}{4B^2}+\frac{1}{B}\left(\frac{R}{R_0}-1\right)}.$$
Dabei wurde der Fehler $\Delta\theta = \pm (\unit[0.3]{\degree C} + 0.005 \theta)$ der Praktikumsanleitung~\cite[S.~81]{Anleitung} entnommen. Es ergeben sich die Temperaturen in Tab.~\ref{tab:messwerte}.

Nun werden die Drücke und Temperaturen als Arrheniusgraph ($\log(p) = \nicefrac{1}{T}$) aufgetragen, und wie erwartet ist ein linearer Zusammenhang ersichtlich.

Mit linearer Regression (gnuplot) werden die Steigung $m$ und der Y-Achsenabschnitt $b$ bestimmt ($\log(p) = mT^{-1}+b$).

\begin{figure}[htb]
  \begin{center}
    \def\svgwidth{8cm}
    \input{arrhenius1.tex}
  \end{center}
  \caption{Arrheniusgraph bei Aufheizung mit linearer Regression.}
  \label{fig:arrhenius1}
\end{figure}
Für die Aufheizung (Abb.~\ref{fig:arrhenius1}) ergeben sich:
\begin{align*}
  m_\textrm{Heiz} &= \unit[(-4826 \pm 34)]{K} \\
  b_\textrm{Heiz} &= (24.38 \pm 0.10).
\end{align*}
\begin{figure}[htb]
  \begin{center}
    \def\svgwidth{8cm}
    \input{arrhenius2.tex}
  \end{center}
  \caption{Arrheniusgraph bei Abkühlung mit linearer Regression.}
  \label{fig:arrhenius2}
\end{figure}
Und für die Abkühlung entsprechend (Abb.~\ref{fig:arrhenius2}):
\begin{align*}
  m_\textrm{Kühl} &= \unit[(-5092 \pm 32)]{K} \\
  b_\textrm{Kühl} &= (25.01 \pm 0.09).
\end{align*}
\newpage
Nun kann man die Geradengleichung umstellen und mit Gleichung~(\ref{eq:dampfdruck}) vergleichen.
\begin{align}
  \log\left(\frac{p}{\unit[1]{Pa}}\right) &= mT^{-1}+b \nonumber \\
p &= \exp\left[mT^{-1}+b\right] = p_0 \exp{\left[\Lambda_VR^{-1}\left(T_0^{-1}-T^{-1}\right)\right]} \label{eq:dampfdruck2} \\
& mT^{-1}+b = \Lambda_VR^{-1}T_0^{-1} - \Lambda_VR^{-1}T^{-1} + \log(\nicefrac{p_0}{\unit[1]{Pa}}) \nonumber \\
m &= - \Lambda_VR^{-1} \nonumber \\
b &= \Lambda_VR^{-1}T_0^{-1} + \log(\nicefrac{p_0}{\unit[1]{Pa}}) \nonumber \\
\Rightarrow \Lambda_V &= -mR \label{eq:lambda}\\
\Rightarrow T_0 &= \frac{\Lambda_VR^{-1}}{b-\log(\nicefrac{p_0}{\unit[1]{Pa}})} = \frac{-m}{b-\log(\nicefrac{p_0}{\unit[1]{Pa}})} = \frac{m}{\log(\nicefrac{p_0}{\unit[1]{Pa}})-b} \label{eq:siedetemperatur}
\end{align}

\subsection{Verdampfungswärme $\Lambda_V$}
Die Verdampfungswärme kann man nun mit der Formel~(\ref{eq:lambda}) einfach ausrechnen.
Dabei wurde $R = \unitfrac[8.3145]{J}{mol~K}$ angenommen~\cite[S.~4]{Anleitung}. 

Der Fehler berechnet sich zu $\sigma_{\Lambda_V} = R\sigma_{m}$.
\begin{align*}
  \Lambda_{V,\textrm{Heiz}} &= \unitfrac[(40129 \pm 280)]{J}{mol} \\
  \Lambda_{V,\textrm{Kühl}} &= \unitfrac[(42333 \pm 264)]{J}{mol}
\end{align*}
Aus den beiden Messreihen wird auch noch ein gewichtetes Mittel gebildet.
\begin{empheq}[box=\fbox]{align*}
  \Lambda_{V} &= \unitfrac[(41296 \pm 193)]{J}{mol}
\end{empheq}


\subsection{Siedepunkt $T_0$}
Unter Benutzung der Formel~(\ref{eq:siedetemperatur}) lässt sich nun der Siedepunkt bestimmen. Dabei wird $p = p_0$ zum Siedezeitpunkt angenommen.
Als Fehler ergibt sich dabei:
\begin{align*}
  \sigma_{T_0} = \sqrt{\left(\frac{\sigma_m}{\log(\nicefrac{p_0}{\unit[1]{Pa}})-b}\right)^2 + \left(\frac{m \sigma_b}{(\log(\nicefrac{p_0}{\unit[1]{Pa}})-b)^2}\right)^2}
\end{align*}

\begin{empheq}[box=\fbox]{align*}
  T_{0,\textrm{Heiz}} &= \unit[(375.2 \pm 4.0)]{K} = \unit[(102.0 \pm 4.0)]{\degree C} \\
  T_{0,\textrm{Kühl}} &= \unit[(377.5 \pm 3.5)]{K} = \unit[(104.4 \pm 3.5)]{\degree C} 
\end{empheq}

\subsection{Dampfdruck bei $\unit[0]{\degree C}$}
Für den Dampfdruck bei $\unit[0]{\degree C}$ kann einfach $T = \unit[273.15]{K}$ in Formel~(\ref{eq:dampfdruck2}) eingesetzt werden.
\begin{align*}
  p &= \exp\left[\frac{m}{\unit[273.15]{K}}+b\right] \\
  \sigma_p &= p\sqrt{\left(\frac{\sigma_m}{\unit[273.15]{K}}\right)^2 + \sigma_b^2}
\end{align*}
\begin{align*}
  p_\textrm{Heiz} &= \unit[(824 \pm 131)]{Pa} \\
  p_\textrm{Kühl} &= \unit[(582 \pm 93)]{Pa}
\end{align*}

Auch hier erscheint es sinnvoll, ein gewichtetes Mittel zu berechnen:
\begin{empheq}[box=\fbox]{align*}
  p &= \unit[(663 \pm 76)]{Pa}
\end{empheq}

\subsection{Siedetemperatur von Wasser auf der Zugspitze}
Nun ist die Siedetemperatur von Wasser bei einer Höhe von $h = \unit[2962]{m}$ über NN zu bestimmen. Dafür wird zuerst der Luftdruck mit der barometrischen Höhenformel bestimmt~\cite[S.~99]{Gerthsen}:
$$ p = \unit[1013]{hPa} \cdot \exp\left(\frac{-h}{\unit[8005]{m}}\right) =  \unit[7.00\cdot10^4]{Pa}. $$

Nun stellt man Gleichung~(\ref{eq:dampfdruck2}) nach $T$ um:
$$ T = \left(\frac{\log(\frac{p_0}{p})R}{\Lambda_V}+T_0^{-1}\right)^{-1} = \unit[363.1]{K} = \unit[89.9]{\degree C}$$
