reset
f1(x) = a*x+b
f2(x) = c*x+d
ff1(x) = (x-b)/a
ff2(x) = (x-d)/c
set terminal epslatex color colortext
set output 'arrhenius1.tex'
set xlabel '$\nicefrac{1}{T}~[\unitfrac{1}{K}]$'
set ylabel '$\log(\nicefrac{p}{\unit[1]{Pa}})~[-]$'
fit f1(x) 'Messwerte.txt' using (log($1*100000)):(1/($4+273.15)):(1/($6)) via a,b
fit f2(x) 'Messwerte.txt' using (log($1*100000)):(1/($5+273.15)):(1/($7)) via c,d
plot \
'Messwerte.txt' using (1/($4+273.15)):(log($1*100000)) lt 1 lc 1 title "Messwerte", \
ff1(x) title "Lineare Regression" lt 1 lc 1
set output 'arrhenius2.tex'
plot \
'' using (1/($5+273.15)):(log($1*100000)) lt 1 lc 1 title "Messwerte", \
ff2(x) title "Lineare Regression" lt 1 lc 1
set output
!epstopdf arrhenius1.eps
!epstopdf arrhenius2.eps
