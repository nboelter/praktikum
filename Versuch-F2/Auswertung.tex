\emph{Anmerkung:} Alle ermittelten Fehler wurden per Gaußscher Fehlerfortpflanzung bestimmt, wenn nicht anders angegeben.
Diese berechnet sich bei einer Größe $y$, die von $n$ fehlerbehafteten Werten $x_i$ mit $i\in\{1,2,\ldots,n\}$ abhängt, wie folgt~\citep{Anleitung}:
\begin{align*}
  \sigma_y &= \sqrt{\sum_{i=1}^n\left(\frac{\partial y}{\partial x_i}\sigma_{x_i}\right)^2}
\end{align*}
Dabei bezeichnet $\sigma_X$ den Fehler des Wertes $X$.
\subsection{Temperaturberechnung mit Hilfe des Dampfdrucks}
Zur Bestimmung des Drucks $p$ am Ort der Probe muss zum Dampfdruck $p_\mathrm{Dampf}$ des gasförmigen Heliums noch der Druck $\rho_\mathrm{h}$ durch die Höhe der Flüssigkeitssäule oberhalb der Probe addiert werden.
Dieser ergibt sich aus der Dichte $\rho=\unit[125]{kg\,m^{-3}}$ des Heliums, der Erdbeschleunigung $g=\unit[9.81]{m\,s^{-2}}$ und der Höhe der Flüssigkeitssäule $h$, die während des Versuchs gemessen wurde.
So erhält man:
\begin{align}
  p&=p_\mathrm{Dampf} + \rho gh
\end{align}
Die angenommen Fehler sind in Tabelle~\ref{tab:fehler} zusammengefasst.
\begin{table}
  \centering
  \begin{tabular}{cccc}
    &\multicolumn{2}{c}{$\sigma_{p_\mathrm{Dampf}}$}&$\sigma_h$\\
    $p_\mathrm{Dampf}$&$100\ldots\unit[1000]{mbar}$&$10\ldots\unit[100]{mbar}$&\\
    \hline
    &$\unit[10]{mbar}$&$\unit[1]{mbar}$&$\unit[2]{mm}$\\
    \hline
  \end{tabular}
  \caption{Geschätzte Fehler der Messwerte für den Dampfdruck $p_\mathrm{Dampf}$ und die Höhe $h$ der Flüssigkeitssäule über der Probe.}
  \label{tab:fehler}
\end{table}
Die unterschiedlichen Werte für die beiden Druckbereiche kommen dabei daher, dass hier zwei verschiedene Manometer verwendet wurden.

Befindet man sich unterhalb der $\lambda$-Temperatur $T_\lambda$, so ist diese Beschreibung nicht mehr korrekt.
Durch die unendlich große spezifische Wärmekapazität muss im gesamten suprafluiden Helium die gleiche Temperatur herrschen. Daher ist eine Messung der Temperatur bei jeder Höhe gleichbedeutend mit einer Messung der Temperatur der suprafluiden Phase direkt am Punkt des Übergangs zur Gasphase.
\subsection{Eichung}\label{sec:eichung}
Die Messwerte des Dampfdrucks werden mit Hilfe von Gleichung~\ref{eq:tempdruck} in die zugehörßige Temperatur umgerechnet. In Tabelle~\ref{tab:tempdruck} sind die Ergebnisse zusammengefasst.
Anschließend wird die so errechnete Temperatur gegen die zugehörige Spannung aufgetragen und eine Regression\footnote{via \emph{gnuplot}, anzupassende Funktion: $f(x)=a(x-b)^{-1}+c$, $\chi_\mathrm{red}^2\approx3.51$.} durchgeführt.
Abbildung~\ref{fig:abkuehl} zeigt den so erhaltenen Graphen.
Das Curie-Weiß-Gesetz scheint hervorragend erfüllt zu sein.
Die erhaltenen Regressionsparameter lauten:
\begin{align*}
  \alpha &= \unit[(489.40\pm2.35)]{K\,\mu V}\\
  U_\mathrm{leer} &= \unit[(131.29\pm0.80)]{\mu V}\\
\end{align*}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{abkuehl}
\end{center}
\caption{Regression an die umgerechneten Messwerte der Abkühlphase.}
\label{fig:abkuehl}
\end{figure}

Versucht man stattdessen, die gemessenen Werte der Aufwärmphase zur Eichung zu verwenden, erhält man keinen Curie-Weiß-Zusammenhang, da die Gleichgewichtsbedinungen nicht gegeben sind.
\subsection{Spezifische Wärmekapazität}
Für die Auswertung der Messdaten beim Aufwärmen des flüssigen Heliums wird angenommen, dass die Änderung der Wärme konstant verläuft.
Diese Annahme lässt sich auch wie folgt schreiben:
\begin{align*}
  \frac{\partial Q}{\partial t} = \mathrm{const.}\label{eq:annahme}
\end{align*}
Hiermit lässt sich nun Gleichung~\ref{eq:spez} umschreiben zu:
\begin{align}
  C &= \frac{\Delta Q}{\Delta T} = \frac{\partial Q}{\partial T}
  = \frac{\partial Q}{\partial t}\frac{\partial t}{\partial T}
  = k \left(\frac{\partial T}{\partial t}\right)
\end{align}
Dabei ist $k$ eine Konstante.

Mit den in Abschnitt~\ref{sec:eichung} erhaltenen Werten für $U_\mathrm{leer}$ und $\alpha$ lassen sich nun die aufgenommenen Messdaten der Gesamtspannung $U_\mathrm{ges}$ in Abhängigkeit von der Zeit in Temperaturen $T(t)$ umrechnen.
In Abbildung~\ref{fig:aufwaerm} ist diese Abhängigkeit aufgetragen.

Die Steigung des erhaltenen Graphen $\partial T/\partial t$ erhält man nun per numerischer Näherung der eigentlichen Ableitung.
Es wird also $\Delta T/\Delta t$ errechnet, wobei $\Delta t=\unit[10]{s}$ festgesetzt wird.
In Anhang~\ref{sec:py2} ist das verwendete Python-Skript zu finden.
Aus der berechneten Steigung ergibt sich nun die Ahängigkeit der spezifischen Wärme von der Temperatur.
In Abbildung~\ref{fig:speziwaermo} ist sie graphisch aufgetragen.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{speziwaermo}
\end{center}
\caption{Abhängigkeit der spezifischen Wärme von der Temperatur}
\label{fig:speziwaermo}
\end{figure}
Die gesuchte $\lambda$-Temperatur des Heliums lässt sich nun aus dem erwartungsgemäß $\lambda$-förmigen Graphen ablesen.
Sie ergibt sich zu $T_\lambda=\unit[(2.145\pm0.008)]{K}$.
Die relative Abweichung vom Literaturwert $T_\mathrm{lit}=\unit[2.1768]{K}$ beträgt $\unit[1.5]{\%}$.
