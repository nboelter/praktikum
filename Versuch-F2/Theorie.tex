Zur Eichung des Thermometers wird die ITS-90-Temperaturskala verwendet.
Für den Dampfdruck $p$ von Helium-4 liefert diese bei Temperaturen $T\in[\unit[0.65]{K};\unit[5]{K}]$ in guter Näherung folgende Abhängigkeit:
\begin{align}
  T&=\Sigma_{i=0}^8 A_i \left(\frac{\ln{p}-B}{C}\right)^i\label{eq:tempdruck}
\end{align}
Dabei ist $T$ die Temperatur in Kelvin und $p$ der Dampfdruck in Pascal.
Die temperaturabhängigen Koeffizienten $A_i$, $i\in\{1,2,\ldots,8\}$, $B$ und $C$ sind in Tabelle~\ref{tab:koeff} zusammengefasst.
Diese Temperaturabhängigkeit liegt in einer Veränderung der sonst konstanten Koeffizienten beim $\lambda$-Punkt des Heliums $T_\lambda$.
\subsection{Suprafluidität bei Helium-4}
Kühlt man Helium-4 zu Temperaturen unter $T_\lambda=\unit[2.1768]{K}$, so kann man einen Phasenübergang vom flüssigen in den suprafluiden Zustand beobachten~\citep{Rubber}.
Dieser zeichnet sich durch eine Viskosität von $\eta=0$ aus.
Hieraus resultieren einige interessante Verhaltensweisen, wie die schon beschriebene Kapillarität und Wärmeleitfähigkeit.
Da sich kein Temperaturgradient ausbilden kann, hat das Helium zu jedem Zeitpunkt an jedem Ort dieselbe Temperatur.
Dies lässt sich beim Übergang zur suprafluiden Phase beobachten.
Ist das Helium noch normalflüssig, so kocht es wie alle Flüssigkeiten bei Temperaturen unterhalb des Siedepunktes.
Wird es durch Kühlung zum Suprafluid, stoppt die Blasenbildung instantan und nur an der Oberfläche findet ein Übergang in die Gasphase statt.
\subsection{Der Phasenübergang}
In der Thermodynamik ist ein Phasenübergang durch Singularitäten in den Suszeptibiltäten definiert.
Diese sind zweite Ableitungen der thermodynamischen Potentiale.
Eine dieser Suszeptibiltäten ist die spezifische Wärme $C$, die beim $\lambda$-Punkt des Heliums divergiert.
Im allgemeinen lässt sie sich ausdrücken via:
\begin{align}
  C=\Delta Q/\Delta T\label{eq:spez}
\end{align}
\subsection{Das Curie-Gesetz}
Die Magnetisierung $M$ eines paramagnetischen Stoffes in einem Magnetfeld $H$ lässt sich über die Beziehung $M=\chi H$ ausdrücken.
Dabei bezeichnet $\chi$ die magnetische Suszeptibiltät des Materials.
Für sie gilt das Curie-Weiß-Gesetz:
\begin{align}
  \chi(T)&=\frac{\kappa}{T-\theta}\label{eq:susz}
\end{align}
Hier bezeichnet $T$ die Temperatur des Stoffes und $\theta$ die stoffspezifische Weiß-Temperatur, bei der sich das magnetische Verhalten des Materials ändert.
$\kappa$ bezeichnet eine Konstante, die sich aus quantenmechanischen Überlegungen ergibt, auf die hier nicht näher eingegangen werden soll.
Dabei verhält dieses sich für $T<\theta$ ferromagnetisch und für $T>\theta$ paramagnetisch.
Bei den im Versuch erreichten Temperaturen verhält sich das bei der Temperaturmessung beteiligte Gadoliniumsulfat paramagnetisch.
\subsection{Das Thermometer}
\begin{figure}[htb]
  \begin{center}
    \def\svgwidth{8cm}
    \input{Versuchsaufbau.pdf_tex}
  \end{center}
  \caption{Schematischer Versuchsaufbau des $^4$He-Verdampfungskryostaten \citep{lpf2}.}
  \label{fig:aufbau}
\end{figure}
Abbildung~\ref{fig:aufbau} zeigt eine schematische Zeichnung des im Versuch verwendeten Badkryostaten.
Dieser wird mit einem Vakuumsystem auf bis zu $\unit[1.05]{K}$ heruntergekühlt~\citep{AnleitungF2}.
Er besteht aus zwei Dewar-Gefäßen; einem inneren, in welches das flüssige Helium geleitet wird und einem äußeren, in das aus Gründen der Vorkühlung und Isolierung flüssiger Stickstoff gefüllt wird.

Im inneren Dewar-Gefäß wird mittels zweier Federbalgmanometer der Dampfdruck gemessen.
Dabei wird eines im Bereich von $\unit[1000]{mbar}$ bis etwa $\unit[100]{mbar}$ betrieben, das andere im Bereich von $\unit[100]{mbar}$ bis etwa $\unit[1]{mbar}$.
An das innere Gefäß ist auch die Vakuumpumpe angeschlossen.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{\textwidth}
\input{sonde.pdf_tex}
\end{center}
\caption{Messsonde mit Spulensystem und Gadoliniumprobe.}
\label{fig:sonde}
\end{figure}
Weiterhin ist hier die Sonde zur Temperaturmessung so untergebracht, dass sie vollständig in das flüssige Helium eintaucht, sobald genügend Helium eingefüllt wurde.

Wie in Abbildung~\ref{fig:sonde} schematisch dargestellt, besteht die Sonde aus einem Spulensystem, das an einen Lock-In-verstärker angeschlossen ist, sowie einer Probe aus paramagnetischem Gadoliniumsulfat.
Durch Anlegen einer Wechselspannung an die Primärspule wird in den Sekundärspulen eine Spannung induziert.
Geht man von perfekt gefertigten Spulen aus, in denen sich keine Probe befindet, so hebt sich die induzierte Spannung auf, da die Spulen gegensinnig gewickelt sind.
Herstellungsbedingt misst man jedoch eine konstante Leerlaufspannung $U_\mathrm{leer}$.
Weiterhin befindet sich im Inneren einer der Sekundärspulen die paramagnetische Probe.
Diese beeinflusst die Induktivität jener Spule, wodurch die gesamte an den Sekundärspulen abfallende Spannung $U_\mathrm{ges}=U_\mathrm{leer}+U_\mathrm{Probe}$ beträgt.
Da $U_\mathrm{Probe}$ proportional zur Magnetisierung der Probe $M$ ist, lässt sich mit Gleichung~\ref{eq:susz} auf die magnetische Suszeptibiltät $\chi(T)$ rückschließen.
Es gilt folglich mit einer Konstanten $\alpha$:
\begin{align}
  U_\mathrm{ges} &= U_\mathrm{leer} + \frac{\alpha}{T-\theta}\label{eq:spannung}
\end{align}
Sind $U_\mathrm{leer}$ und $\alpha$ bekannt und kann man von $\theta\approx0$ ausgehen, so folgt der Zusammenhang zwischen der gemessenen Spannung und der Temperatur:
\begin{align}
  T &= \frac{\alpha}{U_\mathrm{ges}-U_\mathrm{leer}}\label{eq:temperatur}
\end{align}
Die Bestimmung der Konstanten $U_\mathrm{leer}$ und $\alpha$ erfolgt im Versuch durch die Messung des Dampfdrucks während des Abkühlvorgangs.
