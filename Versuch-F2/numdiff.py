from math import *

f = open('Messwerte-aufwaerm.txt', 'r')
data = []
for line in f:
  if len(line)<2:
    break
  if line[0]=='#':
    continue
  data.append(map(float, line.strip().split(' ', 2)))
f.close()

alpha=489.40
Uleer=131.29
i=1
Deltat=9
Told=0

g=open('NumDiff.txt', 'w')
g.write('# Temperaturabhaengigkeit der spezifischen Waerme')
g.write('\n# T [K], C [a.u.]')
for (t,U) in data:
  if t==Deltat*i:
    Tnew=alpha/(U*10**6-Uleer)
    DeltaT=Tnew-Told
    Told=Tnew
    g.write('\n' + str(Tnew) + ' ' + str(Deltat/DeltaT))
    i=i+1
g.close()
