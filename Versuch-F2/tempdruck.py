from math import *

# Koeffizienten
A=[(3.146631,1.392408),(1.357655,0.527153),(0.413923,0.166756),
  (0.091159,0.050988),(0.016349,0.026514),(0.001826,0.001975),
  (-0.004325,-0.017976),(-0.004973,0.005409),(0.0,0.013259)]
B=[10.3,5.6]
C=[1.9,2.9]

rho=0.125 # Dichte fluessigen Heliums [kg/m^3]
ge=9.81 # Erdbeschleunigung [m/s^2]

# Messdaten auslesen und Tripel der Form (p,U,h) in Array ablegen
data=[]
f = open('Messwerte.txt', 'r')
for line in f:
  if line[0]=='#':
    continue
  if len(line)<2:
    break
  data.append(map(float, line.strip().split(' ', 3)))
f.close()

# Umrechnung des Drucks in Pa,
# Verschiebung der Probe beruecksichtigen
P=[]
Deltah=9.7 # Verschiebung der Probe gegenueber der Messskala [cm]
for (p,U,h) in data:
  P.append((100.0*p,U,h+Deltah))

# Schreibe die Ergebnisse in eine Datei
sigmap=0
sigmah=0.2
g = open('TempDruck.txt', 'w')
g.write('# Messwerte mit T statt p und beruecksichtigtem\
 Offset in h.')
g.write('\n# T [K], sigma_T [K], U [\mu V], h [cm]')
for (p,U,h) in P:
  if p>10000:
    sigmap=1000
  else:
    sigmap=100
  # Berechne die Temperatur mit den Koeffizienten
  # fuer T > 2.1768 K.
  #i=0
  #T=0
  #for (J,j) in A:
  #  T=T+J*((log(p)-B[0])/C[0])**i
  #  i=i+1
  ## Beachte Korrektur aufgrund der Hoehe der Fluessigkeitssaule
  #if T>2.1768:
  i=0
  T=0
  for (J,j) in A:
    T=T+J*((log(p+rho*ge*h)-B[0])/C[0])**i
    sigmap = sqrt(sigmap**2 + (sigmah*rho*ge)**2)
    i=i+1
  # Falls hier ein zu kleiner Wert errechnet wird,
  # verwende Koeffizienten fuer T < 2.1768 K.
  if (T<2.1768 or p<1000):
  # Bedingung p<1000 stellt sicher, dass bei kleinem Druck
  # nicht faelschlich die Koeffizienten fuer T>2.1768
  # verwendet werden. (unschoenes Workaround...)
    Told=T
    T=0
    i=0
    for (J,j) in A:
      T=T+j*((log(p)-B[1])/C[1])**i
      i=i+1
  # Schreibe den richtigen Wert fuer T in eine Datei.
  # Fuege auch die Werte von U,h hinzu.
  sigmaT=0
  i=0
  if T>2.1768:
    for (J,j) in A:
      sigmaT=sigmaT+sigmap*i*J*(log(p)-B[0])**(i-1)/(p*C[0]**i)
      i=i+1
  else:
    for (J,j) in A:
      sigmaT=sigmaT+sigmap*i*j*(log(p)-B[1])**(i-1)/(p*C[1]**i)
      i=i+1

  g.write('\n' + str(T) + ' ' + str(sigmaT) + ' ' + str(U) + ' ' +
          str(h))
g.close()
