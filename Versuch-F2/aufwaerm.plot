reset
set xlabel 'Zeit $\unit[t]{[s]}$'
set ylabel 'Temperatur $\unit[T]{[K]}$'
set grid

uleer = 131.29
alpha = 489.40
f(x)=alpha/(x-uleer)

set terminal epslatex color colortext
set output 'aufwaerm.tex'
plot 'Messwerte-aufwaerm.txt' u 1:(f(($2)*10**6)) notitle
set xlabel 'Temperatur $\unit[T]{[K]}$'
set ylabel 'Spezifische Wärme $\unit[\sigma]{[a.u.]}$'
set output 'speziwaermo.tex'
plot 'NumDiff.txt' notitle
set output
