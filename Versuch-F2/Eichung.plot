reset
set xlabel '$\unit[U_\mathrm{ges}]{[\mu V]}$'
set ylabel '$\unit[T]{[K]}$'
set grid
set xrange [245:480]
set yrange [1.4:4.2]
f(x) = a/(x-b)# + c
fit f(x) 'TempDruck.txt' using 3:1:2 via a,b#,c
set terminal epslatex color colortext
set output 'abkuehl.tex'
plot 'TempDruck.txt' u 3:1:2 w errorbars title 'Messdaten', f(x) title 'Regression'
set output
