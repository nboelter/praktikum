reset
lin(x) = m*x+b
fit lin(x) 'Seifenwasser.txt' using 1:($3-$2):4 via m,b
set key top right
set xrange [0:11]
set ylabel '$\Delta F$'
set xlabel 'Versuch'
set terminal epslatex color
set output 'Seifenwasser.tex'
plot 'Seifenwasser.txt' using 1:($3-$2):4 with errorbars title 'Messwerte', lin(x) title 'Lineare Regression'
set output
!epstopdf 'Seifenwasser.eps'
