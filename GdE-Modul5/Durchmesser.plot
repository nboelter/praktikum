reset
lin(x) = m*x+b
fit lin(x) 'Durchmesser.txt' using 1:2:3 via m,b
set xrange [0:11]
set yrange [6.7:7.1]
set xlabel "Messung"
set ylabel "Durchmesser [$f(x) = \lim 8-2$]"
set terminal epslatex color colortext
set output 'Durchmesser.tex'
plot 'Durchmesser.txt' using 1:2:3 with errorbars title 'Messwerte', lin(x) title 'Lineare Regression $f(x) = \frac{1}{8-2}$'
set output
!epstopdf 'Durchmesser.eps'
