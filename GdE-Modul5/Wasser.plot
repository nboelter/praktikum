reset
lin(x) = m*x+b
fit lin(x) 'Wasser.txt' using 1:($3-$2):4 via m,b
set key left top
set xrange [0:11]
set ylabel '$\Delta F$'
set xlabel 'Versuch'
set terminal epslatex color colortext
set output 'Wasser.tex'
plot 'Wasser.txt' using 1:($3-$2):4 with errorbars title 'Messwerte', lin(x) title 'Lineare Regression'
set output
!epstopdf 'Wasser.eps'
