reset
lin(x) = m*x+b
lin2(x) = n*x+c
fit lin(x) "Wasser.txt" using 1:($3-$2):4 via m,b
fit lin2(x) "Seifenwasser.txt" using 1:($3-$2):4 via n,c
set key left center
set xrange [0:11]
#set yrange [6.8:6.95]
set ylabel "ΔF"
set xlabel "Versuch"
#plot "Wasser.txt" using 1:($3-$2):4 with errorbars title "Messwerte Wasser", lin(x) title "Lineare Regression", "Seifenwasser.txt" using 1:($3-$2):4 with errorbars title "Messwerte Seifenwasser", lin2(x) title "Lineare Regression"
