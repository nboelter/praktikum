(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     14679,        468]
NotebookOptionsPosition[     12463,        384]
NotebookOutlinePosition[     12819,        400]
CellTagsIndexPosition[     12776,        397]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"DurchmesserData", " ", "=", " ", 
   RowBox[{"Import", "[", 
    RowBox[{
     RowBox[{"ToFileName", "[", 
      RowBox[{
       RowBox[{"NotebookDirectory", "[", "]"}], ",", " ", 
       "\"\<Durchmesser.txt\>\""}], "]"}], ",", " ", "\"\<Table\>\""}], 
    "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.540652925006154*^9, 3.540652971844523*^9}, 
   3.540653175706175*^9, {3.540653363400185*^9, 3.540653363759469*^9}, 
   3.54065352322899*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"DMesswerte", " ", "=", 
  RowBox[{"DurchmesserData", "[", 
   RowBox[{"[", 
    RowBox[{"All", ",", " ", "2"}], "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.5406533602783403`*^9, 3.540653432804661*^9}, {
  3.540654318654399*^9, 3.5406543195149937`*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "6.9`", ",", "6.85`", ",", "6.9`", ",", "6.88`", ",", "6.88`", ",", "6.89`",
    ",", "6.885`", ",", "6.89`", ",", "6.895`", ",", "6.89`"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.5406533823483677`*^9, 3.540653433157106*^9}, {
   3.540654320145693*^9, 3.540654335733436*^9}, 3.5406544029516172`*^9, 
   3.540654559168809*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"WasserData", " ", "=", " ", 
   RowBox[{"Import", "[", 
    RowBox[{
     RowBox[{"ToFileName", "[", 
      RowBox[{
       RowBox[{"NotebookDirectory", "[", "]"}], ",", " ", 
       "\"\<Wasser.txt\>\""}], "]"}], ",", " ", "\"\<Table\>\""}], "]"}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.540653538412858*^9, 3.540653541802845*^9}, {
  3.540653589761178*^9, 3.540653610010069*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"WMesswerte", " ", "=", " ", 
  RowBox[{"Map", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"#", "[", 
        RowBox[{"[", "1", "]"}], "]"}], "-", 
       RowBox[{"#", "[", 
        RowBox[{"[", "2", "]"}], "]"}]}], ")"}], "&"}], ",", 
    RowBox[{"WasserData", "[", 
     RowBox[{"[", 
      RowBox[{"All", ",", " ", 
       RowBox[{"{", 
        RowBox[{"3", ",", "2"}], "}"}]}], "]"}], "]"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.540653655214333*^9, 3.540653669717692*^9}, {
  3.5406537868663607`*^9, 3.540653853967168*^9}, {3.5406538985940247`*^9, 
  3.540654011138461*^9}, {3.540654065747754*^9, 3.540654143799541*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "27.5`", ",", "28", ",", "28", ",", "28.299999999999997`", ",", "28.5`", 
   ",", "28.`", ",", "27.5`", ",", "29", ",", "30", ",", "29.5`"}], 
  "}"}]], "Output",
 CellChangeTimes->{
  3.540654144571307*^9, {3.540654332350143*^9, 3.54065433581923*^9}, 
   3.540654403019349*^9, 3.540654559266844*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"SeifenWasserData", " ", "=", " ", 
   RowBox[{"Import", "[", 
    RowBox[{
     RowBox[{"ToFileName", "[", 
      RowBox[{
       RowBox[{"NotebookDirectory", "[", "]"}], ",", " ", 
       "\"\<Seifenwasser.txt\>\""}], "]"}], ",", " ", "\"\<Table\>\""}], 
    "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.54065416195007*^9, 3.540654166598501*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SWMesswerte", " ", "=", " ", 
  RowBox[{"Map", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"#", "[", 
        RowBox[{"[", "1", "]"}], "]"}], "-", 
       RowBox[{"#", "[", 
        RowBox[{"[", "2", "]"}], "]"}]}], ")"}], "&"}], ",", 
    RowBox[{"SeifenWasserData", "[", 
     RowBox[{"[", 
      RowBox[{"All", ",", " ", 
       RowBox[{"{", 
        RowBox[{"3", ",", "2"}], "}"}]}], "]"}], "]"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.540654172584545*^9, 3.540654179486102*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
  "13.5`", ",", "13", ",", "13.899999999999999`", ",", "12.5`", ",", "13.`", 
   ",", "11.5`", ",", "12.5`", ",", "12.`", ",", "13", ",", "12.`"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.540654173455246*^9, 3.5406541800022078`*^9}, {
   3.540654332432337*^9, 3.540654335901973*^9}, 3.5406544031020412`*^9, 
   3.540654559349339*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"d", " ", "=", " ", 
  RowBox[{
   RowBox[{"Mean", "[", "DMesswerte", "]"}], "/", "100"}]}]], "Input",
 CellChangeTimes->{{3.540653437489678*^9, 3.5406534521968527`*^9}}],

Cell[BoxData["0.06886`"], "Output",
 CellChangeTimes->{
  3.540653454876368*^9, {3.540654307294026*^9, 3.5406543359355*^9}, 
   3.540654403135598*^9, 3.540654559386134*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[CapitalDelta]d", " ", "=", " ", 
  RowBox[{
   RowBox[{"StandardDeviation", "[", "DMesswerte", "]"}], "/", 
   "100"}]}]], "Input",
 CellChangeTimes->{{3.5406534670123653`*^9, 3.540653509674632*^9}, {
  3.5406542491399717`*^9, 3.540654255026965*^9}}],

Cell[BoxData["0.00014491376746189564`"], "Output",
 CellChangeTimes->{3.5406542555364037`*^9, 3.540654335969121*^9, 
  3.5406544031686697`*^9, 3.540654559435931*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FW", " ", "=", " ", 
  RowBox[{
   RowBox[{"Mean", "[", "WMesswerte", "]"}], "/", "1000"}]}]], "Input",
 CellChangeTimes->{{3.540654190113632*^9, 3.540654213158491*^9}}],

Cell[BoxData["0.02843`"], "Output",
 CellChangeTimes->{{3.540654207349681*^9, 3.540654213908877*^9}, 
   3.540654336002125*^9, 3.540654403201681*^9, 3.5406545594858513`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[CapitalDelta]FW", " ", "=", " ", 
  RowBox[{
   RowBox[{"StandardDeviation", "[", "WMesswerte", "]"}], "/", 
   "100"}]}]], "Input",
 CellChangeTimes->{{3.5406542871228447`*^9, 3.5406542964743633`*^9}, {
  3.540654356552926*^9, 3.540654356807735*^9}}],

Cell[BoxData["0.008327331171776734`"], "Output",
 CellChangeTimes->{
  3.540654297561454*^9, {3.540654336034771*^9, 3.5406543575374804`*^9}, 
   3.540654403234926*^9, 3.540654559536664*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FSW", " ", "=", " ", 
  RowBox[{
   RowBox[{"Mean", "[", "SWMesswerte", "]"}], "/", "1000"}]}]], "Input",
 CellChangeTimes->{{3.540654216146035*^9, 3.540654225931507*^9}}],

Cell[BoxData["0.012690000000000002`"], "Output",
 CellChangeTimes->{3.540654226411869*^9, 3.540654336067862*^9, 
  3.540654403300116*^9, 3.540654559586172*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[CapitalDelta]FSW", " ", "=", 
  RowBox[{
   RowBox[{"StandardDeviation", "[", "SWMesswerte", "]"}], "/", 
   "1000"}]}]], "Input",
 CellChangeTimes->{{3.540654344393196*^9, 3.540654364920187*^9}}],

Cell[BoxData["0.0007340148348485727`"], "Output",
 CellChangeTimes->{3.540654365356605*^9, 3.540654403333853*^9, 
  3.540654559649469*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[CapitalDelta]r", " ", "=", " ", 
  RowBox[{"\[CapitalDelta]d", "/", "2"}]}]], "Input",
 CellChangeTimes->{{3.540640302253113*^9, 3.54064031645833*^9}, {
  3.540640474949699*^9, 3.540640476885548*^9}, {3.540654386079267*^9, 
  3.5406543959021997`*^9}}],

Cell[BoxData["0.00007245688373094782`"], "Output",
 CellChangeTimes->{{3.540640307650422*^9, 3.540640318428173*^9}, {
   3.540640477250372*^9, 3.540640482646614*^9}, 3.5406405437197037`*^9, 
   3.540645698949651*^9, 3.540645821507696*^9, 3.540646301163628*^9, 
   3.540654336233201*^9, {3.540654396524996*^9, 3.5406544033666*^9}, 
   3.540654559685811*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"r", " ", "=", " ", 
  FractionBox["d", "2"]}]], "Input",
 CellChangeTimes->{{3.540639697233448*^9, 3.54063970115809*^9}}],

Cell[BoxData["0.03443`"], "Output",
 CellChangeTimes->{
  3.5406397044571953`*^9, 3.540640482680313*^9, 3.540640543752141*^9, 
   3.540645698982223*^9, 3.540645821542102*^9, 3.540646301198924*^9, 
   3.5406543362669973`*^9, {3.540654399281602*^9, 3.540654403400564*^9}, 
   3.540654559723741*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sigma1", " ", "=", " ", 
  FractionBox["FD1", 
   RowBox[{"4", "*", "\[Pi]", "*", "r"}]]}]], "Input",
 CellChangeTimes->{{3.540639707793812*^9, 3.5406397356933317`*^9}, 
   3.5406458036134253`*^9}],

Cell[BoxData["0.06570977391958442`"], "Output",
 CellChangeTimes->{3.540639796280415*^9, 3.5406404827133503`*^9, 
  3.540640543784238*^9, 3.5406456990158777`*^9, 3.54064582157465*^9, 
  3.5406463012310677`*^9, 3.540654336300705*^9, 3.540654403434066*^9, 
  3.540654559769086*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"dsigma1", " ", "=", " ", 
  RowBox[{"Sqrt", "[", 
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      FractionBox["dFD1", 
       RowBox[{"4", "*", "\[Pi]", "*", "r"}]], ")"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       FractionBox["FD1", 
        RowBox[{"4", "*", "\[Pi]", "*", 
         SuperscriptBox["r", "2"]}]], "dr"}], ")"}], "2"]}], "]"}]}]], "Input",\

 CellChangeTimes->{{3.5406400844672318`*^9, 3.540640099553742*^9}, {
  3.5406401764731903`*^9, 3.540640244574353*^9}, {3.54064581220737*^9, 
  3.5406458135578938`*^9}, {3.540646290234123*^9, 3.540646290784*^9}}],

Cell[BoxData["0.0019829325662493427`"], "Output",
 CellChangeTimes->{3.540640333388548*^9, 3.540640482746385*^9, 
  3.540640543818446*^9, 3.5406456990491257`*^9, 3.540645821608807*^9, 
  3.540646301264284*^9, 3.5406543363335342`*^9, 3.540654403467329*^9, 
  3.540654559818746*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"sigma2", " ", "=", " ", 
  FractionBox["FD2", 
   RowBox[{"4", "*", "\[Pi]", "*", "r"}]]}]], "Input",
 CellChangeTimes->{{3.540639739508441*^9, 3.5406397484848948`*^9}, {
  3.540645808477434*^9, 3.540645809109336*^9}}],

Cell[BoxData["0.029330180479758227`"], "Output",
 CellChangeTimes->{3.540639748874346*^9, 3.540640482778984*^9, 
  3.540640543849719*^9, 3.540645699082058*^9, 3.5406458216409082`*^9, 
  3.540646301296479*^9, 3.540654336366951*^9, 3.540654403499632*^9, 
  3.540654559867475*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"dsigma2", " ", "=", " ", 
  RowBox[{"Sqrt", "[", 
   RowBox[{
    SuperscriptBox[
     RowBox[{"(", 
      FractionBox["dFD2", 
       RowBox[{"4", "*", "\[Pi]", "*", "r"}]], ")"}], "2"], "+", 
    SuperscriptBox[
     RowBox[{"(", 
      RowBox[{
       FractionBox["FD2", 
        RowBox[{"4", "*", "\[Pi]", "*", 
         SuperscriptBox["r", "2"]}]], "dr"}], ")"}], "2"]}], "]"}]}]], "Input",\

 CellChangeTimes->{{3.5406403470591803`*^9, 3.540640354699134*^9}, 
   3.5406458189748707`*^9, {3.540646294263556*^9, 3.540646294560089*^9}}],

Cell[BoxData["0.0017098200408825267`"], "Output",
 CellChangeTimes->{
  3.540640359738616*^9, 3.54064048281249*^9, 3.540640543883624*^9, 
   3.5406456991147013`*^9, 3.5406458216737022`*^9, {3.540646296613347*^9, 
   3.540646301329603*^9}, 3.540654336399444*^9, 3.540654403531551*^9, 
   3.540654559917816*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Export", "[", 
  RowBox[{
   RowBox[{"ToFileName", "[", 
    RowBox[{
     RowBox[{"NotebookDirectory", "[", "]"}], ",", " ", 
     "\"\<Mathematica_Output.tex\>\""}], "]"}], ",", " ", 
   RowBox[{"{", 
    RowBox[{
    "r", ",", "\[CapitalDelta]r", ",", "sigma1", ",", " ", "dsigma1", ",", 
     " ", "sigma2", ",", " ", "dsigma2"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.540654433339191*^9, 3.540654555002771*^9}, {
  3.5406545957934313`*^9, 3.540654606401149*^9}}],

Cell[BoxData["\<\"/Users/niklas/Documents/Studium/Praktikum/branches/\
Messprotokoll-Spielwiese/GdE-Modul5/Mathematica_Output.tex\"\>"], "Output",
 CellChangeTimes->{{3.540654443333241*^9, 3.540654456690774*^9}, 
   3.540654489989202*^9, {3.540654527023982*^9, 3.5406545600039053`*^9}, {
   3.540654598455727*^9, 3.540654607322454*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Systematische Fehler", "Subtitle",
 CellChangeTimes->{{3.5406405926667557`*^9, 3.540640596463867*^9}}],

Cell["\<\
* Ring nicht waagerecht aufgeh\[ADoubleDot]ngt
* \
\>", "Subsubtitle",
 CellChangeTimes->{{3.5406406038081293`*^9, 3.540640613199285*^9}}]
}, Open  ]]
},
WindowSize->{940, 640},
WindowMargins->{{166, Automatic}, {44, Automatic}},
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (October 5, \
2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[557, 20, 481, 12, 27, "Input"],
Cell[CellGroupData[{
Cell[1063, 36, 280, 6, 27, "Input"],
Cell[1346, 44, 371, 8, 27, "Output"]
}, Open  ]],
Cell[1732, 55, 421, 11, 27, "Input"],
Cell[CellGroupData[{
Cell[2178, 70, 689, 18, 27, "Input"],
Cell[2870, 90, 342, 8, 27, "Output"]
}, Open  ]],
Cell[3227, 101, 385, 10, 27, "Input"],
Cell[CellGroupData[{
Cell[3637, 115, 548, 16, 27, "Input"],
Cell[4188, 133, 376, 8, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4601, 146, 193, 4, 27, "Input"],
Cell[4797, 152, 172, 3, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5006, 160, 276, 6, 27, "Input"],
Cell[5285, 168, 165, 2, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5487, 175, 193, 4, 27, "Input"],
Cell[5683, 181, 173, 2, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5893, 188, 277, 6, 27, "Input"],
Cell[6173, 196, 189, 3, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6399, 204, 195, 4, 27, "Input"],
Cell[6597, 210, 159, 2, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6793, 217, 222, 5, 27, "Input"],
Cell[7018, 224, 138, 2, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7193, 231, 277, 5, 27, "Input"],
Cell[7473, 238, 356, 5, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7866, 248, 145, 3, 46, "Input"],
Cell[8014, 253, 296, 5, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8347, 263, 221, 5, 47, "Input"],
Cell[8571, 270, 279, 4, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8887, 279, 633, 17, 50, "Input"],
Cell[9523, 298, 280, 4, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9840, 307, 242, 5, 47, "Input"],
Cell[10085, 314, 277, 4, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10399, 323, 563, 16, 50, "Input"],
Cell[10965, 341, 309, 5, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11311, 351, 501, 12, 27, "Input"],
Cell[11815, 365, 336, 4, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12188, 374, 108, 1, 46, "Subtitle"],
Cell[12299, 377, 148, 4, 43, "Subsubtitle"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
