reset
set terminal epslatex color colortext
set output 'h2p_energy.tex'

set xlabel '$r$ [$\unit{a_0}$]'
set ylabel '$E(r)$ [$\unit{\hbar/s}$]'
v1(r)=v01+d*(1-exp(-a*(r-r01)))**2
fit [1:3] v1(x) 'H2P_energies.txt' u 2:3:4 via v01,d,a,r01

v2(r)=v02+f*(r-r02)**2
fit [1.5:2.5] v2(x) 'H2P_energies.txt' u 2:3:4 via v02,f,r02

set xrange [.5:3.5]

plot 'H2P_energies.txt' u 2:3:4 w errorbars title 'E(r)', v1(x) title 'Morse Potenzial', v2(x) title 'Harmonisches Potenzial'

set output
