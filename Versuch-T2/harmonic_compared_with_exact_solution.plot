reset
set terminal epslatex color colortext
set size 1.2
set output '316.tex'
set xlabel '$r$ [$a_0$]'
set ylabel 'Quadratnormierte Wellenfunktion $\psi$ [$\unit{1/\sqrt{a_0}}$]'
set xrange [-5:5]
f(x)=exp(-x*x/2)/(pi)**.25
plot f(x) t 'Analytische Lösung', \
	'./wf_harmonic_gauss_a_0.1_Ntherm_501.dat' u 1:($2/16385.3722166)**.5 smooth bezier t '$\psi_{\mathrm{Gauss}}$ für $a=0.1$', \
	'wf_harmonic_lorentz_a_0.1_Ntherm_501.dat' u 1:($2/16386.5462162)**.5 smooth bezier t '$\psi_{\mathrm{Lorentz}}$ für $a=0.1$', \
	'wf_harmonic_gauss_a_0.5_Ntherm_501.dat' u 1:($2/16385.6367238)**.5 smooth bezier t '$\psi_{\mathrm{Gauss}}$ für $a=0.5$', \
	'wf_harmonic_lorentz_a_0.5_Ntherm_501.dat' u 1:($2/16384.3919047)**.5 smooth bezier t '$\psi_{\mathrm{Lorentz}}$ für $a=0.5$', \
	'wf_harmonic_gauss_a_1_Ntherm_501.dat' u 1:($2/16380.3398277)**.5 smooth bezier t '$\psi_{\mathrm{Gauss}}$ für $a=1$', \
	'wf_harmonic_lorentz_a_1_Ntherm_501.dat' u 1:($2/16382.6218224)**.5 smooth bezie t '$\psi_{\mathrm{Lorentz}}$ für $a=1$'
set output
