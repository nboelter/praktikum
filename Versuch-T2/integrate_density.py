import scipy.integrate
import numpy
for file in ['wf_H2P_R_2.0_a_1.1_Ntherm_1500_s_16.dat', 'wf_H2P_R_4.0_a_1.0_Ntherm_1500_s_16.dat']:

  data = numpy.loadtxt(file)
  print file, scipy.integrate.simps(data[:,1], data[:,0])

for file in ['wf_yukawa_lambda_0.0_a_1_Ntherm_500_s_16.dat', 'wf_yukawa_lambda_0.5_a_1_Ntherm_500_s_16.dat', 'wf_yukawa_lambda_1.0_a_1_Ntherm_500_s_16.dat']:

  data = numpy.loadtxt(file)
  print file, scipy.integrate.simps(data[:,1]*numpy.exp(data[:,0]), data[:,0])

for file in ['wf_h2_molecule_R_1.4_a_1.2_Ntherm_1500_s_16.dat', 'wf_h2_molecule_R_4.0_a_1.0_Ntherm_1500_s_16.dat']: 
  z = 0.0
  n = 0
  for line in open(file,'r'):
    token = line.strip().split()
    token = map(float, token)
    z = z + token[2]**2
    n = n + 1
  print file, z/n*100
