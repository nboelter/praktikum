reset
set zlabel 'Normierte Ladungsdichte $\rho$' rotate by 90
set ylabel '$y$-Koordinate'
set xlabel '$x$-Koordinate'
set cntrparam bspline


set contour both
set cntrparam levels auto 8
set xrange [-4:4]
set yrange [-4:4]
set view 50, 120, 1.0, 2.0
set dgrid3d 100 100
set terminal epslatex color colortext
set output 'wf_h2m_R_1_4.tex'
splot 'wf_h2_molecule_R_1.4_a_1.2_Ntherm_1500_s_16.dat' using 1:2:($3/789.480665238) with dots notitle
set xrange [-5:5]
set yrange [-5:5]
set output 'wf_h2m_R_4_0.tex'
splot 'wf_h2_molecule_R_4.0_a_1.0_Ntherm_1500_s_16.dat' using 1:2:($3/471.462095705) with dots notitle
set output
