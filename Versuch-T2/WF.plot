reset
set terminal epslatex color colortext
set output 'wf_h2p.tex'
set xlabel '$z/a_0$'
set ylabel 'Normierte Ladungsdichte $\rho$'
plot 'wf_H2P_R_2.0_a_1.1_Ntherm_1500_s_16.dat' using 1:($2/154796.419344) title '$R = 2.0$', 'wf_H2P_R_4.0_a_1.0_Ntherm_1500_s_16.dat' using 1:($2/122223.081209) title '$R = 4.0$'
set output
