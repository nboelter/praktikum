reset
a(l) = (1.0/6.0)*(2.0 - 3.0*l) - (-64.0 - 96.0*l)/(96.0*2.0**(1.0/3.0)*(4.0 + 9.0*l - 27.0*l**2.0 + 3.0*sqrt(3.0)*sqrt(-9.0*l**2.0 - 20.0*l**3.0 + 27.0*l**4.0))**(1.0/3.0)) + (4.0 + 9.0*l - 27.0*l**2.0 + 3.0*sqrt(3.0)*sqrt(-9.0*l**2.0 - 20.0*l**3.0 + 27.0*l**4.0))**(1.0/3.0)/(3.0*2.0**(2.0/3.0))
Energy(l) = 0.5*a(l)**2*(1.0-8*a(l)/(2.0*a(l)+l)**2)

set xlabel 'Abklingparameter $\lambda\,[\unit{a_0}]$'
set ylabel 'Energie $E\,[\unit{E_h}]$'

set terminal epslatex color colortext

set output 'yukawa_energies.tex'
plot 'yukawa_energies.txt' using 1:2:3 with errorbars title 'Grundzustandsenergie', Energy(x) title 'Ritzsches Variationsverfahren'
set output
