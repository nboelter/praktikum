# #$ -M ravenschade@googlemail.com
# #$ -m e
#$ -cwd

a="1"
Ntherm="0"
pre="harmonic_gauss"
s="14"

out="${pre}_a_${a}_Ntherm_${Ntherm}"
time ./main_${pre}.bin ${a} ${Ntherm} $s $out.dat  > $out.out 2> $out.err