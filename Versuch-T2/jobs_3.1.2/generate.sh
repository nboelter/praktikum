rm -f submitall.sh

s="14"
for pot in harmonic;
do
	for twfu in gauss lorentz;
	do
		#for a in `seq 0.0 0.1 1.0 | sed "s/,/./g"`;
        	for a in 0 1;
        	do
                	for ntherm in 500;
                	do
                        	out="${pot}_${twfu}_a_${a}_Ntherm_${ntherm}.sh"
                        	cat template.sh | sed "s/A/$a/g" |  sed "s/POT/$pot/g" | sed "s/TWFU/$twfu/g" | sed "s/NTHERM/$ntherm/g" | sed "s/S/$s/g" > $out
                        	echo "$out" >> submitall.sh
                	done
        	done
	done
done
