reset
set terminal epslatex color colortext
set size 1.2
set output 'morse.tex'
set xlabel 'r [$r_0$]'
set ylabel 'V [D]'
set xrange [0:6]
set yrange [0:1.5]
f(x)=(1-exp(-1*(x-1)))**2
g(x)=(x-1)**2
plot f(x) title 'Morsepotential',g(x) title 'Harmonisches Potenzial'
set output
