cd jobs_3.2.1
echo "3.2.1"
for f in `find . -name "time_*.dat"`;
do
  	f=`basename $f`
  	f2=`echo $f|sed -e 's/\./_/g'`

	echo "set terminal epslatex color colortext; \
	  set output \"$f2.tex\"; \
	  set xlabel 'MC-Zeit \$t\$ [\$\\tau\$]'; \
	  set ylabel 'Energie \$E\$ [\$\\unit{\\hbar/s}\$]'; \
    plot [:][-0.6:-0.4] \"$f\" using (\$1*0.01):(\$2) w l t 'Energie'; \
	  set output" | gnuplot

	echo "set terminal epslatex color colortext; \
	  set output \"$f2\_hist.tex\"; \
	  c=-0.5;f(x)=a*exp(-0.5*((x-c)/b)**2); \
	  fit f(x) \"$f.hist\" via a,b,c; \
	  set xlabel 'Energie [\$\\unit{\\hbar/s}\$]'; \
  	  set ylabel 'Relative Häufigkeit'; \

	  plot \"$f.hist\" w boxes t 'Histogramm',f(x) t 'Fit' lw 2; \
	  set output" | gnuplot
done

for f in `find . -name "autocorr_*.dat"`;
do
  	f=`basename $f`
  	f2=`echo $f|sed -e 's/\./_/g'`

	echo "set terminal epslatex color colortext; \
	  set output \"$f2.tex\"; \
	  A=0;B=100;a=1.0;tau=100.0; \
	  f(x)=a*exp(-x/tau); \
    fit [A:B] f(x) \"$f\" using (0.01*\$1):(\$2) via a,tau; \
	  set xlabel 'MC-Zeit \$t\$ [\$\\tau\$]'; \
	  set ylabel 'Autokorrelation [\$\\unit{arb. unit}\$]'; \
    plot \"$f\" using (0.01*\$1):(\$2) w l t 'Autokorrelation',f(x) t 'Fit'; \
	  set output" | gnuplot
done
mv *.eps *.tex ../

