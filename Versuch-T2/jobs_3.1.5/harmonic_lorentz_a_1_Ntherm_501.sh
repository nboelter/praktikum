# #$ -M ravenschade@googlemail.com
# #$ -m e
#$ -cwd

a="1"
Ntherm="501"
pre="harmonic_lorentz"
s="14"

out="${pre}_a_${a}_Ntherm_${Ntherm}"
time ./main_${pre}.bin ${a} ${Ntherm} $s $out.dat > $out.out 2> $out.err