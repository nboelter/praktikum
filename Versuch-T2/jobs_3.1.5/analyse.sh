for pot in ...;
do
	for twfu in ...;
	do
		rm harmonic_stdv_$twfu.dat
                #for a in `seq 0.0 0.1 1.0 | sed "s/,/./g"`;
                for a in ...;
        	do
        		while [ "${#a}" -lt "3" ];
        		do
        			a="${a}0"
        		done
                	for ntherm in ...;
                	do
                        	out="${pot}_${twfu}_a_${a}_Ntherm_${ntherm}.out"
				stdv=`cat $out | grep "stdv energy = " | sed "s/stdv energy = //g"`
				E=`cat $out | grep "mean energy = " | sed "s/mean energy = //g"`
				binning=`cat "time_${pot}_${twfu}_a_${a}_Ntherm_${ntherm}.dat.binning" | tail -n 1`
				echo "$a $E $stdv $binning" >> harmonic_stdv_$twfu.dat				
				
                	done
        	done
	done
done