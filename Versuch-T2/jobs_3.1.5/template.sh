# #$ -M ravenschade@googlemail.com
# #$ -m e
#$ -cwd

a="A"
Ntherm="NTHERM"
pre="POT_TWFU"
s="S"

out="${pre}_a_${a}_Ntherm_${Ntherm}"
time ./main_${pre}.bin ${a} ${Ntherm} $s $out.dat > $out.out 2> $out.err