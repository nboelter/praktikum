for f in `find . -name "time_*.dat"`;
do
	rm -f $f.binning

	for s in `seq 0 9`;
	do
		binsize=`echo "2^$s" | bc -l `
		../tools/binning.bin $f $binsize >> $f.binning
	done
	naiv=`head -n 1 $f.binning`
	a=`echo $naiv | awk 'BEGIN { FS = " " } ; { print $4}'`
	b=`echo $naiv | awk 'BEGIN { FS = " " } ; { print $5}'`
	c=`echo $naiv | awk 'BEGIN { FS = " " } ; { print $6}'`
	
	cat $f.binning | awk 'BEGIN { FS = " " } ; { print $1, $2, $3, ($4/'$a')^2, ($5/'$b')^2, ($6/'$c')^2 }' > $f.binning.tau
done