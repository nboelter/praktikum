echo "3.1.5"
for f in `find . -name "time_*.dat"`;
do
	echo "set terminal png;set output \"$f.png\";plot \"$f\" w l" | gnuplot
	echo "set terminal png;set output \"$f.hist.png\"; c=0.5;f(x)=a*exp(-0.5*((x-c)/b)**2); fit f(x) \"$f.hist\" via a,b,c;plot \"$f.hist\" w l,f(x) t sprintf(\"a*exp(-0.5*((x-c)/b)**2) mit a=%f, b=%f, c=%f\",a,b,c)" | gnuplot
done

for f in `find . -name "autocorr_*.dat"`;
do
	echo "set terminal png;set output \"$f.png\";A=0;B=100;a=1.0;tau=1.0;f(x)=a*exp(-x/tau); fit [A:B] f(x) \"$f\" via a,tau;plot \"$f\" w l,f(x) t sprintf(\"a*exp(-x/tau) mit a=%f, tau=%f\",a,tau)" | gnuplot
done

echo "3.1.6"
for f in `find . -name "wf_harmonic_gauss_*.dat"`;
do
	a=`echo "$f" | awk 'BEGIN { FS = "_" } ; { print $5 }'`
	echo "set terminal png;set output \"$f.png\";a=1.0;f(x)=a*exp(-0.5*x**2); fit f(x) \"$f\" u 1:(\$2/exp(-$a*\$1**2)) via a;plot \"$f\" u 1:(\$2/exp(-$a*\$1**2)) w l,f(x) t sprintf(\"a*exp(-0.5*x**2) mit a=%f\",a)" | gnuplot
done
echo "3.1.6"
for f in `find . -name "wf_harmonic_lorentz_*.dat"`;
do
	a=`echo "$f" | awk 'BEGIN { FS = "_" } ; { print $5 }'`
	echo "set terminal png;set output \"$f.png\";a=1.0;f(x)=a*exp(-0.5*x**2); fit f(x) \"$f\" u 1:(\$2*($a*\$1**2+1)) via a;plot \"$f\" u 1:(\$2*($a*\$1**2+1)) w l,f(x) t sprintf(\"a*exp(-0.5*x**2) mit a=%f\",a)" | gnuplot
done
