# #$ -M ravenschade@googlemail.com
# #$ -m e
#$ -cwd

a="0.3"
Ntherm="500"
pre="harmonic_lorentz"
s="11"

out="${pre}_a_${a}_Ntherm_${Ntherm}"
time ./main_${pre}.bin ${a} ${Ntherm} $s $out.dat > $out.out 2> $out.err