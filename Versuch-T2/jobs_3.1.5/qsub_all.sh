l=`cat submitall.sh | wc -l`
for i in `seq 1 $l`;
do
	line=`head -n $i submitall.sh | tail -n 1`
	sh qsub.sh "$line"
	while [ "`cat qsub.log | wc -l`" -gt "0"  ];
	do
		echo "wait: $line"
		sleep 1
		sh qsub.sh "$line"
	done
done