# #$ -M ravenschade@googlemail.com
# #$ -m e
#$ -cwd

#Usage: ./main_yukawa.bin <0.0> <a> <Ntherm> <exp_meas> <filename>

a="0.5"
lambda="0.0"
Ntherm="500"
pre="yukawa"
s="12"

out="${pre}_lambda_${lambda}_a_${a}_Ntherm_${Ntherm}_s_${s}"
time ./main_${pre}.bin ${lambda} ${a} ${Ntherm} $s $out.dat > $out.out 2> $out.err