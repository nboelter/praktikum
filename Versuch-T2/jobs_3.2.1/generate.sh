rm -f submitall.sh

s="12"
for pot in yukawa;
do
	for lambda in 0.0;
	do
                #for a in `seq 0.0 0.1 1.0 | sed "s/,/./g"`;
                for a in  0 0.5 1.5;
        	do
                	for ntherm in 500;
                	do
                        	out="${pot}_lambda_${lambda}_a_${a}_Ntherm_${ntherm}_s_${s}.sh"
                        	cat template.sh | sed "s/AAA/$a/g" |  sed "s/POT/$pot/g" | sed "s/LAMBDA/$lambda/g" | sed "s/NTHERM/$ntherm/g" | sed "s/SSS/$s/g" > $out
                        	echo "$out" >> submitall.sh
                	done
        	done
	done
done
