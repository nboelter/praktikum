# #$ -M ravenschade@googlemail.com
# #$ -m e
#$ -cwd

#Usage: ./main_yukawa.bin <LAMBDA> <a> <Ntherm> <exp_meas> <filename>

a="AAA"
lambda="LAMBDA"
Ntherm="NTHERM"
pre="POT"
s="SSS"

out="${pre}_lambda_${lambda}_a_${a}_Ntherm_${Ntherm}_s_${s}"
time ./main_${pre}.bin ${lambda} ${a} ${Ntherm} $s $out.dat > $out.out 2> $out.err