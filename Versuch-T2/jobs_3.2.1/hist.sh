#../tools/histogram.bin 
#Usage: ../tools/histogram.bin <MC data file> <ymin> <ymax> <N_bins>

for f in `find . -name "time_*.dat"`;
do
	../tools/histogram.bin $f -0.65 -0.35 100 > $f.hist
done