from math import *

def velocity(pictures,pixels):
  timeperpicture = 23.0/100.0             #measured during the lab
  distanceperpixel = 6.45                 #from the CoolSNAP EZ datasheet
  distanceperpixel = distanceperpixel/100 #lens magnification
  time = pictures*timeperpicture
  distance = pixels*distanceperpixel
  return distance/time

# prepare Kinesin-1/Kinesin-14 data for further computing
i=0
dataK1 = []
f = open('Geschwindigkeitsmessung-Kinesin1.txt', 'r')
for line in f:
  if len(line) < 2:
    i = i+1
    continue
  if i>3:
    break
  i=0
  if line[0] == '#':
    continue
  dataK1.append(map(float, line.strip().split(' ', 2)))
f.close

i=0
dataK14 = []
f = open('Geschwindigkeitsmessung-Kinesin14.txt', 'r')
for line in f:
  if len(line) < 2:
    i = i+1
    continue
  if i>3:
    break
  i=0
  if line[0] == '#':
    continue
  dataK14.append(map(float, line.strip().split(' ', 2)))
f.close

# calculate velocities from the number of pictures and pixels
# (time and distance --> velocity)
f = open('real-velocities-K1-and-K14.txt', 'w')
f.write('#Kinesin-1-Geschwindigkeiten [microm/s]\n')
for (pictures,pixels) in dataK1:
  f.write(str(velocity(pictures,pixels)) + '\n')
f.write('\n\n#Kinesin-14-Geschwindigkeiten [microm/s]\n')
for (pictures,pixels) in dataK14:
  f.write(str(velocity(pictures,pixels)) + '\n')
f.close
