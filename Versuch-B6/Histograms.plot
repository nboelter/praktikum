reset
set grid
set xlabel 'Geschwindigkeit $[\unit{\mu m/s}]$'
set ylabel 'Häufigkeit'
set xrange [0:2]
set yrange [0:6.1]

set boxwidth 0.045 absolute
set style fill transparent solid 0.5 noborder
binwidth=0.05
bin(x,width)=width*floor(x/width)

set terminal epslatex color colortext
set output 'K1-Histogram.tex'
set table 'histK1.txt'
plot 'real-velocities-K1-and-K14.txt' index 0 u (bin($1,binwidth)):(1.0) smooth freq w boxes lc rgb"green" notitle
set output 'K14-Histogram.tex'
set table 'histK14.txt'
plot 'real-velocities-K1-and-K14.txt' index 1 u (bin($1,binwidth)):(1.0) smooth freq w boxes lc rgb"green" notitle
unset table

gauss1(x)=amplitude1/(sigma1*sqrt(2.*pi))*exp(-(x-position1)**2/(2.*sigma1**2))
gauss14(x)=amplitude14/(sigma14*sqrt(2.*pi))*exp(-(x-position14)**2/(2.*sigma14**2))
amplitude1=5
position1=1.4
sigma1=0.5
amplitude14=6
position14=0.5
sigma14=0.5
fit gauss1(x) 'histK1.txt' via amplitude1, position1, sigma1
fit gauss14(x) 'histK14.txt' via amplitude14, position14, sigma14

set output 'K1-Histogram.tex'
plot 'real-velocities-K1-and-K14.txt' index 0 u (bin($1,binwidth)):(1.0) smooth freq w boxes lc rgb"green" notitle, gauss1(x) title 'Gauß-Regression' lt 1 lc 7
set output 'K14-Histogram.tex'
plot 'real-velocities-K1-and-K14.txt' index 1 u (bin($1,binwidth)):(1.0) smooth freq w boxes lc rgb"green" notitle, gauss14(x) title 'Gauß-Regression' lt 1 lc 7
set output
