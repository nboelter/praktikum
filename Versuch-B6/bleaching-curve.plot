reset
set xlabel 'Zeit $[\unit{s}]$'
set ylabel 'Normierte Helligkeit'
set xrange [0:70]
set yrange [0.70:1.01]
set key outside tmargin left Left horizontal width 0.8
f1(x)=a1*x+b1
f2(x)=a2*x+b2
f3(x)=a3*x+b3
f4(x)=a4*x+b4
f5(x)=a5*x+b5
f6(x)=a6*x+b6
#Mit Anti-Bleach, 100ms Belichtung, 100 Bilder,
#safe mode, Verzögerung 1s, (68.125 s)/(100 Bilder)
fit f1(x) 'bleach-measurement.txt' index 0 u \
              ($1*(68.125/100)):(log(($2-$3)/(68.174-48.368))) via a1,b1
fit f2(x) 'bleach-measurement.txt' index 1 u \
              ($1*(68.125/100)):(log(($2-$3)/(44.918-34.037))) via a2,b2
fit f3(x) 'bleach-measurement.txt' index 2 u \
              ($1*(68.125/100)):(log(($2-$3)/(61.595-49.569))) via a3,b3
#Ohne Anti-Bleach, 100ms Belichtung, 100 Bilder,
#fast mode, keine Verzögerung, (23 s)/(100 Bilder)
fit f4(x) 'bleach-measurement.txt' index 3 u \
              ($1*(23.0/100)):(log(($2-$3)/(50.568-26.890))) via a4,b4
fit f5(x) 'bleach-measurement.txt' index 4 u \
              ($1*(23.0/100)):(log(($2-$3)/(46.753-30.090))) via a5,b5
fit f6(x) 'bleach-measurement.txt' index 5 u \
              ($1*(23.0/100)):(log(($2-$3)/(49.698-31.003))) via a6,b6
set term epslatex color colortext
set output 'Bleaching-curve.tex'
plot 'bleach-measurement.txt' index 0 u \
              ($1*(68.125/100)):(($2-$3)/(68.174-48.368)) title 'Mit AB, 1' lc 1, \
     'bleach-measurement.txt' index 1 u \
              ($1*(68.125/100)):(($2-$3)/(44.918-34.037)) title 'Mit AB, 2' lc 2, \
     'bleach-measurement.txt' index 2 u \
              ($1*(68.125/100)):(($2-$3)/(61.595-49.569)) title 'Mit AB, 3' lc 3, \
     'bleach-measurement.txt' index 3 u \
              ($1*(23.0/100)):(($2-$3)/(50.568-26.890)) title 'Ohne AB, 1' lc 4, \
     'bleach-measurement.txt' index 4 u \
              ($1*(23.0/100)):(($2-$3)/(46.753-30.090)) title 'Ohne AB, 2' lc 5, \
     'bleach-measurement.txt' index 5 u \
              ($1*(23.0/100)):(($2-$3)/(49.698-31.003)) title 'Ohne AB, 3' lc 6, \
     exp(f1(x)) notitle lc 1 lt 1, \
     exp(f2(x)) notitle lc 2 lt 1, \
     exp(f3(x)) notitle lc 3 lt 1, \
     exp(f4(x)) notitle lc 4 lt 1, \
     exp(f5(x)) notitle lc 5 lt 1, \
     exp(f6(x)) notitle lc 6 lt 1
set output
