reset
set fit errorvariables
set xlabel 'Quadratische Kreisfrequenz $\omega^2\,[\unit[10^6]{Hz^2}]$'
set ylabel 'Quadratische Impedanz $Z_0^2\,[\unit[10^5]{\Omega^2}]$'
b = 0.1
f(x) = a*x+b
c1 = 75.2          # R
c2 = 0.383128      # L 
c3 = 0.000001681   # C
set samples 10000
c4 = 5
g(x) = sqrt(c1**2 +(c2*x - 1/(c3*x))**2)
h(x) = 1.0/(sqrt((1.0/(c2*x) - c3*x)**2 + (1/c4)**2))
set terminal epslatex color colortext
set output 'Impedanz-Frequenz-Squared.tex'
fit f(x) 'Messwerte.txt' index 0 using ((2*pi*$1)**2):((1000*$3/$2)**2):(2000000*$3/($2**2)*sqrt(3.3 + (0.33*$3/$2)**2)) via a,b
# Setze neue Induktivität
c2 = sqrt(a)

plot 'Messwerte.txt' index 0 using (1.0/1000000*(2*pi*$1)**2):(10*($3/$2)**2):(20*$3/($2**2)*sqrt(3.3 + (0.33*$3/$2)**2)) with errorbars title 'Messergebnisse', (f(x*1000000)/100000) title 'Lineare Regression' lt 1 lc 2
set output 'Impedanz-Frequenz-Serie.tex'
set xlabel 'Kreisfrequenz $\omega\,[\unit{Hz}]$'
set ylabel 'Impedanz $Z_0\,[\unit{\Omega}]$'
# fit g(x) 'Messwerte.txt' index 1 using ((2*pi*$1)):((1000*$3/$2)):(1000*sqrt((3.3/$2)**2 + (0.33*$3/($2**2))**2)) via c1,c2,c3
plot 'Messwerte.txt' index 1 using ((2*pi*$1)):((1000*$3/$2)):(1000*sqrt((3.3/$2)**2 + (0.33*$3/($2**2))**2)) with errorbars title 'Messergebnisse', g(x) title 'Theorie' lt 1 lc 2
set output 'Impedanz-Frequenz-Parallel.tex'
fit h(x) 'Messwerte.txt' index 2 using (2*pi*$1):(1000*$4/$2):(1000*sqrt(($5/$2)**2 + ($3*$4/($2**2))**2)) via c4
plot 'Messwerte.txt' index 2 using (2*pi*$1):(1000*$4/$2):(1000*sqrt(($5/$2)**2 + ($3*$4/($2**2))**2)) with errorbars title 'Messergebnisse'

#, h(x) title 'Theorie' lt 1 lc 2 # ?!?!?!?

print 'omega_0 (mess)    ', 1279.26, ' Hz'
print 'omega_0 (theorie) ', 1.0/sqrt(c2*c3), ' Hz'
print 'Widerstand ', sqrt(b), ' +/- ', sqrt(b_err), ' Ohm'
print 'Induktivität ', sqrt(a), ' +/- ', sqrt(a_err), ' Henry'
