\subsection{Induktivität und Gesamtwiderstand}
Bei diesem Versuchsteil wurde der Kondensator überbrückt, es wird also $C = \unit[0]{F}$ angenommen. Die Spule und der Widerstand werden in Reihe geschaltet.
Berechnet man nun bei Formel~(\ref{eq:Impedanz}) auf beiden Seiten die Betragsquadrate, ergibt sich:
\begin{align*}
  Z^2 &= L^2 \cdot \omega^2 + R^2.
\end{align*}

Folglich wurde in Abbildung~\ref{fig:spule} die quadratische Impedanz über der quadratischen Kreisfrequenz aufgetragen. Mittels einer linearen Regression\footnote{via gnuplot, $\chi_\mathrm{red}^2 \approx 0.052$} $y = mx+b$ können nun leicht die beiden Größen bestimmt werden.
\begin{empheq}[box=\fbox]{align*}
  L &= \sqrt{m} = \unit[(0.382 \pm 0.067)]{H} \\
  R &= \sqrt{b} = \unit[(0.3 \pm 43.2)]{\Omega}
\end{empheq}
\subsection{Der Serienresonanzkreis}
Nun wurde der Kondensator\footnote{$C = \unit[(1.68 \pm 0.13)]{\mu F}$} mit den anderen beiden Bauteilen in Reihe geschaltet. In Abbildung~\ref{fig:serienkreis} wurde die Impedanz über der Kreisfrequenz aufgetragen, und zusätzlich der theoretische Verlauf mittels Formel~(\ref{eq:Impedanz}) eingezeichnet.
Da sich am Punkt der Resonanz die Wechselstromwiderstände von Spule und Kondensator genau aufheben (vergl. Formel~(\ref{eq:resonanz})), kann im Minimum der Impedanzkurve der \mensch{Ohm}sche-Widerstand als Ordinate und die Resonanzfrequenz als Abszisse abgelesen werden.
\begin{empheq}[box=\fbox]{align*}
  \omega_{0,\mathrm{Impedanz}} &= \unit[(1279 \pm 20)]{Hz} \\
  R &= \unit[(81 \pm 1)]{\Omega}
\end{empheq}

Die Resonanzfrequenz kann auch direkt mit dem Oszilloskop gemessen werden. Da im Resonanzpunkt die gesamte Impedanz aus dem \mensch{Ohm}schen-Widerstand besteht, ist ihr Imaginärteil Null und damit auch die Phasendifferenz.

Die Phasendifferenzen wurden in Abbildung~\ref{fig:phasenverschiebung} über der Kreisfrequenz aufgetragen, dabei wurde die theoretische Kurve mittels Formel~(\ref{eq:phasenverschiebung}) und den mit den Multimetern gemessenen Größen vorhergesagt, und auch mittels dieser Formel die Regression durchgeführt. Aus dem Nulldurchgang dieser Regression wurde dann die Resonanzfrequenz bestimmt.
\begin{empheq}[box=\fbox]{align*}
  \omega_{0,\mathrm{Phasenverschiebung}} &= \unit[(1275 \pm 20)]{Hz}
\end{empheq}
\subsection{Zusammenfassung}
Nun können gewichtete Mittel aus den obigen Ergebnissen berechnet werden:
\begin{empheq}[box=\fbox]{align*}
  \omega_{0} &= \unit[(1277 \pm 15)]{Hz} \\
  R &= \unit[(81 \pm 1)]{\Omega}
\end{empheq}
Zieht man nun vom Gesamtwiderstand den gemessenen \mensch{Ohm}schen Widerstands ab, so erhält man den Widerstand der Spule:
\begin{empheq}[box=\fbox]{align*}
  R_\mathrm{L} &= \unit[(71.0 \pm 1.1)]{\Omega}
\end{empheq}
Umstellen von Formel~(\ref{eq:resonanz}) ermöglicht nun das Berechnen der Kapazität:
\begin{empheq}[box=\fbox]{align*}
  C &= \frac{1}{\omega_0^2 L} = \unit[(1.61 \pm 0.29)]{\mu F}
\end{empheq}
Die über den drei Bauelementen abfallenden Spannungen $U$, $U_\mathrm{C}$ und $U_\mathrm{L+R}$ wurden in Abbildung~\ref{fig:spannungen} aufgetragen.

Für die oben berechnete Resonanzfrequenz wurde ein Zeigerdiagramm der drei Spannungen in Abbidung~\ref{fig:zeigerdiagramm} gezeichnet. Aus dem geometrischen Zusammenhang des Zeigerdiagramms ergibt sich nun auch die Phasenverschiebung zwischen $U$ und $U_\mathrm{L+R}$:
\begin{empheq}[box=\fbox]{align*}
  \varphi &= \arccos\left(\frac{U}{U_\mathrm{L+R}}\right) = \unit[(80.13 \pm 0.94)]{^\circ}
\end{empheq}
Dabei wurde $U$ auf die reelle Achse gezeichnet, da im Resonanzfall die Phasenverschiebung zwischen $U$ und $I$ ja Null ist. 
\subsection{Parallelkreis}
Auch die Impedanz im Parallelkreis wurde in Abbildung~\ref{fig:parallelkreis} über der Kreisfrequenz aufgetragen. Wie erwartet ergibt sich im Vergleich zum Serienresonanzkreis ein umgekehrtes Bild. Bei niedrigen oder hohen Frequenzen ist der Wechselstromwiederstand von Spule bzw. des Kondensators und damit wegen der Parallelschaltung auch die gesamte Impedanz sehr gering, so dass sich hier stattdessen ein Maximum bei mittleren Frequenzen ausbildet.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Impedanz-Frequenz-Squared.tex}
\end{center}
\caption{Frequenzabhängigkeit der Impedanz im $RL$-Schaltkreis ($C$ überbrückt)}
\label{fig:spule}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Impedanz-Frequenz-Serie.tex}
\end{center}
\caption{Frequenzabhängigkeit der Impedanz im Serienresonanzkreis}
\label{fig:serienkreis}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Phasenverschiebung.tex}
\end{center}
\caption{Phasenverschiebung zwischen $U$ und $I$ im Serienresonanzkreis}
\label{fig:phasenverschiebung}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Spannungen.tex}
\end{center}
\caption{Abfallende Spannungen im Serienresonanzkreis}
\label{fig:spannungen}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Impedanz-Frequenz-Parallel.tex}
\end{center}
\caption{Frequenzabhängigkeit der Impedanz im Parallelkreis}
\label{fig:parallelkreis}
\end{figure}
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Zeigerdiagramm.pdf_tex}
\end{center}
\caption{Zeigerdiagramm der Spannungen im Serienresonanzkreis}
\label{fig:zeigerdiagramm}
\end{figure}
