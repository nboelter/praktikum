(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[      6096,        213]
NotebookOptionsPosition[      5134,        175]
NotebookOutlinePosition[      5490,        191]
CellTagsIndexPosition[      5447,        188]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Winkel aus Zeigerdiagramm", "Subtitle",
 CellChangeTimes->{{3.559237632780158*^9, 3.559237639786562*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"ArcCos", "[", 
   RowBox[{"8.98", "/", "52.4"}], "]"}], "*", 
  RowBox[{"360", "/", 
   RowBox[{"(", 
    RowBox[{"2", "\[Pi]"}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.5592350593039722`*^9, 3.5592351166079884`*^9}, 
   3.559237406106718*^9, 3.5592375639984207`*^9}],

Cell[BoxData["80.13228140908515`"], "Output",
 CellChangeTimes->{{3.559237604114588*^9, 3.559237623161248*^9}, 
   3.5592402950132437`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Sqrt", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      FractionBox[
       RowBox[{"1", "*", "0.33"}], 
       RowBox[{
        SqrtBox[
         RowBox[{"1", "-", 
          FractionBox[
           SuperscriptBox["8.98", "2"], 
           SuperscriptBox["52.4", "2"]]}]], " ", "52.4"}]], ")"}], "^", "2"}],
     "+", 
    RowBox[{
     RowBox[{"(", 
      FractionBox[
       RowBox[{"8.98", "*", "4.5"}], 
       RowBox[{
        SqrtBox[
         RowBox[{"1", "-", 
          FractionBox[
           SuperscriptBox["8.98", "2"], 
           SuperscriptBox["52.4", "2"]]}]], " ", 
        SuperscriptBox["52.4", "2"]}]], ")"}], "^", "2"}]}], "]"}], "*", 
  RowBox[{"360", "/", 
   RowBox[{"(", 
    RowBox[{"2", "\[Pi]"}], ")"}]}]}]], "Input",
 CellChangeTimes->{{3.559237373325726*^9, 3.559237423605229*^9}, {
  3.559237474141096*^9, 3.55923751960137*^9}, {3.5592375786857243`*^9, 
  3.5592375831041603`*^9}}],

Cell[BoxData["0.9309673717957901`"], "Output",
 CellChangeTimes->{{3.559237510831259*^9, 3.5592375200956287`*^9}, {
   3.559237583509386*^9, 3.5592376232263193`*^9}, 3.559240295095718*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Resonanzfrequenz aus Phasenverschiebung\
\>", "Subtitle",
 CellChangeTimes->{{3.559240278938397*^9, 3.559240292898746*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{"c1", "=", "0.381958"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sc1", " ", "=", " ", "2.828"}], " "}], "\n", 
 RowBox[{"c2", "=", "75.2"}], "\[IndentingNewLine]", 
 RowBox[{"sc2", " ", "=", " ", "556.6"}], "\n", 
 RowBox[{"c3", "=", 
  RowBox[{"1.60925", "*", 
   RowBox[{"10", "^", 
    RowBox[{"-", "6"}]}]}]}], "\[IndentingNewLine]", 
 RowBox[{"sc3", " ", "=", " ", 
  RowBox[{"1.192", "*", 
   RowBox[{"10", "^", 
    RowBox[{"-", "5"}]}]}]}], "\[IndentingNewLine]", 
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{"0", " ", "\[Equal]", " ", 
    RowBox[{"ArcTan", "[", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"c1", "*", "x"}], ")"}], "-", 
          RowBox[{"1.0", "/", 
           RowBox[{"(", 
            RowBox[{"c3", "*", "x"}], ")"}]}]}], ")"}], "/", "c2"}], ")"}], 
      "*", 
      RowBox[{"360", "/", 
       RowBox[{"(", 
        RowBox[{"2", "*", "pi"}], ")"}]}]}], "]"}]}], ",", "x"}], 
  "]"}]}], "Input",
 CellChangeTimes->{{3.559240015256035*^9, 3.559240119466761*^9}}],

Cell[BoxData["0.381958`"], "Output",
 CellChangeTimes->{{3.5592401137902937`*^9, 3.559240120534205*^9}, 
   3.559240295129304*^9}],

Cell[BoxData["2.828`"], "Output",
 CellChangeTimes->{{3.5592401137902937`*^9, 3.559240120534205*^9}, 
   3.559240295131078*^9}],

Cell[BoxData["75.2`"], "Output",
 CellChangeTimes->{{3.5592401137902937`*^9, 3.559240120534205*^9}, 
   3.559240295132362*^9}],

Cell[BoxData["556.6`"], "Output",
 CellChangeTimes->{{3.5592401137902937`*^9, 3.559240120534205*^9}, 
   3.559240295133643*^9}],

Cell[BoxData["1.60925`*^-6"], "Output",
 CellChangeTimes->{{3.5592401137902937`*^9, 3.559240120534205*^9}, 
   3.559240295134914*^9}],

Cell[BoxData["0.000011920000000000001`"], "Output",
 CellChangeTimes->{{3.5592401137902937`*^9, 3.559240120534205*^9}, 
   3.559240295136273*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{"-", "1275.4999202265208`"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", "1275.4999202265208`"}], "}"}]}], "}"}]], "Output",\

 CellChangeTimes->{{3.5592401137902937`*^9, 3.559240120534205*^9}, 
   3.55924029513764*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"ArCos", "[", 
  FractionBox["R", 
   RowBox[{"R", "+", 
    FractionBox["1", 
     RowBox[{
      RowBox[{"\\", "omega"}], "*", "L"}]]}]], "]"}]], "Input",
 CellChangeTimes->{{3.559292185363667*^9, 3.559292186726755*^9}, {
  3.559292223814563*^9, 3.559292276045266*^9}}]
}, Open  ]]
},
WindowSize->{740, 652},
WindowMargins->{{105, Automatic}, {38, Automatic}},
FrontEndVersion->"8.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (October 5, \
2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 111, 1, 46, "Subtitle"],
Cell[CellGroupData[{
Cell[715, 27, 305, 8, 27, "Input"],
Cell[1023, 37, 139, 2, 27, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1199, 44, 955, 31, 94, "Input"],
Cell[2157, 77, 188, 2, 27, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[2394, 85, 133, 3, 46, "Subtitle"],
Cell[CellGroupData[{
Cell[2552, 92, 1113, 33, 118, "Input"],
Cell[3668, 127, 130, 2, 27, "Output"],
Cell[3801, 131, 127, 2, 27, "Output"],
Cell[3931, 135, 126, 2, 27, "Output"],
Cell[4060, 139, 127, 2, 27, "Output"],
Cell[4190, 143, 133, 2, 30, "Output"],
Cell[4326, 147, 145, 2, 27, "Output"],
Cell[4474, 151, 335, 10, 27, "Output"]
}, Open  ]],
Cell[4824, 164, 294, 8, 57, "Input"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
