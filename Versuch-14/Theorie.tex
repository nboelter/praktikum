\subsection{Wechselspannung und Wechselstrom}
Liegen in einem Stromkreis Wechselspannungen $U(t)$ und -ströme $I(t)$ mit sinusförmigem Verlauf vor, so gilt:
\begin{align*}
  U(t) &= U_0 \cos{(\omega t)} \\
  I(t) &= I_{00} \cos{(\omega t)},
\end{align*}
wobei $U_0$ und $I_{00}$ die jeweiligen Scheitelwerte und $\omega$ die Frequenz bezeichnen.

Kennt man bei einem solchen Stromkreis ohne Phasenverschiebung $\varphi$ zwischen Spannung und Strom den (reellen) Widerstand $R_\Omega$, so lassen sich $U$ und $I$ einfach über das \mensch{Ohm}sche Gesetz ineinander umrechnen: $U = R_\Omega I$.
\subsection{Die Impedanz}
Da im allgemeinen auch Induktivitäten und Kapazitäten in Stromkreisen enthalten sind, wird der Widerstand aufgrund der Phasenverschiebung zeitabhängig.
Es bietet sich dann an, $U(t)$ und $I(t)$ komplex darzustellen via
\begin{align*}
  U(t) &= U_0 e^{i\omega t} \\
  I(t) &= I_0 e^{i\omega t}.
\end{align*}
Es gilt dann $I_0 = I_{00} e^{i\varphi}$, die Phasenverschiebung wurde in die komplexe Amplitude $I_0$ absorbiert.

Aufgrund der Zeitabhängigkeit des Ohmschen Widerstandes wird bei Wechselstromnetzwerken oft die komplexe Größe \emph{Impedanz} $Z$ angegeben, die sich analog zum Widerstand aus komplexer Spannung und komplexem Strom berechnet~\citep{Gerthsen}:
\begin{align*}
  Z &= \frac{U(t)}{I(t)} = \frac{U_0}{I_0} = \frac{U_0}{I_{00}}e^{-i\varphi}.
\end{align*}
Wie leicht zu sehen ist, ist diese Größe zeitunabhängig.

Bezeichnet $Z_L$ den induktiven, $Z_C$ den kapazitiven und $R$ den Ohmschen Widerstand, so gilt für die Impedanz im RLC-Schwingkreis~\citep{Gerthsen}: $Z = Z_L + Z_C + R$.
Um $Z_L$ und $Z_C$ genauer zu bestimmen, nutzt man die von Spule und Kondensator bekannten Zusammenhänge~\citep{Gerthsen}:
\begin{align}
  U &= L\dot I = U_0 e^{i\omega t}\nonumber\\
  \Rightarrow I &= \frac{U_0}{i\omega L} e^{i\omega t}\nonumber\\
  &= \frac{U_0}{Z_L}e^{i\omega t}\label{eq:Z_L}\\
  \nonumber\\
  Q &= CU_0 e^{i\omega t}\nonumber\\
  \Rightarrow I &= \dot Q = i\omega C U_0 e^{i\omega t} = -\frac{\omega C}{i} U_0 e^{i\omega t}\nonumber\\
  &= \frac{U_0}{Z_C} e^{i\omega t}\label{eq:Z_C}.
\end{align}
Die Impedanz in einem solchen Stromkreis wird durch Addition der obigen Widerstände bestimmt~\citep[S.\,403]{Gerthsen}:
\begin{align}
  Z &= i\left(\omega L - \frac{1}{\omega C}\right) + R\label{eq:Impedanz}.
\end{align}
Analog zur Parallelschaltung Ohmscher Widerstände ergeben in einem Parallelkreis die inversen Widerstände addiert die invertierte Impedanz~\citep[S.\,403]{Gerthsen}:
\begin{align}
  \frac{1}{Z} &= i\left(\omega C - \frac{1}{\omega L}\right) + \frac{1}{R}\label{eq:Impedanz-para}.
\end{align}
Da der Strom gegenüber der Spannung im allgemeinen mit dem Winkel $\varphi$ phasenverschoben ist, kann man aus der Impedanz (siehe auch Abb.~\ref{fig:zeiger-impedanz}) die Phasenverschiebung ablesen via:
\begin{align}
  \varphi &= \arctan\frac{\mathfrak{I}(Z)}{\mathfrak{R}(Z)} = \arctan\frac{\omega L - \frac{1}{\omega C}}{R} \label{eq:phasenverschiebung}
\end{align}
\begin{figure}[h!]
\begin{center}
\def\svgwidth{8cm}
\input{Zeiger-Impedanz.pdf_tex}
\end{center}
\caption{Zeigerdiagramm zur Veranschaulichung der Impedanz}
\label{fig:zeiger-impedanz}
\end{figure}
\subsection{Resonanzphänomene}
Betrachtet man einen RLC-Schwingkreis, so ist aus Gleichung~\ref{eq:Impedanz} leicht abzulesen, dass die Impedanz minimal wird wenn $Z_L + Z_C = 0$ gilt:
\begin{align}
  \frac{i}{\omega_0 C} &= i\omega_0 L\nonumber\\
  \Rightarrow \omega_0 &= \frac{1}{\sqrt{LC}}. \label{eq:resonanz}
\end{align}
