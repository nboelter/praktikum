reset
set terminal epslatex color colortext
set output 'Phasenverschiebung.tex'

set key right bottom

L = 0.3831958221044476
R = 75.2
C = 0.000001681
c1 = L
c2 = R
c3 = C
set xlabel 'Kreisfrequenz $\omega\,[\unit{Hz}]$'
set ylabel 'Phasenverschiebung $\varphi\,[\unit{^\circ}]$'
f(x) = atan(((L*x)-1.0/(C*x))/R)*360/(2*pi)
g(x) = atan(((c1*x)-1.0/(c3*x))/c2)*360/(2*pi)
fit g(x) 'Messwerte.txt' index 1 using ($1*2*pi):($4*$1*360/1000000):($5*$1*360/1000000) via c1,c2,c3

plot 'Messwerte.txt' index 1 using ($1*2*pi):($4*$1*360/1000000):($5*$1*360/1000000) with errorbars lt 1 lc 1 title 'Messwerte', \
     g(x) title 'Regression' lt 2 lc 1,  \
     f(x) title 'Theorie' lt 1 lc 2
