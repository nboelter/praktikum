reset
set xlabel 'Kreisfrequenz $\omega\,[\unit{Hz}]$'
set ylabel 'Spannung$\,[\unit{V}]$'
set terminal epslatex color colortext
set output 'Spannungen.tex'
plot 'Messwerte.txt' index 1 using ($1*2*pi):3:(0.33) with errorbars lt 1 lc 1 title '$U$', \
     'Messwerte.txt' index 1 using ($1*2*pi):6:7 with errorbars lt 1 lc 2 title '$U_\mathrm{C}$', \
     'Messwerte.txt' index 1 using ($1*2*pi):8:9 with errorbars lt 1 lc 3 title '$U_\mathrm{L+R}$'
