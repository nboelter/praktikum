reset
set xlabel 'quadrierte Spannung $U^2\,[\unit{kV^2}]$'
set ylabel 'Kraft $F\,[\unit{mN}]$'
set key top left
f1(x)= a1*x+b1
f2(x)= a2*x+b2
f3(x)= a3*x+b3
f4(x)= a4*x+b4
fit f1(x) 'Messwerte.txt' index 2 using ($2)**2:($1*9.81) via a1,b1
fit f2(x) 'Messwerte.txt' index 3 using ($2)**2:($1*9.81) via a2,b2
fit f3(x) 'Messwerte.txt' index 4 using ($2)**2:($1*9.81) via a3,b3
fit f4(x) 'Messwerte.txt' index 5 using ($2)**2:($1*9.81) via a4,b4
set terminal epslatex color colortext
set output 'KrSp1g.tex'
plot 'Messwerte.txt' index 2 using (($2)**2):($1*9.81):(2*$2*0.05) with xerrorbars title 'Messwerte', f1(x) title 'Lineare Regression' lt 1 lc 2
set output                                                                                               
!epstopdf 'KrSp1g.eps'                                                                                   
set output 'KrSp2g.tex'                                                                                  
plot 'Messwerte.txt' index 3 using (($2)**2):($1*9.81):(2*$2*0.05) with xerrorbars title 'Messwerte', f2(x) title 'Lineare Regression' lt 1 lc 2
set output                                          
!epstopdf 'KrSp2g.eps'                              
set output 'KrSp3g.tex'                             
plot 'Messwerte.txt' index 4 using (($2)**2):($1*9.81):(2*$2*0.05) with xerrorbars title 'Messwerte', f3(x) title 'Lineare Regression' lt 1 lc 2
set output                                         
!epstopdf 'KrSp3g.eps'                             
set output 'KrSp4g.tex'                            
plot 'Messwerte.txt' index 5 using (($2)**2):($1*9.81):(2*$2*0.05) with xerrorbars title 'Messwerte', f4(x) title 'Lineare Regression' lt 1 lc 2
set output
!epstopdf 'KrSp4g.eps'
