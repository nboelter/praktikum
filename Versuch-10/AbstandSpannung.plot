reset
set xlabel 'Spannung $U\,[\unit{kV}]$'
set ylabel 'Abstand $d\,[\unit{mm}]$'
set key left top
f(x) = a*x+b
g(x) = c*x+d
fit f(x) 'Messwerte.txt' index 0 using ($1):($2) via a,b
fit g(x) 'Messwerte.txt' index 1 using ($1):($2) via c,d
set terminal epslatex color colortext
set output 'AbSp3g.tex'
plot 'Messwerte.txt' index 0 using ($1):($2):($4):($3) with xyerrorbars title 'Messwerte', f(x) title 'Lineare Regression' lt 1 lc 2
set output
!epstopdf 'AbSp3g.eps'
set output 'AbSp4g.tex'
plot 'Messwerte.txt' index 1 using ($1):($2):($4):($3) with xyerrorbars title 'Messwerte', g(x) title 'Lineare Regression' lt 1 lc 2
set output
!epstopdf 'AbSp4g.eps'
