from math import *

#epsilon_0 via force
def epsilon01(m, Steigung, epsir):
  #Calculate capacitor's surface area
  r = 0.04
  a = 0.001
  A = pi*(r**2+r*a)
  #Calculate given force
  g = 9.81
  F = m*g
  #Return epsilon_0
  return (2*F*Steigung**2)/(epsir*A)

#error in the above value
def epserr1(eps, Steigung, sigmaSteigung):
  return eps*(2*sigmaSteigung/Steigung)

#epsilon_0 via distance
def epsilon02(dfalse, Steigung, epsir):
  #Calculate capacitor's surface area
  r = 0.04
  a = 0.001
  A = pi*(r**2+r*a)
  #Correct the distance's offset
  delta = 0.00094
  d = dfalse - delta
  #Return epsilon_0
  return (2*d**2*Steigung)/(epsir*A)

#error in the second values
def epserr2(eps, d, sigmad, Steigung, sigmaSteigung):
  return eps*sqrt((2*sigmad/d)**2 + (sigmaSteigung/Steigung)**2)

epsir = 1

m1 = 0.003
Steigung1 = 8.56803e-07
sigmaSteigung1 = 3.455e-08
epsilon011 = epsilon01(m1, Steigung1, epsir)
epserr11 = epserr1(epsilon011, Steigung1, sigmaSteigung1)

m2 = 0.004
Steigung2 = 7.14498e-07
sigmaSteigung2 = 2.795e-08
epsilon012 = epsilon01(m2, Steigung2, epsir)
epserr12 = epserr1(epsilon012, Steigung2, sigmaSteigung2)

print "Erste Messreihe:"
print "epsilon_0 =", epsilon011, "+/-", epserr11
print ""
print "Zweite Messreihe:"
print "epsilon_0 =", epsilon012, "+/-", epserr12
print ""
print "Gewichtetes Mittel:", ((epsilon011/epserr11**2)+(epsilon012/epserr12**2))/((1/epserr11**2)+(1/epserr12**2)), "+/-", sqrt(1/((1/epserr11**2)+(1/epserr12**2)))
print ""
print "-----------------"
print ""

sigmad = 0.00001

d1 = 0.002
Steigung1 = 1.45255e-08
sigmaSteigung1 = 4.173e-10
epsilon021 = epsilon02(d1, Steigung1, epsir)
epserr21 = epserr2(epsilon021, d1, sigmad, Steigung1, sigmaSteigung1)

d2 = 0.0025
Steigung2 = 6.01437e-09
sigmaSteigung2 = 9.326e-10
epsilon022 = epsilon02(d2, Steigung2, epsir)
epserr22 = epserr2(epsilon022, d2, sigmad, Steigung2, sigmaSteigung2)

d3 = 0.003
Steigung3 = 3.76391e-09
sigmaSteigung3 = 2.623e-10
epsilon023 = epsilon02(d3, Steigung3, epsir)
epserr23 = epserr2(epsilon023, d3, sigmad, Steigung3, sigmaSteigung3)

d4 = 0.004
Steigung4 = 1.93867e-09
sigmaSteigung4 = 0.6227e-10
epsilon024 = epsilon02(d4, Steigung4, epsir)
epserr24 = epserr2(epsilon024, d4, sigmad, Steigung4, sigmaSteigung4)

print "Dritte Messreihe:"
print "epsilon_0 =", epsilon021, "+/-", epserr21
print ""
print "Vierte Messreihe:"
print "epsilon_0 =", epsilon022, "+/-", epserr22
print ""
print "Fuenfte Messreihe:"
print "epsilon_0 =", epsilon023, "+/-", epserr23
print ""
print "Sechste Messreihe:"
print "epsilon_0 =", epsilon024, "+/-", epserr24
print ""
print "Gewichtetes Mittel:", ((epsilon021/epserr21**2)+(epsilon022/epserr22**2)+(epsilon023/epserr23**2)+(epsilon024/epserr24**2))/((1/epserr21**2)+(1/epserr22**2)+(1/epserr23**2)+(1/epserr24**2)), "+/-", sqrt(1/((1/epserr21**2)+(1/epserr22**2)+(1/epserr23**2)+(1/epserr24**2)))
