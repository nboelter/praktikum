reset
set xlabel 'Primärspannung $U_\mathrm{pr}\,[\unit{V}]$'
set ylabel 'Sekundärspannung $U_\mathrm{se}\,[\unit{V}]$'
f(x) = a1*x+b1
g(x) = a2*x+b2
fit f(x) 'Messwerte.txt' index 2 using 1:2 via a1,b1
fit g(x) 'Messwerte.txt' index 3 using 1:2 via a2,b2
set terminal epslatex color colortext
set output 'SpPrimSek.tex'
plot 'Messwerte.txt' index 2 using 1:2:3:4 with xyerrorbars title 'Messwerte', f(x) title 'Lineare Regression' lt 1 lc 2
set xlabel 'Sekundärspannung $U_\mathrm{se}\,[\unit{V}]$'
set ylabel 'Primärspannung $U_\mathrm{pr}\,[\unit{V}]$'
set output 'SpSekPrim.tex'
plot 'Messwerte.txt' index 3 using 1:2:3:4 with xyerrorbars title 'Messwerte', g(x) title 'Lineare Regression' lt 1 lc 2
