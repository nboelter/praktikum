reset
set xlabel 'Primärstrom $I_\mathrm{pr}\,[\unit{mA}]$'
set ylabel 'Primärspannung $U_\mathrm{pr}\,[\unit{V}]$'
f(x) = a*x+b
fit f(x) 'Messwerte.txt' index 0 using 1:2 via a,b
set terminal epslatex color colortext
set output 'StromSpannungPrim.tex'
plot 'Messwerte.txt' index 0 using 1:2:3:4 with xyerrorbars title 'Messwerte, linearer Teil', 'Messwerte.txt' index 1 using 1:2:3:4 with xyerrorbars title 'Messwerte, nichtlinearer Teil', f(x) title 'Lineare Regression für geringe Ströme' lt 1 lc 3
set output
!epstopdf 'StromSpannungPrim.eps'
