from math import *

#Uebersetzungsverhaeltnis u abhaengig von den Steigungen der Geraden U1 = u*U2 und U2 = U1/u
def Ubersetzung(a1, a2, sigma1, sigma2):
  sigma3 = sigma2/a2**2
  a3 = 1.0/a2
  u = ((a1/sigma1**2)+(a3/sigma3**2))/((1.0/sigma1**2)+(1.0/sigma3**2))
  sigmau = sqrt(1.0/((1/sigma1**2)+(1.0/sigma3**2)))
  return [u, sigmau]

a1 = 10.3413
a2 = 0.0930386
sigma1 = 1.08#0.03627 errors were calculated from (slope of the graph) * (x-error) + (y-error)
sigma2 = 0.02#0.0002752
u, sigmau = Ubersetzung(a1, a2, sigma1, sigma2)
print "Uebersetzungsverhaeltnisse:"
print "u_1 =", a1, "+/-", sigma1
print "u_2 =", 1/a2, "+/-", sigma2/a2**2
print "Gewichtetes Mittel:", u, "+/-", sigmau
