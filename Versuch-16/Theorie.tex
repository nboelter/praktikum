\subsection{Idealer Transformator}
Bei einem \emph{idealen} Transformator handelt es sich um zwei Spulen, die so arrangiert sind, dass der magnetische Fluss durch beide immer gleich ist.
Legt man nun an die sogenannte \emph{Primärspule} eine zeitlich veränderliche Eingangsspannung $U_1$ an, so induziert die entstehende Änderung des Magnetfelds in der zweiten Spule (\emph{Sekundärspule}) eine Spannung $U_2$ (\mensch{Lenz}sche Regel).
Die Größe der induzierten Spannung lässt sich über die \emph{Flussregel} bestimmen \citep[S.\,416]{Gerthsen}:
\begin{align}
  \frac{U_1}{N_1} &= \dot\Phi = \frac{U_2}{N_2}.
  \label{eq:fluss}
\end{align}
Es bezeichnet $N_i$ die Windungszahl der $i$-ten Spule und $\Phi$ den magnetischen Fluss durch jede Windung.
Dieser Effekt wird genutzt, um die Amplitude von Wechselspannungen bei gleichbleibender Frequenz zu verändern.

Ist der Sekundärkreis offen, der Transformator also \emph{unbelastet}, so kann dort natürlich kein Strom fließen.
Der magnetische Fluss $\Phi$ wird ausschließlich vom Leerlaufstrom $I_0 = I_1$ in der Primärspule verursacht.
\subsection{Belasteter Transformator}
Der \emph{belastete} Transformator unterscheidet sich vom unbelasteten durch einen an die Sekundärspule angeschlossenen Verbraucher.
Es kann nun im Sekundärkreis ein Strom fließen.
Hat der angeschlossene Verbraucher den komplexen Widerstand $Z$, so verursacht die durch Gleichung \ref{eq:fluss} gegebene Sekundärspannung $U_2$ einen Strom $I_2 = U_2/Z$ \citep[S.\,416]{Gerthsen}.
Dadurch wird in der Sekundärspule ein Magnetfeld induziert, der Sekundärstrom $I_2$ trägt zum magnetischen Fluss durch beide Spulen bei.
Im Primärkreis wird so ein zusätzlicher Strom $I_1'$ induziert, der dem Leerlaufstrom $I_0$ entgegenwirkt.
Der Fluss $\Phi$ ändert sich also insgesamt nicht.
Es gilt~\citep[S.\,416]{Gerthsen}:
\begin{align*}
  \frac{I_1'}{I_2} &= \frac{N_2}{N_1},\\
  I_1 &= I_0 - \frac{N_2}{N_1}I_2 = I_0 - \frac{N_2 U_2}{N_1 Z},
\end{align*}
wobei $I_1$ der gesamte Primärstrom ist.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Vektorphasen.pdf_tex}
\end{center}
\caption{Schematisches Vektordiagramm für den Phasenwinkel zwischen $U_1$ und $I_1$.}
\label{fig:Vektorphasen}
\end{figure}

Wie aus Abbildung~\ref{fig:Vektorphasen} leicht abzulesen ist, gilt
\begin{align}
  \tan{\varphi} &= \frac{I_0\sin{\varphi_0}}{I_1'+I_0\cos{\varphi_0}}.
  \label{eq:Phase}
\end{align}
\subsection{Reale Transformatoren}
Wie die Bezeichnung des idealen Transformators vermuten lässt, sind die gewünschten Voraussetzungen in der Realität kaum erreichbar.
Bei realen Transformatoren wird beispielsweise oft ein Eisenkern, der durch beide Spulen läuft verwendet (siehe auch Abb.~\ref{fig:Trafo}),
damit der magnetische Fluss pro Windung in beiden Spulen möglichst gleich ist.
\begin{figure}[htb]
\begin{center}
\def\svgwidth{8cm}
\input{Trafo.pdf_tex}
\end{center}
\caption{Aufbau eines typischen Transformators mit Eisenkern, verändert nach~\citep{LP-Transformator}.}
\label{fig:Trafo}
\end{figure}

Schaltet man im Primärkreis einen Widerstand parallel zur Spule (siehe auch Abb.~\ref{fig:Schaltplan}), so ergibt sich eine Phasenverschiebung zwischen dem Strom durch den Transformator $I_\mathrm{1,T}$ und der Spannung $U_1$.
Dies resultiert aus der Selbstinduktivität der Spule, die einen Beitrag zum Strom durch die Spule leistet.
In Abbildung~\ref{fig:Vektordiagramm} ist der Zusammenhang zwischen Phasenverschiebung $\varphi$, Spulenstrom $I_\mathrm{1,T}$, Strom durch den Widerstand $I_\mathrm{1,R}$ und Gesamtstrom im Primärkreis $I_1$ aufgetragen.
Wie in dem Diagramm leicht abzulesen ist, gilt
\begin{align}
  \cos{\frac{\varphi}{2}} &= \frac{I_1}{2I_\mathrm{1,R}}.
\end{align}

\begin{figure}[htb]
\begin{center}
\def\svgwidth{10cm}
\input{Vektordiagramm.pdf_tex}
\end{center}
\caption{Schematisches Vektordiagramm für den Phasenwinkel zwischen $U_1$ und $I_1$ bei der Versuchschaltung, verändert nach \citep{LP-Transformator}.}
\label{fig:Vektordiagramm}
\end{figure}
\subsection{Wirk- und Blindleistung}
Um bei komplexer Wechselstromrechnung auch die Leistung berücksichtigen zu können, wird die Wirkleistung als Realteil und die Blindleistung als Imaginärteil der komplexen Leistung $P$ definiert. Letztere ist das Produkt des komplexen Stroms mit der komplexen Spannung. (Für weitere Details siehe auch Versuch 14, \q{Wechselstromwiderstände}.) 

Die Wirkleistung ist die für die Umwandlung in andere Leistungen verfügbare Leistung, die Blindleistung steht hierfür nicht zur Verfügung.
