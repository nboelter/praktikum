reset
set ylabel 'Primärstrom $I_\mathrm{pr}\,[\unit{A}]$'
set xlabel 'Sekundärstrom $I_\mathrm{se}\,[\unit{A}]$'
set xrange [-0.1:5]
f(x) = a1*x+b1
fit f(x) 'Messwerte.txt' index 4 using 1:2 via a1,b1
set terminal epslatex color colortext
set output 'StromPrimSek.tex'
plot 'Messwerte.txt' index 4 using 1:2:5 with errorbars title 'Messwerte', f(x) lt 1 lc 2 title 'Lineare Regression'
print a1*5+b1
