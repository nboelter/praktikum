reset
set terminal epslatex color colortext
set output 'Phase.tex'
set ytics ('$\frac{1}{4}\pi$' pi/4)
set ytics add ('$\frac{1}{2}\pi$' pi/2) 
set ytics add ('0' 0) 
set xrange [-0.1:5]
set yrange [0:1.8] 
set xlabel 'Sekundärstrom $I_2\,[\unit{A}]$'
set ylabel 'Phasenwinkel $\varphi\,[\unit{rad}]$'
f(x) = atan(a*sin(b)/(x/c + a*cos(b)))
fit f(x) 'Messwerte.txt' index 6 using 1:2:($3*4) via a,b,c
I1 = 0.11
plot 'Messwerte.txt' index 6 using 1:2:($3*4) with errorbars title 'Messwerte' lt 1 lc 1, atan(I1*sin(1.459283)/((x/10.4142681378)+I1*cos(1.459283))) title 'Theoretischer Wert' lt 1 lc 2, (x > 0) ? atan(0.19/(x/10.4142681378)) : pi/2 title 'Theoretischer Wert (korrigiert)' lt 1 lc 3, f(x) title 'Regression mit Formel~(\ref{eq:Phase})' lt 1 lc 4
print f(5)
