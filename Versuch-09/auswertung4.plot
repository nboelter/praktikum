reset
set terminal epslatex color colortext
set output 'C_over_C0_3.tex'
set xlabel '$x\,[\unit{mm}]$'
set ylabel '$C/C_0\,[\unit{-}]$'
set grid
set xrange [-1:8]
set yrange [0:0.6]
g(x)=0.5*(1-erf(x/(1000*sqrt(4*4.192e-10*2700))))
h(x)=0.5*(1-erf(x/(1000*sqrt(4*4.192e-10*5400))))
plot 'Diffus3und4.txt' u (($2)+0.77):(1/($1)) title 'Messung bei $t \approx 40\,\unit{min}$' lc 1, 'Diffus3und4.txt' u (($3)+0.77):(1/($1)) title 'Messung bei $t \approx 43\,\unit{min}$' lc 3, g(x) title 'Theoret. Werte' lt 1 lc 2
set output
!epstopdf C_over_C0_3.eps
set output 'C_over_C0_4.tex'
plot 'Diffus3und4.txt' u (($4)-3.68):(1/($1)) title 'Messung bei $t \approx 80\,\unit{min}$' lc 1, 'Diffus3und4.txt' u (($5)-3.68):(1/($1)) title 'Messung bei $t \approx 90\,\unit{min}$' lc 3, h(x) title 'Theoret. Werte' lt 1 lc 2
set output
!epstopdf C_over_C0_4.eps
