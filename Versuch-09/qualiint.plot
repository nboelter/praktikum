reset
set terminal epslatex color colortext
set output 'qualiint.tex'
set key bottom left
set xlabel '$x\,[\unit{mm}]$'
set ylabel '$c/c_0\,[\unit{m^2}]$'
set xrange [-20:20]
set yrange [-0.1:1.1]
a(x)=0.5*(1-erf(x/sqrt(4*4e-10*1e-10)))
b(x)=0.5*(1-erf(x/sqrt(4*4e-10*1e10)))
c(x)=0.5*(1-erf(x/sqrt(4*4e-10*1e11)))
d(x)=0.5*(1-erf(x/sqrt(4*4e-10*1e12)))
e(x)=0.5*(1-erf(x/sqrt(4*4e-10*1e13)))
plot a(x) title '$t=0~~~~$' lt 1 lc 1, b(x) title '$t=10^{10}$' lt 1 lc 2, c(x) title '$t=10^{11}$' lt 1 lc 3, d(x) title '$t=10^{12}$' lt 1 lc 4, e(x) title '$t=10^{13}$' lt 1 lc 5
set output
!epstopdf qualiint.eps
