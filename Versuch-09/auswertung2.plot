reset
set key top left
set terminal epslatex color colortext
set output 'DKoeff.tex'
set xlabel '$t\,[\unit{s}]$'
set ylabel '$x^2\,[\unit{m^2}]$'
f1(x) = m1*x
f2(x) = m2*x
fit f1(x) 'Diffus1und2.txt' using 1:4 via m1
fit f2(x) 'Diffus1und2.txt' using 1:5 via m2
plot 'Diffus1und2.txt' u 1:4:6 with yerrorbars title 'Messung 1', f1(x) title 'Lin. Reg. (Messung 1)', 'Diffus1und2.txt' u 1:5:7 with yerrorbars title 'Messung 2', f2(x) title 'Lin. Reg. (Messung 2)'
set output
!epstopdf DKoeff.eps
